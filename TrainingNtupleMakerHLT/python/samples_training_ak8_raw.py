def AddQCDSamples(samples):

    samples['QCD_Pt_30to50'] = [
        '/QCD_PT-30to50_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_50to80'] = [
        '/QCD_PT-50to80_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_80to120'] = [
        '/QCD_PT-80to120_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_120to170'] = [
        '/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_170to300'] = [
        '/QCD_PT-170to300_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_300to470'] = [
        '/QCD_PT-300to470_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_470to600'] = [
        '/QCD_PT-470to600_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_600to800'] = [
        '/QCD_PT-600to800_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_800to1000'] = [
        '/QCD_PT-800to1000_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    
    samples['QCD_Pt_1000to1400'] = [
        '/QCD_PT-1000to1400_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','filterEventsInTheDumper=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
def AddAllSamples(samples):
    AddQCDSamples(samples)
