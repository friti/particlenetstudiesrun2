import FWCore.ParameterSet.Config as cms

def modifyPNETAK4Path(process,pathname,minpt,maxeta,l1seedlist=[]):
    ## remove most of the filters in the path
    if not hasattr(process,pathname):
        return;
    if hasattr(process,"hltL1sQuadJetOrHTTOrMuonHTT"):
        seeds = str(process.hltL1sQuadJetOrHTTOrMuonHTT.L1SeedsLogicalExpression).replace(' ','').replace("cms.string('",'').replace("')",'').split("OR");
        for seed in seeds:
            l1seedlist.append(seed) if seed not in l1seedlist else l1seedlist
        getattr(process,pathname).remove(process.hltL1sQuadJetOrHTTOrMuonHTT)
    if hasattr(process,"hlt4PixelOnlyPFCentralJetTightIDPt20"):
        getattr(process,pathname).remove(process.hlt4PixelOnlyPFCentralJetTightIDPt20)
    if hasattr(process,"hlt3PixelOnlyPFCentralJetTightIDPt30"):
        getattr(process,pathname).remove(process.hlt3PixelOnlyPFCentralJetTightIDPt30)
    if hasattr(process,"hlt2PixelOnlyPFCentralJetTightIDPt40"):
        getattr(process,pathname).remove(process.hlt2PixelOnlyPFCentralJetTightIDPt40)
    if hasattr(process,"hlt1PixelOnlyPFCentralJetTightIDPt60"):
        getattr(process,pathname).remove(process.hlt1PixelOnlyPFCentralJetTightIDPt60)        
    if hasattr(process,"hltAK4PFJetsLooseID"):
        process.hltAK4PFJetsLooseID.minPt = cms.double(minpt)
    if hasattr(process,"hltAK4PFJetsTightID"):
        process.hltAK4PFJetsTightID.minPt = cms.double(minpt)
    if hasattr(process,"hlt4PFCentralJetTightIDPt35"):
        getattr(process,pathname).remove(process.hlt4PFCentralJetTightIDPt35)
    if hasattr(process,"hlt3PFCentralJetTightIDPt40"):
        getattr(process,pathname).remove(process.hlt3PFCentralJetTightIDPt40)
    if hasattr(process,"hlt2PFCentralJetTightIDPt50"):
        getattr(process,pathname).remove(process.hlt2PFCentralJetTightIDPt50)
    if hasattr(process,"hlt1PFCentralJetTightIDPt70"):
        getattr(process,pathname).remove(process.hlt1PFCentralJetTightIDPt70)
    if hasattr(process,"hltPFJetForBtagSelector"):
        process.hltPFJetForBtagSelector.inputTag = cms.InputTag("hltAK4PFJets"); 
        process.hltPFJetForBtagSelector.MinPt = cms.double(minpt); 
        process.hltPFJetForBtagSelector.MaxEta = cms.double(maxeta);
        process.hltPFJetForBtagSelector.MinN = cms.int32(1);
    if hasattr(process,"hltPFCentralJetTightIDPt35"):
        getattr(process,pathname).remove(process.hltPFCentralJetTightIDPt35)
    if hasattr(process,"hltBTagCentralJetPt35PFParticleNet2BTagSum0p65"):
        getattr(process,pathname).remove(process.hltBTagCentralJetPt35PFParticleNet2BTagSum0p65)
    if hasattr(process,"hltDeepCombinedSecondaryVertexBJetTagsInfos"):
        getattr(process,pathname).replace(process.hltBoolEnd,process.hltDeepCombinedSecondaryVertexBJetTagsInfos+process.hltBoolEnd);
    if hasattr(process,"hltPFDeepFlavourTagInfos"):
        getattr(process,pathname).replace(process.hltBoolEnd,process.hltPFDeepFlavourTagInfos+process.hltBoolEnd);

def modifyPNETAK8Path(process,pathname,minpt,maxeta,l1seedlist=[]):
    ## remove most of the filters in the path
    if not hasattr(process,pathname):
        return;
    if hasattr(process,"hltL1sSingleJetOrHTTOrMuHTT"):
        seeds = str(process.hltL1sSingleJetOrHTTOrMuHTT.L1SeedsLogicalExpression).replace(' ','').replace("cms.string('",'').replace("')",'').split("OR");
        for seed in seeds:
            l1seedlist.append(seed) if seed not in l1seedlist else l1seedlist
        getattr(process,pathname).remove(process.hltL1sSingleJetOrHTTOrMuHTT)
        if hasattr(process,"hltAK8SingleCaloJet200"):
            getattr(process,pathname).remove(process.hltAK8SingleCaloJet200)
        ## Add AK4 jet clustering
        if hasattr(process,"HLTAK8PFJetsSequence"):
            getattr(process,pathname).replace(process.HLTAK8PFJetsSequence,
                process.HLTAK8PFJetsSequence+process.hltAK4PFJets+process.hltAK4PFJetsLooseID+process.hltAK4PFJetsTightID+process.HLTAK4PFJetsCorrectionSequence)            
        if hasattr(process,"hltAK8PFJetsCorrectedMatchedToCaloJets200"):
            getattr(process,pathname).remove(process.hltAK8PFJetsCorrectedMatchedToCaloJets200)
        if hasattr(process,"hltSingleAK8PFJet250"):
            process.hltSingleAK8PFJet250.inputTag = cms.InputTag("hltAK8PFJets")
            process.hltSingleAK8PFJet250.MinPt = cms.double(minpt);
            process.hltSingleAK8PFJet250.MaxEta = cms.double(maxeta);
            process.hltSingleAK8PFJet250.MinN = cms.int32(1);
        if hasattr(process,"hltAK8PFJets250Constituents"):
            getattr(process,pathname).remove(process.hltAK8PFJets250Constituents)
        if hasattr(process,"hltAK8PFSoftDropJets250"):
            getattr(process,pathname).remove(process.hltAK8PFSoftDropJets250)
        if hasattr(process,"hltAK8SinglePFJets250SoftDropMass40"):
            getattr(process,pathname).remove(process.hltAK8SinglePFJets250SoftDropMass40)        
        if hasattr(process,"HLTJetFlavourTagParticleNetSequencePFAK8"):
            getattr(process,pathname).replace(process.HLTJetFlavourTagParticleNetSequencePFAK8,process.HLTJetFlavourTagParticleNetSequencePF)            
            getattr(process,pathname).replace(process.hltPFJetForBtag,process.hltPFJetForBtag+process.hltPFJetForParticleNetSelectorAK8+process.hltPFJetForParticleNetAK8);
            process.hltPFJetForParticleNetSelectorAK8.inputTag = cms.InputTag("hltAK8PFJets");
            process.hltPFJetForParticleNetSelectorAK8.MinPt = cms.double(minpt);
            process.hltPFJetForParticleNetSelectorAK8.MaxEta = cms.double(maxeta);
            process.hltPFJetForParticleNetSelectorAK8.MinN = cms.int32(1);            
            getattr(process,pathname).replace(process.hltParticleNetJetTagInfos,process.hltParticleNetJetTagsInfosAK8);            
            process.hltParticleNetJetTagsInfosAK8.min_jet_pt = cms.double(minpt);
            process.hltParticleNetJetTagsInfosAK8.max_jet_eta = cms.double(maxeta);
            process.hltParticleNetJetTagsInfosAK8.secondary_vertices = cms.InputTag("hltDeepInclusiveMergedVerticesPF")
            process.hltParticleNetJetTagsInfosAK8.vertex_associator = cms.InputTag( 'hltPrimaryVertexAssociation','original')
            getattr(process,pathname).replace(process.hltParticleNetONNXJetTags,process.hltParticleNetONNXJetTagsAK8)
            getattr(process,pathname).replace(process.hltParticleNetDiscriminatorsJetTags,process.hltParticleNetDiscriminatorsJetTagsAK8)
        if hasattr(process,"hltAK8PFJets250SoftDropMass40"):
            getattr(process,pathname).remove(process.hltAK8PFJets250SoftDropMass40)
        if hasattr(process,"hltAK8SinglePFJets250SoftDropMass40BTagParticleNetBB0p35"):
            getattr(process,pathname).remove(process.hltAK8SinglePFJets250SoftDropMass40BTagParticleNetBB0p35)

def modifyPNETAK8PathAlternative(process,pathname,minpt,maxeta,l1seedlist=[]):
    ## remove most of the filters in the path
    if not hasattr(process,pathname):
        return;
    if hasattr(process,"hltL1sSingleJetOrHTTOrMuHTT"):
        seeds = str(process.hltL1sSingleJetOrHTTOrMuHTT.L1SeedsLogicalExpression).replace(' ','').replace("cms.string('",'').replace("')",'').split("OR");
        for seed in seeds:
            l1seedlist.append(seed) if seed not in l1seedlist else l1seedlist
        getattr(process,pathname).remove(process.hltL1sSingleJetOrHTTOrMuHTT)
        if hasattr(process,"hltAK8SingleCaloJet200"):
            getattr(process,pathname).remove(process.hltAK8SingleCaloJet200)
        if hasattr(process,"hltAK8PFJetsCorrectedMatchedToCaloJets200"):
            getattr(process,pathname).remove(process.hltAK8PFJetsCorrectedMatchedToCaloJets200)
        if hasattr(process,"hltSingleAK8PFJet230"):
            process.hltSingleAK8PFJet230.inputTag = cms.InputTag("hltAK8PFJets")
            process.hltSingleAK8PFJet230.MinPt = cms.double(minpt);
            process.hltSingleAK8PFJet230.MaxEta = cms.double(maxeta);
            process.hltSingleAK8PFJet230.MinN = cms.int32(1);
        if hasattr(process,"hltAK8PFJets230Constituents"):
            getattr(process,pathname).remove(process.hltAK8PFJets230Constituents)
        if hasattr(process,"hltAK8PFSoftDropJets230"):
            getattr(process,pathname).remove(process.hltAK8PFSoftDropJets230)
        if hasattr(process,"hltAK8SinglePFJets230SoftDropMass40"):
            getattr(process,pathname).remove(process.hltAK8SinglePFJets230SoftDropMass40)        
        if hasattr(process,"hltPFJetForPNetSelectorAK8"):
            process.hltPFJetForPNetSelectorAK8.inputTag = cms.InputTag("hltAK8PFJets");
            process.hltPFJetForPNetSelectorAK8.MinPt = cms.double(minpt);
            process.hltPFJetForPNetSelectorAK8.MaxEta = cms.double(maxeta);
            process.hltPFJetForPNetSelectorAK8.MinN = cms.int32(1);            
        if hasattr(process,"hltAK8PFJets230SoftDropMass40"):
            getattr(process,pathname).remove(process.hltAK8PFJets230SoftDropMass40)
        if hasattr(process,"hltAK8SinglePFJets230SoftDropMass40PNetBBTag0p06"):
            getattr(process,pathname).remove(process.hltAK8SinglePFJets230SoftDropMass40PNetBBTag0p06)
