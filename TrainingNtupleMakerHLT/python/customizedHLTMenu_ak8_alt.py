import FWCore.ParameterSet.Config as cms
from Configuration.StandardSequences.Eras import eras

from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing ('analysis')
options.register('nThreads', 1, VarParsing.multiplicity.singleton,VarParsing.varType.int,"nThreads")
options.register('outputName', "hltMenu_step.root", VarParsing.multiplicity.singleton,VarParsing.varType.string,"outputName")
options.register('hltProcessName', "HLTX", VarParsing.multiplicity.singleton,VarParsing.varType.string,"hltProcessName")
options.register('inputFileListFile', "", VarParsing.multiplicity.singleton,VarParsing.varType.string,"input file list as a file")
options.register('jsonFile',  "", VarParsing.multiplicity.singleton,VarParsing.varType.string,"json luminosity file")
options.register('useLocalInputFile', False, VarParsing.multiplicity.singleton,VarParsing.varType.bool,"use local input files")
options.register('datasetType',  "MC126X", VarParsing.multiplicity.singleton,VarParsing.varType.string,"type of dataset")
options.register('l1Menu', "L1Menu_Collisions2023_v1_0_0_xml", VarParsing.multiplicity.singleton,VarParsing.varType.string,"name of the L1 menu to emulate")
options.register('filterOnL1Seeds', False, VarParsing.multiplicity.singleton,VarParsing.varType.bool,"filter on the basis of the L1 seeds used in the PNET paths")
options.register('additionalL1Seeds',[], VarParsing.multiplicity.list,VarParsing.varType.string,"add a list with the L1 seeds to be added to those used by default in the PNET triggers")
options.register('addLastPNETEvaluation', True, VarParsing.multiplicity.singleton,VarParsing.varType.bool,"evaluate last version of PNET tagger")
options.register('addHLTCustomization2023', True, VarParsing.multiplicity.singleton,VarParsing.varType.bool,"add HLT 2023 customizer")
options.register('useSQLiteFile', False,  VarParsing.multiplicity.singleton,VarParsing.varType.bool,"take HCAL PFcalibrations from SQL file contained in customizeHLTFor2023")
options.register('jetPtMin',  150., VarParsing.multiplicity.singleton,VarParsing.varType.float,"jetPtMin")
options.register('jetEtaMax', 2.5, VarParsing.multiplicity.singleton,VarParsing.varType.float,"jetEtaMax")
options.register('pnetPath', "HLT_AK8PFJet230_SoftDropMass40_PFAK8PNetBBTag0p06_v4",VarParsing.multiplicity.singleton,VarParsing.varType.string,"trigger path PNET AK8")  
options.register('hltPathToRemove', "HLT_QuadPFJet70_50_40_35_PNet2BTagMean0p65_v4", VarParsing.multiplicity.singleton,VarParsing.varType.string,"trigger path to be removed")
options.parseArguments()

process = cms.Process(options.hltProcessName,eras.Run3)

process.load('Configuration.StandardSequences.Services_cff')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 25
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')

from Configuration.AlCa.GlobalTag import GlobalTag

## load the menu
from ParticleNetStudiesRun2.TrainingNtupleMakerHLT.customizeHLTFor2023 import customizeHLTFor2023
from HLTrigger.Configuration.customizeHLTforALL import customizeHLTforAll
from HLTrigger.Configuration.customizeHLTforCMSSW import customizeHLTforCMSSW
from HLTrigger.Configuration.Eras import modifyHLTforEras

if options.datasetType == "MC126X":
    process.load('ParticleNetStudiesRun2.TrainingNtupleMakerHLT.hlt_step_mc_ak8_alt_cff')
    process.GlobalTag = GlobalTag(process.GlobalTag,'126X_mcRun3_2023_forPU65_v4',options.l1Menu+',L1TUtmTriggerMenuRcd')
    if options.addHLTCustomization2023:
        customizeHLTFor2023(process,options.useSQLiteFile)
    isMC=True;
elif options.datasetType == "MC124X":
    process.load('ParticleNetStudiesRun2.TrainingNtupleMakerHLT.hlt_step_mc_ak8_alt_cff')
    process.GlobalTag = GlobalTag(process.GlobalTag,'auto:phase1_2022_realistic_postEE',options.l1Menu+',L1TUtmTriggerMenuRcd')
    if options.addHLTCustomization2023:
        customizeHLTFor2023(process,options.useSQLiteFile)
    isMC=True;
elif options.datasetType == "Data2022" or options.datasetType == "ZeroBias2022":
    process.load('ParticleNetStudiesRun2.TrainingNtupleMakerHLT.hlt_step_data_2022_ak8_alt_cff')
    process.GlobalTag = GlobalTag(process.GlobalTag,'130X_dataRun3_HLT_v2',options.l1Menu+',L1TUtmTriggerMenuRcd')
    if options.addHLTCustomization2023:
        customizeHLTFor2023(process,options.useSQLiteFile)
    isMC=False;
    ## https://github.com/cms-l1-dpg/L1MenuRun3/tree/master/development/L1Menu_Collisions2023_v1_0_0/PrescaleTable                                                                               
    if options.datasetType == "ZeroBias2022":
        process.load('L1Trigger.L1TGlobal.hackConditions_cff')
        process.L1TGlobalPrescalesVetosFract.PrescaleXMLFile = cms.string('UGT_BASE_RS_PRESCALES_L1MenuCollisions2023_v1_0_0.xml')
        process.L1TGlobalPrescalesVetosFract.FinOrMaskXMLFile = cms.string('UGT_BASE_RS_FINOR_MASK_L1MenuCollisions2023_v1_0_0.xml')
        process.simGtStage2Digis.AlgorithmTriggersUnmasked = cms.bool(False)
        process.simGtStage2Digis.AlgorithmTriggersUnprescaled = cms.bool(False)
        process.simGtStage2Digis.PrescaleSet = cms.uint32(3)

else:
    sys.exit("Dataset type not known, abort");

customizeHLTforAll(process,"GRun")
customizeHLTforCMSSW(process, menuType="GRun")
modifyHLTforEras(process)

## edit the PNET paths
from ParticleNetStudiesRun2.TrainingNtupleMakerHLT.modifyPNETPaths_cff import modifyPNETAK8PathAlternative
L1seedList = []
modifyPNETAK8PathAlternative(process,options.pnetPath,options.jetPtMin,options.jetEtaMax,L1seedList)
if options.additionalL1Seeds:
    L1seedList.extend(options.additionalL1Seeds)

## Build a possible L1-seed selection                                                                                                                                                      
process.hltL1SeedForPreSelection = process.hltL1sSingleJetOrHTTOrMuHTT.clone();
process.hltL1SeedForPreSelection.L1SeedsLogicalExpression = cms.string(' OR '.join(L1seedList));
process.hltL1SeedForPreSelectionSequence = cms.Sequence(
    process.SimL1Emulator +
    process.hltL1SeedForPreSelection
)
process.hltL1SeedPathForPreSelection = cms.Path(
    process.HLTBeginSequence +
    process.hltL1SeedForPreSelectionSequence +
    process.HLTEndSequence
)
process.schedule.append(process.hltL1SeedPathForPreSelection)

if options.filterOnL1Seeds:
    getattr(process,options.pnetPath).replace(process.HLTBeginSequence,process.HLTBeginSequence+process.hltL1SeedForPreSelectionSequence);

if hasattr(process,options.hltPathToRemove):
    process.schedule.remove(getattr(process,options.hltPathToRemove))

## source
process.source = cms.Source("PoolSource",
    secondaryFileNames = cms.untracked.vstring()
)

process.source.fileNames = cms.untracked.vstring(options.inputFiles)
if options.inputFileListFile:
    list_file = open(options.inputFileListFile,"r");
    for f in list_file:
        f = f.replace("[\"","");
        f = f.replace("\"]","");
        f = f.replace(" ","");
        f = f.replace("\n","");
        f = f.split("\",\"");
        for ifile in f:
            options.inputFiles.append(ifile);
    process.source.fileNames = cms.untracked.vstring(options.inputFiles)
if options.useLocalInputFile:
    file_names = ["file:"+f for f in options.inputFiles]
    process.source.fileNames = cms.untracked.vstring(file_names)


import FWCore.PythonUtilities.LumiList as LumiList
if options.jsonFile:
    process.source.lumisToProcess = LumiList.LumiList(filename = options.jsonFile).getVLuminosityBlockRange()

## options
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
)

process.options = cms.untracked.PSet(
    numberOfStreams = cms.untracked.uint32(options.nThreads),
    numberOfThreads = cms.untracked.uint32(options.nThreads),
    wantSummary = cms.untracked.bool(True)
)

## output 
process.hltOutputFull = cms.OutputModule( "PoolOutputModule",
    fileName = cms.untracked.string( options.outputName),
    fastCloning = cms.untracked.bool( False ),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string( 'RECO' ),
        filterName = cms.untracked.string( '' )
    ),
   outputCommands = cms.untracked.vstring(
        'drop *_*_*_SIM',
        'keep *_*generator*_*_SIM',
        'keep *_*_*_HLT',
        'keep *_*_*_LHC',
        'drop *_*Scouting*_*_HLT',
        'keep *_*hltTriggerSummaryAOD*_*_*'+options.hltProcessName+'*',
        'keep *_*TriggerResults*_*_*'+options.hltProcessName+'*',
        'keep *_*hltFixedGridRhoFastjetAll*_*_*'+options.hltProcessName+'*',
        'keep *_*hltAK8CaloJetsCorrectedIDPassed*_*_*'+options.hltProcessName+'*',
        'keep *_*hltAK4PFJetsCorrected*_*_*'+options.hltProcessName+'*'
    ),
    overrideBranchesSplitLevel = cms.untracked.VPSet(
    ),
)


## select events in output                                                                                                                                                                            
process.hltOutputFull.SelectEvents = cms.untracked.PSet(
    SelectEvents = cms.vstring(options.pnetPath)
)

process.FullOutput = cms.FinalPath(process.hltOutputFull)
process.schedule.append(process.FullOutput)

## build the output collections for storing jets for PNET training / optimization
from ParticleNetStudiesRun2.TrainingNtupleMakerHLT.patAK8JetsForHLT import customizeHLTForAK8PNET
customizeHLTForAK8PNET(process,
    options,
    jetCollection="hltPFJetForPNetAK8", ## jets on top of which PNET AK8 was inferred 
    pathName=options.pnetPath,
    outputModuleName="hltOutputFull",
    isMC=isMC,
    addLastPNETEvaluation=options.addLastPNETEvaluation,
    isAlternative=True                   
)
