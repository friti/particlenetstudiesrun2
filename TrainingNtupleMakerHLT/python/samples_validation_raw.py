def AddDYSamples(samples):

    samples['DYJetsToLL'] = [
        '/DYTo2L_MLL-50_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-RnD_126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddQCDSamples(samples):

    samples['QCD_Pt_30to50'] = [
        '/QCD_PT-30to50_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_50to80'] = [
        '/QCD_PT-50to80_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_80to120'] = [
        '/QCD_PT-80to120_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_120to170'] = [
        '/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_170to300'] = [
        '/QCD_PT-170to300_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
    samples['QCD_Pt_300to470'] = [
        '/QCD_PT-300to470_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_470to600'] = [
        '/QCD_PT-470to600_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_600toInf'] = [
        '/QCD_PT-600toInf_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    
def AddTTbarSamples(samples):

    samples['TTbar'] = [
        '/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddHHSamples(samples):


    samples['GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]


    samples['GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-RnD_126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddEphemeralHLTSamples(samples):
    
    samples["EphemeralHLTPhysics0-Run2022G"] = [
        '/EphemeralHLTPhysics0/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics1-Run2022G"] = [
        '/EphemeralHLTPhysics1/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics2-Run2022G"] = [
        '/EphemeralHLTPhysics2/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics3-Run2022G"] = [
        '/EphemeralHLTPhysics3/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics4-Run2022G"] = [
        '/EphemeralHLTPhysics4/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics5-Run2022G"] = [
        '/EphemeralHLTPhysics5/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics6-Run2022G"] = [
        '/EphemeralHLTPhysics6/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics7-Run2022G"] = [
        '/EphemeralHLTPhysics7/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralHLTPhysics8-Run2022G"] = [
        '/EphemeralHLTPhysics8/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=Data2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

def AddEphemeralZeroBiasSamples(samples):
    
    samples["EphemeralZeroBias0-Run2022G"] = [
        '/EphemeralZeroBias0/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias1-Run2022G"] = [
        '/EphemeralZeroBias1/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias2-Run2022G"] = [
        '/EphemeralZeroBias2/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias3-Run2022G"] = [
        '/EphemeralZeroBias3/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias4-Run2022G"] = [
        '/EphemeralZeroBias4/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias5-Run2022G"] = [
        '/EphemeralZeroBias5/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias6-Run2022G"] = [
        '/EphemeralZeroBias6/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias7-Run2022G"] = [
        '/EphemeralZeroBias7/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

    samples["EphemeralZeroBias8-Run2022G"] = [
        '/EphemeralZeroBias8/Run2022G-v1/RAW',
        ['nThreads=4','datasetType=ZeroBias2022','filterOnL1Seeds=True','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        25000,
        '/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_eraG_362433_362760_Golden.json',
        -1
    ]

def AddAllSamples(samples):
    AddDYSamples(samples)
    AddQCDSamples(samples)
    AddTTbarSamples(samples)
    AddHHSamples(samples)
    AddEphemeralHLTSamples(samples)
    AddEphemeralZeroBiasSamples(samples)
