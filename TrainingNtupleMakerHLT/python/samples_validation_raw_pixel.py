def AddQCDSamples(samples):

    samples['QCD_Pt_30to50'] = [
        '/QCD_PT-30to50_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_50to80'] = [
        '/QCD_PT-50to80_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_80to120'] = [
        '/QCD_PT-80to120_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_120to170'] = [
        '/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_170to300'] = [
        '/QCD_PT-170to300_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
    samples['QCD_Pt_300to470'] = [
        '/QCD_PT-300to470_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_470to600'] = [
        '/QCD_PT-470to600_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_600toInf'] = [
        '/QCD_PT-600toInf_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    
def AddTTbarSamples(samples):

    samples['TTbar'] = [
        '/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddHHSamples(samples):


    samples['GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto4B_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]


    samples['GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-GTv3Digi_126X_mcRun3_2023_forPU65_v3-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=False','useSQLiteFile=True','addHLTCustomization2023=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]


def AddAllSamples(samples):
    AddQCDSamples(samples)
    AddTTbarSamples(samples)
    AddHHSamples(samples)
