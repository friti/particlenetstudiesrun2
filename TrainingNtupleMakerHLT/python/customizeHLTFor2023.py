from HLTrigger.Configuration.common import *
import FWCore.ParameterSet.Config as cms
import os

HCAL_PFclusters_2023=[0.6,0.5,0.5,0.5]
HCAL_PFrechits_2023=[0.4,0.3,0.3,0.3]

HCAL_PFclusters_2022=[0.125, 0.25, 0.35, 0.35]
HCAL_PFrechits_2022=[0.1, 0.2, 0.3, 0.3]

def customizeHCALFor2023(process):
    return customizeHCAL(process, HCAL_PFclusters_2023, HCAL_PFrechits_2023)

def customizeHCALFor2022(process):
    return customizeHCAL(process, HCAL_PFclusters_2022, HCAL_PFrechits_2022)

def customizeHCAL(process, HCAL_PFclusters, HCAL_PFrechits):
    if hasattr(process, "hltParticleFlowClusterHBHE"):
        process.hltParticleFlowClusterHBHE.seedFinder.thresholdsByDetector[0].seedingThreshold=HCAL_PFclusters
        process.hltParticleFlowClusterHBHE.initialClusteringStep.thresholdsByDetector[0].gatheringThreshold=HCAL_PFrechits
        process.hltParticleFlowClusterHBHE.pfClusterBuilder.recHitEnergyNorms[0].recHitEnergyNorm=HCAL_PFrechits
        process.hltParticleFlowClusterHBHE.pfClusterBuilder.positionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=HCAL_PFrechits
        process.hltParticleFlowClusterHBHE.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=HCAL_PFrechits
    if hasattr(process, "hltParticleFlowClusterHCAL"):
        process.hltParticleFlowClusterHCAL.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=HCAL_PFrechits
    if hasattr(process, "hltParticleFlowRecHitHBHE"):
        process.hltParticleFlowRecHitHBHE.producers[0].qualityTests[0].cuts[0].threshold=HCAL_PFrechits

    if hasattr(process, "hltEgammaHoverE"):
        process.hltEgammaHoverE.eThresHB=HCAL_PFrechits
    if hasattr(process, "hltEgammaHoverEUnseeded"):
        process.hltEgammaHoverEUnseeded.eThresHB=HCAL_PFrechits
    if hasattr(process, "hltEgammaHToverET"):
        process.hltEgammaHToverET.eThresHB=HCAL_PFrechits
    if hasattr(process, "hltFixedGridRhoFastjetECALMFForMuons"):
        process.hltFixedGridRhoFastjetECALMFForMuons.eThresHB=HCAL_PFrechits
    if hasattr(process, "hltFixedGridRhoFastjetAllCaloForMuons"):
        process.hltFixedGridRhoFastjetAllCaloForMuons.eThresHB=HCAL_PFrechits
    if hasattr(process, "hltFixedGridRhoFastjetHCAL"):
        process.hltFixedGridRhoFastjetHCAL.eThresHB=HCAL_PFrechits

    if hasattr(process, "hltParticleFlowClusterHBHECPUOnly"):
        process.hltParticleFlowClusterHBHECPUOnly.seedFinder.thresholdsByDetector[0].seedingThreshold=HCAL_PFclusters
        process.hltParticleFlowClusterHBHECPUOnly.initialClusteringStep.thresholdsByDetector[0].gatheringThreshold=HCAL_PFrechits
        process.hltParticleFlowClusterHBHECPUOnly.pfClusterBuilder.recHitEnergyNorms[0].recHitEnergyNorm=HCAL_PFrechits
        process.hltParticleFlowClusterHBHECPUOnly.pfClusterBuilder.positionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=HCAL_PFrechits
        process.hltParticleFlowClusterHBHECPUOnly.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=HCAL_PFrechits
    if hasattr(process, "hltParticleFlowClusterHCALCPUOnly"):
        process.hltParticleFlowClusterHCALCPUOnly.pfClusterBuilder.allCellsPositionCalc.logWeightDenominatorByDetector[0].logWeightDenominator=HCAL_PFrechits
    if hasattr(process, "hltParticleFlowRecHitHBHECPUOnly"):
        process.hltParticleFlowRecHitHBHECPUOnly.producers[0].qualityTests[0].cuts[0].threshold=HCAL_PFrechits
    
    return process


def customizeHCALinCaloJets(process, HCAL_PFrechits):
    if hasattr(process, "hltTowerMakerForAll"):
        process.hltTowerMakerForAll.HBThreshold1 = HCAL_PFrechits[0]
        process.hltTowerMakerForAll.HBThreshold2 = HCAL_PFrechits[1]
        process.hltTowerMakerForAll.HBThreshold  = HCAL_PFrechits[2]
    return process



def customizeHCALinCaloJetsFor2023(process):
    return customizeHCALinCaloJets(process, HCAL_PFrechits_2023)

def customizeHCALinCaloJetsFor2022(process):
    return customizeHCALinCaloJets(process, HCAL_PFrechits_2022)


def customizePFHadronCalibrationFor2023(process):
    if hasattr(process, "GlobalTag") and hasattr(process.GlobalTag, "toGet"):
        process.GlobalTag.toGet.append(
            cms.PSet(
                record = cms.string("PFCalibrationRcd"),
                label = cms.untracked.string('HLT'),
                connect = cms.string("sqlite_file:"+os.environ['CMSSW_BASE']+"/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/PFCalibration.db"),
                tag = cms.string('PFCalibration_CMSSW_13_0_0_pre4_HLT_126X_mcRun3_2023'),
                snapshotTime = cms.string('9999-12-31 23:59:59.000'),
            )
        )
    else:
        print("Warning process.GlobalTag not found. customizePFHadronCalibrationFor2023 will not be applied.")
    return process


def customizeJECFor2023(process):
    if hasattr(process, "GlobalTag") and hasattr(process.GlobalTag, "toGet"):
        process.GlobalTag.toGet.append(
            cms.PSet(
                record = cms.string("JetCorrectionsRecord"),
                label = cms.untracked.string('AK4PFHLT'),
                connect = cms.string("sqlite_file:"+os.environ['CMSSW_BASE']+"/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/Run3Winter23Digi.db"),
                tag = cms.string('JetCorrectorParametersCollection_Run3Winter23Digi_AK4PFHLT'),
                snapshotTime = cms.string('9999-12-31 23:59:59.000'),
            )
        )
        process.GlobalTag.toGet.append(
            cms.PSet(
                record = cms.string("JetCorrectionsRecord"),
                label = cms.untracked.string('AK8PFHLT'),
                connect = cms.string("sqlite_file:"+os.environ['CMSSW_BASE']+"/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/Run3Winter23Digi.db"),
                tag = cms.string('JetCorrectorParametersCollection_Run3Winter23Digi_AK8PFHLT'),
                snapshotTime = cms.string('9999-12-31 23:59:59.000'),
            )
        )
        process.GlobalTag.toGet.append(
            cms.PSet(
                record = cms.string("JetCorrectionsRecord"),
                label = cms.untracked.string('AK4CaloHLT'),
                connect = cms.string("sqlite_file:"+os.environ['CMSSW_BASE']+"/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/Run3Winter23Digi.db"),
                tag = cms.string('JetCorrectorParametersCollection_Run3Winter23Digi_AK4CaloHLT'),
                snapshotTime = cms.string('9999-12-31 23:59:59.000'),
            )
        )
        process.GlobalTag.toGet.append(
            cms.PSet(
                record = cms.string("JetCorrectionsRecord"),
                label = cms.untracked.string('AK8CaloHLT'),
                connect = cms.string("sqlite_file:"+os.environ['CMSSW_BASE']+"/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/Run3Winter23Digi.db"),
                tag = cms.string('JetCorrectorParametersCollection_Run3Winter23Digi_AK8CaloHLT'),
                snapshotTime = cms.string('9999-12-31 23:59:59.000'),
            )
        )
    else:
        raise Exception("Warning process.GlobalTag not found. customizeJECFor2023 will not be applied.")
    return process


def customizeHLTFor2023(process,useSQLiteFile):

    process = customizeHCALFor2023(process)
    process = customizeHCALinCaloJetsFor2023(process)
    if useSQLiteFile:
        process = customizePFHadronCalibrationFor2023(process)
        process = customizeJECFor2023(process)

    return process
