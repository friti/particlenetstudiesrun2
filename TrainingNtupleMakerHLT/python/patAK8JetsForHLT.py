import FWCore.ParameterSet.Config as cms
import os, sys

def customizeHLTForAK8PNET(process,
        options,
        jetCollection,
        pathName="",
        outputModuleName="",
        isMC=True,
        addLastPNETEvaluation=False,
        isAlternative=False,
        minPtGenJets=10,
        maxEtaGenJets=3.0):

    print("customizeHLTForAK8PNET --> in order to create PAT containers for AK8 PNET")

    if isMC:
        from PhysicsTools.PatAlgos.slimming.prunedGenParticles_cfi import prunedGenParticles;
        setattr(process,"hltPrunedGenParticlesWithStatusOne",prunedGenParticles.clone());
        getattr(process,"hltPrunedGenParticlesWithStatusOne").select.append("keep status == 1");
        setattr(process,"hltPrunedGenParticles",prunedGenParticles.clone());
        getattr(process,"hltPrunedGenParticles").src = cms.InputTag("hltPrunedGenParticlesWithStatusOne")
    
        ## packed gen particles
        from PhysicsTools.PatAlgos.slimming.packedGenParticles_cfi import packedGenParticles
        setattr(process,"hltPackedGenParticles",packedGenParticles.clone());
        getattr(process,"hltPackedGenParticles").inputCollection = cms.InputTag("hltPrunedGenParticlesWithStatusOne")
        getattr(process,"hltPackedGenParticles").map = cms.InputTag("hltPrunedGenParticles");
        getattr(process,"hltPackedGenParticles").inputOriginal = cms.InputTag("genParticles")
  
        ## AK8 Gen jets
        from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJetsNoNu
        setattr(process,"hltGenParticlesForJetsNoNu",genParticlesForJetsNoNu.clone());
  
        from RecoJets.JetProducers.ak8GenJets_cfi import ak8GenJets, ak8GenJetsConstituents, ak8GenJetsSoftDrop
        setattr(process,"hltAK8GenJets",ak8GenJets.clone());
        getattr(process,"hltAK8GenJets").src = cms.InputTag("hltGenParticlesForJetsNoNu");
        getattr(process,"hltAK8GenJets").jetPtMin = cms.double(minPtGenJets);
  
        setattr(process,"hltAK8GenJetsConstituents",ak8GenJetsConstituents.clone());
        getattr(process,"hltAK8GenJetsConstituents").src = cms.InputTag("hltAK8GenJets");
        getattr(process,"hltAK8GenJetsConstituents").cut = cms.string("pt > "+str(minPtGenJets)+" && abs(eta) < "+str(maxEtaGenJets));
  
        setattr(process,"hltAK8GenJetsSoftDrop",ak8GenJetsSoftDrop.clone());
        getattr(process,"hltAK8GenJetsSoftDrop").jetPtMin = cms.double(minPtGenJets);
        getattr(process,"hltAK8GenJetsSoftDrop").src = cms.InputTag("hltAK8GenJetsConstituents","constituents");

        ## Hadron flavor for GEN jets                                                                                                                                                         
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetPartons
        setattr(process,"hltPatJetPartons",patJetPartons.clone());
        getattr(process,"hltPatJetPartons").particles = cms.InputTag("hltPrunedGenParticles");
  
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetFlavourAssociation
        setattr(process,"hltAK8GenJetsFlavourAssociation",patJetFlavourAssociation.clone());
        getattr(process,"hltAK8GenJetsFlavourAssociation").jets = cms.InputTag("hltAK8GenJets");
        getattr(process,"hltAK8GenJetsFlavourAssociation").bHadrons = cms.InputTag("hltPatJetPartons","bHadrons");
        getattr(process,"hltAK8GenJetsFlavourAssociation").cHadrons = cms.InputTag("hltPatJetPartons","cHadrons");
        getattr(process,"hltAK8GenJetsFlavourAssociation").partons = cms.InputTag("hltPatJetPartons","physicsPartons");
        getattr(process,"hltAK8GenJetsFlavourAssociation").leptons = cms.InputTag("hltPatJetPartons","leptons");
        getattr(process,"hltAK8GenJetsFlavourAssociation").rParam = cms.double(0.8)

        from PhysicsTools.PatAlgos.producersLayer1.jetProducer_cfi import _patJets
        setattr(process,"hltPatGenJetsAK8",_patJets.clone());
        getattr(process,"hltPatGenJetsAK8").jetSource = cms.InputTag("hltAK8GenJets");
        getattr(process,"hltPatGenJetsAK8").addAssociatedTracks = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").addJetCorrFactors   = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").jetCorrFactorsSource = cms.VInputTag();
        getattr(process,"hltPatGenJetsAK8").addBTagInfo         = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").addDiscriminators   = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").discriminatorSources = cms.VInputTag("");
        getattr(process,"hltPatGenJetsAK8").addJetCharge        = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").addAssociatedTracks = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").addGenPartonMatch   = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").embedGenPartonMatch = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").addGenJetMatch      = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").embedGenJetMatch    = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").getJetMCFlavour     = cms.bool(True);
        getattr(process,"hltPatGenJetsAK8").addJetFlavourInfo   = cms.bool(True);
        getattr(process,"hltPatGenJetsAK8").JetFlavourInfoSource  = cms.InputTag("hltAK8GenJetsFlavourAssociation");
        getattr(process,"hltPatGenJetsAK8").useLegacyJetMCFlavour = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8").JetPartonMapSource = cms.InputTag("");
        getattr(process,"hltPatGenJetsAK8").genPartonMatch = cms.InputTag("");
        getattr(process,"hltPatGenJetsAK8").jetChargeSource = cms.InputTag("");
        getattr(process,"hltPatGenJetsAK8").trackAssociationSource = cms.InputTag("");
  
        ## Soft Drop jets 
        setattr(process,"hltAK8GenJetsFlavourAssociationSoftDropSubJets",getattr(process,"hltAK8GenJetsFlavourAssociation").clone());
        getattr(process,"hltAK8GenJetsFlavourAssociationSoftDropSubJets").groomedJets = cms.InputTag("hltAK8GenJetsSoftDrop");
        getattr(process,"hltAK8GenJetsFlavourAssociationSoftDropSubJets").subjets = cms.InputTag("hltAK8GenJetsSoftDrop","SubJets");

        setattr(process,"hltPatGenJetsAK8SoftDrop",getattr(process,"hltPatGenJetsAK8").clone());
        getattr(process,"hltPatGenJetsAK8SoftDrop").jetSource = cms.InputTag("hltAK8GenJetsSoftDrop");
        getattr(process,"hltPatGenJetsAK8SoftDrop").addJetFlavourInfo = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8SoftDrop").getJetMCFlavour = cms.bool(False);
        getattr(process,"hltPatGenJetsAK8SoftDrop").JetFlavourInfoSource  = cms.InputTag("");

        setattr(process,"hltPatGenJetsAK8SoftDropSubJets",getattr(process,"hltPatGenJetsAK8").clone());
        getattr(process,"hltPatGenJetsAK8SoftDropSubJets").jetSource  = cms.InputTag("hltAK8GenJetsSoftDrop","SubJets");
        getattr(process,"hltPatGenJetsAK8SoftDropSubJets").JetFlavourInfoSource  = cms.InputTag("hltAK8GenJetsFlavourAssociationSoftDropSubJets","SubJets");
        
        setattr(process,"hltPatGenJetsAK8SoftDropPacked",          
                cms.EDProducer("BoostedJetMerger",
                            jetSrc = cms.InputTag("hltPatGenJetsAK8SoftDrop"),
                               subjetSrc = cms.InputTag("hltPatGenJetsAK8SoftDropSubJets")
                           )
            );

        setattr(process,"hltPackedGenJetsAK8",
                cms.EDProducer("GenJetSubstructurePacker",
                               algoLabels = cms.vstring('SoftDrop'),
                               algoTags = cms.VInputTag(cms.InputTag("hltPatGenJetsAK8SoftDropPacked")),
                               distMax = cms.double(0.8),
                               fixDaughters = cms.bool(True),
                               packedGenParticles = cms.InputTag("hltPackedGenParticles"),
                               jetSrc = cms.InputTag("hltPatGenJetsAK8"))
            );  

        ## Sequence for operations
        if hasattr(process,pathName):
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPrunedGenParticlesWithStatusOne")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPrunedGenParticles")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPackedGenParticles")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltGenParticlesForJetsNoNu")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8GenJets")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8GenJetsConstituents")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8GenJetsSoftDrop")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetPartons")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8GenJetsFlavourAssociation")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatGenJetsAK8")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8GenJetsFlavourAssociationSoftDropSubJets")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatGenJetsAK8SoftDrop")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatGenJetsAK8SoftDropSubJets")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatGenJetsAK8SoftDropPacked")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPackedGenJetsAK8")+process.hltBoolEnd);
            
        ## Update output file
        if hasattr(process,outputModuleName):
            getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltPatGenJetsAK8SoftDropPacked_*_*"+options.hltProcessName+"*");
            getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltPackedGenJetsAK8_*_*"+options.hltProcessName+"*");

    ## Slimmed vertexes
    from PhysicsTools.PatAlgos.slimming.offlineSlimmedPrimaryVertices_cfi import offlineSlimmedPrimaryVertices
    setattr(process,"hltOfflineSlimmedPrimaryVertices",offlineSlimmedPrimaryVertices.clone());
    getattr(process,"hltOfflineSlimmedPrimaryVertices").src = cms.InputTag("hltVerticesPFFilter")
    getattr(process,"hltOfflineSlimmedPrimaryVertices").score = cms.InputTag("hltPrimaryVertexAssociation","original")
        
    ## Packed PF candidates
    from PhysicsTools.PatAlgos.slimming.packedPFCandidates_cfi import packedPFCandidates
    setattr(process,"hltPackedPFCandidates",packedPFCandidates.clone());
    getattr(process,"hltPackedPFCandidates").inputCollection = cms.InputTag("hltParticleFlow");
    getattr(process,"hltPackedPFCandidates").inputVertices = cms.InputTag("hltOfflineSlimmedPrimaryVertices");
    getattr(process,"hltPackedPFCandidates").originalVertices = cms.InputTag("hltVerticesPFFilter");
    getattr(process,"hltPackedPFCandidates").originalTracks = cms.InputTag("hltPFMuonMerging");
    getattr(process,"hltPackedPFCandidates").vertexAssociator = cms.InputTag("hltPrimaryVertexAssociation","original");
    getattr(process,"hltPackedPFCandidates").PuppiSrc = cms.InputTag("");
    getattr(process,"hltPackedPFCandidates").PuppiNoLepSrc = cms.InputTag("");
    getattr(process,"hltPackedPFCandidates").chargedHadronIsolation = cms.InputTag("");
    getattr(process,"hltPackedPFCandidates").secondaryVerticesForWhiteList =  cms.VInputTag(cms.InputTag("hltDeepInclusiveMergedVerticesPF"));
    getattr(process,"hltPackedPFCandidates").covarianceVersion = cms.int32(0);
    ## Slimmed secondary vertexes
    from PhysicsTools.PatAlgos.slimming.slimmedSecondaryVertices_cfi import slimmedSecondaryVertices
    setattr(process,"hltSlimmedSecondaryVertices",slimmedSecondaryVertices.clone());
    getattr(process,"hltSlimmedSecondaryVertices").src = cms.InputTag("hltDeepInclusiveMergedVerticesPF");
    getattr(process,"hltSlimmedSecondaryVertices").packedPFCandidates = cms.InputTag("hltPackedPFCandidates");
  
    if hasattr(process,pathName):
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPrimaryVertexAssociation")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltOfflineSlimmedPrimaryVertices")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPackedPFCandidates")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedSecondaryVertices")+process.hltBoolEnd);

    if hasattr(process,outputModuleName):
        getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltOfflineSlimmedPrimaryVertices"+"*_*_*"+options.hltProcessName+"*");
        getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltPackedPFCandidates"+"*_*_*"+options.hltProcessName+"*");
        getattr(process,outputModuleName).outputCommands.append("keep *_*"+"hltSlimmedSecondaryVertices"+"*_*_*"+options.hltProcessName+"*");

    getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
        branch = cms.untracked.string('recoVertexs_hltSlimmedSecondaryVertices__*'),
        splitLevel = cms.untracked.int32(99)
    ));
    
    getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
        branch = cms.untracked.string('recoVertexs_hltOfflineSlimmedPrimaryVertices__*'),
        splitLevel = cms.untracked.int32(99)
    ));

    getattr(process,outputModuleName).overrideBranchesSplitLevel.append(cms.untracked.PSet(
        branch = cms.untracked.string('patPackedCandidates_hltPackedPFCandidates__*'),
        splitLevel = cms.untracked.int32(99)
    ));

    ## jet collection
    if isMC:
        ## parton jet flavour  
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetFlavourId_cff import patJetFlavourAssociation
        setattr(process,"hltPatJetAK8FlavourAssociation",getattr(process,"hltAK8GenJetsFlavourAssociation").clone());
        getattr(process,"hltPatJetAK8FlavourAssociation").jets = cms.InputTag(jetCollection);

        ## matching with gen-jets
        from PhysicsTools.PatAlgos.mcMatchLayer0.jetMatch_cfi import patJetGenJetMatch
        setattr(process,"hltPatJetAK8GenJetMatch",patJetGenJetMatch.clone());
        getattr(process,"hltPatJetAK8GenJetMatch").src = cms.InputTag(jetCollection);
        getattr(process,"hltPatJetAK8GenJetMatch").matched = cms.InputTag("hltAK8GenJets");
        getattr(process,"hltPatJetAK8GenJetMatch").maxDeltaR = cms.double(0.8);

    from RecoJets.JetProducers.nJettinessAdder_cfi import Njettiness
    setattr(process,"hltNjettinessAK8PFJets",Njettiness.clone());
    getattr(process,"hltNjettinessAK8PFJets").src = cms.InputTag(jetCollection);
    getattr(process,"hltNjettinessAK8PFJets").R0 = cms.double(0.8);
    getattr(process,"hltNjettinessAK8PFJets").srcWeights = cms.InputTag("");
  
    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    setattr(process,"hltPatJetAK8CorrFactorsProducer",JetCorrFactorsProducer.clone());
    getattr(process,"hltPatJetAK8CorrFactorsProducer").extraJPTOffset = cms.string('L1FastJet');
    if isMC:
        getattr(process,"hltPatJetAK8CorrFactorsProducer").levels = cms.vstring('L1FastJet','L2Relative','L3Absolute');
    else:
        getattr(process,"hltPatJetAK8CorrFactorsProducer").levels = cms.vstring('L1FastJet','L2Relative','L3Absolute','L2L3Residual');
    getattr(process,"hltPatJetAK8CorrFactorsProducer").payload = cms.string('AK8PFHLT');
    getattr(process,"hltPatJetAK8CorrFactorsProducer").primaryVertices = cms.InputTag("hltVerticesPFFilter");
    getattr(process,"hltPatJetAK8CorrFactorsProducer").rho = cms.InputTag("hltFixedGridRhoFastjetAll");
    getattr(process,"hltPatJetAK8CorrFactorsProducer").useNPV = cms.bool(False);
    getattr(process,"hltPatJetAK8CorrFactorsProducer").useRho = cms.bool(True);
    getattr(process,"hltPatJetAK8CorrFactorsProducer").src = cms.InputTag(jetCollection);

    if addLastPNETEvaluation:
        setattr(process,"hltParticleNetONNXLastJetTagsAK8",process.hltParticleNetONNXJetTagsAK8.clone());
        getattr(process,"hltParticleNetONNXLastJetTagsAK8").preprocess_json = cms.string("ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/ParticleNetAK8/preprocess.json")
        getattr(process,"hltParticleNetONNXLastJetTagsAK8").model_path = cms.FileInPath("ParticleNetStudiesRun2/TrainingNtupleMakerHLT/data/ParticleNetAK8/particle-net.onnx")
        getattr(process,"hltParticleNetONNXLastJetTagsAK8").flav_names = cms.vstring(
            "probHtt",
            "probHtm",
            "probHte",
            "probHbb",
            "probHcc",
            "probHqq",
            "probHgg",
            "probQCD2hf",
            "probQCD1hf",
            "probQCD0hf"
        )
    
    from PhysicsTools.PatAlgos.producersLayer1.jetProducer_cfi import _patJets
    setattr(process,"hltPatJetsAK8",_patJets.clone());
    getattr(process,"hltPatJetsAK8").jetSource = cms.InputTag(jetCollection);
    getattr(process,"hltPatJetsAK8").addAssociatedTracks = cms.bool(False);
    getattr(process,"hltPatJetsAK8").addJetCharge = cms.bool(False);    
    getattr(process,"hltPatJetsAK8").addJetCorrFactors = cms.bool(True);
    getattr(process,"hltPatJetsAK8").jetCorrFactorsSource = cms.VInputTag("hltPatJetAK8CorrFactorsProducer");

    if isMC:
        getattr(process,"hltPatJetsAK8").addGenJetMatch   = cms.bool(True);
        getattr(process,"hltPatJetsAK8").embedGenJetMatch = cms.bool(True);
        getattr(process,"hltPatJetsAK8").genJetMatch      = cms.InputTag("hltPatJetAK8GenJetMatch");
        getattr(process,"hltPatJetsAK8").addGenPartonMatch   = cms.bool(False);
        getattr(process,"hltPatJetsAK8").addJetFlavourInfo    = cms.bool(True);
        getattr(process,"hltPatJetsAK8").getJetMCFlavour      = cms.bool(True);
        getattr(process,"hltPatJetsAK8").JetFlavourInfoSource = cms.InputTag("hltPatJetAK8FlavourAssociation");
        getattr(process,"hltPatJetsAK8").JetPartonMapSource   = cms.InputTag("");
        getattr(process,"hltPatJetsAK8").genPartonMatch       = cms.InputTag("");
    else:
        getattr(process,"hltPatJetsAK8").addGenJetMatch   = cms.bool(False);
        getattr(process,"hltPatJetsAK8").embedGenJetMatch = cms.bool(False);
        getattr(process,"hltPatJetsAK8").genJetMatch      = cms.InputTag("");
        getattr(process,"hltPatJetsAK8").addGenPartonMatch   = cms.bool(False);
        getattr(process,"hltPatJetsAK8").addJetFlavourInfo    = cms.bool(False);
        getattr(process,"hltPatJetsAK8").getJetMCFlavour      = cms.bool(False);
        getattr(process,"hltPatJetsAK8").JetFlavourInfoSource = cms.InputTag("");
        getattr(process,"hltPatJetsAK8").JetPartonMapSource   = cms.InputTag("");
        getattr(process,"hltPatJetsAK8").genPartonMatch       = cms.InputTag("");

    getattr(process,"hltPatJetsAK8").addBTagInfo          = cms.bool(True);                                                                                                   
    getattr(process,"hltPatJetsAK8").addDiscriminators    = cms.bool(True);                                                                                                    
    getattr(process,"hltPatJetsAK8").addTagInfos          = cms.bool(False);
    getattr(process,"hltPatJetsAK8").jetChargeSource = cms.InputTag("");
    getattr(process,"hltPatJetsAK8").trackAssociationSource = cms.InputTag("");

    ## clone here for the soft-drop jets
    setattr(process,"hltPatJetsAK8SoftDrop",getattr(process,"hltPatJetsAK8").clone());

    if not isAlternative:
        getattr(process,"hltPatJetsAK8").discriminatorSources = cms.VInputTag(                                                                                                                 
            cms.InputTag('hltParticleNetONNXJetTagsAK8','probHtt'),
            cms.InputTag('hltParticleNetONNXJetTagsAK8','probHbb'),
            cms.InputTag('hltParticleNetONNXJetTagsAK8','probHcc'),
            cms.InputTag('hltParticleNetONNXJetTagsAK8','probHqq'),
            cms.InputTag('hltParticleNetONNXJetTagsAK8','probHgg'),
            cms.InputTag('hltParticleNetONNXJetTagsAK8','probQCD')
        )
    else:
        getattr(process,"hltPatJetsAK8").discriminatorSources = cms.VInputTag(                                                                                                                 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHtt"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHtm"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHte"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHbb"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHcc"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHqq"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probHgg"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probQCD2hf"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probQCD1hf"), 
            cms.InputTag("hltParticleNetONNXJetTagsAK8","probQCD0hf")
        )
        

    if addLastPNETEvaluation:
        getattr(process,"hltPatJetsAK8").discriminatorSources.extend([
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHtt"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHtm"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHte"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHbb"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHcc"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHqq"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probHgg"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probQCD2hf"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probQCD1hf"), 
            cms.InputTag("hltParticleNetONNXLastJetTagsAK8","probQCD0hf")
        ])

    getattr(process,"hltPatJetsAK8").userData.userFloats.src = cms.VInputTag(
        cms.InputTag("hltNjettinessAK8PFJets:tau1"),
        cms.InputTag("hltNjettinessAK8PFJets:tau2"),
        cms.InputTag("hltNjettinessAK8PFJets:tau3"),
        cms.InputTag("hltNjettinessAK8PFJets:tau4"));

    from PhysicsTools.PatAlgos.selectionLayer1.jetSelector_cfi import selectedPatJets
    setattr(process,"hltSelectedPatJetsAK8",selectedPatJets.clone());
    getattr(process,"hltSelectedPatJetsAK8").src = cms.InputTag("hltPatJetsAK8");
    getattr(process,"hltSelectedPatJetsAK8").cut = cms.string("pt > "+str(options.jetPtMin)+" && abs(eta) < "+str(options.jetEtaMax));

    from RecoJets.JetProducers.ak8PFJets_cfi import ak8PFJetsCHSSoftDrop, ak8PFJetsCHSConstituents 
    setattr(process,"hltAK8PFJetsConstituents",ak8PFJetsCHSConstituents.clone());
    getattr(process,"hltAK8PFJetsConstituents").src = cms.InputTag(jetCollection)
    getattr(process,"hltAK8PFJetsConstituents").cut = cms.string("pt > "+str(options.jetPtMin)+" && abs(eta) < "+str(options.jetEtaMax));
    
    setattr(process,"hltAK8PFJetsSoftDrop",ak8PFJetsCHSSoftDrop.clone());
    getattr(process,"hltAK8PFJetsSoftDrop").src = cms.InputTag("hltAK8PFJetsConstituents","constituents");
    getattr(process,"hltAK8PFJetsSoftDrop").jetPtMin = cms.double(options.jetPtMin);
    
    setattr(process,"hltPatJetAK8SoftDropCorrFactorsProducer",getattr(process,"hltPatJetAK8CorrFactorsProducer").clone());
    getattr(process,"hltPatJetAK8SoftDropCorrFactorsProducer").src = cms.InputTag("hltAK8PFJetsSoftDrop");
        
    if isMC:
        setattr(process,"hltPatJetSoftDropAK8GenJetMatch",patJetGenJetMatch.clone());
        getattr(process,"hltPatJetSoftDropAK8GenJetMatch").src = cms.InputTag("hltAK8PFJetsSoftDrop");
        getattr(process,"hltPatJetSoftDropAK8GenJetMatch").matched = cms.InputTag("hltAK8GenJets");
        getattr(process,"hltPatJetSoftDropAK8GenJetMatch").maxDeltaR = cms.double(0.8)

    getattr(process,"hltPatJetsAK8SoftDrop").jetSource = cms.InputTag("hltAK8PFJetsSoftDrop");
    getattr(process,"hltPatJetsAK8SoftDrop").jetCorrFactorsSource = cms.VInputTag("hltPatJetAK8SoftDropCorrFactorsProducer");

    if isMC:
        getattr(process,"hltPatJetsAK8SoftDrop").addGenJetMatch   = cms.bool(True);
        getattr(process,"hltPatJetsAK8SoftDrop").embedGenJetMatch = cms.bool(True);  
        getattr(process,"hltPatJetsAK8SoftDrop").genJetMatch       = cms.InputTag("hltPatJetSoftDropAK8GenJetMatch");
        getattr(process,"hltPatJetsAK8SoftDrop").addJetFlavourInfo    = cms.bool(False);
        getattr(process,"hltPatJetsAK8SoftDrop").getJetMCFlavour      = cms.bool(False);
        getattr(process,"hltPatJetsAK8SoftDrop").JetFlavourInfoSource = cms.InputTag("");
    else:
        getattr(process,"hltPatJetsAK8SoftDrop").addGenJetMatch   = cms.bool(False);
        getattr(process,"hltPatJetsAK8SoftDrop").embedGenJetMatch = cms.bool(False);  
        getattr(process,"hltPatJetsAK8SoftDrop").genJetMatch       = cms.InputTag("");
        getattr(process,"hltPatJetsAK8SoftDrop").addJetFlavourInfo    = cms.bool(False);
        getattr(process,"hltPatJetsAK8SoftDrop").getJetMCFlavour      = cms.bool(False);
        getattr(process,"hltPatJetsAK8SoftDrop").JetFlavourInfoSource = cms.InputTag("");
    getattr(process,"hltPatJetsAK8SoftDrop").addBTagInfo          = cms.bool(False);                                                                                                   
    getattr(process,"hltPatJetsAK8SoftDrop").addDiscriminators    = cms.bool(False);                                                                                                    
    getattr(process,"hltPatJetsAK8SoftDrop").discriminatorSources = cms.VInputTag();
    getattr(process,"hltPatJetsAK8SoftDrop").userData.userFloats.src = cms.VInputTag();

    ## soft-drop sub-jets
    if isMC:
        setattr(process,"hltPatJetAK8SoftDropSubJetsFlavourAssociation",getattr(process,"hltPatJetAK8FlavourAssociation").clone());
        getattr(process,"hltPatJetAK8SoftDropSubJetsFlavourAssociation").groomedJets = cms.InputTag("hltAK8PFJetsSoftDrop");
        getattr(process,"hltPatJetAK8SoftDropSubJetsFlavourAssociation").subjets = cms.InputTag("hltAK8PFJetsSoftDrop","SubJets");
  
    setattr(process,"hltNjettinessAK8PFSoftDropSubJets",getattr(process,"hltNjettinessAK8PFJets").clone());
    getattr(process,"hltNjettinessAK8PFSoftDropSubJets").src = cms.InputTag("hltAK8PFJetsSoftDrop","SubJets");

    setattr(process,"hltPatJetAK8SoftDropSubJetsCorrFactorsProducer",getattr(process,"hltPatJetAK8CorrFactorsProducer").clone());
    getattr(process,"hltPatJetAK8SoftDropSubJetsCorrFactorsProducer").src = cms.InputTag("hltAK8PFJetsSoftDrop","SubJets");
    getattr(process,"hltPatJetAK8SoftDropSubJetsCorrFactorsProducer").payload = cms.string('AK4PFHLT');

    if isMC:
        setattr(process,"hltPatJetSoftDropSubJetsAK8GenJetMatch",patJetGenJetMatch.clone());
        getattr(process,"hltPatJetSoftDropSubJetsAK8GenJetMatch").src = cms.InputTag("hltAK8PFJetsSoftDrop","SubJets");
        getattr(process,"hltPatJetSoftDropSubJetsAK8GenJetMatch").matched = cms.InputTag("hltAK8GenJetsSoftDrop","SubJets");
        getattr(process,"hltPatJetSoftDropSubJetsAK8GenJetMatch").maxDeltaR = cms.double(0.8)

    setattr(process,"hltPatJetsAK8SoftDropSubJets",getattr(process,"hltPatJetsAK8").clone());
    getattr(process,"hltPatJetsAK8SoftDropSubJets").jetSource = cms.InputTag("hltAK8PFJetsSoftDrop","SubJets");
    getattr(process,"hltPatJetsAK8SoftDropSubJets").jetCorrFactorsSource = cms.VInputTag(cms.InputTag("hltPatJetAK8SoftDropSubJetsCorrFactorsProducer"));

    if isMC:
        getattr(process,"hltPatJetsAK8SoftDropSubJets").JetFlavourInfoSource = cms.InputTag("hltPatJetAK8SoftDropSubJetsFlavourAssociation","SubJets");
        getattr(process,"hltPatJetsAK8SoftDropSubJets").genJetMatch = cms.InputTag("hltPatJetSoftDropSubJetsAK8GenJetMatch");

    getattr(process,"hltPatJetsAK8SoftDropSubJets").userData.userFloats.src = cms.VInputTag(
        cms.InputTag("hltNjettinessAK8PFSoftDropSubJets:tau1"),
        cms.InputTag("hltNjettinessAK8PFSoftDropSubJets:tau2"),
        cms.InputTag("hltNjettinessAK8PFSoftDropSubJets:tau3"),
        cms.InputTag("hltNjettinessAK8PFSoftDropSubJets:tau4")
    );
    getattr(process,"hltPatJetsAK8SoftDropSubJets").addBTagInfo          = cms.bool(False);
    getattr(process,"hltPatJetsAK8SoftDropSubJets").addDiscriminators    = cms.bool(False);
    getattr(process,"hltPatJetsAK8SoftDropSubJets").discriminatorSources = cms.VInputTag();
  
    ## Merge soft-drop and soft-drop subjets
    from PhysicsTools.PatAlgos.slimming.slimmedJets_cfi import slimmedJetsAK8
    setattr(process,"hltSlimmedJetsAK8SoftDropSubJets",slimmedJetsAK8.clone());  
    getattr(process,"hltSlimmedJetsAK8SoftDropSubJets").dropJetVars = cms.string('1');
    getattr(process,"hltSlimmedJetsAK8SoftDropSubJets").dropSpecific = cms.string('1');
    getattr(process,"hltSlimmedJetsAK8SoftDropSubJets").dropTagInfos = cms.string('1');
    getattr(process,"hltSlimmedJetsAK8SoftDropSubJets").dropTrackRefs = cms.string('1');
    getattr(process,"hltSlimmedJetsAK8SoftDropSubJets").packedPFCandidates = cms.InputTag("hltPackedPFCandidates");
    getattr(process,"hltSlimmedJetsAK8SoftDropSubJets").src = cms.InputTag("hltPatJetsAK8SoftDropSubJets");
        
    setattr(process,"hltSlimmedJetsAK8SoftDropPacked",
            cms.EDProducer("BoostedJetMerger",
                           jetSrc = cms.InputTag("hltPatJetsAK8SoftDrop"),
                           subjetSrc = cms.InputTag("hltSlimmedJetsAK8SoftDropSubJets")
                       )
    );
    
    ## Merge and slim the final jet collection
    setattr(process,"hltPackedPatJetsAK8",
            cms.EDProducer("JetSubstructurePacker",
                           algoLabels = cms.vstring('SoftDrop'),
                           algoTags = cms.VInputTag(cms.InputTag("hltSlimmedJetsAK8SoftDropPacked")),
                           distMax = cms.double(0.8),
                           fixDaughters = cms.bool(True),
                           packedPFCandidates = cms.InputTag("hltPackedPFCandidates"),
                        jetSrc = cms.InputTag("hltSelectedPatJetsAK8"))
    );
    
    from PhysicsTools.PatAlgos.slimming.slimmedJets_cfi import slimmedJetsAK8
    setattr(process,"hltSlimmedPackedJetsAK8",slimmedJetsAK8.clone());
    getattr(process,"hltSlimmedPackedJetsAK8").dropDaughters = cms.string('pt < '+str(options.jetPtMin));
    getattr(process,"hltSlimmedPackedJetsAK8").dropJetVars = cms.string('pt < '+str(options.jetPtMin));
    getattr(process,"hltSlimmedPackedJetsAK8").dropSpecific = cms.string('pt < '+str(options.jetPtMin));
    getattr(process,"hltSlimmedPackedJetsAK8").dropTagInfos = cms.string('pt < '+str(options.jetPtMin));
    getattr(process,"hltSlimmedPackedJetsAK8").dropTrackRefs = cms.string('1');
    getattr(process,"hltSlimmedPackedJetsAK8").rekeyDaughters = cms.string("0");
    getattr(process,"hltSlimmedPackedJetsAK8").src = cms.InputTag("hltPackedPatJetsAK8");
    getattr(process,"hltSlimmedPackedJetsAK8").packedPFCandidates = cms.InputTag("hltPackedPFCandidates");
   
    ## Final jet collection
    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    setattr(process,"hltSlimmedJetsAK8",updatedPatJets.clone());
    getattr(process,"hltSlimmedJetsAK8").jetSource = cms.InputTag("hltSlimmedPackedJetsAK8");
    getattr(process,"hltSlimmedJetsAK8").addJetCorrFactors = cms.bool(False);
    getattr(process,"hltSlimmedJetsAK8").addTagInfos = cms.bool(False);
    getattr(process,"hltSlimmedJetsAK8").addBTagInfo = cms.bool(True);
    getattr(process,"hltSlimmedJetsAK8").addDiscriminators = cms.bool(True);
   
    ## convert to slimmed jets without dropping info
    if(hasattr(process,pathName)):
        if isMC:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetAK8FlavourAssociation")+process.hltBoolEnd);
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetAK8GenJetMatch")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltNjettinessAK8PFJets")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetAK8CorrFactorsProducer")+process.hltBoolEnd);
        if addLastPNETEvaluation:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltParticleNetONNXLastJetTagsAK8")+process.hltBoolEnd);                 
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetsAK8")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSelectedPatJetsAK8")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8PFJetsConstituents")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltAK8PFJetsSoftDrop")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetAK8SoftDropCorrFactorsProducer")+process.hltBoolEnd);
        if isMC:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetSoftDropAK8GenJetMatch")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetsAK8SoftDrop")+process.hltBoolEnd);
        if isMC:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetAK8SoftDropSubJetsFlavourAssociation")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltNjettinessAK8PFSoftDropSubJets")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetAK8SoftDropSubJetsCorrFactorsProducer")+process.hltBoolEnd);
        if isMC:
            getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetSoftDropSubJetsAK8GenJetMatch")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPatJetsAK8SoftDropSubJets")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedJetsAK8SoftDropSubJets")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedJetsAK8SoftDropPacked")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltPackedPatJetsAK8")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedPackedJetsAK8")+process.hltBoolEnd);
        getattr(process,pathName).replace(process.hltBoolEnd,getattr(process,"hltSlimmedJetsAK8")+process.hltBoolEnd);

    if(hasattr(process,outputModuleName)):
        getattr(process,outputModuleName).outputCommands.append("keep *pat*Jets*_*"+"hltSlimmedJetsAK8_*_*"+options.hltProcessName+"*");
        getattr(process,outputModuleName).outputCommands.append("keep *pat*Jets*_*"+"hltSlimmedJetsAK8SoftDropPacked_*_*"+options.hltProcessName+"*");

    return process;
    


