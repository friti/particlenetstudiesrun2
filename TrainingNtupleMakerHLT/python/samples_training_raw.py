def AddDYSamples(samples):

    samples['DYJetsToLL'] = [
        '/DYTo2L_MLL-50_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-RnD_126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddHiggsSamples(samples):
    samples['GluGluHToBB_M-125'] = [
        '/GluGluHToBB_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['VBFHToBB_M-125'] = [
        '/VBFHToBB_M-125_dipoleRecoilOn_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGluHToTauTau_M-125'] = [
        '/GluGluHToTauTau_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['VBFHToTauTau_M-125'] = [
        '/VBFHToTauTau_M125_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddQCDSamples(samples):

    samples['QCD_Pt_30to50'] = [
        '/QCD_PT-30to50_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_50to80'] = [
        '/QCD_PT-50to80_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_80to120'] = [
        '/QCD_PT-80to120_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_120to170'] = [
        '/QCD_PT-120to170_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_170to300'] = [
        '/QCD_PT-170to300_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_300to470'] = [
        '/QCD_PT-300to470_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_470to600'] = [
        '/QCD_PT-470to600_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['QCD_Pt_600toInf'] = [
        '/QCD_PT-600toInf_TuneCP5_13p6TeV_pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddTTbarSamples(samples):

    samples['TTbar'] = [
        '/TT_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

def AddHHSamples(samples):
    
    samples['GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p35'] = [        
        '/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-1p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-3p00'] = [
        '/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-3p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-m2p00'] = [
        '/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-m2p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
    samples['GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00'] = [
        '/GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Winter23Digi-RnD_126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
    samples['VBFHHto2B2Tau_CV-1_C2V-1_C3-1'] = [
        '/VBFHHto2B2Tau_CV-1_C2V-1_C3-1_TuneCP5_13p6TeV_madgraph-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]

    samples['VBFHHto2B2Tau_CV-1_C2V-1_C3-2'] = [
        '/VBFHHto2B2Tau_CV-1_C2V-1_C3-2_TuneCP5_13p6TeV_madgraph-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
    
    samples['VBFHHto2B2Tau_CV-1_C2V-2_C3-1'] = [
        '/VBFHHto2B2Tau_CV-1_C2V-2_C3-1_TuneCP5_13p6TeV_madgraph-pythia8/Run3Winter23Digi-126X_mcRun3_2023_forPU65_v1-v2/GEN-SIM-RAW',
        ['nThreads=4','datasetType=MC126X','filterOnL1Seeds=False','saveL1Objects=True','dumpOnlyJetMatchedToGen=True','useSQLiteFile=True'],
        'EventAwareLumiBased',
        5000,
        '',
        -1
    ]
        
def AddAllSamples(samples):
    AddDYSamples(samples)
    AddHiggsSamples(samples)
    AddQCDSamples(samples)
    AddTTbarSamples(samples)
    AddHHSamples(samples)
