import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register(
    'useLocalInputFile', False, VarParsing.multiplicity.singleton,VarParsing.varType.bool,
    "use local input files");

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'datasetType',"MC126X",VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'identifier of the dataset that needs to be processed');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'hltProcessName',"HLTX",VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'name of the hlt process to be considered');

options.register (
    'filterEventsInTheDumper',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True events without an ak8 jet passing selection are rejected/filtered out');

options.register (
    'saveL1Objects',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True, L1 objects are saved --> only valid for private miniAOD');

options.register (
    'saveLHEObjects',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True, LHE objects are saved --> turn-on only for simulated samples coming from ME');

options.register (
    'muonPtMin',15.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',15.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtAK8Min',180.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for AK8 jets in ntuples');

options.register (
    'jetPtAK4Min',25.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for AK4 jets in ntuples');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets in ntuples');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for offline and online jets');

options.register (
    'dumpOnlyJetMatchedToGen', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'dRJetGenMatch',0.8,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets');


options.parseArguments()

# Define the CMSSW process
process = cms.Process("DnnTree")

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

if options.useLocalInputFile:
    file_list = ["file:"+s for s in options.inputFiles]
    process.source.fileNames = cms.untracked.vstring(file_list)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

isMC = False;
from Configuration.AlCa.GlobalTag import GlobalTag
if options.datasetType == "MC126X":
    process.GlobalTag = GlobalTag(process.GlobalTag, '126X_mcRun3_2023_forPU65_v4', '')
    isMC=True;
elif options.datasetType == "MC124X":
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic_postEE', '')
    isMC=True;
elif options.datasetType == "Data2022" or options.datasetType == "ZeroBias2022":
    process.GlobalTag = GlobalTag(process.GlobalTag, '130X_dataRun3_HLT_v2','')
    isMC=False;
else:
    sys.exit("Dataset type not known, abort");

## deep tau evaluation                                                                                                                                                                                
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.                                                                                                                           
tauIdEmbedder.runTauID()

#### Final dumper
process.dnntree = cms.EDAnalyzer('TrainingNtupleMakerAK8',
        #### General flags
        xsec              = cms.double(options.xsec),
        isMC              = cms.bool(isMC),
        saveLHEObjects    = cms.bool(options.saveLHEObjects),
        dumpOnlyJetMatchedToGen = cms.bool(options.dumpOnlyJetMatchedToGen),
        filterEvents      = cms.bool(options.filterEventsInTheDumper),
        ### Object selection (applied on miniAOD, HLT and Gen objects for jets)
        muonPtMin         = cms.double(options.muonPtMin),
        muonEtaMax        = cms.double(2.4),
        electronPtMin     = cms.double(options.electronPtMin),
        electronEtaMax    = cms.double(2.5),
        tauPtMin          = cms.double(options.tauPtMin),
        tauEtaMax         = cms.double(2.5),
        jetPtAK4Min       = cms.double(options.jetPtAK4Min),
        jetPtAK8Min       = cms.double(options.jetPtAK8Min),
        jetEtaMax         = cms.double(options.jetEtaMax),
        jetEtaMin         = cms.double(options.jetEtaMin),
        dRJetGenMatch     = cms.double(options.dRJetGenMatch),
        ### Simulation quantities from miniAOD
        pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
        genEventInfo      = cms.InputTag("generator"),
        genParticles      = cms.InputTag("prunedGenParticles"),
        lheInfo           = cms.InputTag("externalLHEProducer"),
        ### miniAOD objects
        filterResults     = cms.InputTag("TriggerResults","", "PAT"),
        triggerResults    = cms.InputTag("TriggerResults","", options.hltProcessName),
        triggerL1Algos    = cms.InputTag("gtStage2Digis"),         
        stageL1Trigger    = cms.uint32(2),
        rho               = cms.InputTag("fixedGridRhoFastjetAll"),
        pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
        sVertices         = cms.InputTag("slimmedSecondaryVertices"),
        muons             = cms.InputTag("slimmedMuons"),
        electrons         = cms.InputTag("slimmedElectrons"),
        taus              = cms.InputTag("slimmedTaus"),
        met               = cms.InputTag("slimmedMETs"),
        jetsAK4           = cms.InputTag("slimmedJetsPuppi"),
        jetsAK8           = cms.InputTag("slimmedJetsAK8"),
        subJetCollectionName = cms.string("SoftDropPuppi"),                                 
        ### HLT objects
        hltJetsAK4        = cms.InputTag("hltAK4PFJetsCorrected"),
        hltJetsAK8        = cms.InputTag("hltSlimmedJetsAK8"),
        subJetCollectionNameHLT = cms.string("SoftDrop"),                                 
        hltPVertices      = cms.InputTag("hltOfflineSlimmedPrimaryVertices"),
        hltSVertices      = cms.InputTag("hltSlimmedSecondaryVertices"),
        hltRho            = cms.InputTag("hltFixedGridRhoFastjetAll"),
        ### L1 objects                                                                                                                                          
        saveL1Objects     = cms.bool(options.saveL1Objects),
        triggerL1Jets     = cms.InputTag("caloStage2Digis","Jet"),
        triggerL1EtSum    = cms.InputTag("caloStage2Digis","EtSum"),
        triggerL1Muons    = cms.InputTag("gmtStage2Digis","Muon"),
        ### GEN jets
        genJetsAK8        = cms.InputTag("hltPackedGenJetsAK8"),                                    
        genJets           = cms.InputTag("slimmedGenJets"),                                    
        genSubJetCollectionName = cms.string("SoftDrop"),
)

process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

process.dnnTreePath = cms.Path(process.dnntree,process.edTask)
