#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Candidate/interface/CandidateFwd.h"
#include "FWCore/Utilities/interface/transform.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Common/interface/RefToPtr.h"

//
// class decleration
//

class GenJetSubstructurePacker : public edm::stream::EDProducer<> {
public:
  explicit GenJetSubstructurePacker(const edm::ParameterSet&);
  ~GenJetSubstructurePacker() override;

private:
  void produce(edm::Event&, const edm::EventSetup&) override;

  // ----------member data ---------------------------
  // data labels
  float distMax_;
  edm::EDGetTokenT<edm::View<pat::Jet>> jetToken_;
  std::vector<std::string> algoLabels_;
  std::vector<edm::InputTag> algoTags_;
  std::vector<edm::EDGetTokenT<edm::View<pat::Jet>>> algoTokens_;
  bool fixDaughters_;
  edm::EDGetTokenT<edm::Association<pat::PackedGenParticleCollection>> pf2pc_;
};

GenJetSubstructurePacker::GenJetSubstructurePacker(const edm::ParameterSet& iConfig): 
  distMax_(iConfig.getParameter<double>("distMax")),
  jetToken_(consumes<edm::View<pat::Jet>>(iConfig.getParameter<edm::InputTag>("jetSrc"))),
  algoLabels_(iConfig.getParameter<std::vector<std::string>>("algoLabels")),
  algoTags_(iConfig.getParameter<std::vector<edm::InputTag>>("algoTags")),
  fixDaughters_(iConfig.getParameter<bool>("fixDaughters")) {
  algoTokens_ = edm::vector_transform(algoTags_, [this](edm::InputTag const& tag) { return consumes<edm::View<pat::Jet>>(tag); });
  if (fixDaughters_) {
    pf2pc_ = consumes<edm::Association<pat::PackedGenParticleCollection>>(iConfig.getParameter<edm::InputTag>("packedGenParticles"));
  }
  //register products
  produces<std::vector<pat::Jet>>();
}

GenJetSubstructurePacker::~GenJetSubstructurePacker() {}

// ------------ method called to produce the data  ------------
void GenJetSubstructurePacker::produce(edm::Event& iEvent, const edm::EventSetup&) {
 
  auto outputs = std::make_unique<std::vector<pat::Jet>>();
  
  edm::Handle<edm::View<pat::Jet>> jetHandle;
  std::vector<edm::Handle<edm::View<pat::Jet>>> algoHandles;
  
  edm::Handle<edm::Association<pat::PackedGenParticleCollection>> pf2pc;
  if (fixDaughters_) {
    iEvent.getByToken(pf2pc_, pf2pc);
  }

  iEvent.getByToken(jetToken_, jetHandle);
  algoHandles.resize(algoTags_.size());
  
  for (size_t i = 0; i < algoTags_.size(); ++i) {
    iEvent.getByToken(algoTokens_[i], algoHandles[i]);
  }

  // Loop over the input jets that will be modified.
  for (auto const& ijet : *jetHandle) {
    // Copy the jet.
    outputs->push_back(ijet);
    // Loop over the substructure collections
    unsigned int index = 0;

    for (auto const& ialgoHandle : algoHandles) {
      std::vector<edm::Ptr<pat::Jet>> nextSubjets;
      float dRMin = distMax_;
      for (auto const& jjet : *ialgoHandle) {
        if (reco::deltaR(ijet, jjet) < dRMin) {
          for (auto const& userfloatstr : jjet.userFloatNames()) {
            outputs->back().addUserFloat(userfloatstr, jjet.userFloat(userfloatstr));
          }
          for (auto const& userintstr : jjet.userIntNames()) {
            outputs->back().addUserInt(userintstr, jjet.userInt(userintstr));
          }
          for (auto const& usercandstr : jjet.userCandNames()) {
            outputs->back().addUserCand(usercandstr, jjet.userCand(usercandstr));
          }
          for (size_t ida = 0; ida < jjet.numberOfDaughters(); ++ida) {
	    reco::CandidatePtr candPtr = jjet.daughterPtr(ida);
            nextSubjets.push_back(edm::Ptr<pat::Jet>(candPtr));
          }
          break;
        }
      }
      outputs->back().addSubjets(nextSubjets, algoLabels_[index]);
      ++index;
    }

    // fix daughters
    if (fixDaughters_) {
      std::vector<reco::CandidatePtr> daughtersInSubjets;
      std::vector<reco::CandidatePtr> daughtersNew;
      const std::vector<reco::CandidatePtr>& jdausPF = outputs->back().daughterPtrVector();
      std::vector<reco::CandidatePtr> jdaus;
      jdaus.reserve(jdausPF.size());
      // Convert the daughters to packed candidates. This is easier than ref-navigating through PUPPI or CHS to particleFlow.
      for (auto const& jdau : jdausPF) {
        jdaus.push_back(edm::refToPtr((*pf2pc)[jdau]));
      }

      for (const edm::Ptr<pat::Jet>& subjet : outputs->back().subjets()) {
        const std::vector<reco::CandidatePtr>& sjdaus = subjet->daughterPtrVector();
        // check that the subjet does not contain any extra constituents not contained in the jet
        bool skipSubjet = false;
        for (const reco::CandidatePtr& dau : sjdaus) {
          if (std::find(jdaus.begin(), jdaus.end(), dau) == jdaus.end()) {
            skipSubjet = true;
            break;
          }
        }
        if (skipSubjet)
          continue;

        daughtersInSubjets.insert(daughtersInSubjets.end(), sjdaus.begin(), sjdaus.end());
        daughtersNew.push_back(reco::CandidatePtr(subjet));
      }
      for (const reco::CandidatePtr& dau : jdaus) {
        if (std::find(daughtersInSubjets.begin(), daughtersInSubjets.end(), dau) == daughtersInSubjets.end()) {
          daughtersNew.push_back(dau);
        }
      }
      outputs->back().clearDaughters();
      for (const auto& dau : daughtersNew)
        outputs->back().addDaughter(dau);
    }
  }
  
  iEvent.put(std::move(outputs));
}

//define this as a plug-in
DEFINE_FWK_MODULE(GenJetSubstructurePacker);
