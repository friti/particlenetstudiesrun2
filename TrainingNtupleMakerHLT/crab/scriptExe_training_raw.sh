BASE=$PWD

if [ -f "CMSRunAnalysis.tar.gz" ] 
then
    echo "tar -xf CMSRunAnalysis.tar.gz"
    tar -xf CMSRunAnalysis.tar.gz
fi 

if [ -f "run_and_lumis.tar.gz" ] 
then
    echo "tar -xf run_and_lumis.tar.gz"
    tar -xf run_and_lumis.tar.gz
fi

if [ -f "input_files.tar.gz" ] 
then
    echo "tar -xf input_files.tar.gz"
    tar -xf input_files.tar.gz
fi

cd $CMSSW_BASE/src
eval `scram runtime -sh`
cd $BASE

echo "PATH = "$PATH
echo "PYTHON3PATH = "$PYTHON3PATH
echo "LD_LIBRARY_PATH = "$LD_LIBRARY_PATH
echo "CMSSW BASE = "$CMSSW_BASE
echo "CMSSW RELEASE BASE = "$CMSSW_RELEASE_BASE
echo "CMSSW DATA PATH = "$CMSSW_DATA_PATH
echo "CMSSW SEARCH PATH = "$CMSSW_SEARCH_PATH

echo "cmsRun -e -j FrameworkJobReport.xml customizedHLTMenu.py outputName=hltMenu_step.root "$2" inputFileListFile=job_input_file_list_"$1".txt jsonFile=job_lumis_"$1".json "$3" "$4" "$7
cmsRun -e -j FrameworkJobReport.xml customizedHLTMenu.py outputName=hltMenu_step.root $2 inputFileListFile=job_input_file_list_$1.txt jsonFile=job_lumis_$1.json $3 $4 $7

echo "cmsRun -e -j FrameworkJobReport.xml miniaod_step_mc_cfg.py "$2" inputFiles=hltMenu_step.root outputName=miniAOD_step.root useLocalInputFile=True"
cmsRun -e -j FrameworkJobReport.xml miniaod_step_mc_cfg.py $2 inputFiles=hltMenu_step.root outputName=miniAOD_step.root useLocalInputFile=True
rm hltMenu_step.root

echo "cmsRun -e -j FrameworkJobReport.xml makeTrainingNtuple_cfg.py "$2" "$3" "$5" "$6" useLocalInputFile=True inputFiles=miniAOD_step.root"
cmsRun -e -j FrameworkJobReport.xml makeTrainingNtuple_cfg.py $2 $3 $5 $6 useLocalInputFile=True inputFiles=miniAOD_step.root
