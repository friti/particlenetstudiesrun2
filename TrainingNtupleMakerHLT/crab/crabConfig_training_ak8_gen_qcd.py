import os
from CRABClient.UserUtilities import config

### General options
config = config()
config.General.transferOutputs = True
config.General.transferLogs = False
### Job type setting
basepath = os.getenv('CMSSW_BASE')+'/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT'
config.JobType.pluginName  = 'PrivateMC'
config.JobType.psetName    = basepath+'/python/mc_generation_126X/gen_sim_step_qcd.py'
config.JobType.allowUndistributedCMSSW = True
config.JobType.sendPythonFolder = True
config.JobType.disableAutomaticOutputCollection = True
config.JobType.outputFiles = []
config.JobType.numCores    = 4
config.JobType.maxMemoryMB = 5500
config.JobType.pyCfgParams = ['nThreads='+str(config.JobType.numCores)]
### custom script to be executed
config.JobType.scriptExe   = 'scriptExe_training_ak8_gen_qcd.sh'
config.JobType.inputFiles  = ['scriptExe_training_ak8_gen_qcd.sh',basepath+'/python/mc_generation_126X/gen_sim_step_qcd.py',basepath+'/python/mc_generation_126X/digi_raw_step.py',basepath+'/python/customizedHLTMenu_ak8.py',basepath+'/python/miniaod_step_mc_cfg.py',basepath+'/test/makeTrainingNtuple_ak8_cfg.py',basepath+'/python/mc_generation_126X/pileup.txt']
## Data
config.Data.splitting   = 'EventBased'
config.Data.outLFNDirBase = '/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetHLT/NtupleTrainingAK8'
config.Site.storageSite   = 'T2_CH_CERN'

from ParticleNetStudiesRun2.TrainingNtupleMakerHLT.samples_training_ak8_gen import AddAllSamples
samples = {};
AddAllSamples(samples);
dset = os.getcwd().replace(os.path.dirname(os.getcwd())+'/','')
config.Data.outputPrimaryDataset = samples[dset][0]
config.JobType.scriptArgs = samples[dset][1]
config.Data.unitsPerJob   = samples[dset][2] 
config.Data.totalUnits    = samples[dset][3]
config.JobType.outputFiles.append(samples[dset][4])
config.JobType.scriptArgs.append('nEvents='+str(config.Data.unitsPerJob))
config.JobType.scriptArgs.append('outputName='+str(samples[dset][4]))
print ('Submitting jobs with script args: '+' '.join(config.JobType.scriptArgs)+' splitting: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' totalUnits: '+str(config.Data.totalUnits))
