BASE=$PWD
BASE_CMSSW=$CMSSW_BASE/src

## Run CMSSW 126X the GEN + SIM + DIGIRAW steps
source /cvmfs/cms.cern.ch/cmsset_default.sh
scram p CMSSW CMSSW_12_6_3
cd CMSSW_12_6_3/src
eval `scram runtime -sh`
cd $BASE

nevents="$(echo ${16} | cut -d'=' -f2)"
nloops="$(echo ${15} | cut -d'=' -f2)"
nEventPerLoop=$((nevents/nloops))
nEventPerLoop=${nEventPerLoop%.*}
echo "Produce nevents="$nevents" in nsteps "$nloops" of n="$nEventPerLoop" events with a different mass"

filelist=""
for ((i=1;i<=$nloops;i++)); 
do    
    echo "cmsRun -e -j FrameworkJobReport.xml gen_sim_step_xtohh.py jobNum="$1" nEvents="$nEventPerLoop" "$2" outputName=genSimStep_"$i".root "$9" "${10}" "${11}" "+${12}+" "${13}" "${14}" generationStep="$i" jobEvents="$nevents
    cmsRun -e -j FrameworkJobReport.xml gen_sim_step_xtohh.py jobNum=$1 nEvents=$nEventPerLoop $2 outputName=genSimStep_$i.root $9 ${10} ${11} ${12} ${13} ${14} generationStep=$i jobEvents=$nevents
    filelist+="file:genSimStep_"$i".root,"
done
filelist=${filelist::-1}

echo "cmsRun -e -j FrameworkJobReport.xml digi_raw_step.py "$2" inputFiles="$filelist" outputName=digirawStep.root"
cmsRun -e -j FrameworkJobReport.xml digi_raw_step.py $2 inputFiles=$filelist outputName=digirawStep.root
rm genSimStep*.root

## Run CMSSW 13X the HLT+RECO+MINIAOD+NTUPLES
cd $BASE_CMSSW
eval `scram runtime -sh`
cd $BASE

echo "cmsRun -e -j FrameworkJobReport.xml customizedHLTMenu_ak8.py outputName=hltMenu_step.root "$2" "$3" "$4" "$7" useLocalInputFile=True inputFiles="digirawStep.root
cmsRun -e -j FrameworkJobReport.xml customizedHLTMenu_ak8.py outputName=hltMenu_step.root $2 $3 $4 $7 useLocalInputFile=True inputFiles=digirawStep.root
rm digirawStep.root

echo "cmsRun -e -j FrameworkJobReport.xml miniaod_step_mc_cfg.py "$2" inputFiles=hltMenu_step.root useLocalInputFile=True outputName=miniAOD_step.root"
cmsRun -e -j FrameworkJobReport.xml miniaod_step_mc_cfg.py $2 inputFiles=hltMenu_step.root useLocalInputFile=True outputName=miniAOD_step.root
rm hltMenu_step.root

echo "cmsRun -e -j FrameworkJobReport.xml makeTrainingNtuple_ak8_cfg.py "$2" "$3" "$5" "$6" useLocalInputFile=True inputFiles=miniAOD_step.root "$8" "${17}
cmsRun -e -j FrameworkJobReport.xml makeTrainingNtuple_ak8_cfg.py $2 $3 $5 $6 useLocalInputFile=True inputFiles=miniAOD_step.root $8 ${17}
