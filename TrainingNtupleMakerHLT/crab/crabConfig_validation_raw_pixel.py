import os
from CRABClient.UserUtilities import config

### General options
config = config()
config.General.transferOutputs = True
config.General.transferLogs = False
### Job type setting
basepath = os.getenv('CMSSW_BASE')+'/src/ParticleNetStudiesRun2/TrainingNtupleMakerHLT'
config.JobType.pluginName  = 'Analysis'
config.JobType.psetName    = basepath+'/test/makeValidationNtuple_cfg.py'
config.JobType.allowUndistributedCMSSW = True
config.JobType.numCores    = 4
config.JobType.maxMemoryMB = 5500
config.JobType.pyCfgParams = ['nThreads='+str(config.JobType.numCores)]
### custom script to be executed
config.JobType.scriptExe   = 'scriptExe_validation_raw.sh'
config.JobType.inputFiles  = ['scriptExe_validation_raw.sh',basepath+'/python/customizedHLTMenu.py',basepath+'/python/miniaod_step_mc_cfg.py',basepath+'/test/makeValidationNtuple_cfg.py']
config.JobType.sendPythonFolder = True
## data and outout storage
config.Data.inputDBS      = 'global'
config.Data.outLFNDirBase = '/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetHLT/NtupleValidationAK4_pixel'
config.Site.storageSite   = 'T2_CH_CERN'

## make list of all samples
from ParticleNetStudiesRun2.TrainingNtupleMakerHLT.samples_validation_raw_pixel import AddAllSamples
samples = {};
AddAllSamples(samples);

## consider only the same that matches the name of the current directory created by the main script submitting this workflow
dset = os.getcwd().replace(os.path.dirname(os.getcwd())+'/','')
config.Data.partialDataset = True
config.Data.allowNonValidInputDataset = True
config.Data.inputDataset   = samples[dset][0]
config.JobType.scriptArgs  = samples[dset][1]
config.Data.splitting      = samples[dset][2]
config.Data.unitsPerJob    = samples[dset][3]
config.Data.lumiMask       = samples[dset][4] if samples[dset][4]  else ''
config.Data.totalUnits     = samples[dset][5] if samples[dset][5] > 0 else -1
if samples[dset][4]:
    print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' lumiMask: '+config.Data.lumiMask+' totalUnits: '+str(config.Data.totalUnits))
else:
    print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' totalUnits: '+str(config.Data.totalUnits))
