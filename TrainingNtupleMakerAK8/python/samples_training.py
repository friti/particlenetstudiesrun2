def AddTTbarSamples(samples):

    samples['TTTo2L2Nu'] = [
        '/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=86.46','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        350000,
        '',
        50000000
    ]

def AddWJetSamples(samples):

    samples['WJetsToLNu_Pt-100To250'] = [
        '/WJetsToLNu_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=7.672e+02','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['WJetsToLNu_Pt-250To400'] = [
        '/WJetsToLNu_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=2.774e+01','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['WJetsToLNu_Pt-400To600'] = [
        '/WJetsToLNu_Pt-400To600_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.504e+00','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['WJetsToLNu_Pt-600ToInf'] = [
        '/WJetsToLNu_Pt-600ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=5.387e-01','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

def AddQCDHTPythiaSamples(samples):

    samples['QCD_HT_pythia_300to500'] = [
        '/QCD_HT300to500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.234e+05','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_pythia_500to700'] = [
        '/QCD_HT500to700_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.004e+04','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_pythia_700to1000'] = [
        '/QCD_HT700to1000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.398e+03','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_pythia_1000to1500'] = [
        '/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.114e+03','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_pythia_1500to2000'] = [
        '/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.083e+02','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_pythia_2000toInf'] = [
        '/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.198e+01','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

def AddQCDHTBEnrichedSamples(samples):

    samples['QCD_HT_bEnriched_300to500'] = [
        '/QCD_HT300to500_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=2.739e+04','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_bEnriched_500to700'] = [
        '/QCD_HT500to700_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.014e+03','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_bEnriched_700to1000'] = [
        '/QCD_HT700to1000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=7.183e+02','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_bEnriched_1000to1500'] = [
        '/QCD_HT1000to1500_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.384e+02','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_bEnriched_1500to2000'] = [
        '/QCD_HT1500to2000_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.453e+01','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]

    samples['QCD_HT_bEnriched_2000toInf'] = [
        '/QCD_HT2000toInf_BGenFilter_TuneCP5_13TeV-madgraph-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=3.137e+00','isMC=True','jetPtMin=200','jetEtaMax=2.5','jetSoftDropMassMin=10','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        250000,
        '',
        25000000
    ]


def AddXtoHHSamples(samples):

    samples['XtoHH_pythia8_13TeV_part1'] = [
        '/XtoHH_pythia8_13TeV_part1/rgerosa-RunIISummer20UL18MiniAODv2_106X_upgrade2018_realistic-MINIAODSIM-a6118159aa572f9866b285165dc71369/USER',
        ['xsec=1','isMC=True','jetPtMin=200','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        50000,
        '',
        -1,
        'phys03'
    ]

    samples['XtoHH_pythia8_13TeV_part2'] = [
        '/XtoHH_pythia8_13TeV_part2/rgerosa-RunIISummer20UL18MiniAODv2_106X_upgrade2018_realistic-MINIAODSIM-a6118159aa572f9866b285165dc71369/USER',
        ['xsec=1','isMC=True','jetPtMin=200','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        50000,
        '',
        -1,
        'phys03'
    ]

    samples['XtoHH_pythia8_13TeV_part3'] = [
        '/XtoHH_pythia8_13TeV_part3/rgerosa-RunIISummer20UL18MiniAODv2_106X_upgrade2018_realistic-MINIAODSIM-a6118159aa572f9866b285165dc71369/USER',
        ['xsec=1','isMC=True','jetPtMin=200','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        50000,
        '',
        -1,
        'phys03'
    ]

    samples['XtoHH_pythia8_13TeV_part4'] = [
        '/XtoHH_pythia8_13TeV_part4/rgerosa-RunIISummer20UL18MiniAODv2_106X_upgrade2018_realistic-MINIAODSIM-a6118159aa572f9866b285165dc71369/USER',
        ['xsec=1','isMC=True','jetPtMin=200','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        50000,
        '',
        -1,
        'phys03'
    ]

    samples['XtoHH_pythia8_13TeV_part5'] = [
        '/XtoHH_pythia8_13TeV_part5/rgerosa-RunIISummer20UL18MiniAODv2_106X_upgrade2018_realistic-MINIAODSIM-a6118159aa572f9866b285165dc71369/USER',
        ['xsec=1','isMC=True','jetPtMin=200','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','evaluatePNETTraining=True','evaluateParTTraining=False'],
        'EventAwareLumiBased',
        50000,
        '',
        -1,
        'phys03'
    ]

def AddAllSamples(samples):
    AddTTbarSamples(samples)
    AddWJetSamples(samples)
    AddQCDHTPythiaSamples(samples)
    AddQCDHTBEnrichedSamples(samples)
    AddXtoHHSamples(samples)
    
