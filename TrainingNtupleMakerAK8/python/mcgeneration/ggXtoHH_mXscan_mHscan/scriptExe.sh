BASE=$PWD
BASE_CMSSW=$CMSSW_BASE/src

## Run CMSSW 126X the GEN + SIM + DIGIRAW steps
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc700
scram p CMSSW_10_6_37
cd CMSSW_10_6_37/src
eval `scram runtime -sh`
cd $BASE

nevents="$(echo ${9} | cut -d'=' -f2)"
nloops="$(echo ${8} | cut -d'=' -f2)"
nEventPerLoop=$((nevents/nloops))
nEventPerLoop=${nEventPerLoop%.*}
echo "Produce nevents="$nevents" in nsteps of "$nloops" each of n="$nEventPerLoop" events with a different mX and mH masses"

filelist=""
for ((i=1;i<=$nloops;i++)); 
do    
    echo "cmsRun -e -j FrameworkJobReport.xml gen_step.py jobNum="$1" nEvents="$nEventPerLoop" outputName=genStep_"$i".root "$2" "$3" "$4" "$5" "$6" "$7" generationStep="$i" jobEvents="$nevents" "${10}
    cmsRun -e -j FrameworkJobReport.xml gen_step.py jobNum=$1 nEvents=$nEventPerLoop outputName=genStep_$i.root $2 $3 $4 $5 $6 $7 generationStep=$i jobEvents=$nevents ${10}
    echo "cmsRun -e -j FrameworkJobReport.xml sim_step.py.py "${10}" inputFiles=file:genStep_"$i".root outputName=simStep_"$i".root"
    cmsRun -e -j FrameworkJobReport.xml sim_step.py ${10} inputFiles=file:genStep_$i.root outputName=simStep_$i.root
    filelist+="file:simStep_"$i".root,"
done
filelist=${filelist::-1}

echo "cmsRun -e -j FrameworkJobReport.xml digi_raw_step.py "${10}" inputFiles="$filelist" outputName=digirawStep.root"
cmsRun -e -j FrameworkJobReport.xml digi_raw_step.py ${10} inputFiles=$filelist outputName=digirawStep.root
rm simStep*.root

scram p CMSSW CMSSW_10_2_16_UL
cd CMSSW_10_2_16_UL/src
eval `scram runtime -sh`
cd ../../

echo "cmsRun -e -j FrameworkJobReport.xml hlt_step.py "${10}" inputFiles=file:digirawStep.root outputName=hltStep.root"
cmsRun -e -j FrameworkJobReport.xml hlt_step.py ${10} inputFiles=file:digirawStep.root outputName=hltStep.root
rm digirawStep.root

cd CMSSW_10_6_37/src
eval `scram runtime -sh`
cd $BASE

echo "cmsRun -e -j FrameworkJobReport.xml reco_step.py "${10}" inputFiles=file:hltStep.root outputName=recoStep.root"
cmsRun -e -j FrameworkJobReport.xml reco_step.py ${10} inputFiles=file:hltStep.root outputName=recoStep.root
rm hltStep.root

echo "cmsRun -e -j FrameworkJobReport.xml miniaod_step.py "${10}" inputFiles=file:recoStep.root "${11}
cmsRun -e -j FrameworkJobReport.xml miniaod_step.py ${10} inputFiles=file:recoStep.root  ${11}
rm recoStep.root
