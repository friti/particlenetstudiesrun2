import FWCore.ParameterSet.Config as cms
from Configuration.Eras.Era_Run2_2018_cff import Run2_2018
from FWCore.ParameterSet.VarParsing import VarParsing
import os

options = VarParsing ('analysis')
options.register('jobNum', 0, VarParsing.multiplicity.singleton,VarParsing.varType.int,"job id number used as run-id")
options.register('jobEvents', 0, VarParsing.multiplicity.singleton,VarParsing.varType.int,"total number of events produced in the crab job")
options.register('nEvents',  0, VarParsing.multiplicity.singleton,VarParsing.varType.int,"number of events to simulate")
options.register('generationStep', 1, VarParsing.multiplicity.singleton,VarParsing.varType.int,"which step in the generation job, used for lumi-id")
options.register('nThreads', 1, VarParsing.multiplicity.singleton,VarParsing.varType.int,"number of threads")
options.register('outputName', "genStep.root", VarParsing.multiplicity.singleton,VarParsing.varType.string,"name of the output file")
options.register('resonanceMassMin', 500., VarParsing.multiplicity.singleton,VarParsing.varType.float,"resonanceMassMin")
options.register('resonanceMassMax', 6500., VarParsing.multiplicity.singleton,VarParsing.varType.float,"resonanceMassMin")
options.register('resonanceMassStepSize', 50, VarParsing.multiplicity.singleton,VarParsing.varType.int,"resonanceMassStepSize")
options.register('higgsMassMin', 30., VarParsing.multiplicity.singleton,VarParsing.varType.float,"higgsMassMin")
options.register('higgsMassMax', 220., VarParsing.multiplicity.singleton,VarParsing.varType.float,"higgsMassMax")
options.register('higgsMassStepSize', 1, VarParsing.multiplicity.singleton,VarParsing.varType.int,"higgsMassStepSize")
options.parseArguments()


process = cms.Process('GEN',Run2_2018)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.GeometrySimDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.Generator_cff')
process.load('IOMC.EventVertexGenerators.VtxSmearedRealistic25ns13TeVEarly2018Collision_cfi')
process.load('GeneratorInterface.Core.genFilterSummary_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(options.nEvents)
)

# Input source
process.source = cms.Source("EmptySource",
    firstRun = cms.untracked.uint32(options.jobNum),
    numberEventsInRun = cms.untracked.uint32(options.jobEvents),
    firstLuminosityBlock  = cms.untracked.uint32(options.generationStep),
    numberEventsInLuminosityBlock = cms.untracked.uint32(options.nEvents),
    firstEvent = cms.untracked.uint32((options.generationStep-1)*options.nEvents+1) 
)


process.options = cms.untracked.PSet()
process.options.numberOfThreads = cms.untracked.uint32(options.nThreads)
process.options.numberOfStreams = cms.untracked.uint32(options.nThreads)
process.options.numberOfConcurrentLuminosityBlocks =  cms.untracked.uint32(options.nThreads)
process.options.wantSummary = cms.untracked.bool(True)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('gen_step.py nevts:'+str(options.nEvents)),
    name = cms.untracked.string('\\$Source$'),
    version = cms.untracked.string('\\$Revision$')
)

# Output definition
process.RAWSIMoutput = cms.OutputModule("PoolOutputModule",
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('generation_step')
    ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(1),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN'),
        filterName = cms.untracked.string('')
    ),
    eventAutoFlushCompressedSize = cms.untracked.int32(20971520),
    fileName = cms.untracked.string(options.outputName),
    outputCommands = process.RAWSIMEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Global tag
process.genstepfilter.triggerConditions=cms.vstring("generation_step")
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag,'106X_upgrade2018_realistic_v11_L1v1', '')

## generate radom value of the mass in the range set by the job
import os,random
random.seed = os.urandom(1000)
## generate random Higgs mass
hmass_rnd = random.randint(options.higgsMassMin,options.higgsMassMax);
hmass = hmass_rnd - (hmass_rnd%options.higgsMassStepSize)
## geenrate random resonance mass
rmass_rnd = random.randint(options.resonanceMassMin,options.resonanceMassMax);
rmass = rmass_rnd - (rmass_rnd%options.resonanceMassStepSize)
## generator seed
process.RandomNumberGeneratorService.generator.initialSeed = random.randint(0,999999)
print("Generate resonance mass of ",rmass," and Higgs mass ",hmass);

from Configuration.Generator.Pythia8CommonSettings_cfi import pythia8CommonSettingsBlock
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import pythia8CP5SettingsBlock
from Configuration.Generator.PSweightsPythia.PythiaPSweightsSettings_cfi import pythia8PSweightsSettingsBlock

process.generator = cms.EDFilter("Pythia8ConcurrentGeneratorFilter",
        comEnergy = cms.double(13000.0),
        crossSection = cms.untracked.double(1),
        filterEfficiency = cms.untracked.double(1.0),
        maxEventsToPrint = cms.untracked.int32(0),
        pythiaHepMCVerbosity = cms.untracked.bool(False),
        pythiaPylistVerbosity = cms.untracked.int32(0),
        PythiaParameters = cms.PSet(
            pythia8CommonSettingsBlock,
            pythia8CP5SettingsBlock,
            pythia8PSweightsSettingsBlock,
            processParameters = cms.vstring(
                'ExtraDimensionsG*:all = on',
                ## Graviton properties: mass, braching ration, width (narrow)
                '5100039:m0 = '+str(rmass),
                '5100039:mMin = '+str(options.resonanceMassMin),
                '5100039:mMax = '+str(options.resonanceMassMax),
                '5100039:onMode  = off',
                '5100039:onIfAny = 25',
                 ## Higgs boson properties: allowed masses, branching ratios, width (narrow)
                '25:m0 = '+str(hmass),
                '25:mMin = '+str(options.higgsMassMin),
                '25:mMax = '+str(options.higgsMassMax),
                '25:mWidth = 0.00374',
                '25:onMode = off',
                '25:oneChannel = 1 0.14280 100 5 -5',
                '25:addChannel = 1 0.14280 100 4 -4',
                '25:addChannel = 1 0.14280 100 15 -15',
                '25:addChannel = 1 0.14280 100 13 -13',
                '25:addChannel = 1 0.14280 100 11 -11',
                '25:addChannel = 1 0.14280 100 21 21',
                '25:addChannel = 1 0.04760 100 3 -3',
                '25:addChannel = 1 0.04760 100 2 -2',
                '25:addChannel = 1 0.04760 100 1 -1',                                
        ),
        parameterSets = cms.vstring(
                'pythia8CommonSettings',
                'pythia8CP5Settings',
                'processParameters'
        ),

    )
)

# Path and EndPath definitions
process.generation_step = cms.Path(process.pgen)
process.genfiltersummary_step = cms.EndPath(process.genFilterSummary)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RAWSIMoutput_step = cms.EndPath(process.RAWSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.generation_step,process.genfiltersummary_step,process.endjob_step,process.RAWSIMoutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

# filter all path with the production filter sequence
for path in process.paths:
	getattr(process,path).insert(0, process.generator)

# customisation of the process.

# Automatic addition of the customisation function from Configuration.DataProcessing.Utils
from Configuration.DataProcessing.Utils import addMonitoring 

#call to customisation function addMonitoring imported from Configuration.DataProcessing.Utils
process = addMonitoring(process)

# End of customisation functions
# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion


