#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <thread>
#include <atomic>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "TFile.h"
#include "TChain.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TString.h"
#include "TTreeReader.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "ROOT/TSeq.hxx"
#include "Compression.h"

/// Simple object to store resonance and daughter GEN kinematics
class Resonance {
public:
  Resonance(){};
  ~Resonance(){};
  TLorentzVector p4;
  std::vector<int> daugid;
  std::vector<TLorentzVector> daugp4;
};

/// thresholds for matching
float dRCone = 0.8;
float dRConeCounting = 0.8;
float dRMatchingDaughterJet = 0.8;
float dRMatchingDaughters = 0.8;
float dRMatchingPF = 0.1;
float ptGenLeptonMin  = 8;
float ptGenTauVisibleMin = 15;

/// atomic counters
std::atomic_uint nJetsTotal = 0;
std::atomic_uint nJetsRejectedEvents = 0;
std::atomic_uint nJetsRejectedBaseCuts = 0;
std::atomic_uint nJetsRejectedSignalResCuts = 0;
std::atomic_uint nJetsRejectedJetId = 0;
std::atomic_uint nJetsRejectedLeptonContent = 0;
std::atomic_uint nJetsRejectedMassCut = 0;
std::atomic_uint nJetsTraining = 0;

/// sample type definition
enum class sample_type {undefined=-1,qcd=0,ttbar=1,wjet=2,XtoHH=3,ggHH=4};

sample_type convertSampleType (const std::string & sample_name){
  if(sample_name == "sample_type::undefined") return sample_type::undefined;
  else if(sample_name == "sample_type::qcd") return sample_type::qcd;
  else if(sample_name == "sample_type::ttbar") return sample_type::ttbar;
  else if(sample_name == "sample_type::wjet") return sample_type::wjet;
  else if(sample_name == "sample_type::XtoHH") return sample_type::XtoHH;
  else if(sample_name == "sample_type::ggHH") return sample_type::ggHH;
  else return sample_type::undefined;
}


/// Global parameters that are parsed from command line
std::string inputFileList;
std::string inputFileDIR;
std::string outputDIR;
std::string outputFileName;
unsigned int nThreads;
bool useXRootD;
unsigned int maxNumberOfFilesToBeProcessed;
bool mergeThreadOutputFiles;
float jetPtMin;
float jetEtaMax;
float jetEtaMin;
float jetMassTruthMin;
float pfCandPtMin;
float pfCandPuppiWeightMin;
float losttrackPtMin;
sample_type sample;
std::string sample_name ;
bool saveOnlyGenMatchedJets;
bool saveOnlyResonanceMatchedJets;
bool saveLeptonOppositeFlavor;
bool compressOutputFile;

/// creation of output tree
void ntupleCreation (const int & workerID, 
		     const std::vector<std::string> & fileList, 
		     const std::vector<std::shared_ptr<TTree> > & trees_out, 
		     const int & nevents, 
		     const std::atomic_int & nthreads){

  std::unique_ptr<TChain> tree_in  (new TChain("dnntree/tree","dnntree/tree"));
  for(size_t ifile = 0; ifile < fileList.size(); ifile++)
    tree_in->Add(fileList.at(ifile).c_str());
  auto tree_out = trees_out.at(workerID);

  TTreeReader reader (tree_in.get());

  TTreeReaderValue<unsigned int> run   (reader,"run");
  TTreeReaderValue<unsigned int> lumi  (reader,"lumi");
  TTreeReaderValue<unsigned int> event (reader,"event");
  TTreeReaderValue<unsigned int> putrue (reader,"putrue");
  TTreeReaderValue<float>        wgt   (reader,"wgt");
  TTreeReaderValue<float>        rho (reader,"rho");
  TTreeReaderValue<unsigned int> npv (reader,"npv");
  TTreeReaderValue<unsigned int> nsv (reader,"nsv");
  TTreeReaderValue<float>        met (reader,"met");
  
  TTreeReaderValue<std::vector<float> >  gen_particle_pt  (reader,"gen_particle_pt");
  TTreeReaderValue<std::vector<float> >  gen_particle_eta  (reader,"gen_particle_eta");
  TTreeReaderValue<std::vector<float> >  gen_particle_phi  (reader,"gen_particle_phi");
  TTreeReaderValue<std::vector<float> >  gen_particle_mass  (reader,"gen_particle_mass");
  TTreeReaderValue<std::vector<int> >    gen_particle_id  (reader,"gen_particle_id");
  TTreeReaderValue<std::vector<unsigned int> > gen_particle_daughters_igen  (reader,"gen_particle_daughters_igen");
  TTreeReaderValue<std::vector<unsigned int> > gen_particle_daughters_status  (reader,"gen_particle_daughters_status");
  TTreeReaderValue<std::vector<int> >    gen_particle_daughters_charge  (reader,"gen_particle_daughters_charge");
  TTreeReaderValue<std::vector<int> >    gen_particle_daughters_id  (reader,"gen_particle_daughters_id");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_pt  (reader,"gen_particle_daughters_pt");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_eta  (reader,"gen_particle_daughters_eta");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_phi  (reader,"gen_particle_daughters_phi");
  TTreeReaderValue<std::vector<float> >  gen_particle_daughters_mass  (reader,"gen_particle_daughters_mass");

  TTreeReaderValue<std::vector<float> > jet_pt (reader,"jet_pt");
  TTreeReaderValue<std::vector<float> > jet_eta (reader,"jet_eta");
  TTreeReaderValue<std::vector<float> > jet_phi (reader,"jet_phi");
  TTreeReaderValue<std::vector<float> > jet_mass (reader,"jet_mass");
  TTreeReaderValue<std::vector<float> > jet_pt_raw (reader,"jet_pt_raw");
  TTreeReaderValue<std::vector<float> > jet_mass_raw (reader,"jet_mass_raw");

  TTreeReaderValue<std::vector<float> > jet_pnet_probHbb (reader,"jet_pnet_probHbb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probHcc (reader,"jet_pnet_probHcc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probHqq (reader,"jet_pnet_probHqq");
  TTreeReaderValue<std::vector<float> > jet_pnet_probQCDbb (reader,"jet_pnet_probQCDbb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probQCDb (reader,"jet_pnet_probQCDb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probQCDcc (reader,"jet_pnet_probQCDcc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probQCDc (reader,"jet_pnet_probQCDc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probQCDothers (reader,"jet_pnet_probQCDothers");

  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHtt (reader,"jet_pnetlast_probHtt");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHtm (reader,"jet_pnetlast_probHtm");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHte (reader,"jet_pnetlast_probHte");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHbb (reader,"jet_pnetlast_probHbb");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHcc (reader,"jet_pnetlast_probHcc");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHqq (reader,"jet_pnetlast_probHqq");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probHgg (reader,"jet_pnetlast_probHgg");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probQCD0hf (reader,"jet_pnetlast_probQCD0hf");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probQCD1hf (reader,"jet_pnetlast_probQCD1hf");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probQCD2hf (reader,"jet_pnetlast_probQCD2hf");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_masscorr (reader,"jet_pnetlast_masscorr");

  TTreeReaderValue<std::vector<unsigned int> > jet_id (reader,"jet_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_hflav (reader,"jet_hflav");
  TTreeReaderValue<std::vector<int> > jet_pflav (reader,"jet_pflav");
  TTreeReaderValue<std::vector<unsigned int> > jet_nbhad (reader,"jet_nbhad");
  TTreeReaderValue<std::vector<unsigned int> > jet_nchad (reader,"jet_nchad");    
  TTreeReaderValue<std::vector<unsigned int> > jet_ncand (reader,"jet_ncand");
  TTreeReaderValue<std::vector<float> > jet_chf (reader,"jet_chf");
  TTreeReaderValue<std::vector<float> > jet_nhf (reader,"jet_nhf");
  TTreeReaderValue<std::vector<float> > jet_elf (reader,"jet_elf");
  TTreeReaderValue<std::vector<float> > jet_phf (reader,"jet_phf");
  TTreeReaderValue<std::vector<float> > jet_muf (reader,"jet_muf");
 
  TTreeReaderValue<std::vector<float> > jet_genmatch_pt (reader,"jet_genmatch_pt");
  TTreeReaderValue<std::vector<float> > jet_genmatch_eta (reader,"jet_genmatch_eta");
  TTreeReaderValue<std::vector<float> > jet_genmatch_phi (reader,"jet_genmatch_phi");
  TTreeReaderValue<std::vector<float> > jet_genmatch_mass (reader,"jet_genmatch_mass");
  TTreeReaderValue<std::vector<float> > jet_genmatch_wnu_pt (reader,"jet_genmatch_wnu_pt");
  TTreeReaderValue<std::vector<float> > jet_genmatch_wnu_eta (reader,"jet_genmatch_wnu_eta");
  TTreeReaderValue<std::vector<float> > jet_genmatch_wnu_phi (reader,"jet_genmatch_wnu_phi");
  TTreeReaderValue<std::vector<float> > jet_genmatch_wnu_mass (reader,"jet_genmatch_wnu_mass");

  TTreeReaderValue<std::vector<float> > jet_softdrop_pt (reader,"jet_softdrop_pt");
  TTreeReaderValue<std::vector<float> > jet_softdrop_eta (reader,"jet_softdrop_eta");
  TTreeReaderValue<std::vector<float> > jet_softdrop_phi (reader,"jet_softdrop_phi");
  TTreeReaderValue<std::vector<float> > jet_softdrop_mass (reader,"jet_softdrop_mass");

  TTreeReaderValue<std::vector<float> > jet_softdrop_genmatch_pt (reader,"jet_softdrop_genmatch_pt");
  TTreeReaderValue<std::vector<float> > jet_softdrop_genmatch_eta (reader,"jet_softdrop_genmatch_eta");
  TTreeReaderValue<std::vector<float> > jet_softdrop_genmatch_phi (reader,"jet_softdrop_genmatch_phi");
  TTreeReaderValue<std::vector<float> > jet_softdrop_genmatch_mass (reader,"jet_softdrop_genmatch_mass");
    
  TTreeReaderValue<std::vector<float> > tau_pt (reader,"tau_pt");
  TTreeReaderValue<std::vector<float> > tau_eta (reader,"tau_eta");
  TTreeReaderValue<std::vector<float> > tau_phi (reader,"tau_phi");
  TTreeReaderValue<std::vector<float> > tau_mass (reader,"tau_mass");
  TTreeReaderValue<std::vector<float> > tau_dz (reader,"tau_dz");
  TTreeReaderValue<std::vector<float> > tau_dxy (reader,"tau_dxy");
  TTreeReaderValue<std::vector<unsigned int> > tau_decaymode (reader,"tau_decaymode");
  TTreeReaderValue<std::vector<int> > tau_charge (reader,"tau_charge");
  TTreeReaderValue<std::vector<float> > tau_idjet (reader,"tau_idjet");
  TTreeReaderValue<std::vector<float> > tau_idele (reader,"tau_idele");
  TTreeReaderValue<std::vector<float> > tau_idmu (reader,"tau_idmu");
  TTreeReaderValue<std::vector<unsigned int> > tau_idjet_wp (reader,"tau_idjet_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_idmu_wp (reader,"tau_idmu_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_idele_wp (reader,"tau_idele_wp");

  TTreeReaderValue<std::vector<float> > tau_boosted_pt (reader,"tau_boosted_pt");
  TTreeReaderValue<std::vector<float> > tau_boosted_eta (reader,"tau_boosted_eta");
  TTreeReaderValue<std::vector<float> > tau_boosted_phi (reader,"tau_boosted_phi");
  TTreeReaderValue<std::vector<float> > tau_boosted_mass (reader,"tau_boosted_mass");
  TTreeReaderValue<std::vector<float> > tau_boosted_dz (reader,"tau_boosted_dz");
  TTreeReaderValue<std::vector<float> > tau_boosted_dxy (reader,"tau_boosted_dxy");
  TTreeReaderValue<std::vector<unsigned int> > tau_boosted_decaymode (reader,"tau_boosted_decaymode");
  TTreeReaderValue<std::vector<int> > tau_boosted_charge (reader,"tau_boosted_charge");
  TTreeReaderValue<std::vector<float> > tau_boosted_idjet (reader,"tau_boosted_idjet");
  TTreeReaderValue<std::vector<float> > tau_boosted_idele (reader,"tau_boosted_idele");
  TTreeReaderValue<std::vector<float> > tau_boosted_idmu (reader,"tau_boosted_idmu");
  TTreeReaderValue<std::vector<unsigned int> > tau_boosted_idjet_wp (reader,"tau_boosted_idjet_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_boosted_idmu_wp (reader,"tau_boosted_idmu_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_boosted_idele_wp (reader,"tau_boosted_idele_wp");

  TTreeReaderValue<std::vector<float> > muon_pt (reader,"muon_pt");
  TTreeReaderValue<std::vector<float> > muon_eta (reader,"muon_eta");
  TTreeReaderValue<std::vector<float> > muon_phi (reader,"muon_phi");
  TTreeReaderValue<std::vector<float> > muon_mass (reader,"muon_mass");
  TTreeReaderValue<std::vector<int> > muon_charge (reader,"muon_charge");
  TTreeReaderValue<std::vector<unsigned int> > muon_id (reader,"muon_id");
  TTreeReaderValue<std::vector<unsigned int> > muon_iso (reader,"muon_iso");
  TTreeReaderValue<std::vector<float> > muon_d0 (reader,"muon_d0");
  TTreeReaderValue<std::vector<float> > muon_dz (reader,"muon_dz");

  TTreeReaderValue<std::vector<float> > electron_pt (reader,"electron_pt");
  TTreeReaderValue<std::vector<float> > electron_eta (reader,"electron_eta");
  TTreeReaderValue<std::vector<float> > electron_phi (reader,"electron_phi");
  TTreeReaderValue<std::vector<float> > electron_mass (reader,"electron_mass");
  TTreeReaderValue<std::vector<int> > electron_charge (reader,"electron_charge");
  TTreeReaderValue<std::vector<unsigned int> > electron_id (reader,"electron_id");
  TTreeReaderValue<std::vector<float> > electron_d0 (reader,"electron_d0");
  TTreeReaderValue<std::vector<float> > electron_dz (reader,"electron_dz");
  TTreeReaderValue<std::vector<float> > electron_idscore (reader,"electron_idscore");

  TTreeReaderValue<std::vector<float> > photon_pt (reader,"photon_pt");
  TTreeReaderValue<std::vector<float> > photon_eta (reader,"photon_eta");
  TTreeReaderValue<std::vector<float> > photon_phi (reader,"photon_phi");
  TTreeReaderValue<std::vector<float> > photon_mass (reader,"photon_mass");
  TTreeReaderValue<std::vector<unsigned int> > photon_id (reader,"photon_id");
  TTreeReaderValue<std::vector<float> > photon_idscore (reader,"photon_idscore");

  TTreeReaderValue<std::vector<float> > jet_pfcand_pt (reader,"jet_pfcandidate_pt");
  TTreeReaderValue<std::vector<float> > jet_pfcand_eta (reader,"jet_pfcandidate_eta");
  TTreeReaderValue<std::vector<float> > jet_pfcand_phi (reader,"jet_pfcandidate_phi");
  TTreeReaderValue<std::vector<float> > jet_pfcand_mass (reader,"jet_pfcandidate_mass");
  TTreeReaderValue<std::vector<float> > jet_pfcand_energy (reader,"jet_pfcandidate_energy");
  TTreeReaderValue<std::vector<float> > jet_pfcand_calofraction (reader,"jet_pfcandidate_calofraction");
  TTreeReaderValue<std::vector<float> > jet_pfcand_hcalfraction (reader,"jet_pfcandidate_hcalfraction");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dxy (reader,"jet_pfcandidate_dxy");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dz (reader,"jet_pfcandidate_dz");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dxysig (reader,"jet_pfcandidate_dxysig");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dzsig (reader,"jet_pfcandidate_dzsig");
  TTreeReaderValue<std::vector<float> > jet_pfcand_pperp_ratio (reader,"jet_pfcandidate_candjet_pperp_ratio");
  TTreeReaderValue<std::vector<float> > jet_pfcand_ppara_ratio (reader,"jet_pfcandidate_candjet_ppara_ratio");
  TTreeReaderValue<std::vector<float> > jet_pfcand_deta (reader,"jet_pfcandidate_candjet_deta");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dphi (reader,"jet_pfcandidate_candjet_dphi");
  TTreeReaderValue<std::vector<float> > jet_pfcand_etarel (reader,"jet_pfcandidate_candjet_etarel");
  TTreeReaderValue<std::vector<float> > jet_pfcand_puppiw (reader,"jet_pfcandidate_puppiw");
  TTreeReaderValue<std::vector<int> > jet_pfcand_charge (reader,"jet_pfcandidate_charge");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_frompv (reader,"jet_pfcandidate_frompv");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_id (reader,"jet_pfcandidate_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_ijet (reader,"jet_pfcandidate_ijet");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nhits (reader,"jet_pfcandidate_nhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_npixhits (reader,"jet_pfcandidate_npixhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nstriphits (reader,"jet_pfcandidate_nstriphits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nlosthits (reader,"jet_pfcandidate_nlosthits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_npixlayers (reader,"jet_pfcandidate_npixlayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nstriplayers (reader,"jet_pfcandidate_nstriplayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_track_qual (reader,"jet_pfcandidate_track_qual");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_track_chi2 (reader,"jet_pfcandidate_track_chi2");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_track_algo (reader,"jet_pfcandidate_track_algo");
  TTreeReaderValue<std::vector<float> > jet_pfcand_track_pterr (reader,"jet_pfcandidate_track_pterr");
  TTreeReaderValue<std::vector<float> > jet_pfcand_track_etaerr (reader,"jet_pfcandidate_track_etaerr");
  TTreeReaderValue<std::vector<float> > jet_pfcand_track_phierr (reader,"jet_pfcandidate_track_phierr");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_d3d (reader,"jet_pfcandidate_trackjet_d3d");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_d3dsig (reader,"jet_pfcandidate_trackjet_d3dsig");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_dist (reader,"jet_pfcandidate_trackjet_dist");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_decayL (reader,"jet_pfcandidate_trackjet_decayL");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_tau_signal (reader,"jet_pfcandidate_tau_signal");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_tau_boosted_signal (reader,"jet_pfcandidate_tau_boosted_signal");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_id (reader,"jet_pfcandidate_muon_id");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_muon_chi2 (reader,"jet_pfcandidate_muon_chi2");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_muon_segcomp (reader,"jet_pfcandidate_muon_segcomp");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_isglobal (reader,"jet_pfcandidate_muon_isglobal");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_nvalidhit (reader,"jet_pfcandidate_muon_nvalidhit");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_nstation (reader,"jet_pfcandidate_muon_nstation");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_eOverP (reader,"jet_pfcandidate_electron_eOverP");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_detaIn (reader,"jet_pfcandidate_electron_detaIn");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_dphiIn (reader,"jet_pfcandidate_electron_dphiIn");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_r9 (reader,"jet_pfcandidate_electron_r9");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_sigIetaIeta (reader,"jet_pfcandidate_electron_sigIetaIeta");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_sigIphiIphi (reader,"jet_pfcandidate_electron_sigIphiIphi");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_convProb (reader,"jet_pfcandidate_electron_convProb");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_photon_r9 (reader,"jet_pfcandidate_photon_r9");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_photon_sigIetaIeta (reader,"jet_pfcandidate_photon_sigIetaIeta");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_photon_eVeto (reader,"jet_pfcandidate_photon_eVeto");

  TTreeReaderValue<std::vector<float> > jet_losttrack_pt (reader,"jet_losttrack_pt");
  TTreeReaderValue<std::vector<float> > jet_losttrack_eta (reader,"jet_losttrack_eta");
  TTreeReaderValue<std::vector<float> > jet_losttrack_phi (reader,"jet_losttrack_phi");
  TTreeReaderValue<std::vector<float> > jet_losttrack_mass (reader,"jet_losttrack_mass");
  TTreeReaderValue<std::vector<float> > jet_losttrack_energy (reader,"jet_losttrack_energy");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dxy (reader,"jet_losttrack_dxy");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dz (reader,"jet_losttrack_dz");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dxysig (reader,"jet_losttrack_dxysig");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dzsig (reader,"jet_losttrack_dzsig");
  TTreeReaderValue<std::vector<float> > jet_losttrack_deta (reader,"jet_losttrack_candjet_deta");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dphi (reader,"jet_losttrack_candjet_dphi");
  TTreeReaderValue<std::vector<float> > jet_losttrack_etarel (reader,"jet_losttrack_candjet_etarel");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_frompv (reader,"jet_losttrack_frompv");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_ijet (reader,"jet_losttrack_ijet");
  TTreeReaderValue<std::vector<int> > jet_losttrack_charge (reader,"jet_losttrack_charge");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nhits (reader,"jet_losttrack_nhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_npixhits (reader,"jet_losttrack_npixhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nstriphits (reader,"jet_losttrack_nstriphits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nlosthits (reader,"jet_losttrack_nlosthits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_npixlayers (reader,"jet_losttrack_npixlayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nstriplayers (reader,"jet_losttrack_nstriplayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_track_qual (reader,"jet_losttrack_track_qual");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_track_chi2 (reader,"jet_losttrack_track_chi2");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_track_algo (reader,"jet_losttrack_track_algo");
  TTreeReaderValue<std::vector<float> > jet_losttrack_track_pterr (reader,"jet_losttrack_track_pterr");
  TTreeReaderValue<std::vector<float> > jet_losttrack_track_etaerr (reader,"jet_losttrack_track_etaerr");
  TTreeReaderValue<std::vector<float> > jet_losttrack_track_phierr (reader,"jet_losttrack_track_phierr");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_d3d (reader,"jet_losttrack_trackjet_d3d");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_d3dsig (reader,"jet_losttrack_trackjet_d3dsig");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_dist (reader,"jet_losttrack_trackjet_dist");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_decayL (reader,"jet_losttrack_trackjet_decayL");

  TTreeReaderValue<std::vector<float> > jet_sv_pt (reader,"jet_sv_pt");
  TTreeReaderValue<std::vector<float> > jet_sv_eta (reader,"jet_sv_eta");
  TTreeReaderValue<std::vector<float> > jet_sv_phi (reader,"jet_sv_phi");
  TTreeReaderValue<std::vector<float> > jet_sv_mass (reader,"jet_sv_mass");
  TTreeReaderValue<std::vector<float> > jet_sv_energy (reader,"jet_sv_energy");
  TTreeReaderValue<std::vector<float> > jet_sv_chi2 (reader,"jet_sv_chi2");
  TTreeReaderValue<std::vector<float> > jet_sv_dxy (reader,"jet_sv_dxy");
  TTreeReaderValue<std::vector<float> > jet_sv_dxysig (reader,"jet_sv_dxysig");
  TTreeReaderValue<std::vector<float> > jet_sv_d3d (reader,"jet_sv_d3d");
  TTreeReaderValue<std::vector<float> > jet_sv_d3dsig (reader,"jet_sv_d3dsig");
  TTreeReaderValue<std::vector<unsigned int> > jet_sv_ntrack (reader,"jet_sv_ntrack");
  TTreeReaderValue<std::vector<unsigned int> > jet_sv_ijet (reader,"jet_sv_ijet");
  
  reader.SetTree(tree_in.get());
  auto beginEntry = (Long64_t) nevents*workerID/nthreads;
  auto endEntry   = (Long64_t) nevents*std::min(nthreads.load(std::memory_order_relaxed),workerID+1)/nthreads-1;
  reader.SetEntriesRange(beginEntry,endEntry);
  std::cout<<"ntupleCreation --> thread "<<workerID<<" starting from entry "<<beginEntry<<" to entry "<<endEntry<<std::endl;

  // output ntupla
  unsigned int run_b, lumi_b, event_b, npv_b, nsv_b, npu_b;
  float wgt_b,rho_b,met_b;
  int sample_b;
  unsigned int ijet_b;

  tree_out->Branch("run",&run_b,"run/i"); 
  tree_out->Branch("lumi",&lumi_b,"lumi/i"); 
  tree_out->Branch("event",&event_b,"event/i"); 
  tree_out->Branch("npv",&npv_b,"npv/i"); 
  tree_out->Branch("nsv",&nsv_b,"nsv/i"); 
  tree_out->Branch("npu",&npu_b,"npu/i"); 
  tree_out->Branch("wgt",&wgt_b,"wgt/F"); 
  tree_out->Branch("rho",&rho_b,"rho/F"); 
  tree_out->Branch("met",&met_b,"met/F"); 
  tree_out->Branch("sample",&sample_b,"sample/I");
  tree_out->Branch("ijet",&ijet_b,"ijet/i");

  float jet_pt_b, jet_eta_b,jet_phi_b,jet_mass_b,jet_pt_raw_b,jet_mass_raw_b;

  tree_out->Branch("jet_pt",&jet_pt_b,"jet_pt/F");
  tree_out->Branch("jet_eta",&jet_eta_b,"jet_eta/F");
  tree_out->Branch("jet_phi",&jet_phi_b,"jet_phi/F");
  tree_out->Branch("jet_mass",&jet_mass_b,"jet_mass/F");
  tree_out->Branch("jet_pt_raw",&jet_pt_raw_b,"jet_pt_raw/F");
  tree_out->Branch("jet_mass_raw",&jet_mass_raw_b,"jet_mass_raw/F");

  float jet_pnet_probHbb_b,jet_pnet_probHcc_b,jet_pnet_probHqq_b, jet_pnet_probQCDbb_b, jet_pnet_probQCDb_b, jet_pnet_probQCDcc_b;
  float jet_pnet_probQCDc_b, jet_pnet_probQCDothers_b;  
  float jet_pnetlast_probHbb_b,jet_pnetlast_probHcc_b,jet_pnetlast_probHqq_b,jet_pnetlast_probHgg_b,jet_pnetlast_probQCD0hf_b,jet_pnetlast_probQCD1hf_b,jet_pnetlast_probQCD2hf_b;
  float jet_pnetlast_probHtt_b,jet_pnetlast_probHtm_b,jet_pnetlast_probHte_b,jet_pnetlast_masscorr_b;

  tree_out->Branch("jet_pnet_probHbb",&jet_pnet_probHbb_b,"jet_pnet_probHbb/F");
  tree_out->Branch("jet_pnet_probHcc",&jet_pnet_probHcc_b,"jet_pnet_probHcc/F");
  tree_out->Branch("jet_pnet_probHqq",&jet_pnet_probHqq_b,"jet_pnet_probHqq/F");
  tree_out->Branch("jet_pnet_probQCDbb",&jet_pnet_probQCDbb_b,"jet_pnet_probQCDbb/F");
  tree_out->Branch("jet_pnet_probQCDb",&jet_pnet_probQCDb_b,"jet_pnet_probQCDb/F");
  tree_out->Branch("jet_pnet_probQCDcc",&jet_pnet_probQCDcc_b,"jet_pnet_probQCDcc/F");
  tree_out->Branch("jet_pnet_probQCDc",&jet_pnet_probQCDc_b,"jet_pnet_probQCDc/F");
  tree_out->Branch("jet_pnet_probQCDothers",&jet_pnet_probQCDothers_b,"jet_pnet_probQCDothers/F");
  tree_out->Branch("jet_pnetlast_probHbb",&jet_pnetlast_probHbb_b,"jet_pnetlast_probHbb/F");
  tree_out->Branch("jet_pnetlast_probHcc",&jet_pnetlast_probHcc_b,"jet_pnetlast_probHcc/F");
  tree_out->Branch("jet_pnetlast_probHqq",&jet_pnetlast_probHqq_b,"jet_pnetlast_probHqq/F");
  tree_out->Branch("jet_pnetlast_probHgg",&jet_pnetlast_probHgg_b,"jet_pnetlast_probHgg/F");
  tree_out->Branch("jet_pnetlast_probHtt",&jet_pnetlast_probHtt_b,"jet_pnetlast_probHtt/F");
  tree_out->Branch("jet_pnetlast_probHtm",&jet_pnetlast_probHtm_b,"jet_pnetlast_probHtm/F");
  tree_out->Branch("jet_pnetlast_probHte",&jet_pnetlast_probHte_b,"jet_pnetlast_probHte/F");
  tree_out->Branch("jet_pnetlast_probQCD0hf",&jet_pnetlast_probQCD0hf_b,"jet_pnetlast_probQCD0hf/F");
  tree_out->Branch("jet_pnetlast_probQCD1hf",&jet_pnetlast_probQCD1hf_b,"jet_pnetlast_probQCD1hf/F");
  tree_out->Branch("jet_pnetlast_probQCD2hf",&jet_pnetlast_probQCD2hf_b,"jet_pnetlast_probQCD2hf/F");
  tree_out->Branch("jet_pnetlast_masscorr",&jet_pnetlast_masscorr_b,"jet_pnetlast_masscorr/F");

  unsigned int jet_ncand_b, jet_nbhad_b, jet_nchad_b, jet_hflav_b;
  float jet_chf_b, jet_nhf_b, jet_phf_b, jet_elf_b, jet_muf_b;
  int jet_pflav_b, jet_tauflav_b, jet_muflav_b, jet_elflav_b, jet_lepflav_b;
  int jet_flav_2prong_partonjet_match_b, jet_flav_2prong_parton_match_b;

  tree_out->Branch("jet_chf",&jet_chf_b,"jet_chf/F");
  tree_out->Branch("jet_nhf",&jet_nhf_b,"jet_nhf/F");
  tree_out->Branch("jet_phf",&jet_phf_b,"jet_phf/F");
  tree_out->Branch("jet_elf",&jet_elf_b,"jet_elf/F");
  tree_out->Branch("jet_muf",&jet_muf_b,"jet_muf/F");
  tree_out->Branch("jet_ncand",&jet_ncand_b,"jet_ncand/i");
  tree_out->Branch("jet_nbhad",&jet_nbhad_b,"jet_nbhad/i");
  tree_out->Branch("jet_nchad",&jet_nchad_b,"jet_nchad/i");
  tree_out->Branch("jet_hflav",&jet_hflav_b,"jet_hflav/i");
  tree_out->Branch("jet_pflav",&jet_pflav_b,"jet_pflav/I");
  tree_out->Branch("jet_muflav",&jet_muflav_b,"jet_muflav/I");
  tree_out->Branch("jet_elflav",&jet_elflav_b,"jet_elflav/I");
  tree_out->Branch("jet_lepflav",&jet_lepflav_b,"jet_lepflav/I");
  tree_out->Branch("jet_tauflav",&jet_tauflav_b,"jet_tauflav/I");
  tree_out->Branch("jet_flav_2prong_partonjet_match",&jet_flav_2prong_partonjet_match_b,"jet_flav_2prong_partonjet_match/I");
  tree_out->Branch("jet_flav_2prong_parton_match",&jet_flav_2prong_parton_match_b,"jet_flav_2prong_parton_match/I");

  float jet_genmatch_pt_b,jet_genmatch_eta_b,jet_genmatch_phi_b,jet_genmatch_mass_b;
  float jet_genmatch_wnu_pt_b, jet_genmatch_wnu_eta_b, jet_genmatch_wnu_phi_b, jet_genmatch_wnu_mass_b;
  float jet_genmatch_nu_pt_b, jet_genmatch_nu_eta_b, jet_genmatch_nu_phi_b, jet_genmatch_nu_mass_b, jet_genmatch_nu_energy_b;

  tree_out->Branch("jet_genmatch_pt",&jet_genmatch_pt_b,"jet_genmatch_pt/F");
  tree_out->Branch("jet_genmatch_eta",&jet_genmatch_eta_b,"jet_genmatch_eta/F");
  tree_out->Branch("jet_genmatch_phi",&jet_genmatch_phi_b,"jet_genmatch_phi/F");
  tree_out->Branch("jet_genmatch_mass",&jet_genmatch_mass_b,"jet_genmatch_mass/F");
  tree_out->Branch("jet_genmatch_wnu_pt",&jet_genmatch_wnu_pt_b,"jet_genmatch_wnu_pt/F");
  tree_out->Branch("jet_genmatch_wnu_eta",&jet_genmatch_wnu_eta_b,"jet_genmatch_wnu_eta/F");
  tree_out->Branch("jet_genmatch_wnu_phi",&jet_genmatch_wnu_phi_b,"jet_genmatch_wnu_phi/F");
  tree_out->Branch("jet_genmatch_wnu_mass",&jet_genmatch_wnu_mass_b,"jet_genmatch_wnu_mass/F");
  tree_out->Branch("jet_genmatch_nu_pt",&jet_genmatch_nu_pt_b,"jet_genmatch_nu_pt/F");
  tree_out->Branch("jet_genmatch_nu_eta",&jet_genmatch_nu_eta_b,"jet_genmatch_nu_eta/F");
  tree_out->Branch("jet_genmatch_nu_phi",&jet_genmatch_nu_phi_b,"jet_genmatch_nu_phi/F");
  tree_out->Branch("jet_genmatch_nu_mass",&jet_genmatch_nu_mass_b,"jet_genmatch_nu_mass/F");
  tree_out->Branch("jet_genmatch_nu_energy",&jet_genmatch_nu_energy_b,"jet_genmatch_nu_energy/F");

  float jet_softdrop_pt_b, jet_softdrop_eta_b, jet_softdrop_phi_b, jet_softdrop_mass_b;

  tree_out->Branch("jet_softdrop_pt",&jet_softdrop_pt_b,"jet_softdrop_pt/F");
  tree_out->Branch("jet_softdrop_eta",&jet_softdrop_eta_b,"jet_softdrop_eta/F");
  tree_out->Branch("jet_softdrop_phi",&jet_softdrop_phi_b,"jet_softdrop_phi/F");
  tree_out->Branch("jet_softdrop_mass",&jet_softdrop_mass_b,"jet_softdrop_mass/F");

  float jet_softdrop_genmatch_pt_b, jet_softdrop_genmatch_eta_b, jet_softdrop_genmatch_phi_b, jet_softdrop_genmatch_mass_b;

  tree_out->Branch("jet_softdrop_genmatch_pt",&jet_softdrop_genmatch_pt_b,"jet_softdrop_genmatch_pt/F");
  tree_out->Branch("jet_softdrop_genmatch_eta",&jet_softdrop_genmatch_eta_b,"jet_softdrop_genmatch_eta/F");
  tree_out->Branch("jet_softdrop_genmatch_phi",&jet_softdrop_genmatch_phi_b,"jet_softdrop_genmatch_phi/F");
  tree_out->Branch("jet_softdrop_genmatch_mass",&jet_softdrop_genmatch_mass_b,"jet_softdrop_genmatch_mass/F");

  float jet_pt_truth_b,jet_eta_truth_b,jet_phi_truth_b,jet_mass_truth_b;
  float jet_pt_truth_vis_b,jet_eta_truth_vis_b,jet_phi_truth_vis_b,jet_mass_truth_vis_b;

  tree_out->Branch("jet_pt_truth",&jet_pt_truth_b,"jet_pt_truth/F");
  tree_out->Branch("jet_eta_truth",&jet_eta_truth_b,"jet_eta_truth/F");
  tree_out->Branch("jet_phi_truth",&jet_phi_truth_b,"jet_phi_truth/F");
  tree_out->Branch("jet_mass_truth",&jet_mass_truth_b,"jet_mass_truth/F");
  tree_out->Branch("jet_pt_truth_vis",&jet_pt_truth_vis_b,"jet_pt_truth_vis/F");
  tree_out->Branch("jet_eta_truth_vis",&jet_eta_truth_vis_b,"jet_eta_truth_vis/F");
  tree_out->Branch("jet_phi_truth_vis",&jet_phi_truth_vis_b,"jet_phi_truth_vis/F");
  tree_out->Branch("jet_mass_truth_vis",&jet_mass_truth_vis_b,"jet_mass_truth_vis/F");

  std::vector<float> jet_taumatch_pt_b, jet_taumatch_eta_b, jet_taumatch_phi_b, jet_taumatch_mass_b;
  std::vector<float> jet_taumatch_idjet_b, jet_taumatch_idele_b, jet_taumatch_idmu_b;
  std::vector<int>   jet_taumatch_decaymode_b, jet_taumatch_charge_b;
  std::vector<float> jet_taumatch_dxy_b, jet_taumatch_dz_b;
  std::vector<int>   jet_taumatch_idjet_wp_b, jet_taumatch_idmu_wp_b, jet_taumatch_idele_wp_b;

  tree_out->Branch("jet_taumatch_pt","std::vector<float>",&jet_taumatch_pt_b);
  tree_out->Branch("jet_taumatch_eta","std::vector<float>",&jet_taumatch_eta_b);
  tree_out->Branch("jet_taumatch_phi","std::vector<float>",&jet_taumatch_phi_b);
  tree_out->Branch("jet_taumatch_mass","std::vector<float>",&jet_taumatch_mass_b);
  tree_out->Branch("jet_taumatch_charge","std::vector<int",&jet_taumatch_charge_b);
  tree_out->Branch("jet_taumatch_decaymode","std::vector<int>",&jet_taumatch_decaymode_b);
  tree_out->Branch("jet_taumatch_idjet","std::vector<float>",&jet_taumatch_idjet_b);
  tree_out->Branch("jet_taumatch_idmu","std::vector<float>",&jet_taumatch_idmu_b);
  tree_out->Branch("jet_taumatch_idele","std::vector<float>",&jet_taumatch_idele_b);
  tree_out->Branch("jet_taumatch_dxy_b","std::vector<float>",&jet_taumatch_dxy_b);
  tree_out->Branch("jet_taumatch_dz_b","std::vector<float>",&jet_taumatch_dz_b);
  tree_out->Branch("jet_taumatch_idjet_wp_b","std::vector<int>",&jet_taumatch_idjet_wp_b);
  tree_out->Branch("jet_taumatch_idmu_wp_b","std::vector<int>",&jet_taumatch_idmu_wp_b);
  tree_out->Branch("jet_taumatch_idele_wp_b","std::vector<int>",&jet_taumatch_idele_wp_b);

  std::vector<float> jet_boostedtaumatch_pt_b, jet_boostedtaumatch_eta_b, jet_boostedtaumatch_phi_b, jet_boostedtaumatch_mass_b;
  std::vector<float> jet_boostedtaumatch_idjet_b, jet_boostedtaumatch_idele_b, jet_boostedtaumatch_idmu_b;
  std::vector<int>   jet_boostedtaumatch_decaymode_b, jet_boostedtaumatch_charge_b;
  std::vector<float> jet_boostedtaumatch_dxy_b, jet_boostedtaumatch_dz_b;
  std::vector<int>   jet_boostedtaumatch_idjet_wp_b, jet_boostedtaumatch_idmu_wp_b, jet_boostedtaumatch_idele_wp_b;

  tree_out->Branch("jet_boostedtaumatch_pt","std::vector<float>",&jet_boostedtaumatch_pt_b);
  tree_out->Branch("jet_boostedtaumatch_eta","std::vector<float>",&jet_boostedtaumatch_eta_b);
  tree_out->Branch("jet_boostedtaumatch_phi","std::vector<float>",&jet_boostedtaumatch_phi_b);
  tree_out->Branch("jet_boostedtaumatch_mass","std::vector<float>",&jet_boostedtaumatch_mass_b);
  tree_out->Branch("jet_boostedtaumatch_charge","std::vector<int",&jet_boostedtaumatch_charge_b);
  tree_out->Branch("jet_boostedtaumatch_decaymode","std::vector<int>",&jet_boostedtaumatch_decaymode_b);
  tree_out->Branch("jet_boostedtaumatch_idjet","std::vector<float>",&jet_boostedtaumatch_idjet_b);
  tree_out->Branch("jet_boostedtaumatch_idmu","std::vector<float>",&jet_boostedtaumatch_idmu_b);
  tree_out->Branch("jet_boostedtaumatch_idele","std::vector<float>",&jet_boostedtaumatch_idele_b);
  tree_out->Branch("jet_boostedtaumatch_dxy_b","std::vector<float>",&jet_boostedtaumatch_dxy_b);
  tree_out->Branch("jet_boostedtaumatch_dz_b","std::vector<float>",&jet_boostedtaumatch_dz_b);
  tree_out->Branch("jet_boostedtaumatch_idjet_wp_b","std::vector<int>",&jet_boostedtaumatch_idjet_wp_b);
  tree_out->Branch("jet_boostedtaumatch_idmu_wp_b","std::vector<int>",&jet_boostedtaumatch_idmu_wp_b);
  tree_out->Branch("jet_boostedtaumatch_idele_wp_b","std::vector<int>",&jet_boostedtaumatch_idele_wp_b);

  std::vector<float> jet_mumatch_pt_b, jet_mumatch_eta_b, jet_mumatch_phi_b, jet_mumatch_mass_b, jet_mumatch_dxy_b, jet_mumatch_dz_b;
  std::vector<int> jet_mumatch_id_b, jet_mumatch_iso_b, jet_mumatch_charge_b;

  tree_out->Branch("jet_mumatch_pt","std::vector<float>",&jet_mumatch_pt_b);
  tree_out->Branch("jet_mumatch_eta","std::vector<float>",&jet_mumatch_eta_b);
  tree_out->Branch("jet_mumatch_phi","std::vector<float>",&jet_mumatch_phi_b);
  tree_out->Branch("jet_mumatch_mass","std::vector<float>",&jet_mumatch_mass_b);
  tree_out->Branch("jet_mumatch_dxy","std::vector<float>",&jet_mumatch_dxy_b);
  tree_out->Branch("jet_mumatch_dz","std::vector<float>",&jet_mumatch_dz_b);
  tree_out->Branch("jet_mumatch_id","std::vector<int>",&jet_mumatch_id_b);
  tree_out->Branch("jet_mumatch_iso","std::vector<int>",&jet_mumatch_iso_b);
  tree_out->Branch("jet_mumatch_charge","std::vector<int>",&jet_mumatch_charge_b);

  std::vector<float> jet_elematch_pt_b, jet_elematch_eta_b, jet_elematch_phi_b, jet_elematch_mass_b, jet_elematch_dxy_b, jet_elematch_dz_b, jet_elematch_idscore_b;
  std::vector<int> jet_elematch_id_b, jet_elematch_charge_b;

  tree_out->Branch("jet_elmatch_pt","std::vector<float>",&jet_elematch_pt_b);
  tree_out->Branch("jet_elmatch_eta","std::vector<float>",&jet_elematch_eta_b);
  tree_out->Branch("jet_elmatch_phi","std::vector<float>",&jet_elematch_phi_b);
  tree_out->Branch("jet_elmatch_mass","std::vector<float>",&jet_elematch_mass_b);
  tree_out->Branch("jet_elmatch_dxy","std::vector<float>",&jet_elematch_dxy_b);
  tree_out->Branch("jet_elmatch_dz","std::vector<float>",&jet_elematch_dz_b);
  tree_out->Branch("jet_elmatch_id","std::vector<int>",&jet_elematch_id_b);
  tree_out->Branch("jet_elmatch_idscore","std::vector<float>",&jet_elematch_idscore_b);
  tree_out->Branch("jet_elmatch_charge","std::vector<int>",&jet_elematch_charge_b);
 
  std::vector<float> jet_phomatch_pt_b, jet_phomatch_eta_b, jet_phomatch_phi_b, jet_phomatch_mass_b, jet_phomatch_idscore_b;
  std::vector<int> jet_phomatch_id_b;

  tree_out->Branch("jet_phomatch_pt","std::vector<float>",&jet_phomatch_pt_b);
  tree_out->Branch("jet_phomatch_eta","std::vector<float>",&jet_phomatch_eta_b);
  tree_out->Branch("jet_phomatch_phi","std::vector<float>",&jet_phomatch_phi_b);
  tree_out->Branch("jet_phomatch_mass","std::vector<float>",&jet_phomatch_mass_b);
  tree_out->Branch("jet_phomatch_id","std::vector<int>",&jet_phomatch_id_b);
  tree_out->Branch("jet_phomatch_idscore","std::vector<float>",&jet_phomatch_idscore_b);

  std::vector<float> jet_pfcand_pt_b, jet_pfcand_eta_b, jet_pfcand_phi_b, jet_pfcand_mass_b, jet_pfcand_energy_b, jet_pfcand_pt_log_b, jet_pfcand_energy_log_b, jet_pfcand_calofraction_b;
  std::vector<float> jet_pfcand_hcalfraction_b, jet_pfcand_dxy_b, jet_pfcand_dz_b, jet_pfcand_dxysig_b, jet_pfcand_dzsig_b, jet_pfcand_pperp_ratio_b, jet_pfcand_ppara_ratio_b;
  std::vector<float> jet_pfcand_deta_b, jet_pfcand_dphi_b, jet_pfcand_etarel_b, jet_pfcand_puppiw_b;

  tree_out->Branch("jet_pfcand_pt","std::vector<float>",&jet_pfcand_pt_b);
  tree_out->Branch("jet_pfcand_eta","std::vector<float>",&jet_pfcand_eta_b);
  tree_out->Branch("jet_pfcand_phi","std::vector<float>",&jet_pfcand_phi_b);
  tree_out->Branch("jet_pfcand_mass","std::vector<float>",&jet_pfcand_mass_b);
  tree_out->Branch("jet_pfcand_energy","std::vector<float>",&jet_pfcand_energy_b);
  tree_out->Branch("jet_pfcand_pt_log","std::vector<float>",&jet_pfcand_pt_log_b);
  tree_out->Branch("jet_pfcand_energy_log","std::vector<float>",&jet_pfcand_energy_log_b);
  tree_out->Branch("jet_pfcand_calofraction","std::vector<float>",&jet_pfcand_calofraction_b);
  tree_out->Branch("jet_pfcand_hcalfraction","std::vector<float>",&jet_pfcand_hcalfraction_b);
  tree_out->Branch("jet_pfcand_dxy","std::vector<float>",&jet_pfcand_dxy_b);
  tree_out->Branch("jet_pfcand_dxysig","std::vector<float>",&jet_pfcand_dxysig_b);
  tree_out->Branch("jet_pfcand_dz","std::vector<float>",&jet_pfcand_dz_b);
  tree_out->Branch("jet_pfcand_dzsig","std::vector<float>",&jet_pfcand_dzsig_b);
  tree_out->Branch("jet_pfcand_pperp_ratio","std::vector<float>",&jet_pfcand_pperp_ratio_b);
  tree_out->Branch("jet_pfcand_ppara_ratio","std::vector<float>",&jet_pfcand_ppara_ratio_b);
  tree_out->Branch("jet_pfcand_deta","std::vector<float>",&jet_pfcand_deta_b);
  tree_out->Branch("jet_pfcand_dphi","std::vector<float>",&jet_pfcand_dphi_b);
  tree_out->Branch("jet_pfcand_etarel","std::vector<float>",&jet_pfcand_etarel_b);
  tree_out->Branch("jet_pfcand_puppiw","std::vector<float>",&jet_pfcand_puppiw_b);

  // All floats to allow transformations
  std::vector<float> jet_pfcand_frompv_b, jet_pfcand_id_b, jet_pfcand_track_qual_b, jet_pfcand_track_chi2_b, jet_pfcand_track_pterr_b, jet_pfcand_track_etaerr_b, jet_pfcand_track_phierr_b;
  std::vector<float> jet_pfcand_nhits_b, jet_pfcand_npixhits_b, jet_pfcand_nstriphits_b, jet_pfcand_nlosthits_b, jet_pfcand_npixlayers_b, jet_pfcand_nstriplayers_b, jet_pfcand_track_algo_b;
  std::vector<float> jet_pfcand_tau_signal_b, jet_pfcand_tau_boosted_signal_b, jet_pfcand_charge_b;
  std::vector<float> jet_pfcand_muon_id_b, jet_pfcand_muon_chi2_b, jet_pfcand_muon_segcomp_b, jet_pfcand_muon_isglobal_b, jet_pfcand_muon_nvalidhit_b, jet_pfcand_muon_nstation_b;
  std::vector<float> jet_pfcand_electron_eOverP_b, jet_pfcand_electron_detaIn_b, jet_pfcand_electron_dphiIn_b, jet_pfcand_electron_r9_b, jet_pfcand_electron_sigIetaIeta_b;
  std::vector<float> jet_pfcand_electron_convProb_b, jet_pfcand_electron_sigIphiIphi_b;
  
  tree_out->Branch("jet_pfcand_frompv","std::vector<float>",&jet_pfcand_frompv_b);
  tree_out->Branch("jet_pfcand_id","std::vector<float>",&jet_pfcand_id_b);
  tree_out->Branch("jet_pfcand_charge","std::vector<float>",&jet_pfcand_charge_b);
  tree_out->Branch("jet_pfcand_track_qual","std::vector<float>",&jet_pfcand_track_qual_b);
  tree_out->Branch("jet_pfcand_track_chi2","std::vector<float>",&jet_pfcand_track_chi2_b);    
  tree_out->Branch("jet_pfcand_track_algo","std::vector<float>",&jet_pfcand_track_algo_b);    
  tree_out->Branch("jet_pfcand_track_pterr","std::vector<float>",&jet_pfcand_track_pterr_b);    
  tree_out->Branch("jet_pfcand_track_etaerr","std::vector<float>",&jet_pfcand_track_etaerr_b);    
  tree_out->Branch("jet_pfcand_track_phierr","std::vector<float>",&jet_pfcand_track_phierr_b);    
  tree_out->Branch("jet_pfcand_nhits","std::vector<float>",&jet_pfcand_nhits_b);    
  tree_out->Branch("jet_pfcand_npixhits","std::vector<float>",&jet_pfcand_npixhits_b);    
  tree_out->Branch("jet_pfcand_nstriphits","std::vector<float>",&jet_pfcand_nstriphits_b);    
  tree_out->Branch("jet_pfcand_nlosthits","std::vector<float>",&jet_pfcand_nlosthits_b);    
  tree_out->Branch("jet_pfcand_npixlayers","std::vector<float>",&jet_pfcand_npixlayers_b);    
  tree_out->Branch("jet_pfcand_nstriplayers","std::vector<float>",&jet_pfcand_nstriplayers_b);    
  tree_out->Branch("jet_pfcand_tau_signal","std::vector<float>",&jet_pfcand_tau_signal_b);
  tree_out->Branch("jet_pfcand_tau_boosted_signal","std::vector<float>",&jet_pfcand_tau_boosted_signal_b);
  tree_out->Branch("jet_pfcand_muon_id","std::vector<float>",&jet_pfcand_muon_id_b);
  tree_out->Branch("jet_pfcand_muon_chi2","std::vector<float>",&jet_pfcand_muon_chi2_b);
  tree_out->Branch("jet_pfcand_muon_segcomp","std::vector<float>",&jet_pfcand_muon_segcomp_b);
  tree_out->Branch("jet_pfcand_muon_isglobal","std::vector<float>",&jet_pfcand_muon_isglobal_b);
  tree_out->Branch("jet_pfcand_muon_nvalidhit","std::vector<float>",&jet_pfcand_muon_nvalidhit_b);
  tree_out->Branch("jet_pfcand_muon_nstation","std::vector<float>",&jet_pfcand_muon_nstation_b);
  tree_out->Branch("jet_pfcand_electron_eOverP","std::vector<float>",&jet_pfcand_electron_eOverP_b);
  tree_out->Branch("jet_pfcand_electron_detaIn","std::vector<float>",&jet_pfcand_electron_detaIn_b);
  tree_out->Branch("jet_pfcand_electron_dphiIn","std::vector<float>",&jet_pfcand_electron_dphiIn_b);
  tree_out->Branch("jet_pfcand_electron_r9","std::vector<float>",&jet_pfcand_electron_r9_b);
  tree_out->Branch("jet_pfcand_electron_sigIetaIeta","std::vector<float>",&jet_pfcand_electron_sigIetaIeta_b);
  tree_out->Branch("jet_pfcand_electron_sigIphiIphi","std::vector<float>",&jet_pfcand_electron_sigIphiIphi_b);
  tree_out->Branch("jet_pfcand_electron_convProb","std::vector<float>",&jet_pfcand_electron_convProb_b);
  
  std::vector<float> jet_pfcand_photon_sigIetaIeta_b, jet_pfcand_photon_r9_b, jet_pfcand_photon_eVeto_b;
  
  tree_out->Branch("jet_pfcand_photon_sigIetaIeta","std::vector<float>",&jet_pfcand_photon_sigIetaIeta_b);
  tree_out->Branch("jet_pfcand_photon_r9","std::vector<float>",&jet_pfcand_photon_r9_b);
  tree_out->Branch("jet_pfcand_photon_eVeto","std::vector<float>",&jet_pfcand_photon_eVeto_b);

  std::vector<float> jet_pfcand_trackjet_d3d_b, jet_pfcand_trackjet_d3dsig_b;
  std::vector<float> jet_pfcand_trackjet_dist_b, jet_pfcand_trackjet_decayL_b;
      
  tree_out->Branch("jet_pfcand_trackjet_d3d","std::vector<float>",&jet_pfcand_trackjet_d3d_b);
  tree_out->Branch("jet_pfcand_trackjet_d3dsig","std::vector<float>",&jet_pfcand_trackjet_d3dsig_b);
  tree_out->Branch("jet_pfcand_trackjet_dist","std::vector<float>",&jet_pfcand_trackjet_dist_b);
  tree_out->Branch("jet_pfcand_trackjet_decayL","std::vector<float>",&jet_pfcand_trackjet_decayL_b);
  
  std::vector<float> jet_sv_pt_b, jet_sv_pt_log_b, jet_sv_eta_b, jet_sv_phi_b, jet_sv_mass_b, jet_sv_energy_b, jet_sv_energy_log_b;
  std::vector<float> jet_sv_deta_b, jet_sv_dphi_b, jet_sv_chi2_b, jet_sv_dxy_b, jet_sv_dxysig_b;
  std::vector<float> jet_sv_d3d_b, jet_sv_d3dsig_b;
  std::vector<float> jet_sv_ntrack_b;
  
  tree_out->Branch("jet_sv_pt","std::vector<float>",&jet_sv_pt_b);
  tree_out->Branch("jet_sv_pt_log","std::vector<float>",&jet_sv_pt_log_b);
  tree_out->Branch("jet_sv_eta","std::vector<float>",&jet_sv_eta_b);
  tree_out->Branch("jet_sv_phi","std::vector<float>",&jet_sv_phi_b);
  tree_out->Branch("jet_sv_mass","std::vector<float>",&jet_sv_mass_b);
  tree_out->Branch("jet_sv_energy","std::vector<float>",&jet_sv_energy_b);
  tree_out->Branch("jet_sv_energy_log","std::vector<float>",&jet_sv_energy_log_b);
  tree_out->Branch("jet_sv_deta","std::vector<float>",&jet_sv_deta_b);
  tree_out->Branch("jet_sv_dphi","std::vector<float>",&jet_sv_dphi_b);
  tree_out->Branch("jet_sv_chi2","std::vector<float>",&jet_sv_chi2_b);
  tree_out->Branch("jet_sv_dxy","std::vector<float>",&jet_sv_dxy_b);
  tree_out->Branch("jet_sv_dxysig","std::vector<float>",&jet_sv_dxysig_b);
  tree_out->Branch("jet_sv_d3d","std::vector<float>",&jet_sv_d3d_b);
  tree_out->Branch("jet_sv_d3dsig","std::vector<float>",&jet_sv_d3dsig_b);
  tree_out->Branch("jet_sv_ntrack","std::vector<float>",&jet_sv_ntrack_b);      

  std::vector<float> jet_losttrack_pt_b, jet_losttrack_eta_b, jet_losttrack_phi_b, jet_losttrack_mass_b, jet_losttrack_energy_b, jet_losttrack_pt_log_b, jet_losttrack_energy_log_b;
  std::vector<float> jet_losttrack_dxy_b, jet_losttrack_dz_b, jet_losttrack_dxysig_b, jet_losttrack_dzsig_b;
  std::vector<float> jet_losttrack_deta_b, jet_losttrack_dphi_b, jet_losttrack_etarel_b;
  
  tree_out->Branch("jet_losttrack_pt","std::vector<float>",&jet_losttrack_pt_b);
  tree_out->Branch("jet_losttrack_eta","std::vector<float>",&jet_losttrack_eta_b);
  tree_out->Branch("jet_losttrack_phi","std::vector<float>",&jet_losttrack_phi_b);
  tree_out->Branch("jet_losttrack_mass","std::vector<float>",&jet_losttrack_mass_b);
  tree_out->Branch("jet_losttrack_energy","std::vector<float>",&jet_losttrack_energy_b);
  tree_out->Branch("jet_losttrack_pt_log","std::vector<float>",&jet_losttrack_pt_log_b);
  tree_out->Branch("jet_losttrack_energy_log","std::vector<float>",&jet_losttrack_energy_log_b);
  tree_out->Branch("jet_losttrack_dxy","std::vector<float>",&jet_losttrack_dxy_b);
  tree_out->Branch("jet_losttrack_dxysig","std::vector<float>",&jet_losttrack_dxysig_b);
  tree_out->Branch("jet_losttrack_dz","std::vector<float>",&jet_losttrack_dz_b);
  tree_out->Branch("jet_losttrack_dzsig","std::vector<float>",&jet_losttrack_dzsig_b);
  tree_out->Branch("jet_losttrack_deta","std::vector<float>",&jet_losttrack_deta_b);
  tree_out->Branch("jet_losttrack_dphi","std::vector<float>",&jet_losttrack_dphi_b);
  tree_out->Branch("jet_losttrack_etarel","std::vector<float>",&jet_losttrack_etarel_b);
      
  // All floats to allow transformations
  std::vector<float> jet_losttrack_charge_b, jet_losttrack_frompv_b, jet_losttrack_id_b, jet_losttrack_track_qual_b, jet_losttrack_track_chi2_b;
  std::vector<float> jet_losttrack_track_pterr_b, jet_losttrack_track_etaerr_b, jet_losttrack_track_phierr_b;
  std::vector<float> jet_losttrack_nhits_b, jet_losttrack_npixhits_b, jet_losttrack_nstriphits_b, jet_losttrack_nlosthits_b;
  std::vector<float> jet_losttrack_npixlayers_b, jet_losttrack_nstriplayers_b, jet_losttrack_track_algo_b;
  
  tree_out->Branch("jet_losttrack_frompv","std::vector<float>",&jet_losttrack_frompv_b);
  tree_out->Branch("jet_losttrack_id","std::vector<float>",&jet_losttrack_id_b);
  tree_out->Branch("jet_losttrack_charge","std::vector<float>",&jet_losttrack_charge_b);
  tree_out->Branch("jet_losttrack_track_qual","std::vector<float>",&jet_losttrack_track_qual_b);
  tree_out->Branch("jet_losttrack_track_chi2","std::vector<float>",&jet_losttrack_track_chi2_b);    
  tree_out->Branch("jet_losttrack_track_algo","std::vector<float>",&jet_losttrack_track_algo_b);    
  tree_out->Branch("jet_losttrack_track_pterr","std::vector<float>",&jet_losttrack_track_pterr_b);    
  tree_out->Branch("jet_losttrack_track_etaerr","std::vector<float>",&jet_losttrack_track_etaerr_b);    
  tree_out->Branch("jet_losttrack_track_phierr","std::vector<float>",&jet_losttrack_track_phierr_b);    
  tree_out->Branch("jet_losttrack_nhits","std::vector<float>",&jet_losttrack_nhits_b);    
  tree_out->Branch("jet_losttrack_npixhits","std::vector<float>",&jet_losttrack_npixhits_b);    
  tree_out->Branch("jet_losttrack_nstriphits","std::vector<float>",&jet_losttrack_nstriphits_b);    
  tree_out->Branch("jet_losttrack_nlosthits","std::vector<float>",&jet_losttrack_nlosthits_b);    
  tree_out->Branch("jet_losttrack_npixlayers","std::vector<float>",&jet_losttrack_npixlayers_b);    
  tree_out->Branch("jet_losttrack_nstriplayers","std::vector<float>",&jet_losttrack_nstriplayers_b);    

  std::vector<float> jet_losttrack_trackjet_d3d_b, jet_losttrack_trackjet_d3dsig_b;
  std::vector<float> jet_losttrack_trackjet_dist_b, jet_losttrack_trackjet_decayL_b;
      
  tree_out->Branch("jet_losttrack_trackjet_d3d","std::vector<float>",&jet_losttrack_trackjet_d3d_b);
  tree_out->Branch("jet_losttrack_trackjet_d3dsig","std::vector<float>",&jet_losttrack_trackjet_d3dsig_b);
  tree_out->Branch("jet_losttrack_trackjet_dist","std::vector<float>",&jet_losttrack_trackjet_dist_b);
  tree_out->Branch("jet_losttrack_trackjet_decayL","std::vector<float>",&jet_losttrack_trackjet_decayL_b);

  while(reader.Next()){

    // Gen leptons from resonance decay as well as neutrinos
    std::vector<TLorentzVector> genLepFromResonance4V;
    std::vector<TLorentzVector> genMuonsFromResonance4V;
    std::vector<TLorentzVector> genElectronsFromResonance4V;
    std::vector<int> genMuonsFromResonanceCharge;
    std::vector<int> genElectronsFromResonanceCharge;
  

    for(size_t igen = 0; igen < gen_particle_pt->size(); igen++){
      // select resonances like Higgs, W, Z, taus
      if(abs(gen_particle_id->at(igen)) == 25 or
	 abs(gen_particle_id->at(igen)) == 23 or
	 abs(gen_particle_id->at(igen)) == 24 or
	 abs(gen_particle_id->at(igen)) == 15){	
	for(size_t idau = 0; idau < gen_particle_daughters_id->size(); idau++){
	  // select electrons or muons from the resonance / tau decay
	  if(gen_particle_daughters_igen->at(idau) == igen and
	     (abs(gen_particle_daughters_id->at(idau)) == 11 or
	      abs(gen_particle_daughters_id->at(idau)) == 13)){
	    TLorentzVector gen4V;
	    gen4V.SetPtEtaPhiM(gen_particle_daughters_pt->at(idau),gen_particle_daughters_eta->at(idau),gen_particle_daughters_phi->at(idau),gen_particle_daughters_mass->at(idau));
	    if(std::find(genLepFromResonance4V.begin(),genLepFromResonance4V.end(),gen4V) == genLepFromResonance4V.end())
	      genLepFromResonance4V.push_back(gen4V);
	    if(abs(gen_particle_daughters_id->at(idau)) == 13 and 
	       std::find(genMuonsFromResonance4V.begin(),genMuonsFromResonance4V.end(),gen4V) == genMuonsFromResonance4V.end()){
	      genMuonsFromResonance4V.push_back(gen4V);
	      genMuonsFromResonanceCharge.push_back(gen_particle_daughters_charge->at(idau));
	    }
	    if(abs(gen_particle_daughters_id->at(idau)) == 11 and 
	       std::find(genElectronsFromResonance4V.begin(),genElectronsFromResonance4V.end(),gen4V) == genElectronsFromResonance4V.end()){
	      genElectronsFromResonance4V.push_back(gen4V);
	      genElectronsFromResonanceCharge.push_back(gen_particle_daughters_charge->at(idau));			
	    }
	  }
	}
      }
    }

    // Gen hadronic taus	
    std::vector<TLorentzVector> tau_gen_visible;
    std::vector<TLorentzVector> tau_gen;
    std::vector<int> tau_gen_charge;
    std::vector<unsigned int> tau_gen_nch;
    std::vector<unsigned int> tau_gen_np0;
    std::vector<unsigned int> tau_gen_nnh;

    for(size_t igen = 0; igen < gen_particle_pt->size(); igen++){
      if(abs(gen_particle_id->at(igen)) == 15){ // hadronic or leptonic tau
	TLorentzVector tau_gen_tmp;
	unsigned int tau_gen_nch_tmp = 0;
	unsigned int tau_gen_np0_tmp = 0;
	unsigned int tau_gen_nnh_tmp = 0;
	for(size_t idau = 0; idau < gen_particle_daughters_pt->size(); idau++){
	  if(gen_particle_daughters_igen->at(idau) == igen and
	     abs(gen_particle_daughters_id->at(idau)) != 11 and // no mu
	     abs(gen_particle_daughters_id->at(idau)) != 13 and // no el
	     abs(gen_particle_daughters_id->at(idau)) != 12 and // no neutrinos
	     abs(gen_particle_daughters_id->at(idau)) != 14 and
	     abs(gen_particle_daughters_id->at(idau)) != 16){
	    TLorentzVector tmp4V; 
	    tmp4V.SetPtEtaPhiM(gen_particle_daughters_pt->at(idau),gen_particle_daughters_eta->at(idau),gen_particle_daughters_phi->at(idau),gen_particle_daughters_mass->at(idau));
	    tau_gen_tmp += tmp4V;
	    if (gen_particle_daughters_charge->at(idau) != 0 and gen_particle_daughters_status->at(idau) == 1) tau_gen_nch_tmp ++; // charged particles
	    else if(gen_particle_daughters_charge->at(idau) == 0 and gen_particle_daughters_id->at(idau) == 111) tau_gen_np0_tmp++;
	    else if(gen_particle_daughters_charge->at(idau) == 0 and gen_particle_daughters_id->at(idau) != 111) tau_gen_nnh_tmp++;
	  }
	}
	if(tau_gen_tmp.Pt() > 0){ // good hadronic tau
	  tau_gen_visible.push_back(tau_gen_tmp);
	  tau_gen_tmp.SetPtEtaPhiM(gen_particle_pt->at(igen),gen_particle_eta->at(igen),gen_particle_phi->at(igen),gen_particle_mass->at(igen));
	  tau_gen_charge.push_back((gen_particle_id->at(igen) > 0) ? -1 : 1);
	  tau_gen.push_back(tau_gen_tmp);
	  tau_gen_nch.push_back(tau_gen_nch_tmp);
	  tau_gen_np0.push_back(tau_gen_np0_tmp);
	  tau_gen_nnh.push_back(tau_gen_nnh_tmp);
	}
      }
    }

    // Build 2-prong resonances matching Higgs/W/Z pdgId                                                                                                                                           
    bool badGenEvent = false;
    std::vector<Resonance> resonances;
    std::vector<unsigned int> skipTaus;
    for(size_t igen = 0; igen < gen_particle_pt->size(); igen++){
      if(abs(gen_particle_id->at(igen)) == 25 or
	 abs(gen_particle_id->at(igen)) == 23 or
	 abs(gen_particle_id->at(igen)) == 24){
	TLorentzVector resonace4V;
	resonace4V.SetPtEtaPhiM(gen_particle_pt->at(igen),gen_particle_eta->at(igen),gen_particle_phi->at(igen),gen_particle_mass->at(igen));
	resonances.push_back(Resonance());
	resonances.back().p4 = resonace4V;
	for(size_t idau = 0; idau < gen_particle_daughters_id->size(); idau++){
	  if(gen_particle_daughters_igen->at(idau) == igen){
	    resonances.back().daugid.push_back(gen_particle_daughters_id->at(idau));	    
	    TLorentzVector daughter4V;
	    daughter4V.SetPtEtaPhiM(gen_particle_daughters_pt->at(idau),gen_particle_daughters_eta->at(idau),gen_particle_daughters_phi->at(idau),gen_particle_daughters_mass->at(idau));
	    resonances.back().daugp4.push_back(daughter4V);
	  }
	}	
	if(resonances.back().daugp4.size() != 2){
	  std::cerr<<"Found more than two decay particles from Higgs or W or Z --> please check!!! --> rejecting the event"<<std::endl;
	  badGenEvent = true;
	}
	// replace the tau gen kinematics with visible one for the daughters
	for(size_t idau = 0; idau > resonances.back().daugp4.size(); idau++){
	  if(abs(resonances.back().daugid.at(idau)) == 15){
	    float minDR  = 1000;
	    int tauIndex = -1;
	    for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){
	      if(tau_gen_visible.at(itau).DeltaR(resonances.back().daugp4.at(idau)) < minDR and std::find(skipTaus.begin(),skipTaus.end(),itau) == skipTaus.end()){
		resonances.back().daugp4.at(idau) = tau_gen_visible.at(itau);
		tauIndex = itau;
	      }       	
	    }
	    skipTaus.push_back(tauIndex);
	  }
	}
      }
    }

    if(badGenEvent){
      nJetsRejectedEvents++;
      continue;
    }

    // list used later in the jet-event loop
    std::vector<unsigned int> vetoResonancePosition;
    
    /// Loop over the main jet collection
    for(size_t ijet = 0; ijet < jet_pt->size(); ijet++){      
      nJetsTotal++;

      ///// Selection
      if(jet_pt->at(ijet) < jetPtMin or
	 fabs(jet_eta->at(ijet)) > jetEtaMax or 
	 fabs(jet_eta->at(ijet)) < jetEtaMin or
	 (saveOnlyGenMatchedJets and jet_genmatch_pt->at(ijet) <= 0)){
	nJetsRejectedBaseCuts++;
	continue;
      }

      //// Jet 4V
      TLorentzVector jet4V;
      jet4V.SetPtEtaPhiM(jet_pt->at(ijet),jet_eta->at(ijet),jet_phi->at(ijet),jet_mass->at(ijet));

      // Matching with the resonance for signal jets using the kinematics
      int match_index_dR_parton_jet = -1;
      int match_index_dR_partons = -1;
      float mindR_jetres = 1000;
      for(size_t ipair = 0; ipair < resonances.size(); ipair++){
	float dRjres = jet4V.DeltaR(resonances.at(ipair).p4);
	if(dRjres < dRCone and dRjres < mindR_jetres){
	  mindR_jetres = dRjres;
	  float dR1 = resonances.at(ipair).daugp4.front().DeltaR(jet4V);
	  float dR2 = resonances.at(ipair).daugp4.back().DeltaR(jet4V);
	  float dR  = resonances.at(ipair).daugp4.front().DeltaR(resonances.at(ipair).daugp4.back());
	  if(dR1 < dRMatchingDaughterJet and dR2 < dRMatchingDaughterJet)
	    match_index_dR_parton_jet = ipair;
	  if(dR < dRMatchingDaughters)
	    match_index_dR_partons = ipair;
	}
      }

      // matching based on dR parton-jet
      if(match_index_dR_parton_jet != -1 and resonances.at(match_index_dR_parton_jet).daugid.size() == 2){
	if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 15 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 15)// X(tt)
	  jet_flav_2prong_partonjet_match_b = 7;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 5 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 5) // X(bb)
	  jet_flav_2prong_partonjet_match_b = 6;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 4 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 4) // X(cc)
	  jet_flav_2prong_partonjet_match_b = 5;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) <= 3 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) <= 3) // X(qq)
	  jet_flav_2prong_partonjet_match_b = 4;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 21 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 21) // X(gg)                                
	  jet_flav_2prong_partonjet_match_b = 3;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 13 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 13) // X(mm)                        
	  jet_flav_2prong_partonjet_match_b = 2;
	else if(abs(resonances.at(match_index_dR_parton_jet).daugid.at(0)) == 11 and abs(resonances.at(match_index_dR_parton_jet).daugid.at(1)) == 11) // X(ee)                        
	  jet_flav_2prong_partonjet_match_b = 1;
	else
	  jet_flav_2prong_partonjet_match_b = 0; // whatever remains                                                                                                                              
      }
      else
	jet_flav_2prong_partonjet_match_b = -1; // only real QCD jets / not-matched resonances

      // matching based on dR parton-parton
      if(match_index_dR_partons != -1 and resonances.at(match_index_dR_partons).daugid.size() == 2){
	if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 15 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 15) // X(tt)                                 
	  jet_flav_2prong_parton_match_b = 7;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 5 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 5) // X(bb)                                 
	  jet_flav_2prong_parton_match_b = 6;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 4 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 4) // X(cc)                                 
	  jet_flav_2prong_parton_match_b = 5;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) <= 3 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) <= 3) // X(qq)                                 
	  jet_flav_2prong_parton_match_b = 4;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 21 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 21) // X(gg)                                      
	  jet_flav_2prong_parton_match_b = 3;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 13 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 13) // X(mm)                                      
	  jet_flav_2prong_parton_match_b = 2;
	else if(abs(resonances.at(match_index_dR_partons).daugid.at(0)) == 11 and abs(resonances.at(match_index_dR_partons).daugid.at(1)) == 11) // X(ee)                                      
	  jet_flav_2prong_parton_match_b = 1;
	else
	  jet_flav_2prong_parton_match_b = 0; // whatever remains                                                                                                                                 
      }
      else 
	jet_flav_2prong_parton_match_b = -1;  // only real QCD jets / not-matched resonances

      // for signal samples exclude all jets unmatched (if enabled) to avoid labelling as QCD signal jets that are not matched to a resonance
      if(saveOnlyResonanceMatchedJets and 
	 jet_flav_2prong_partonjet_match_b == -1 and 
	 jet_flav_2prong_parton_match_b == -1) 
	continue;

      // Matching with gen muons/electrons/hadronic taus                                                                                                                                           
      std::vector<TLorentzVector> genMuonsMatched;
      std::vector<TLorentzVector> genElectronsMatched;
      std::vector<TLorentzVector> genTausHMatched;
      std::vector<TLorentzVector> genLepton4V;
      std::vector<TLorentzVector> genLeptonVis4V;
      TLorentzVector genLepton4V_total;
      TLorentzVector genLeptonVis4V_total;

      // pt ordering priviledge in the assignement
      for(size_t igen = 0; igen < genMuonsFromResonance4V.size(); igen++){
	float dR = jet4V.DeltaR(genMuonsFromResonance4V.at(igen));
	if(dR < dRConeCounting){
	  genMuonsMatched.push_back(genMuonsFromResonance4V.at(igen));
	  genLepton4V.push_back(genMuonsFromResonance4V.at(igen));
	  genLeptonVis4V.push_back(genMuonsFromResonance4V.at(igen));
	  genLepton4V_total += genMuonsFromResonance4V.at(igen);
	  genLeptonVis4V_total += genMuonsFromResonance4V.at(igen);
	}
      }
      for(size_t igen = 0; igen < genElectronsFromResonance4V.size(); igen++){
	float dR = jet4V.DeltaR(genElectronsFromResonance4V.at(igen));
	if(dR < dRConeCounting){
	  genElectronsMatched.push_back(genElectronsFromResonance4V.at(igen));
	  genLepton4V.push_back(genElectronsFromResonance4V.at(igen));
	  genLeptonVis4V.push_back(genElectronsFromResonance4V.at(igen));
	  genLepton4V_total += genElectronsFromResonance4V.at(igen);
	  genLeptonVis4V_total += genElectronsFromResonance4V.at(igen);
	}
      }    
      for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){
	float dR = tau_gen_visible.at(itau).DeltaR(jet4V);
	if(dR < dRConeCounting){
	  genTausHMatched.push_back(tau_gen_visible.at(itau));
	  genLepton4V.push_back(tau_gen.at(itau));
	  genLeptonVis4V.push_back(tau_gen_visible.at(itau));
	  genLepton4V_total += tau_gen.at(itau);
	  genLeptonVis4V_total += tau_gen_visible.at(itau);
	}
      }

      // Jet id applied only to jets not overlapping with gen-leptons                                                                                                                             
      if(genMuonsMatched.size() == 0 and
	 genElectronsMatched.size() == 0 and
	 genTausHMatched.size() == 0 and
	 jet_id->at(ijet) == 0){
	nJetsRejectedJetId++;
	continue;
      }

      // Reject jets on the basis of lepton content                                                                                                                                                
      if(not saveLeptonOppositeFlavor  and
	 ((genTausHMatched.size() != 0 and genMuonsMatched.size() != 0) or
	  (genTausHMatched.size() != 0 and genElectronsMatched.size() != 0) or
	  (genElectronsMatched.size() != 0 and genMuonsMatched.size() != 0))){
	nJetsRejectedLeptonContent++;
	continue;
      }
      
      // reject on the basis of the GEN lepton 
      bool reject = false;
      for(size_t igen = 0; igen < genMuonsMatched.size(); igen++){
	if(genMuonsMatched.at(igen).Pt() < ptGenLeptonMin) 
	  reject = true;
      }
      for(size_t igen = 0; igen < genElectronsMatched.size(); igen++){
	if(genElectronsMatched.at(igen).Pt() < ptGenLeptonMin) 
	  reject = true;
      }
      for(size_t igen = 0; igen < genTausHMatched.size(); igen++){
	if(genTausHMatched.at(igen).Pt() < ptGenTauVisibleMin) 
	  reject = true;
      }
      if(reject){
	nJetsRejectedLeptonContent++;
	continue;
      }
          
      /// apply neutrino contribution to GEN softdrop (status 1 neutrinos)
      TLorentzVector jet_gen_4V;
      TLorentzVector jet_gen_wnu_4V;
      TLorentzVector jet_gen_softdrop_4V;
      jet_gen_4V.SetPtEtaPhiM(jet_genmatch_pt->at(ijet),jet_genmatch_eta->at(ijet),jet_genmatch_phi->at(ijet),jet_genmatch_mass->at(ijet));
      jet_gen_wnu_4V.SetPtEtaPhiM(jet_genmatch_wnu_pt->at(ijet),jet_genmatch_wnu_eta->at(ijet),jet_genmatch_wnu_phi->at(ijet),jet_genmatch_wnu_mass->at(ijet));
      jet_gen_softdrop_4V.SetPtEtaPhiM(jet_softdrop_genmatch_pt->at(ijet),jet_softdrop_genmatch_eta->at(ijet),jet_softdrop_genmatch_phi->at(ijet),jet_softdrop_genmatch_mass->at(ijet));
      TLorentzVector jet_gen_neutrino_4V = (jet_gen_wnu_4V-jet_gen_4V);
      TLorentzVector jet_gen_softdrop_wnu_4V = (jet_gen_softdrop_4V+jet_gen_neutrino_4V);
      
      // take the mass of the closest resonance                                                                                                                                                  
      float minDR = 1000;
      int closest_resonance = -1;
      for(size_t ires = 0; ires < resonances.size(); ires++){
	// skip resonances that decay directly with a neutrino                                                                                                                                   
	if(fabs(resonances.at(ires).daugid.at(0)) == 12 or fabs(resonances.at(ires).daugid.at(0)) == 14 or fabs(resonances.at(ires).daugid.at(0)) == 16) continue;
	if(fabs(resonances.at(ires).daugid.at(1)) == 12 or fabs(resonances.at(ires).daugid.at(1)) == 14 or fabs(resonances.at(ires).daugid.at(1)) == 16) continue;
	if(find(vetoResonancePosition.begin(),vetoResonancePosition.end(),ires) != vetoResonancePosition.end()) continue;
	if(jet4V.DeltaR(resonances.at(ires).p4) < minDR and jet4V.DeltaR(resonances.at(ires).p4) < dRCone){
	  minDR = jet4V.DeltaR(resonances.at(ires).p4);
	  closest_resonance = ires;
	}
      }

      // if found take the truth kinematic of the resonance from its GEN level (neutrinos included) and blacklist the resonance                                                                  
      if(closest_resonance != -1){
	vetoResonancePosition.push_back(closest_resonance);
	// truth corresponds to the pole resonance kinematic properties
	jet_pt_truth_b   = resonances.at(closest_resonance).p4.Pt();
	jet_eta_truth_b  = resonances.at(closest_resonance).p4.Eta();
	jet_phi_truth_b  = resonances.at(closest_resonance).p4.Phi();
	jet_mass_truth_b = resonances.at(closest_resonance).p4.M();
	// visible kinematic properties i.e. deconvolve the neutrino 4V
	jet_pt_truth_vis_b = (resonances.at(closest_resonance).p4-jet_gen_neutrino_4V).Pt();
	jet_eta_truth_vis_b = (resonances.at(closest_resonance).p4-jet_gen_neutrino_4V).Eta();
	jet_phi_truth_vis_b = (resonances.at(closest_resonance).p4-jet_gen_neutrino_4V).Phi();
	jet_mass_truth_vis_b = (resonances.at(closest_resonance).p4-jet_gen_neutrino_4V).M();
      }
      else{ 
	// take the gen soft drop in QCD jets or un-matched jets, no need for the neutrino correction, everything fixed to be soft-drop  
	jet_pt_truth_b   = jet_gen_wnu_4V.Pt();
	jet_eta_truth_b  = jet_gen_wnu_4V.Eta();
	jet_phi_truth_b  = jet_gen_wnu_4V.Phi();
	jet_mass_truth_b = jet_gen_wnu_4V.M();
	jet_pt_truth_vis_b = jet_gen_4V.Pt();
	jet_eta_truth_vis_b = jet_gen_4V.Eta();
	jet_phi_truth_vis_b = jet_gen_4V.Phi();
	jet_mass_truth_vis_b = jet_gen_softdrop_4V.M();
      }
      
      // selection on the truth mass
      if(jet_mass_truth_b < jetMassTruthMin){
	nJetsRejectedMassCut++;
	continue;
      }
          
      // good jets for the analysis	  
      nJetsTraining++;	  
      
      /// Fill event branches                                                                                                                                                                      
      run_b = *run;
      lumi_b = *lumi;
      event_b = *event;
      npu_b = *putrue;
      npv_b = *npv;
      nsv_b = *nsv;
      wgt_b = *wgt;
      rho_b = *rho;
      met_b = *met;
      sample_b = static_cast<int>(sample);
      ijet_b = ijet;

      // fill jet branches                                                                                                                                                                         
      jet_pt_b = jet_pt->at(ijet);
      jet_eta_b = jet_eta->at(ijet);
      jet_phi_b = jet_phi->at(ijet);
      jet_mass_b = jet_mass->at(ijet);      
      jet_pt_raw_b = jet_pt_raw->at(ijet);      
      jet_mass_raw_b = jet_mass_raw->at(ijet);      
      
      jet_chf_b = jet_chf->at(ijet);
      jet_nhf_b = jet_nhf->at(ijet);
      jet_phf_b = jet_phf->at(ijet);
      jet_elf_b = jet_elf->at(ijet);
      jet_muf_b = jet_muf->at(ijet);

      jet_ncand_b = jet_ncand->at(ijet);
      jet_nbhad_b = jet_nbhad->at(ijet);
      jet_nchad_b = jet_nchad->at(ijet);	  
      jet_hflav_b = jet_hflav->at(ijet);
      jet_pflav_b = jet_pflav->at(ijet);

      jet_pnet_probHbb_b = jet_pnet_probHbb->at(ijet);
      jet_pnet_probHcc_b = jet_pnet_probHcc->at(ijet);
      jet_pnet_probHqq_b = jet_pnet_probHqq->at(ijet);
      jet_pnet_probQCDbb_b = jet_pnet_probQCDbb->at(ijet);
      jet_pnet_probQCDb_b = jet_pnet_probQCDb->at(ijet);
      jet_pnet_probQCDcc_b = jet_pnet_probQCDcc->at(ijet);
      jet_pnet_probQCDc_b = jet_pnet_probQCDc->at(ijet);
      jet_pnet_probQCDothers_b = jet_pnet_probQCDothers->at(ijet);
    
      jet_pnetlast_probHbb_b = jet_pnetlast_probHbb->at(ijet);
      jet_pnetlast_probHcc_b = jet_pnetlast_probHcc->at(ijet);
      jet_pnetlast_probHqq_b = jet_pnetlast_probHqq->at(ijet);
      jet_pnetlast_probHgg_b = jet_pnetlast_probHgg->at(ijet);
      jet_pnetlast_probQCD0hf_b = jet_pnetlast_probQCD0hf->at(ijet);
      jet_pnetlast_probQCD1hf_b = jet_pnetlast_probQCD1hf->at(ijet);
      jet_pnetlast_probQCD2hf_b = jet_pnetlast_probQCD2hf->at(ijet);
      jet_pnetlast_probHtt_b = jet_pnetlast_probHtt->at(ijet);
      jet_pnetlast_probHtm_b = jet_pnetlast_probHtm->at(ijet);
      jet_pnetlast_probHte_b = jet_pnetlast_probHte->at(ijet);
      jet_pnetlast_masscorr_b = jet_pnetlast_masscorr->at(ijet);
      
      jet_genmatch_pt_b = jet_genmatch_pt->at(ijet);
      jet_genmatch_eta_b = jet_genmatch_eta->at(ijet);
      jet_genmatch_phi_b = jet_genmatch_phi->at(ijet);
      jet_genmatch_mass_b = jet_genmatch_mass->at(ijet);

      jet_genmatch_wnu_pt_b = jet_genmatch_wnu_pt->at(ijet);
      jet_genmatch_wnu_eta_b = jet_genmatch_wnu_eta->at(ijet);
      jet_genmatch_wnu_phi_b = jet_genmatch_wnu_phi->at(ijet);
      jet_genmatch_wnu_mass_b = jet_genmatch_wnu_mass->at(ijet);
      
      jet_genmatch_nu_pt_b = jet_gen_neutrino_4V.Pt();
      jet_genmatch_nu_eta_b = jet_gen_neutrino_4V.Eta();
      jet_genmatch_nu_phi_b = jet_gen_neutrino_4V.Phi();
      jet_genmatch_nu_mass_b = jet_gen_neutrino_4V.M();

      jet_muflav_b = genMuonsMatched.size();
      jet_elflav_b = genElectronsMatched.size();
      jet_tauflav_b = genTausHMatched.size();
      jet_lepflav_b = genMuonsMatched.size()+genElectronsMatched.size()+genTausHMatched.size();
      
      // soft drop                                                                                                                                                                                 
      jet_softdrop_pt_b = jet_softdrop_pt->at(ijet);
      jet_softdrop_eta_b = jet_softdrop_eta->at(ijet);
      jet_softdrop_phi_b = jet_softdrop_phi->at(ijet);
      jet_softdrop_mass_b = jet_softdrop_mass->at(ijet);
      
      jet_softdrop_genmatch_pt_b = jet_softdrop_genmatch_pt->at(ijet);
      jet_softdrop_genmatch_eta_b = jet_softdrop_genmatch_eta->at(ijet);
      jet_softdrop_genmatch_phi_b = jet_softdrop_genmatch_phi->at(ijet);
      jet_softdrop_genmatch_mass_b = jet_softdrop_genmatch_mass->at(ijet);
    
      // SV	  
      jet_sv_pt_b.clear(); jet_sv_eta_b.clear(); jet_sv_phi_b.clear(); jet_sv_mass_b.clear(); jet_sv_energy_b.clear();
      jet_sv_deta_b.clear(); jet_sv_dphi_b.clear();
      jet_sv_chi2_b.clear(); jet_sv_dxy_b.clear(); jet_sv_dxysig_b.clear();
      jet_sv_d3d_b.clear(); jet_sv_d3dsig_b.clear(); jet_sv_ntrack_b.clear();
      jet_sv_pt_log_b.clear(); jet_sv_energy_log_b.clear();
    
      for(size_t isv = 0; isv < jet_sv_pt->size(); isv++){
	
	if(ijet != jet_sv_ijet->at(isv)) continue;
	
	jet_sv_pt_b.push_back(jet_sv_pt->at(isv));
	jet_sv_pt_log_b.push_back(std::isnan(std::log(jet_sv_pt->at(isv))) ? 0 : std::log(jet_sv_pt->at(isv)));
	jet_sv_eta_b.push_back(jet_sv_eta->at(isv));	  
	jet_sv_phi_b.push_back(jet_sv_phi->at(isv));	  
	jet_sv_energy_b.push_back(jet_sv_energy->at(isv));
	jet_sv_energy_log_b.push_back(std::isnan(std::log(jet_sv_energy->at(isv))) ? 0 : std::log(jet_sv_energy->at(isv)));
	jet_sv_mass_b.push_back(jet_sv_mass->at(isv));
	jet_sv_deta_b.push_back(jet_sv_eta->at(isv)-jet_eta->at(ijet));
	jet_sv_dphi_b.push_back(jet_sv_phi->at(isv)-jet_phi->at(ijet));
	jet_sv_chi2_b.push_back(jet_sv_chi2->at(isv));
        jet_sv_dxy_b.push_back(std::isnan(jet_sv_dxy->at(isv)) ? 0 : jet_sv_dxy->at(isv));
        jet_sv_dxysig_b.push_back(std::isnan(jet_sv_dxysig->at(isv)) ? 0 : jet_sv_dxysig->at(isv));
	jet_sv_d3d_b.push_back(std::isnan(jet_sv_d3d->at(isv)) ? 0 : jet_sv_d3d->at(isv));
        jet_sv_d3dsig_b.push_back(std::isnan(jet_sv_d3dsig->at(isv)) ? 0 : jet_sv_d3dsig->at(isv));
	jet_sv_ntrack_b.push_back(jet_sv_ntrack->at(isv));
      }

      // Lost tracks
      jet_losttrack_pt_b.clear(); jet_losttrack_eta_b.clear(); jet_losttrack_phi_b.clear(); jet_losttrack_mass_b.clear(); jet_losttrack_energy_b.clear(); 
      jet_losttrack_dxy_b.clear(); jet_losttrack_dz_b.clear(); jet_losttrack_dxysig_b.clear(); jet_losttrack_dzsig_b.clear(); 
      jet_losttrack_deta_b.clear(); jet_losttrack_dphi_b.clear(); 
      jet_losttrack_pt_log_b.clear(); jet_losttrack_energy_log_b.clear(); jet_losttrack_etarel_b.clear(); 
      jet_losttrack_charge_b.clear(); jet_losttrack_frompv_b.clear(); jet_losttrack_id_b.clear(); jet_losttrack_track_qual_b.clear(); jet_losttrack_track_chi2_b.clear();
      jet_losttrack_trackjet_d3d_b.clear(); jet_losttrack_trackjet_d3dsig_b.clear();      
      jet_losttrack_trackjet_dist_b.clear(); jet_losttrack_trackjet_decayL_b.clear();
      jet_losttrack_track_pterr_b.clear(); jet_losttrack_track_etaerr_b.clear(); jet_losttrack_track_phierr_b.clear();
      jet_losttrack_nhits_b.clear(); jet_losttrack_npixhits_b.clear(); jet_losttrack_nstriphits_b.clear(); jet_losttrack_nlosthits_b.clear();
      jet_losttrack_npixlayers_b.clear(); jet_losttrack_nstriplayers_b.clear(), jet_losttrack_track_algo_b.clear();


      for(size_t icand = 0; icand < jet_losttrack_pt->size(); icand++){
	
	if(ijet != jet_losttrack_ijet->at(icand)) continue;
	if(jet_losttrack_pt->at(icand) < losttrackPtMin) continue;

	jet_losttrack_pt_b.push_back(jet_losttrack_pt->at(icand));
	jet_losttrack_pt_log_b.push_back(std::isnan(std::log(jet_losttrack_pt->at(icand))) ? 0 : std::log(jet_losttrack_pt->at(icand)));	  
	jet_losttrack_eta_b.push_back(jet_losttrack_eta->at(icand));
	jet_losttrack_phi_b.push_back(jet_losttrack_phi->at(icand));
	jet_losttrack_mass_b.push_back(jet_losttrack_mass->at(icand));
	jet_losttrack_energy_b.push_back(jet_losttrack_energy->at(icand));
	jet_losttrack_energy_log_b.push_back(std::isnan(std::log(jet_losttrack_energy->at(icand))) ? 0 : std::log(jet_losttrack_energy->at(icand)));	    
	jet_losttrack_dxy_b.push_back(std::isnan(jet_losttrack_dxy->at(icand)) ? 0 : jet_losttrack_dxy->at(icand));
	jet_losttrack_dz_b.push_back(std::isnan(jet_losttrack_dz->at(icand)) ? 0 : jet_losttrack_dz->at(icand));
	jet_losttrack_dzsig_b.push_back(std::isnan(jet_losttrack_dzsig->at(icand)) ? 0 : jet_losttrack_dzsig->at(icand));
	jet_losttrack_dxysig_b.push_back(std::isnan(jet_losttrack_dxysig->at(icand)) ? 0 : jet_losttrack_dxysig->at(icand));
	jet_losttrack_deta_b.push_back(jet_losttrack_deta->at(icand));
	jet_losttrack_dphi_b.push_back(jet_losttrack_dphi->at(icand));
	jet_losttrack_etarel_b.push_back(std::isnan(jet_losttrack_etarel->at(icand)) ? 0 : jet_losttrack_etarel->at(icand));
	jet_losttrack_frompv_b.push_back(jet_losttrack_frompv->at(icand));
	jet_losttrack_charge_b.push_back(jet_losttrack_charge->at(icand));
	jet_losttrack_track_qual_b.push_back(jet_losttrack_track_qual->at(icand));
	jet_losttrack_track_chi2_b.push_back(jet_losttrack_track_chi2->at(icand));
	jet_losttrack_track_pterr_b.push_back((std::isnan(jet_losttrack_track_pterr->at(icand)) ? 0 : jet_losttrack_track_pterr->at(icand)));
	jet_losttrack_track_etaerr_b.push_back((std::isnan(jet_losttrack_track_etaerr->at(icand)) ? 0 : jet_losttrack_track_etaerr->at(icand)));
	jet_losttrack_track_phierr_b.push_back((std::isnan(jet_losttrack_track_phierr->at(icand)) ? 0 : jet_losttrack_track_phierr->at(icand)));
	jet_losttrack_track_algo_b.push_back(jet_losttrack_track_algo->at(icand));
	jet_losttrack_nhits_b.push_back(jet_losttrack_nhits->at(icand));
	jet_losttrack_npixhits_b.push_back(jet_losttrack_npixhits->at(icand));
	jet_losttrack_nstriphits_b.push_back(jet_losttrack_nstriphits->at(icand));
	jet_losttrack_nlosthits_b.push_back(jet_losttrack_nlosthits->at(icand));
	jet_losttrack_npixlayers_b.push_back(jet_losttrack_npixlayers->at(icand));
	jet_losttrack_nstriplayers_b.push_back(jet_losttrack_nstriplayers->at(icand));	
	jet_losttrack_trackjet_d3d_b.push_back(std::isnan(jet_losttrack_trackjet_d3d->at(icand)) ? 0 : jet_losttrack_trackjet_d3d->at(icand));
	jet_losttrack_trackjet_d3dsig_b.push_back(std::isnan(jet_losttrack_trackjet_d3dsig->at(icand)) ? 0 : jet_losttrack_trackjet_d3dsig->at(icand));
	jet_losttrack_trackjet_dist_b.push_back(std::isnan(jet_losttrack_trackjet_dist->at(icand)) ? 0 : jet_losttrack_trackjet_dist->at(icand));
	jet_losttrack_trackjet_decayL_b.push_back(std::isnan(jet_losttrack_trackjet_decayL->at(icand)) ? 0 : jet_losttrack_trackjet_decayL->at(icand));	
      }

      // PF candidates	
      jet_pfcand_pt_b.clear(); jet_pfcand_eta_b.clear(); jet_pfcand_phi_b.clear(); jet_pfcand_mass_b.clear(); jet_pfcand_energy_b.clear(); jet_pfcand_calofraction_b.clear();
      jet_pfcand_hcalfraction_b.clear(); jet_pfcand_dxy_b.clear(); jet_pfcand_dz_b.clear(); jet_pfcand_dxysig_b.clear(); jet_pfcand_dzsig_b.clear(); 
      jet_pfcand_pperp_ratio_b.clear(); jet_pfcand_ppara_ratio_b.clear(); jet_pfcand_deta_b.clear(); jet_pfcand_dphi_b.clear(); 
      jet_pfcand_pt_log_b.clear(); jet_pfcand_energy_log_b.clear(); jet_pfcand_etarel_b.clear(); jet_pfcand_puppiw_b.clear(); 
      jet_pfcand_charge_b.clear(); jet_pfcand_frompv_b.clear(); jet_pfcand_id_b.clear(); jet_pfcand_track_qual_b.clear(); jet_pfcand_track_chi2_b.clear();	  
      jet_pfcand_trackjet_d3d_b.clear(); jet_pfcand_trackjet_d3dsig_b.clear(); jet_pfcand_trackjet_dist_b.clear(); jet_pfcand_trackjet_decayL_b.clear();	  
      jet_pfcand_track_pterr_b.clear(); jet_pfcand_track_etaerr_b.clear(); jet_pfcand_track_phierr_b.clear();
      jet_pfcand_nhits_b.clear(); jet_pfcand_npixhits_b.clear(); jet_pfcand_nstriphits_b.clear(); jet_pfcand_nlosthits_b.clear();
      jet_pfcand_npixlayers_b.clear(); jet_pfcand_nstriplayers_b.clear(), jet_pfcand_track_algo_b.clear();
      jet_pfcand_tau_signal_b.clear(); jet_pfcand_tau_boosted_signal_b.clear();
      jet_pfcand_muon_id_b.clear(); jet_pfcand_muon_chi2_b.clear(); jet_pfcand_muon_segcomp_b.clear(); jet_pfcand_muon_isglobal_b.clear(); 
      jet_pfcand_muon_nvalidhit_b.clear(); jet_pfcand_muon_nstation_b.clear();
      jet_pfcand_electron_eOverP_b.clear(); jet_pfcand_electron_detaIn_b.clear(); jet_pfcand_electron_dphiIn_b.clear(); jet_pfcand_electron_r9_b.clear();
      jet_pfcand_electron_convProb_b.clear(); jet_pfcand_electron_sigIetaIeta_b.clear(); jet_pfcand_electron_sigIphiIphi_b.clear();
      jet_pfcand_photon_sigIetaIeta_b.clear(); jet_pfcand_photon_r9_b.clear(); jet_pfcand_photon_eVeto_b.clear();
      
      std::vector<TLorentzVector> muon4V_fromPF;
      std::vector<TLorentzVector> electron4V_fromPF;
      std::vector<TLorentzVector> photon4V_fromPF;
      TLorentzVector tau4V_fromPF;
      TLorentzVector tau4V_boosted_fromPF;
      
      for(size_t icand = 0; icand < jet_pfcand_pt->size(); icand++){
	
	if(ijet != jet_pfcand_ijet->at(icand)) continue;
	if(jet_pfcand_pt->at(icand) < pfCandPtMin) continue;
	if(jet_pfcand_puppiw->at(icand) < pfCandPuppiWeightMin) continue;

	jet_pfcand_pt_b.push_back(jet_pfcand_pt->at(icand));
	jet_pfcand_pt_log_b.push_back(std::isnan(std::log(jet_pfcand_pt->at(icand))) ? 0 : std::log(jet_pfcand_pt->at(icand)));	  
	jet_pfcand_eta_b.push_back(jet_pfcand_eta->at(icand));
	jet_pfcand_phi_b.push_back(jet_pfcand_phi->at(icand));
	jet_pfcand_mass_b.push_back(jet_pfcand_mass->at(icand));
	jet_pfcand_energy_b.push_back(jet_pfcand_energy->at(icand));
	jet_pfcand_energy_log_b.push_back(std::isnan(std::log(jet_pfcand_energy->at(icand))) ? 0 : std::log(jet_pfcand_energy->at(icand)));	    
	jet_pfcand_calofraction_b.push_back(std::isnan(jet_pfcand_calofraction->at(icand)) ? 0 : jet_pfcand_calofraction->at(icand));	    
	jet_pfcand_hcalfraction_b.push_back(std::isnan(jet_pfcand_hcalfraction->at(icand)) ? 0 : jet_pfcand_hcalfraction->at(icand));	    	
	jet_pfcand_dxy_b.push_back(std::isnan(jet_pfcand_dxy->at(icand)) ? 0 : jet_pfcand_dxy->at(icand));
	jet_pfcand_dz_b.push_back(std::isnan(jet_pfcand_dz->at(icand)) ? 0 : jet_pfcand_dz->at(icand));
	jet_pfcand_dzsig_b.push_back(std::isnan(jet_pfcand_dzsig->at(icand)) ? 0 : jet_pfcand_dzsig->at(icand));
	jet_pfcand_dxysig_b.push_back(std::isnan(jet_pfcand_dxysig->at(icand)) ? 0 : jet_pfcand_dxysig->at(icand));
	jet_pfcand_pperp_ratio_b.push_back(std::isnan(jet_pfcand_pperp_ratio->at(icand)) ? 0 : jet_pfcand_pperp_ratio->at(icand));
	jet_pfcand_ppara_ratio_b.push_back(std::isnan(jet_pfcand_ppara_ratio->at(icand)) ? 0 : jet_pfcand_ppara_ratio->at(icand));
	jet_pfcand_deta_b.push_back(jet_pfcand_deta->at(icand));
	jet_pfcand_dphi_b.push_back(jet_pfcand_dphi->at(icand));
	jet_pfcand_etarel_b.push_back(std::isnan(jet_pfcand_etarel->at(icand)) ? 0 : jet_pfcand_etarel->at(icand));
	jet_pfcand_frompv_b.push_back(jet_pfcand_frompv->at(icand));
	jet_pfcand_charge_b.push_back(jet_pfcand_charge->at(icand));
	jet_pfcand_puppiw_b.push_back(jet_pfcand_puppiw->at(icand));

	jet_pfcand_track_qual_b.push_back(jet_pfcand_track_qual->at(icand));
	jet_pfcand_track_chi2_b.push_back(jet_pfcand_track_chi2->at(icand));       
	jet_pfcand_track_algo_b.push_back(jet_pfcand_track_algo->at(icand));
	jet_pfcand_track_pterr_b.push_back((std::isnan(jet_pfcand_track_pterr->at(icand)) ? 0 : jet_pfcand_track_pterr->at(icand)));
	jet_pfcand_track_etaerr_b.push_back((std::isnan(jet_pfcand_track_etaerr->at(icand)) ? 0 : jet_pfcand_track_etaerr->at(icand)));
	jet_pfcand_track_phierr_b.push_back((std::isnan(jet_pfcand_track_phierr->at(icand)) ? 0 : jet_pfcand_track_phierr->at(icand)));
	jet_pfcand_nhits_b.push_back(jet_pfcand_nhits->at(icand));
	jet_pfcand_npixhits_b.push_back(jet_pfcand_npixhits->at(icand));
	jet_pfcand_nstriphits_b.push_back(jet_pfcand_nstriphits->at(icand));
	jet_pfcand_nlosthits_b.push_back(jet_pfcand_nlosthits->at(icand));
	jet_pfcand_npixlayers_b.push_back(jet_pfcand_npixlayers->at(icand));
	jet_pfcand_nstriplayers_b.push_back(jet_pfcand_nstriplayers->at(icand));	

	if(jet_pfcand_id->at(icand) == 11 and jet_pfcand_charge->at(icand) != 0) 
	  jet_pfcand_id_b.push_back(0);
	else if(jet_pfcand_id->at(icand) == 13 and jet_pfcand_charge->at(icand) != 0)
	  jet_pfcand_id_b.push_back(1);
	else if(jet_pfcand_id->at(icand) == 22 and jet_pfcand_charge->at(icand) == 0)
	  jet_pfcand_id_b.push_back(2);
	else if(jet_pfcand_id->at(icand) != 22 and jet_pfcand_charge->at(icand) == 0 and jet_pfcand_id->at(icand) != 1 and jet_pfcand_id->at(icand) != 2)
	  jet_pfcand_id_b.push_back(3);
	else if(jet_pfcand_id->at(icand) != 11 and jet_pfcand_id->at(icand) != 13 and jet_pfcand_charge->at(icand) != 0)
	  jet_pfcand_id_b.push_back(4);
	else if(jet_pfcand_charge->at(icand) == 0  and jet_pfcand_id->at(icand) == 1)
	  jet_pfcand_id_b.push_back(5);
	else if(jet_pfcand_charge->at(icand) == 0  and jet_pfcand_id->at(icand) == 2)
	  jet_pfcand_id_b.push_back(6);
	else
	  jet_pfcand_id_b.push_back(-1);    

	jet_pfcand_trackjet_d3d_b.push_back(std::isnan(jet_pfcand_trackjet_d3d->at(icand)) ? 0 : jet_pfcand_trackjet_d3d->at(icand));
	jet_pfcand_trackjet_d3dsig_b.push_back(std::isnan(jet_pfcand_trackjet_d3dsig->at(icand)) ? 0 : jet_pfcand_trackjet_d3dsig->at(icand));
	jet_pfcand_trackjet_dist_b.push_back(std::isnan(jet_pfcand_trackjet_dist->at(icand)) ? 0 : jet_pfcand_trackjet_dist->at(icand));
	jet_pfcand_trackjet_decayL_b.push_back(std::isnan(jet_pfcand_trackjet_decayL->at(icand)) ? 0 : jet_pfcand_trackjet_decayL->at(icand));
		
	jet_pfcand_tau_signal_b.push_back(jet_pfcandidate_tau_signal->at(icand));
	jet_pfcand_tau_boosted_signal_b.push_back(jet_pfcandidate_tau_boosted_signal->at(icand));

	jet_pfcand_muon_id_b.push_back(jet_pfcandidate_muon_id->at(icand));
	jet_pfcand_muon_chi2_b.push_back(jet_pfcandidate_muon_chi2->at(icand));
	jet_pfcand_muon_segcomp_b.push_back(jet_pfcandidate_muon_segcomp->at(icand));
	jet_pfcand_muon_isglobal_b.push_back(jet_pfcandidate_muon_isglobal->at(icand));
	jet_pfcand_muon_nvalidhit_b.push_back(jet_pfcandidate_muon_nvalidhit->at(icand));
	jet_pfcand_muon_nstation_b.push_back(jet_pfcandidate_muon_nstation->at(icand));

	jet_pfcand_electron_eOverP_b.push_back(std::isnan(jet_pfcandidate_electron_eOverP->at(icand)) ? 0 : jet_pfcandidate_electron_eOverP->at(icand));
	jet_pfcand_electron_detaIn_b.push_back(jet_pfcandidate_electron_detaIn->at(icand));
	jet_pfcand_electron_dphiIn_b.push_back(jet_pfcandidate_electron_dphiIn->at(icand));
	jet_pfcand_electron_r9_b.push_back(std::isnan(jet_pfcandidate_electron_r9->at(icand)) ? 0 : jet_pfcandidate_electron_r9->at(icand));
	jet_pfcand_electron_sigIetaIeta_b.push_back(jet_pfcandidate_electron_sigIetaIeta->at(icand));
	jet_pfcand_electron_convProb_b.push_back(std::isnan(jet_pfcandidate_electron_convProb->at(icand)) ? 0 : jet_pfcandidate_electron_convProb->at(icand));
	jet_pfcand_electron_sigIphiIphi_b.push_back(std::isnan(jet_pfcandidate_electron_sigIphiIphi->at(icand)) ? 0 : jet_pfcandidate_electron_sigIphiIphi->at(icand));

	jet_pfcand_photon_sigIetaIeta_b.push_back(std::isnan(jet_pfcandidate_photon_sigIetaIeta->at(icand)) ? 0 : jet_pfcandidate_photon_sigIetaIeta->at(icand));
	jet_pfcand_photon_r9_b.push_back(std::isnan(jet_pfcandidate_photon_r9->at(icand)) ? 0 : jet_pfcandidate_photon_r9->at(icand));
	jet_pfcand_photon_eVeto_b.push_back(std::isnan(jet_pfcandidate_photon_eVeto->at(icand)) ? 0 : jet_pfcandidate_photon_eVeto->at(icand));

	// take muons
	if(jet_pfcand_id->at(icand) == 11){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  electron4V_fromPF.push_back(tmp4V);
	}
	// take electrons
	if(jet_pfcand_id->at(icand) == 13){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  muon4V_fromPF.push_back(tmp4V);
	}
	// take photons
	if(jet_pfcand_id->at(icand) == 22){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  photon4V_fromPF.push_back(tmp4V);
	}
	// take every tau candidate
	if(jet_pfcandidate_tau_signal->at(icand)){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  tau4V_fromPF += tmp4V;
	}	
	if(jet_pfcandidate_tau_boosted_signal->at(icand)){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  tau4V_boosted_fromPF += tmp4V;
	}	
      }

      // Matching with reco tau
      jet_taumatch_pt_b.clear(); jet_taumatch_eta_b.clear(); jet_taumatch_phi_b.clear(); jet_taumatch_mass_b.clear(); jet_taumatch_decaymode_b.clear(); jet_taumatch_idjet_b.clear();
      jet_taumatch_idele_b.clear(); jet_taumatch_idmu_b.clear(); jet_taumatch_dxy_b.clear(); jet_taumatch_dz_b.clear();
      jet_taumatch_charge_b.clear(); jet_taumatch_idjet_wp_b.clear(); jet_taumatch_idele_wp_b.clear(); jet_taumatch_idmu_wp_b.clear();
      
      for(size_t itau = 0; itau < tau_pt->size(); itau++){
	TLorentzVector tmp4V;
	tmp4V.SetPtEtaPhiM(tau_pt->at(itau),tau_eta->at(itau),tau_phi->at(itau),tau_mass->at(itau));
	float dR = tmp4V.DeltaR(jet4V);
	if (dR < dRConeCounting){ // small numerical roundoff due to float to double approximation                                                                                                   
	  jet_taumatch_pt_b.push_back(tau_pt->at(itau));
	  jet_taumatch_eta_b.push_back(tau_eta->at(itau));
	  jet_taumatch_phi_b.push_back(tau_phi->at(itau));
	  jet_taumatch_mass_b.push_back(tau_mass->at(itau));
	  jet_taumatch_charge_b.push_back(tau_charge->at(itau));
	  jet_taumatch_decaymode_b.push_back(tau_decaymode->at(itau));
	  jet_taumatch_dxy_b.push_back(tau_dxy->at(itau));
	  jet_taumatch_dz_b.push_back(tau_dz->at(itau));
	  jet_taumatch_idjet_b.push_back(tau_idjet->at(itau));
	  jet_taumatch_idele_b.push_back(tau_idele->at(itau));
	  jet_taumatch_idmu_b.push_back(tau_idmu->at(itau));
	  jet_taumatch_idjet_wp_b.push_back(tau_idjet_wp->at(itau));
	  jet_taumatch_idele_wp_b.push_back(tau_idele_wp->at(itau));
	  jet_taumatch_idmu_wp_b.push_back(tau_idmu_wp->at(itau));	  
	}
      }
      
      // Matching with reco tau
      jet_boostedtaumatch_pt_b.clear(); jet_boostedtaumatch_eta_b.clear(); jet_boostedtaumatch_phi_b.clear(); jet_boostedtaumatch_mass_b.clear();
      jet_boostedtaumatch_decaymode_b.clear(); jet_boostedtaumatch_idjet_b.clear();
      jet_boostedtaumatch_idele_b.clear(); jet_boostedtaumatch_idmu_b.clear(); jet_boostedtaumatch_dxy_b.clear(); jet_boostedtaumatch_dz_b.clear();
      jet_boostedtaumatch_charge_b.clear(); jet_boostedtaumatch_idjet_wp_b.clear(); jet_boostedtaumatch_idele_wp_b.clear(); jet_boostedtaumatch_idmu_wp_b.clear();
      
      for(size_t itau = 0; itau < tau_boosted_pt->size(); itau++){
	TLorentzVector tmp4V;
	tmp4V.SetPtEtaPhiM(tau_boosted_pt->at(itau),tau_boosted_eta->at(itau),tau_boosted_phi->at(itau),tau_boosted_mass->at(itau));
	float dR = tmp4V.DeltaR(jet4V);
	if (dR < dRConeCounting){ // small numerical roundoff due to float to double approximation                                                                                                   
	  jet_boostedtaumatch_pt_b.push_back(tau_boosted_pt->at(itau));
	  jet_boostedtaumatch_eta_b.push_back(tau_boosted_eta->at(itau));
	  jet_boostedtaumatch_phi_b.push_back(tau_boosted_phi->at(itau));
	  jet_boostedtaumatch_mass_b.push_back(tau_boosted_mass->at(itau));
	  jet_boostedtaumatch_charge_b.push_back(tau_boosted_charge->at(itau));
	  jet_boostedtaumatch_decaymode_b.push_back(tau_boosted_decaymode->at(itau));
	  jet_boostedtaumatch_dxy_b.push_back(tau_boosted_dxy->at(itau));
	  jet_boostedtaumatch_dz_b.push_back(tau_boosted_dz->at(itau));
	  jet_boostedtaumatch_idjet_b.push_back(tau_boosted_idjet->at(itau));
	  jet_boostedtaumatch_idele_b.push_back(tau_boosted_idele->at(itau));
	  jet_boostedtaumatch_idmu_b.push_back(tau_boosted_idmu->at(itau));
	  jet_boostedtaumatch_idjet_wp_b.push_back(tau_boosted_idjet_wp->at(itau));
	  jet_boostedtaumatch_idele_wp_b.push_back(tau_boosted_idele_wp->at(itau));
	  jet_boostedtaumatch_idmu_wp_b.push_back(tau_boosted_idmu_wp->at(itau));	  
	}
      }
      
      // Matching with muon by considering only the highest pt one
      std::sort(muon4V_fromPF.begin(),muon4V_fromPF.end(),
		[&](const TLorentzVector & a, const TLorentzVector & b){
		  return a.Pt() > b.Pt();
		});
      
      jet_mumatch_pt_b.clear(); jet_mumatch_eta_b.clear(); jet_mumatch_phi_b.clear(); jet_mumatch_mass_b.clear();
      jet_mumatch_dxy_b.clear(); jet_mumatch_dz_b.clear(); jet_mumatch_id_b.clear(); jet_mumatch_iso_b.clear();
      jet_mumatch_charge_b.clear();

      for(size_t ipfmu = 0; ipfmu < muon4V_fromPF.size(); ipfmu++){
	float minDR = 1000;
	int ipos = -1;
	for(size_t imu = 0; imu < muon_pt->size(); imu++){	
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(muon_pt->at(imu),muon_eta->at(imu),muon_phi->at(imu),muon_mass->at(imu));
	  float dR = tmp4V.DeltaR(muon4V_fromPF.at(ipfmu));
	  if(dR < dRMatchingPF and dR < minDR){
	    minDR = dR;
	    ipos = imu;
	  }
	}
	if(ipos >= 0) {
	  jet_mumatch_pt_b.push_back(muon_pt->at(ipos));
	  jet_mumatch_eta_b.push_back(muon_eta->at(ipos));
	  jet_mumatch_phi_b.push_back(muon_phi->at(ipos));
	  jet_mumatch_mass_b.push_back(muon_mass->at(ipos));
	  jet_mumatch_charge_b.push_back(muon_charge->at(ipos));
	  jet_mumatch_dxy_b.push_back(muon_d0->at(ipos));
	  jet_mumatch_dz_b.push_back(muon_dz->at(ipos));
	  jet_mumatch_id_b.push_back(muon_id->at(ipos));
	  jet_mumatch_iso_b.push_back(muon_iso->at(ipos));
	}
	else{
	  jet_mumatch_pt_b.push_back(muon4V_fromPF.at(ipfmu).Pt());
	  jet_mumatch_eta_b.push_back(muon4V_fromPF.at(ipfmu).Eta());
	  jet_mumatch_phi_b.push_back(muon4V_fromPF.at(ipfmu).Phi());
	  jet_mumatch_mass_b.push_back(muon4V_fromPF.at(ipfmu).M());
	  jet_mumatch_charge_b.push_back(-0);
	  jet_mumatch_dxy_b.push_back(-1);
	  jet_mumatch_dz_b.push_back(-1);
	  jet_mumatch_id_b.push_back(-1);
	  jet_mumatch_iso_b.push_back(-1);
	}
      }
      
      // Matching with electron by considering only the highest pt one
      std::sort(electron4V_fromPF.begin(),electron4V_fromPF.end(),
		[&](const TLorentzVector & a, const TLorentzVector & b){
		  return a.Pt() > b.Pt();
		});
      
      jet_elematch_pt_b.clear(); jet_elematch_eta_b.clear(); jet_elematch_phi_b.clear(); jet_elematch_mass_b.clear();
      jet_elematch_dxy_b.clear(); jet_elematch_dz_b.clear(); jet_elematch_id_b.clear();  jet_elematch_charge_b.clear();
      jet_elematch_idscore_b.clear();
      
      for(size_t pfele = 0; pfele < electron4V_fromPF.size(); pfele++){
	float minDR = 1000;
	int ipos = -1;
	for(size_t iele = 0; iele < electron_pt->size(); iele++){	
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(electron_pt->at(iele),electron_eta->at(iele),electron_phi->at(iele),electron_mass->at(iele));
	  float dR = tmp4V.DeltaR(electron4V_fromPF.at(pfele));
	  if(dR < dRMatchingPF and dR < minDR){
	    minDR = dR;
	    ipos = iele;
	  }
	}
	if(ipos >= 0) {
	  jet_elematch_pt_b.push_back(electron_pt->at(ipos));
	  jet_elematch_eta_b.push_back(electron_eta->at(ipos));
	  jet_elematch_phi_b.push_back(electron_phi->at(ipos));
	  jet_elematch_mass_b.push_back(electron_mass->at(ipos));
	  jet_elematch_charge_b.push_back(electron_charge->at(ipos));
	  jet_elematch_dxy_b.push_back(electron_d0->at(ipos));
	  jet_elematch_dz_b.push_back(electron_dz->at(ipos));
	  jet_elematch_id_b.push_back(electron_id->at(ipos));
	  jet_elematch_idscore_b.push_back(electron_idscore->at(ipos));
	}
	else{
	  jet_elematch_pt_b.push_back(electron4V_fromPF.at(pfele).Pt());
	  jet_elematch_eta_b.push_back(electron4V_fromPF.at(pfele).Eta());
	  jet_elematch_phi_b.push_back(electron4V_fromPF.at(pfele).Phi());
	  jet_elematch_mass_b.push_back(electron4V_fromPF.at(pfele).M());
	  jet_elematch_charge_b.push_back(0);
	  jet_elematch_dxy_b.push_back(-1);
	  jet_elematch_dz_b.push_back(-1);
	  jet_elematch_id_b.push_back(-1);
	  jet_elematch_idscore_b.push_back(-1);
	}
      }

      // Matching with photon by considering only the highest pt one
      std::sort(photon4V_fromPF.begin(),photon4V_fromPF.end(),
		[&](const TLorentzVector & a, const TLorentzVector & b){
		  return a.Pt() > b.Pt();
		});
      
      jet_phomatch_pt_b.clear(); jet_phomatch_eta_b.clear(); jet_phomatch_phi_b.clear(); jet_phomatch_mass_b.clear();
      jet_phomatch_id_b.clear(); jet_phomatch_idscore_b.clear();
      
      for(size_t pfpho = 0; pfpho < photon4V_fromPF.size(); pfpho++){
	float minDR = 1000;
	int ipos = -1;
	for(size_t ipho = 0; ipho < photon_pt->size(); ipho++){	
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(photon_pt->at(ipho),photon_eta->at(ipho),photon_phi->at(ipho),photon_mass->at(ipho));
	  float dR = tmp4V.DeltaR(photon4V_fromPF.at(pfpho));
	  if(dR < dRMatchingPF and dR < minDR){
	    minDR = dR;
	    ipos = ipho;
	  }
	}
	if(ipos >= 0) {
	  jet_phomatch_pt_b.push_back(photon_pt->at(ipos));
	  jet_phomatch_eta_b.push_back(photon_eta->at(ipos));
	  jet_phomatch_phi_b.push_back(photon_phi->at(ipos));
	  jet_phomatch_mass_b.push_back(photon_mass->at(ipos));
	  jet_phomatch_id_b.push_back(photon_id->at(ipos));
	  jet_phomatch_idscore_b.push_back(photon_idscore->at(ipos));
	}
	else{
	  jet_phomatch_pt_b.push_back(photon4V_fromPF.at(pfpho).Pt());
	  jet_phomatch_eta_b.push_back(photon4V_fromPF.at(pfpho).Eta());
	  jet_phomatch_phi_b.push_back(photon4V_fromPF.at(pfpho).Phi());
	  jet_phomatch_mass_b.push_back(photon4V_fromPF.at(pfpho).M());
	  jet_phomatch_id_b.push_back(-1);
	  jet_phomatch_idscore_b.push_back(-1);	
	}
      }
      tree_out->Fill();
    }
  }
  std::cout<<"ntupleCreation --> thread "<<workerID<<" stopping "<<std::endl;
};
    
    
// Main function
int main(int argc, char **argv){

  boost::program_options::options_description desc("Main options");
  desc.add_options()
    ("inputFileList,i", boost::program_options::value<std::string>(&inputFileList)->default_value(""), "File that contains a list of ROOT files to be processed")
    ("inputFileDIR,d", boost::program_options::value<std::string>(&inputFileDIR)->default_value(""), "Directory that contained files to process")
    ("outputDIR,o", boost::program_options::value<std::string>(&outputDIR)->default_value(""), "Output directory where files need to be created")
    ("outputFileName,f", boost::program_options::value<std::string>(&outputFileName)->default_value(""), "Base name for the output file")
    ("nThreads,n", boost::program_options::value<unsigned int>(&nThreads)->default_value(1), "Number of threads to be used")
    ("maxNumberOfFilesToBeProcessed,max", boost::program_options::value<unsigned int>(&maxNumberOfFilesToBeProcessed)->default_value(1), "When running giving an input directory, split execution in chunks of Nfiles")
    ("mergeThreadOutputFiles,merge", boost::program_options::value<bool>(&mergeThreadOutputFiles)->default_value(false), "Merge the output files of the job into a single one")
    ("jetPtMin,jptmin", boost::program_options::value<float>(&jetPtMin)->default_value(180), "Min jet pT to apply")
    ("jetEtaMax,jetamax", boost::program_options::value<float>(&jetEtaMax)->default_value(2.5), "Max jet eta to apply")
    ("jetEtaMin,jetamin", boost::program_options::value<float>(&jetEtaMin)->default_value(0.), "Min jet eta to apply")
    ("jetMassTruthMin,jmass", boost::program_options::value<float>(&jetMassTruthMin)->default_value(20), "Min mass selection to apply based on the truth definition given in the code")
    ("pfCandPtMin,pfptmin", boost::program_options::value<float>(&pfCandPtMin)->default_value(0.), "Minimum pf candidate pt")
    ("losttrackPtMin,ltrkptmin", boost::program_options::value<float>(&losttrackPtMin)->default_value(1.), "Minimum lost track pt")
    ("pfCandPuppiWeightMin,puppiwmin", boost::program_options::value<float>(&pfCandPuppiWeightMin)->default_value(0.), "Minimum pf candidate puppi weight")
    ("sample-type,s", boost::program_options::value<std::string>(&sample_name)->default_value(""), "Type of physics sample")
    ("useXRootD,x", boost::program_options::value<bool>(&useXRootD)->default_value(true), "Read files via xrootd protocol instead of local-mount")
    ("saveOnlyGenMatchedJets", boost::program_options::value<bool>(&saveOnlyGenMatchedJets)->default_value(true), "save or not jets matched to GEN jets")
    ("saveOnlyResonanceMatchedJets", boost::program_options::value<bool>(&saveOnlyResonanceMatchedJets)->default_value(false), "In case of signal type of sample, save only jets matched to a resonance")
    ("saveLeptonOppositeFlavor", boost::program_options::value<bool>(&saveLeptonOppositeFlavor)->default_value(true), "save or not jets matched to two GEN leptons of opposite flavour")
    ("compressOutputFile,c", boost::program_options::value<bool>(&compressOutputFile)->default_value(false), "Compress output file to save space")
    ("help,h", "Produce help message interface");

  boost::program_options::variables_map options;
  
  try{
    boost::program_options::store(boost::program_options::command_line_parser(argc,argv).options(desc).run(),options);
    boost::program_options::notify(options);
  }
  catch(std::exception &ex) {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cerr << "Use makeSkimmedNtuplesForTrainingAK8 --help to get a list of all the allowed options"  << std::endl;
    return 999;
  } 
  catch(...) {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if(options.count("help")) {
    std::cout << "Usage: makeSkimmedNtuplesForTrainingAK8 [options]\n";
    std::cout << desc;
    return 0;
  }

  // Conversion from string to sample type                                                                                                                                                            
  sample = convertSampleType(sample_name);

  std::cout<<"makeSkimmedNtuplesForTrainingAK8.cpp Parameter Summary"<<std::endl;
  std::cout<<"inputFileList --> "<<inputFileList<<std::endl;
  std::cout<<"inputFileDIR --> "<<inputFileDIR<<std::endl;
  std::cout<<"outputDIR --> "<<outputDIR<<std::endl;
  std::cout<<"outputFileName --> "<<outputFileName<<std::endl;
  std::cout<<"nThreads --> "<<nThreads<<std::endl;
  std::cout<<"maxNumberOfFilesToBeProcessed --> "<<maxNumberOfFilesToBeProcessed<<std::endl;
  std::cout<<"mergeThreadOutputFiles --> "<<mergeThreadOutputFiles<<std::endl;
  std::cout<<"jetPtMin --> "<<jetPtMin<<std::endl;
  std::cout<<"jetEtaMax --> "<<jetEtaMax<<std::endl;
  std::cout<<"jetEtaMin --> "<<jetEtaMin<<std::endl; 
  std::cout<<"jetMassTruthMin --> "<<jetMassTruthMin<<std::endl;
  std::cout<<"pfCandPtMin --> "<<pfCandPtMin<<std::endl;
  std::cout<<"losttrackPtMin --> "<<losttrackPtMin<<std::endl;
  std::cout<<"pfCandPuppiWeightMin --> "<<pfCandPuppiWeightMin<<std::endl;
  std::cout<<"sample_type --> "<<static_cast<int>(sample)<<std::endl;
  std::cout<<"saveOnlyGenMatchedJets --> "<<saveOnlyGenMatchedJets<<std::endl;
  std::cout<<"saveOnlyResonanceMatchedJets --> "<<saveOnlyResonanceMatchedJets<<std::endl;
  std::cout<<"saveLeptonOppositeFlavor --> "<<saveLeptonOppositeFlavor<<std::endl;
  std::cout<<"compressOutputFile --> "<<compressOutputFile<<std::endl;

  if(not inputFileList.empty() and not inputFileDIR.empty()){
    std::cerr<<"You cannot set inputFileList and inputFileDIR to be simultaneously non empty --> please either provide a directory or a file list"<<std::endl;
    return 1001;
  }

  gSystem->Exec(("mkdir -p "+outputDIR).c_str());

  std::string xrootd_eos = std::string(getenv("EOS_MGM_URL"))+"//";


  // Prepare for multi-threading
  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();

  // Prepare the workflow
  if(not inputFileList.empty() and boost::filesystem::is_regular_file(inputFileList)){ // the input string is a file containing the list of ROOT files to process --> for batch mode
    
    std::vector<std::string> fileList;
    std::vector<std::shared_ptr<TFile> >  files_out;
    std::vector<std::shared_ptr<TTree> >  trees_out;      

    std::ifstream inputFile (inputFileList);
    if(inputFile.is_open()){
      std::string line;
      while (getline(inputFile,line)) {
	if(useXRootD)
	  fileList.push_back(xrootd_eos+line);
	else
	  fileList.push_back(line);
      }
    }
    inputFile.close();

    for(size_t ithread = 0; ithread < nThreads; ithread++){
      files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_thread%zu.root",ithread)).Data()),"RECREATE"));
      trees_out.emplace_back(new TTree("tree","tree"));
      if(not compressOutputFile){
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	files_out.back()->SetCompressionLevel(4);
      }
      else{
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	files_out.back()->SetCompressionLevel(6);	
      }
    }      
    
    // Count number of events to split in threads
    std::cout<<"Count total number of events to process .."<<std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::unique_ptr<TChain> tree_in  (new TChain("dnntree/tree","dnntree/tree"));
    for(size_t ifile = 0; ifile < fileList.size(); ifile++)
      tree_in->Add(fileList.at(ifile).c_str());
    long int nevents = tree_in->GetEntries();
    auto stop  = std::chrono::high_resolution_clock::now(); 
    std::chrono::duration<float> duration = stop-start;
    std::cout<<"Duration of counting loop is "<<duration.count()<<" nevets = "<<nevents<<std::endl;

    // operation/function that will be executed in parallel  
    std::cout<<"Event loop to build output tree .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::vector<std::thread> threads;
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(ntupleCreation, workerID, fileList, trees_out, nevents, nThreads);
    for (auto && worker : threads) worker.join();
    stop  = std::chrono::high_resolution_clock::now(); 
    duration = stop-start;
    std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
    std::cout<<"Total jets in input "<<nJetsTotal<<std::endl;
    std::cout<<"Jets rejected by events propertie: "<<nJetsRejectedEvents<<" fraction "<<float(nJetsRejectedEvents)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by base cuts: "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by signal resonance only: "<<nJetsRejectedSignalResCuts<<" fraction "<<float(nJetsRejectedSignalResCuts)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by jet id: "<<nJetsRejectedJetId<<" fraction "<<float(nJetsRejectedJetId)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by mu/ele/tau content: "<<nJetsRejectedLeptonContent<<" fraction "<<float(nJetsRejectedLeptonContent)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets rejected by mass cut: "<<nJetsRejectedMassCut<<" fraction "<<float(nJetsRejectedMassCut)/float(nJetsTotal)<<std::endl;
    std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
    threads.clear();
    
    std::cout<<"Writing the outout files .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::string name_list = "";
    for(size_t ifile = 0; ifile < files_out.size(); ifile++){
      files_out.at(ifile)->cd();
      name_list += files_out.at(ifile)->GetName();
      name_list += " ";
      trees_out.at(ifile)->Write("",TObject::kOverwrite);
    }
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;

    if(mergeThreadOutputFiles and files_out.size() > 1){
      std::cout<<"Merging outout files .."<<std::endl;
      std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
      gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
      std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
      std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
      unsigned int total_entries = tree->GetEntries();
      gSystem->Exec(("rm "+name_list).c_str());
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
    }   
  }
  else if(not inputFileDIR.empty() and boost::filesystem::is_directory(inputFileDIR)){ // pathInput is a real path to a directory

    std::cout<<"Build list of files in "<<inputFileDIR<<std::endl;

    std::vector<std::string> fileList;

    for(auto const & entry :  boost::filesystem::recursive_directory_iterator(inputFileDIR)){
      TString filePath (entry.path().string());
      if(filePath.Contains(".root")){
	if(useXRootD)
	  fileList.push_back(xrootd_eos+entry.path().string());
	else
	  fileList.push_back(entry.path().string());
      }
    }      

    // split the job into n-steps (blocks) each containing n-files 
    unsigned int numberOfFiles = fileList.size();
    unsigned int numberOfBlocks = 1;  
    if(numberOfFiles > maxNumberOfFilesToBeProcessed){
      if(numberOfFiles%maxNumberOfFilesToBeProcessed == 0)
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed;
      else
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed+1;
    }

    std::cout<<"In iterative mode --> Number of files "<<numberOfFiles<<" numberOfBlocks "<<numberOfBlocks<<std::endl;

    // Loop on the blocks
    for(unsigned int iblock = 0; iblock < numberOfBlocks; iblock++){          
      std::vector<std::string> fileList_block;
      if(maxNumberOfFilesToBeProcessed == 1){
	for(size_t ifile = 0; ifile < fileList.size(); ifile++)
	  fileList_block.push_back(fileList.at(ifile));      
      }
      else{
	for(size_t ifile = iblock*maxNumberOfFilesToBeProcessed; ifile < std::min(fileList.size(),size_t((iblock+1)*maxNumberOfFilesToBeProcessed)); ifile++)
	  fileList_block.push_back(fileList.at(ifile));
      }    
      std::cout<<"Block number "<<iblock<<" number of files "<<fileList_block.size()<<std::endl;
      
      // Build job list and output files --> minimum compression to speed-up the training hjob
      std::vector<std::shared_ptr<TFile> >  files_out;
      std::vector<std::shared_ptr<TTree> >  trees_out;      
      for(unsigned int ithread = 0; ithread < nThreads; ithread++){
	files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_block%d_thread%d.root",iblock,ithread)).Data()),"RECREATE"));
	trees_out.emplace_back(new TTree("tree","tree"));
	if(not compressOutputFile){
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	  files_out.back()->SetCompressionLevel(4);
	}
	else{
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	  files_out.back()->SetCompressionLevel(6);	  
	}
      }      

      // Count number of events to split in threads
      std::cout<<"Count total number of events to process .."<<std::endl;
      auto start = std::chrono::high_resolution_clock::now();
      std::unique_ptr<TChain> tree_in  (new TChain("dnntree/tree","dnntree/tree"));
      for(size_t ifile = 0; ifile < fileList_block.size(); ifile++)
	tree_in->Add(fileList_block.at(ifile).c_str());
      long int nevents = tree_in->GetEntries();
      auto stop  = std::chrono::high_resolution_clock::now(); 
      std::chrono::duration<float> duration = stop-start;
      std::cout<<"Duration of counting loop is "<<duration.count()<<" nevents = "<<nevents<<std::endl;

      nJetsTotal = 0;
      nJetsRejectedEvents = 0;
      nJetsRejectedBaseCuts = 0;
      nJetsRejectedSignalResCuts = 0;
      nJetsRejectedJetId = 0;
      nJetsRejectedLeptonContent = 0;
      nJetsRejectedMassCut = 0;
      nJetsTraining = 0;
      
      // operation/function that will be executed in parallel  
      start = std::chrono::high_resolution_clock::now();
      std::vector<std::thread> threads;
      for (auto workerID : ROOT::TSeqI(nThreads))
	threads.emplace_back(ntupleCreation, workerID, fileList_block, trees_out, nevents, nThreads);
      for (auto && worker : threads) 
	worker.join();
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
      std::cout<<"Total jets in input "<<nJetsTotal<<std::endl;
      std::cout<<"Jets rejected by events propertie: "<<nJetsRejectedEvents<<" fraction "<<float(nJetsRejectedEvents)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by base cuts: "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by signal resonance only: "<<nJetsRejectedSignalResCuts<<" fraction "<<float(nJetsRejectedSignalResCuts)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by jet id: "<<nJetsRejectedJetId<<" fraction "<<float(nJetsRejectedJetId)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by mu/ele/tau content: "<<nJetsRejectedLeptonContent<<" fraction "<<float(nJetsRejectedLeptonContent)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by mass cut: "<<nJetsRejectedMassCut<<" fraction "<<float(nJetsRejectedMassCut)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
      threads.clear();      

      std::cout<<"Writing the outout files .."<<std::endl;
      start = std::chrono::high_resolution_clock::now();
      std::string name_list = "";
      for(size_t ifile = 0; ifile < files_out.size(); ifile++){
	files_out.at(ifile)->cd();
	name_list += files_out.at(ifile)->GetName();
	name_list += " ";
	trees_out.at(ifile)->Write("",TObject::kOverwrite);
      }
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;
      
      if(mergeThreadOutputFiles and files_out.size() > 1){
	std::cout<<"Merging outout files .."<<std::endl;
	std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
	gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
	std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
	std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
	unsigned int total_entries = tree->GetEntries();
	gSystem->Exec(("rm "+name_list).c_str());
	stop  = std::chrono::high_resolution_clock::now(); 
	duration = stop-start;
	std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
      } 
    }
  }
  std::cout<<"Exiting from the code"<<std::endl;

}
