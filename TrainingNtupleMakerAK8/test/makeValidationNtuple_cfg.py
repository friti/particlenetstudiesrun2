### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'muonPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtMin',180.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for AK8 jets');

options.register (
    'dumpOnlyJetMatchedToGen', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'minimum pt for reco PF candidates');

options.register (
    'applyJECs', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply updated JECs on the fly via pat modules');

options.register (
    'applyTauEnergyScale', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply tau energy scale corrections');

options.register (
    'useFastSVFit', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'use fast SVFit');


options.parseArguments()

# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(0)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
else:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')

####################
#### Apply JECs ####
####################

if options.applyJECs:
    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    process.patJetCorrFactorsProducer = JetCorrFactorsProducer.clone();
    process.patJetCorrFactorsProducer.extraJPTOffset = cms.string('L1FastJet');
    if options.isMC:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute');
    else:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute','L2L3Residual');
    process.patJetCorrFactorsProducer.payload = cms.string('AK8PFPuppi');
    process.patJetCorrFactorsProducer.primaryVertices = cms.InputTag("offlineSlimmedPrimaryVertices");
    process.patJetCorrFactorsProducer.rho  = cms.InputTag("fixedGridRhoFastjetAll");
    process.patJetCorrFactorsProducer.useNPV = cms.bool(True);
    process.patJetCorrFactorsProducer.useRho = cms.bool(True);
    process.patJetCorrFactorsProducer.src = cms.InputTag("slimmedJetsAK8");

    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    process.slimmedJetsAK8Calibrated = pdatedPatJets.clone(
        jetSource = 'slimmedJetsAK8',
        addJetCorrFactors = ( True if options.applyJECs else False),
        jetCorrFactorsSource = "patJetCorrFactorsProducer"
    )
    
 
##########################    
### Evaluate the version of particleNet
##########################

pnetDiscriminators = [];
process.pfParticleNetAK8LastJetTagInfos = cms.EDProducer("ParticleNetFeatureEvaluator",
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsAK8Calibrated" if options.applyJECs else "slimmedJetsAK8"),
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        taus = cms.InputTag("slimmedTaus"),
        jet_radius = cms.double(0.8),
        min_jet_pt = cms.double(options.jetPtMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                            
        use_puppiP4 = cms.bool(False),
        min_puppi_wgt = cms.double(-1),
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK8LastJetTags = boostedJetONNXJetTagsProducer.clone();
process.pfParticleNetAK8LastJetTags.src = cms.InputTag("pfParticleNetAK8LastJetTagInfos");
process.pfParticleNetAK8LastJetTags.flav_names = cms.vstring('probHtt','probHtm','probHte','probHbb','probHcc','probHqq','probHgg','probQCD2hf','probQCD1hf','probQCD0hf','masscorr');
process.pfParticleNetAK8LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParticleNetAK8/Puppi/PNETUL/ClassReg/preprocess.json');
process.pfParticleNetAK8LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParticleNetAK8/Puppi/PNETUL/ClassReg/particle-net.onnx');
pnetDiscriminators.extend([
    "pfParticleNetAK8LastJetTags:probHtt",
    "pfParticleNetAK8LastJetTags:probHtm",
    "pfParticleNetAK8LastJetTags:probHte",
    "pfParticleNetAK8LastJetTags:probHbb",
    "pfParticleNetAK8LastJetTags:probHcc",
    "pfParticleNetAK8LastJetTags:probHqq",
    "pfParticleNetAK8LastJetTags:probHgg",
    "pfParticleNetAK8LastJetTags:probQCD2hf",
    "pfParticleNetAK8LastJetTags:probQCD1hf",
    "pfParticleNetAK8LastJetTags:probQCD0hf",
    "pfParticleNetAK8LastJetTags:masscorr"
])

## Update final jet collection                                                                                                                                                                 
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
## original mass regression
from RecoBTag.FeatureTools.pfDeepBoostedJetTagInfos_cfi import pfDeepBoostedJetTagInfos
process.pfParticleNetAK8JetTagInfos = pfDeepBoostedJetTagInfos.clone(
    jet_radius = 0.8,
    min_pt_for_track_properties = 0.95,
    min_jet_pt = options.jetPtMin,
    max_jet_eta = options.jetEtaMax,
    use_puppiP4 = False,
    min_puppi_wgt = -1,
    vertices = "offlineSlimmedPrimaryVertices",
    secondary_vertices = "slimmedSecondaryVertices",
    pf_candidates = "packedPFCandidates",
    jets = "slimmedJetsAK8Calibrated" if options.applyJECs else "slimmedJetsAK8",
    puppi_value_map = "",
    vertex_associator = ""
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetMassRegressionJetTags = boostedJetONNXJetTagsProducer.clone(
    src = 'pfParticleNetAK8JetTagInfos',
    preprocess_json = 'RecoBTag/Combined/data/ParticleNetAK8/MassRegression/V01/preprocess.json',
    model_path = 'RecoBTag/Combined/data/ParticleNetAK8/MassRegression/V01/particle-net.onnx',
    flav_names = ["mass"]
)

process.slimmedJetsAK8Upadted = updatedPatJets.clone(
    jetSource = ("slimmedJetsAK8Calibrated" if options.applyJECs else "slimmedJetsAK8"),
    addJetCorrFactors = False,
    discriminatorSources = pnetDiscriminators
)

process.slimmedJetsAK8Upadted.discriminatorSources.extend(["pfParticleNetMassRegressionJetTags:mass"]);

### SV fit
process.selectedMuons = cms.EDFilter("PATMuonRefSelector",
    src = cms.InputTag("slimmedMuons"),
    cut = cms.string("pt > "+str(options.muonPtMin)+" && isLooseMuon && abs(eta) < 2.4"),
)

process.selectedElectrons = cms.EDFilter("PATElectronRefSelector",
    src = cms.InputTag("slimmedElectrons"),
    cut = cms.string("pt > "+str(options.electronPtMin)+" && abs(eta) < 2.5 && electronID('mvaEleID-Fall17-iso-V2-wp90')"),
)

process.selectedTaus = cms.EDFilter("PATTauRefSelector",
    src = cms.InputTag("slimmedTaus"),
    cut = cms.string("pt > "+str(options.tauPtMin)+" && abs(eta) < 2.5 && tauID('byVVLooseDeepTau2017v2p1VSjet') && tauID('byVLooseDeepTau2017v2p1VSmu') && tauID('byVVLooseDeepTau2017v2p1VSe')")
)

process.selectedLeptons = cms.EDProducer("CandViewMerger",
    src = cms.VInputTag(
        cms.InputTag("selectedMuons"),
        cms.InputTag("selectedElectrons"),
        cms.InputTag("selectedTaus")
    )
)

process.selectedLeptonPairs = cms.EDProducer("CandViewShallowCloneCombiner",
    decay = cms.string('selectedLeptons@+ selectedLeptons@-'),
    cut   = cms.string('mass > 0'),
    checkCharge = cms.bool(True)
)

#### add gen jets with neutrino
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
process.genParticlesForJets = genParticlesForJets.clone(
    src = cms.InputTag("packedGenParticles")
)

from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
process.ak4GenJetsWithNu = ak4GenJets.clone( 
    src = "genParticlesForJets" 
)

#### Final dumper
process.dnntree = cms.EDAnalyzer('ValidationTreeMakerAK8',
    #### General flags
    xsec  = cms.double(options.xsec),
    dumpOnlyJetMatchedToGen = cms.bool(options.dumpOnlyJetMatchedToGen),
    pnetDiscriminatorLabels = cms.vstring(),                                 
    ### Object selection 
    muonPtMin         = cms.double(options.muonPtMin),
    electronPtMin     = cms.double(options.electronPtMin),
    tauPtMin          = cms.double(options.tauPtMin),
    jetPtMin          = cms.double(options.jetPtMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    genParticles      = cms.InputTag("prunedGenParticles"),
    ### miniAOD objects
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    taus              = cms.InputTag("slimmedTaus"),
    boostedTaus       = cms.InputTag("slimmedTausBoosted"),
    jets              = cms.InputTag("slimmedJetsAK8Upadted"),
    met               = cms.InputTag("slimmedMETsUpdated") if options.applyJECs else cms.InputTag("slimmedMETs"),
    genJets           = cms.InputTag("slimmedGenJets"),                                 
    genJetsWithNu     = cms.InputTag("ak4GenJetsWithNu"),                                 
    genSoftDropSubJets = cms.InputTag("slimmedGenJetsAK8SoftDropSubJets"),
    ### svfit computation
    tauESFileName       = cms.FileInPath("RecoTauTag/data/TauES/TauES_dm_DeepTau2017v2p1VSjet_2018ReReco.root"),
    tauFESFileName      = cms.FileInPath("RecoTauTag/data/TauES/TauFES_eta-dm_DeepTau2017v2p1VSe_2018ReReco.root"),
    applyTauEnergyScale = cms.bool(options.applyTauEnergyScale),                                 
    useFastSVFit        = cms.bool(options.useFastSVFit),
    svFitLeptonCandidates = cms.InputTag("selectedLeptonPairs"),                                 
)

for element in pnetDiscriminators:
    element = element.split(":")[-1];
    process.dnntree.pnetDiscriminatorLabels.append(element);

########
process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

########
process.dnnTreePath = cms.Path(process.dnntree,process.edTask)

