### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os

options = VarParsing ('python')
options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');
options.register(
    'jetPtCut',200.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet pT threshold');
options.register(
    'jetEtaCut',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet eta threshold');
options.register(
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet eta threshold');
options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');
options.parseArguments()

process = cms.Process("PNETTIMING")

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(8),
    numberOfStreams = cms.untracked.uint32(0)
)

process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
else:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')

#### Infere standard PNET in release
from RecoBTag.FeatureTools.pfDeepBoostedJetTagInfos_cfi import pfDeepBoostedJetTagInfos
process.pfParticleNetAK8JetTagInfos = pfDeepBoostedJetTagInfos.clone(
    jet_radius = 0.8,
    min_jet_pt  = cms.double(options.jetPtCut),
    max_jet_eta = cms.double(options.jetEtaCut),
    min_pt_for_track_properties = 0.95,
    use_puppiP4 = False,
    min_puppi_wgt = -1,
    vertices = "offlineSlimmedPrimaryVertices",
    secondary_vertices = "slimmedSecondaryVertices",
    pf_candidates = "packedPFCandidates",
    jets = "slimmedJetsAK8",
    puppi_value_map = "",
    vertex_associator = ""
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK8JetTags = boostedJetONNXJetTagsProducer.clone(
    src = 'pfParticleNetAK8JetTagInfos',
    preprocess_json = 'RecoBTag/Combined/data/ParticleNetAK8/MD-2prong/V01/preprocess.json',
    model_path = 'RecoBTag/Combined/data/ParticleNetAK8/MD-2prong/V01/particle-net.onnx',
    flav_names = ["probHbb",  "probHcc",  "probHqq",   "probQCDbb",  "probQCDcc", "probQCDb", "probQCDc", "probQCDother"]
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK8MassRegJetTags = boostedJetONNXJetTagsProducer.clone(
    src = 'pfParticleNetAK8JetTagInfos',
    preprocess_json = 'RecoBTag/Combined/data/ParticleNetAK8/MassRegression/V01/preprocess.json',
    model_path = 'RecoBTag/Combined/data/ParticleNetAK8/MassRegression/V01/particle-net.onnx',
    flav_names = ["mass"]
)

#### Infere new versions of PNET
process.pfParticleNetAK8LastJetTagInfos = cms.EDProducer("ParticleNetFeatureEvaluator",
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsAK8"),
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        taus = cms.InputTag("slimmedTaus"),
        jet_radius = cms.double(0.8),
        min_jet_pt = cms.double(options.jetPtCut),
        max_jet_eta = cms.double(options.jetEtaCut),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                               
        use_puppiP4 = cms.bool(False),
        min_puppi_wgt = cms.double(-1),
        include_neutrals = cms.bool(True),
        flip_ip_sign = cms.bool(False),
        sort_by_sip2dsig = cms.bool(False),
        sip3dSigMax = cms.double(-1),
        min_pt_for_track_properties = cms.double(-1),
        dump_feature_tree = cms.bool(False)
)

process.pfParticleNetAK8LastJetTags =  boostedJetONNXJetTagsProducer.clone(
    src = cms.InputTag("pfParticleNetAK8LastJetTagInfos"),
    flav_names = cms.vstring('probHtt','probHtm','probHte','probHbb','probHcc','probHqq','probHgg','probQCD2hf','probQCD1hf','probQCD0hf','masscorr'),
    preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParticleNetAK8/Puppi/PNETUL/ClassReg/preprocess.json'),
    model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK8/data/ParticleNetAK8/Puppi/PNETUL/ClassReg/particle-net.onnx')
)

from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsAK8Updated = updatedPatJets.clone(
    jetSource = "slimmedJetsAK8",
    addJetCorrFactors = False,
    discriminatorSources = cms.VInputTag(
        cms.InputTag("pfParticleNetAK8JetTags:probHbb"),
        cms.InputTag("pfParticleNetAK8JetTags:probHcc"),
        cms.InputTag("pfParticleNetAK8JetTags:probHqq"),
        cms.InputTag("pfParticleNetAK8JetTags:probQCDbb"),
        cms.InputTag("pfParticleNetAK8JetTags:probQCDcc"),
        cms.InputTag("pfParticleNetAK8JetTags:probQCDb"),
        cms.InputTag("pfParticleNetAK8JetTags:probQCDc"),
        cms.InputTag("pfParticleNetAK8JetTags:probQCDother"),
        cms.InputTag("pfParticleNetAK8MassRegJetTags:mass"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHtt"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHtm"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHte"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHbb"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHcc"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHqq"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probHgg"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probQCD2hf"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probQCD1hf"),
        cms.InputTag("pfParticleNetAK8LastJetTags:probQCD0hf"),
        cms.InputTag("pfParticleNetAK8LastJetTags:masscorr"),
    )
)

process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

process.path = cms.Path(
    process.slimmedJetsAK8Updated,process.edTask
)

process.output = cms.OutputModule( "PoolOutputModule",
    fileName = cms.untracked.string( options.outputName ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(4),
    eventAutoFlushCompressedSize = cms.untracked.int32(31457280),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string( 'RECO' ),
        filterName = cms.untracked.string( '' )
    ),
    overrideBranchesSplitLevel = cms.untracked.VPSet(),
    outputCommands = cms.untracked.vstring(
        'drop *',
        'keep *_*slimmedJetsAK8Updated*_*_*',
    )
)

process.endpath = cms.EndPath(process.output);
process.schedule = cms.Schedule(process.path,process.endpath);
