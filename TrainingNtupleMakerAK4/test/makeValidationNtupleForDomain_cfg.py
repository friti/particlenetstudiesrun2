### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys, warnings

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register (
    'era',"2018",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'era that identifies the data taking period: 2016PreVFP, 2016PostVBF, 2017, 2018, 2022, 2023, 2024');

options.register(
    'analysisRegion', "dimuon", VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'string that indicates which pre-selection to apply. The allowed values are: dimuon, dielectron, emu, mutau, etau, ttcharm, dijet'
);

options.register(
    'evaluatePNETModels','base',VarParsing.multiplicity.list, VarParsing.varType.string,
    'Name of the PNET models to be considered. Possible values are: base, k0p01, k0p1, k0p25w2, k0p5, k0p35w3, k0p35a2p0w3, k0p5emu, k0p5mtau, k0p35splitw3'
);

options.register(
    'evaluateParTModels','base',VarParsing.multiplicity.list, VarParsing.varType.string,
    'Name of the ParT models to be considered. Possible values are: base, k0p35a2p0w3, k0p35splitw3, k0p35, k0p25, k0p25adjv3, k0p25adjv4, k0p25adjv4fgsm'
);

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'muonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'photonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for photons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtMin',25.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for jets');

options.register (
    'usePuppiJets',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'use puppi jets instead of CHS one');

options.register (
    'dRJetGenMatch',0.4,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets'
);

options.register (
    'applyJECs', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply updated JECs on the fly via pat modules');

options.register (
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'minimum pt for reco PF candidates'
);

options.register (
    'lostTrackPtMin',1.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for lost tracks'
);

options.register (
    'dRLostTrackJet',0.2,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum dR between lost track and jet axis'
);

options.register(
    'lheProducer', "externalLHEProducer", VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'name of the module producing the LHE record in the event'
);

options.register(
    'roccorData', 'ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/RochesterCorrections/RoccoR2018UL.txt', VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'location of rochester correction files via edm::FilePath'
);

options.register (
    'selectOnParTScore', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply selection on parT score instead of pnet')

options.parseArguments()

if options.era not in ['2016PreVFP','2016PostVFP','2017','2018','2022','2023', '2024']:
    sys.exit("Invalid era abort program");

if options.analysisRegion not in ['dimuon','dielectron','emu','mutau','etau','ttcharm','dijet']:
    sys.exit("analysis region not known  --> retry");

for model in options.evaluatePNETModels:
    if model not in ['base', 'k0p01', 'k0p1', 'k0p25w2', 'k0p5', 'k0p35w3', 'k0p35a2p0w3', 'k0p5emu', 'k0p5mtau', 'k0p35splitw3']:
        warnings.warn("PNET model to evaluate not known  --> model-key "+model+" --> skip it");
        options.evaluatePNETModels.remove(model)
print("Evaluate PNET for the following models:",options.evaluatePNETModels);
for model in options.evaluateParTModels:
    if model not in ['base', 'k0p35a2p0w3', 'k0p35splitw3', 'k0p35', 'k0p25', 'k0p25adjv3', 'k0p25adjv4','k0p25adjv4fgsm']:
        warnings.warn("ParT model to evaluate not known  --> model-key "+model+" --> skip it");
        options.evaluateParTModels.remove(model)
print("Evaluate ParT for the following models:",options.evaluateParTModels);
    
# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    if options.era == "2024":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2024_realistic', '')
    elif options.era == "2023":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2023_realistic', '')
    elif options.era == "2022EE":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic_postEE', '')
    elif options.era == "2022":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic', '')
    elif options.era == "2018":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
    elif options.era == "2017":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2017_realistic', '')
    elif options.era == "2016PreVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc_pre_vfp', '')
    elif options.era == "2016PostVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc', '')
else:
    if options.era in ['2023','2022EE','2022']:
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run3_data','')
    else:
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')


##### Jet energy correction application if requested
if options.applyJECs:
    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    process.patJetCorrFactorsProducer = JetCorrFactorsProducer.clone();
    process.patJetCorrFactorsProducer.extraJPTOffset = cms.string('L1FastJet');
    if options.isMC:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute');
    else:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute','L2L3Residual');
    if options.usePuppiJets:
        process.patJetCorrFactorsProducer.levels.remove('L1FastJet');
    process.patJetCorrFactorsProducer.payload = cms.string('AK4PFPuppi' if options.usePuppiJets else 'AK4PFchs');
    process.patJetCorrFactorsProducer.primaryVertices = cms.InputTag("offlineSlimmedPrimaryVertices");
    process.patJetCorrFactorsProducer.rho  = cms.InputTag("fixedGridRhoFastjetAll");
    process.patJetCorrFactorsProducer.useNPV = cms.bool(True);
    process.patJetCorrFactorsProducer.useRho = cms.bool(True);
    process.patJetCorrFactorsProducer.src = cms.InputTag("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets");
    
    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    process.slimmedJetsCalibrated = updatedPatJets.clone(
        jetSource = 'slimmedJetsPuppi' if options.usePuppiJets else "slimmedJets",
        addJetCorrFactors = ( True if options.applyJECs else False),
        jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsProducer"))
    )

    from PhysicsTools.PatUtils.tools.runMETCorrectionsAndUncertainties import runMetCorAndUncFromMiniAOD
    runMetCorAndUncFromMiniAOD(process,
       isData  = not options.isMC,
       postfix = "Updated"
    )

### Re-run pileup jet id                                                                                                                                                                           
from RecoJets.JetProducers.PileupJetID_cfi import pileupJetId
process.pileupJetIdUpdated = pileupJetId.clone(
    jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets")),
    inputIsCorrected = True,
    applyJec = False,
    vertexes = cms.InputTag("offlineSlimmedPrimaryVertices"),
)

if options.era == "2018":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL18
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL18)
elif options.era == "2017":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL17
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL17)
elif options.era == "2016PostVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16)
elif options.era == "2016PreVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16APV
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16APV)

### Rochester corrections for muons
process.RandomNumberGeneratorService = cms.Service("RandomNumberGeneratorService",
    correctedMuons = cms.PSet(
        initialSeed = cms.untracked.uint32(1),
        engineName  = cms.untracked.string('TRandom3')
    )
)

######## Correct muons with the Rochester corrections                                                                                                                                                 
process.correctedMuons = cms.EDProducer("RochesterCorrectedMuonProducer",
    src     = cms.InputTag("slimmedMuons"),
    gens    = cms.InputTag("prunedGenParticles"),
    data    = cms.FileInPath(options.roccorData),
    isMC    = cms.bool(options.isMC),
)

### produce GEN jets with neutrinos
if options.isMC:
    from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
    process.genParticlesForJets = genParticlesForJets.clone(
        src = cms.InputTag("packedGenParticles")
    )
    
    from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
    process.ak4GenJetsWithNu = ak4GenJets.clone(
        src = "genParticlesForJets"
    )

## deep tau evaluation
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.                                                                                                                        
tauIdEmbedder.runTauID()

### Evaluate particle-net regression + classification                                                                                                                                                 
pnetDiscriminatorNames = [];
pnetDiscriminatorLabels = [];
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer

if options.evaluatePNETModels:

    process.pfParticleNetAK4LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets")),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.4),
        min_jet_pt = cms.double(options.jetPtMin),
        min_jet_eta = cms.double(options.jetEtaMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                              
        min_pt_for_track_properties = cms.double(-1),
        min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
        max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
        min_pt_for_taus = cms.double(options.tauPtMin),
        max_eta_for_taus = cms.double(2.5),
        dump_feature_tree = cms.bool(False)
    )    


    model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegDomain/"
    output_nodes = ['probmu','probele',
                    'probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p',
                    'probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu']

    for pnet_version in options.evaluatePNETModels:
        setattr(process,"pfParticleNetAK4"+pnet_version+"JetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParticleNetAK4LastJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+pnet_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-net-"+pnet_version+".onnx"),
            debugMode = cms.untracked.bool(True)
        ))
        for node in output_nodes:
            pnetDiscriminatorLabels.append(pnet_version+"_"+node);
            pnetDiscriminatorNames.append("pfParticleNetAK4"+pnet_version+"JetTags"+":"+node);
        

## ParT discriminators
parTDiscriminatorNames = [];
parTDiscriminatorLabels = [];

if options.evaluateParTModels:

    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParTFeatureEvaluator_cfi import ParTFeatureEvaluator
    process.pfParTAK4LastJetTagInfos = ParTFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets")),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.4),
        min_jet_pt = cms.double(options.jetPtMin),
        min_jet_eta = cms.double(options.jetEtaMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                              
        min_pt_for_track_properties = cms.double(-1),
        min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
        max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
        min_pt_for_taus = cms.double(options.tauPtMin),
        max_eta_for_taus = cms.double(2.5),
    )    
        
    model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/"
    output_nodes = ['probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p',
                    'probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu']

    for parT_version in options.evaluateParTModels:
        setattr(process,"pfParTAK4"+parT_version+"JetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParTAK4LastJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+parT_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-transformer-"+parT_version+".onnx"),
            debugMode = cms.untracked.bool(True)
        ))
        for node in output_nodes:
            parTDiscriminatorLabels.append(parT_version+"_"+node);
            parTDiscriminatorNames.append("pfParTAK4"+parT_version+"JetTags"+":"+node);
    
## Update final jet collection                                                                                                                                                    
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = "slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets"),
    addJetCorrFactors = False,
)
process.slimmedJetsUpdated.userData.userInts.src += ['pileupJetIdUpdated:fullId'];
if pnetDiscriminatorNames:
    process.slimmedJetsUpdated.discriminatorSources = pnetDiscriminatorNames;
if parTDiscriminatorNames:
    process.slimmedJetsUpdated.discriminatorSources += parTDiscriminatorNames;

#### GEN trees only for MC
process.gentree = cms.EDAnalyzer("WeightsTreeMaker",
        lheInfo = cms.InputTag(options.lheProducer),
        genInfo = cms.InputTag("generator"),
        pileupInfo = cms.InputTag("slimmedAddPileupInfo"),
        lheRunInfo = cms.InputTag(options.lheProducer),
        genLumiInfo = cms.InputTag("generator"),
)

#### Final Dumper
process.dnntree = cms.EDAnalyzer('ValidationTreeMakerAK4',
    #### General flags
    xsec              = cms.double(options.xsec),
    isMC              = cms.bool(options.isMC),
    dumpOnlyJetMatchedToGen  = cms.bool(False),
    usePuppiJets      = cms.bool(options.usePuppiJets),
    pnetDiscriminatorLabels = cms.vstring(),
    pnetDiscriminatorNames  = cms.vstring(),
    parTDiscriminatorLabels = cms.vstring(),
    parTDiscriminatorNames  = cms.vstring(),
    ### Object selection (applied on miniAOD, HLT and Gen objects for jets)
    muonPtMin         = cms.double(options.muonPtMin),
    muonEtaMax        = cms.double(2.4),
    electronPtMin     = cms.double(options.electronPtMin),
    electronEtaMax    = cms.double(2.5),
    photonPtMin       = cms.double(options.photonPtMin),
    photonEtaMax      = cms.double(2.5),
    tauPtMin          = cms.double(options.tauPtMin),
    tauEtaMax         = cms.double(2.5),
    jetPtMin          = cms.double(options.jetPtMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    jetEtaMin         = cms.double(options.jetEtaMin),
    dRJetGenMatch     = cms.double(options.dRJetGenMatch),
    ### lepton pairs
    leptonPairs       = cms.InputTag(""),                                 
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    lheInfo           = cms.InputTag(options.lheProducer),
    genParticles      = cms.InputTag("prunedGenParticles"),
    ### miniAOD objects
    triggerResults    = cms.InputTag("TriggerResults","", "HLT"),
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    taus              = cms.InputTag("slimmedTausUpdated"),
    photons           = cms.InputTag("slimmedPhotons"),
    jets              = cms.InputTag("slimmedJetsUpdated"),
    met               = cms.InputTag("slimmedMETsUpdated") if options.applyJECs else cms.InputTag("slimmedMETs"),
    ### Gen jets  
    genJets           = cms.InputTag("slimmedGenJets"),
    genJetsWithNu     = cms.InputTag("ak4GenJetsWithNu"),
    genJetsFlavour    = cms.InputTag("slimmedGenJetsFlavourInfos"),                                 
)

for element in pnetDiscriminatorNames:
    process.dnntree.pnetDiscriminatorNames.append(element);
for element in pnetDiscriminatorLabels:
    process.dnntree.pnetDiscriminatorLabels.append(element);
for element in parTDiscriminatorNames:
    process.dnntree.parTDiscriminatorNames.append(element);
for element in parTDiscriminatorLabels:
    process.dnntree.parTDiscriminatorLabels.append(element);

    
## selection for the various analysis regions
if options.analysisRegion == "dimuon":
    print ("add dimuon process selection")
    process.load('ParticleNetStudiesRun2.TrainingNtupleMakerAK4.dimuon_skim_cff');
    process.dnntree.leptonPairs = cms.InputTag("dimuonPairs")
elif options.analysisRegion == "dielectron":
    print ("add dielectron process selection")
    process.load('ParticleNetStudiesRun2.TrainingNtupleMakerAK4.dielectron_skim_cff');
    process.dnntree.leptonPairs = cms.InputTag("dielectronPairs")
elif options.analysisRegion == "emu":
    print ("add emu process selection")
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.emu_skim_cff import emuSelection
    process = emuSelection(process,"pfParTAK4"+options.evaluateParTModels[0]+"JetTags" if options.selectOnParTScore else
                           "pfParticleNetAK4"+options.evaluatePNETModels[0]+"JetTags")
    process.dnntree.leptonPairs = cms.InputTag("emuPairs")
elif options.analysisRegion == "mutau":
    print ("add mutau process selection")
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.mutau_skim_cff import mutauSelection
    process = mutauSelection(process,"pfParTAK4"+options.evaluateParTModels[0]+"JetTags" if options.selectOnParTScore else
                             "pfParticleNetAK4"+options.evaluatePNETModels[0]+"JetTags")
    process.dnntree.leptonPairs = cms.InputTag("mutauPairs")
elif options.analysisRegion == "etau":
    print ("add etau process selection")
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.etau_skim_cff import etauSelection
    process = etauSelection(process,"pfParTAK4"+options.evaluateParTModels[0]+"JetTags" if options.selectOnParTScore else
                            "pfParticleNetAK4"+options.evaluatePNETModels[0]+"JetTags")
    process.dnntree.leptonPairs = cms.InputTag("etauPairs")
elif options.analysisRegion == "ttcharm":
    print ("add ttcharm process selection")
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ttcharm_skim_cff import ttcharmSelection
    process = ttcharmSelection(process,"pfParTAK4"+options.evaluateParTModels[0]+"JetTags" if options.selectOnParTScore else
                               "pfParticleNetAK4"+options.evaluatePNETModels[0]+"JetTags")
    process.dnntree.leptonPairs = cms.InputTag("leptonJetPairs")
elif options.analysisRegion == "dijet":
    print ("add dijet process selection")
    process.load('ParticleNetStudiesRun2.TrainingNtupleMakerAK4.dijet_skim_cff');
    process.dnntree.leptonPairs = cms.InputTag("dijetPairs")

# task
process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

# path
if options.isMC:
    process.path = cms.Path(process.gentree+
                            process.leptonSelection*
                            process.jetSelection*
                            process.dnntree,
                            process.edTask)
else:
    process.path = cms.Path(process.leptonSelection*
                            process.jetSelection*
                            process.dnntree,
                            process.edTask)
