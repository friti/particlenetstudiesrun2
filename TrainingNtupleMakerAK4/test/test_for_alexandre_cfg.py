### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys, warnings

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register (
    'era',"2018",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'era that identifies the data taking period: 2016PreVFP, 2016PostVBF, 2017, 2018');

options.register(
    'evaluateParTModels','',VarParsing.multiplicity.list, VarParsing.varType.string,
    'Name of the ParT models to be considered. Possible values are: base, k0p25'
);

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'jetPtMin',25.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for jets');

options.parseArguments()

if options.era != "2018"  and options.era != "2017"  and options.era != "2016PostVFP" and options.era != "2016PreVFP":
    sys.exit("Invalid era abort program");

for model in options.evaluateParTModels:
    if model not in ['base', 'k0p35a2p0w3', 'k0p35splitw3', 'k0p35', 'k0p25']:
        warnings.warn("ParT model to evaluate not known  --> model-key "+model+" --> skip it");
        options.evaluateParTModels.remove(model)
print("Evaluate ParT for the following models:",options.evaluateParTModels);
    
# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    if options.era == "2018":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
    elif options.era == "2017":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2017_realistic', '')
    elif options.era == "2016PreVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc_pre_vfp', '')
    elif options.era == "2016PostVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc', '')
else:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')


### produce GEN jets with neutrinos
if options.isMC:
    from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
    process.genParticlesForJets = genParticlesForJets.clone(
        src = cms.InputTag("packedGenParticles")
    )
    
    from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
    process.ak4GenJetsWithNu = ak4GenJets.clone(
        src = "genParticlesForJets"
    )

## deep tau evaluation
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.                                                                                                                        
tauIdEmbedder.runTauID()

### Evaluate ParT regression + classification                                                                                                                                                 
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer

parTDiscriminatorNames = [];
parTDiscriminatorLabels = [];

if options.evaluateParTModels:

    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParTFeatureEvaluator_cfi import ParTFeatureEvaluator
    process.pfParTAK4LastJetTagInfos = ParTFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJets"),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.4),
        min_jet_pt = cms.double(options.jetPtMin),
        min_jet_eta = cms.double(options.jetEtaMin),
        max_jet_eta = cms.double(options.jetEtaMax),
    )    
        
    model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/"
    output_nodes = ['probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p',
                    'probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu']

    for parT_version in options.evaluateParTModels:
        setattr(process,"pfParTAK4"+parT_version+"JetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParTAK4LastJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+parT_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-transformer-"+parT_version+".onnx"),
            debugMode = cms.untracked.bool(True)
        ))
        for node in output_nodes:
            parTDiscriminatorLabels.append(parT_version+"_"+node);
            parTDiscriminatorNames.append("pfParTAK4"+parT_version+"JetTags"+":"+node);
    
## Update final jet collection                                                                                                                                                    
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = "slimmedJets",
    addJetCorrFactors = False,
)
if parTDiscriminatorNames:
    process.slimmedJetsUpdated.discriminatorSources += parTDiscriminatorNames;

#### GEN trees only for MC
process.gentree = cms.EDAnalyzer("WeightsTreeMaker",
        lheInfo = cms.InputTag("externalLHEProducer"),
        genInfo = cms.InputTag("generator"),
        pileupInfo = cms.InputTag("slimmedAddPileupInfo"),
        lheRunInfo = cms.InputTag("externalLHEProducer"),
        genLumiInfo = cms.InputTag("generator"),
)

#### Final Dumper
process.dnntree = cms.EDAnalyzer('ValidationTreeMakerAK4',
    #### General flags
    xsec              = cms.double(options.xsec),
    isMC              = cms.bool(options.isMC),
    dumpOnlyJetMatchedToGen  = cms.bool(False),
    parTDiscriminatorLabels = cms.vstring(),
    parTDiscriminatorNames  = cms.vstring(),
    ### Object selection (applied on miniAOD, HLT and Gen objects for jets)
    jetPtMin          = cms.double(options.jetPtMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    jetEtaMin         = cms.double(options.jetEtaMin),
    ### lepton pairs
    leptonPairs       = cms.InputTag(""),                                 
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    genParticles      = cms.InputTag("prunedGenParticles"),
    ### miniAOD objects
    triggerResults    = cms.InputTag("TriggerResults","", "HLT"),
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    taus              = cms.InputTag("slimmedTaus"),
    photons           = cms.InputTag("slimmedPhotons"),
    jets              = cms.InputTag("slimmedJetsUpdated"),
    met               = cms.InputTag("slimmedMETs"),
    ### Gen jets  
    genJets           = cms.InputTag("slimmedGenJets"),
    genJetsWithNu     = cms.InputTag("ak4GenJetsWithNu"),
    genJetsFlavour    = cms.InputTag("slimmedGenJetsFlavourInfos"),                                 
)

for element in parTDiscriminatorNames:
    process.dnntree.parTDiscriminatorNames.append(element);
for element in parTDiscriminatorLabels:
    process.dnntree.parTDiscriminatorLabels.append(element);
    
# task
process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

# path
if options.isMC:
    process.path = cms.Path(process.gentree+
                            process.dnntree,
                            process.edTask)
else:
    process.path = cms.Path(process.dnntree,
                            process.edTask)
