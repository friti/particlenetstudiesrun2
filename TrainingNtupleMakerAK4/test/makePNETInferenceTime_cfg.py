### CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os

options = VarParsing ('python')
options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');
options.register(
    'jetPtMin',15.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet pT threshold');
options.register(
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'tau pT threshold');
options.register(
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet eta threshold');
options.register(
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet eta threshold');
options.register(
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'jet eta threshold');
options.register (
    'lostTrackPtMin',1.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for lost tracks');
options.register (
    'dRLostTrackJet',0.2,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum dR between lost track and jet axis');
options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.parseArguments()

process = cms.Process("PNETTIMING")

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(8),
    numberOfStreams = cms.untracked.uint32(8)
)

process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
else:
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')


#### inputs needed for bjet energy regression
process.bJetVars = cms.EDProducer("JetRegressionVarProducer",
    pvsrc = cms.InputTag("offlineSlimmedPrimaryVertices"),
    src   = cms.InputTag("slimmedJets"),
    svsrc = cms.InputTag("slimmedSecondaryVertices"),
)

process.jercVars = cms.EDProducer("BetaStarPackedCandidateVarProducer",
    srcJet = cms.InputTag("slimmedJets"),
    srcPF = cms.InputTag("packedPFCandidates"),
    maxDR = cms.double(0.4)
)

process.slimmedJetsWithUserData = cms.EDProducer("PATJetUserDataEmbedder",
   src = cms.InputTag("slimmedJets"),
   userFloats = cms.PSet(
       leadTrackPt = cms.InputTag("bJetVars:leadTrackPt"),
       leptonPtRelv0 = cms.InputTag("bJetVars:leptonPtRelv0"),
       leptonPtRelInvv0 = cms.InputTag("bJetVars:leptonPtRelInvv0"),
       leptonDeltaR = cms.InputTag("bJetVars:leptonDeltaR"),
       vtxPt = cms.InputTag("bJetVars:vtxPt"),
       vtxMass = cms.InputTag("bJetVars:vtxMass"),
       vtx3dL = cms.InputTag("bJetVars:vtx3dL"),
       vtx3deL = cms.InputTag("bJetVars:vtx3deL"),
       ptD = cms.InputTag("bJetVars:ptD"),
       chFPV0EF = cms.InputTag("jercVars:chargedFromPV0EnergyFraction"),
   ),
     userInts = cms.PSet(
         vtxNtrk = cms.InputTag("bJetVars:vtxNtrk"),
         leptonPdgId = cms.InputTag("bJetVars:leptonPdgId"),
     ),
)

process.bjetRegressionNN = cms.EDProducer("BJetEnergyRegressionMVA",
        backend = cms.string("ONNX"),
        src = cms.InputTag("slimmedJetsWithUserData"),
        pvsrc = cms.InputTag("offlineSlimmedPrimaryVertices"),
        svsrc = cms.InputTag("slimmedSecondaryVertices"),
        rhosrc = cms.InputTag("fixedGridRhoFastjetAll"),
        weightFile =  cms.FileInPath("PhysicsTools/NanoAOD/data/breg_training_2018.onnx"),
        name = cms.string("JetRegNN"),
        isClassifier = cms.bool(False),
        variablesOrder = cms.vstring([
            "Jet_pt","Jet_eta","rho","Jet_mt","Jet_leadTrackPt","Jet_leptonPtRel","Jet_leptonDeltaR","Jet_neHEF",
            "Jet_neEmEF","Jet_vtxPt","Jet_vtxMass","Jet_vtx3dL","Jet_vtxNtrk","Jet_vtx3deL",
            "Jet_numDaughters_pt03","Jet_energyRing_dR0_em_Jet_rawEnergy","Jet_energyRing_dR1_em_Jet_rawEnergy",
            "Jet_energyRing_dR2_em_Jet_rawEnergy","Jet_energyRing_dR3_em_Jet_rawEnergy","Jet_energyRing_dR4_em_Jet_rawEnergy",
            "Jet_energyRing_dR0_neut_Jet_rawEnergy","Jet_energyRing_dR1_neut_Jet_rawEnergy","Jet_energyRing_dR2_neut_Jet_rawEnergy",
            "Jet_energyRing_dR3_neut_Jet_rawEnergy","Jet_energyRing_dR4_neut_Jet_rawEnergy","Jet_energyRing_dR0_ch_Jet_rawEnergy",
            "Jet_energyRing_dR1_ch_Jet_rawEnergy","Jet_energyRing_dR2_ch_Jet_rawEnergy","Jet_energyRing_dR3_ch_Jet_rawEnergy",
            "Jet_energyRing_dR4_ch_Jet_rawEnergy","Jet_energyRing_dR0_mu_Jet_rawEnergy","Jet_energyRing_dR1_mu_Jet_rawEnergy",
            "Jet_energyRing_dR2_mu_Jet_rawEnergy","Jet_energyRing_dR3_mu_Jet_rawEnergy","Jet_energyRing_dR4_mu_Jet_rawEnergy",
            "Jet_chHEF","Jet_chEmEF","Jet_leptonPtRelInv","isEle","isMu","isOther","Jet_mass","Jet_ptd"
        ]),
        variables = cms.PSet(
           Jet_pt = cms.string("pt*jecFactor('Uncorrected')"),
           Jet_mt = cms.string("mt*jecFactor('Uncorrected')"),
           Jet_eta = cms.string("eta"),
           Jet_mass = cms.string("mass*jecFactor('Uncorrected')"),
           Jet_ptd = cms.string("userFloat('ptD')"),
           Jet_leadTrackPt = cms.string("userFloat('leadTrackPt')"),
           Jet_vtxNtrk = cms.string("userInt('vtxNtrk')"),
           Jet_vtxMass = cms.string("userFloat('vtxMass')"),
           Jet_vtx3dL = cms.string("userFloat('vtx3dL')"),
           Jet_vtx3deL = cms.string("userFloat('vtx3deL')"),
           Jet_vtxPt = cms.string("userFloat('vtxPt')"),
           Jet_leptonPtRel = cms.string("userFloat('leptonPtRelv0')"),
           Jet_leptonPtRelInv = cms.string("userFloat('leptonPtRelInvv0')*jecFactor('Uncorrected')"),
           Jet_leptonDeltaR = cms.string("userFloat('leptonDeltaR')"),
           Jet_neHEF = cms.string("neutralHadronEnergyFraction()"),
           Jet_neEmEF = cms.string("neutralEmEnergyFraction()"),
           Jet_chHEF = cms.string("chargedHadronEnergyFraction()"),
           Jet_chEmEF = cms.string("chargedEmEnergyFraction()"),
           isMu = cms.string("?abs(userInt('leptonPdgId'))==13?1:0"),
           isEle = cms.string("?abs(userInt('leptonPdgId'))==11?1:0"),
           isOther = cms.string("?userInt('leptonPdgId')==0?1:0"),
        ),
        inputTensorName = cms.string("ffwd_inp:0"),
        outputTensorName = cms.string("ffwd_out/BiasAdd:0"),
        outputNames = cms.vstring(["corr","res"]),
        outputFormulas = cms.vstring(["at(0)*0.27912887930870056+1.0545977354049683","0.5*(at(2)-at(1))*0.27912887930870056"])
)

process.cjetRegressionNN = process.bjetRegressionNN.clone()
process.cjetRegressionNN.weightFile = cms.FileInPath('PhysicsTools/NanoAOD/data/creg_training_2018.onnx')
process.cjetRegressionNN.variablesOrder = cms.vstring([
        "Jet_pt","Jet_eta","rho","Jet_mt","Jet_leadTrackPt","Jet_leptonPtRel","Jet_leptonDeltaR",
        "Jet_neHEF","Jet_neEmEF","Jet_vtxPt","Jet_vtxMass","Jet_vtx3dL","Jet_vtxNtrk","Jet_vtx3deL",
        "Jet_numDaughters_pt03","Jet_chEmEF","Jet_chHEF", "Jet_ptd","Jet_mass",
        "Jet_energyRing_dR0_em_Jet_rawEnergy","Jet_energyRing_dR1_em_Jet_rawEnergy",
        "Jet_energyRing_dR2_em_Jet_rawEnergy","Jet_energyRing_dR3_em_Jet_rawEnergy","Jet_energyRing_dR4_em_Jet_rawEnergy",
        "Jet_energyRing_dR0_neut_Jet_rawEnergy","Jet_energyRing_dR1_neut_Jet_rawEnergy","Jet_energyRing_dR2_neut_Jet_rawEnergy",
        "Jet_energyRing_dR3_neut_Jet_rawEnergy","Jet_energyRing_dR4_neut_Jet_rawEnergy","Jet_energyRing_dR0_ch_Jet_rawEnergy",
        "Jet_energyRing_dR1_ch_Jet_rawEnergy","Jet_energyRing_dR2_ch_Jet_rawEnergy","Jet_energyRing_dR3_ch_Jet_rawEnergy",
        "Jet_energyRing_dR4_ch_Jet_rawEnergy","Jet_energyRing_dR0_mu_Jet_rawEnergy","Jet_energyRing_dR1_mu_Jet_rawEnergy",
        "Jet_energyRing_dR2_mu_Jet_rawEnergy","Jet_energyRing_dR3_mu_Jet_rawEnergy","Jet_energyRing_dR4_mu_Jet_rawEnergy"]
);
process.cjetRegressionNN.outputFormulas = cms.vstring(["at(0)*0.24325256049633026+0.993854820728302","0.5*(at(2)-at(1))*0.24325256049633026"]);

#### Infere PNET-AK4
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
process.pfParticleNetAK4LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
    muons = cms.InputTag("slimmedMuons"),
    electrons = cms.InputTag("slimmedElectrons"),
    photons = cms.InputTag("slimmedPhotons"),
    taus = cms.InputTag("slimmedTaus"),
    vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
    secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
    jets = cms.InputTag("slimmedJetsWithUserData"),
    losttracks = cms.InputTag("lostTracks"),
    jet_radius = cms.double(0.4),
    min_jet_pt = cms.double(options.jetPtMin),
    min_jet_eta = cms.double(options.jetEtaMin),
    max_jet_eta = cms.double(options.jetEtaMax),
    min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                                
    min_pt_for_track_properties = cms.double(-1),
    min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
    max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
    min_pt_for_taus = cms.double(options.tauPtMin),
    max_eta_for_taus = cms.double(2.5),
    dump_feature_tree = cms.bool(False)
)

output_nodes = ['probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p','probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu']
from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK4LastJetTags =  boostedJetONNXJetTagsProducer.clone(
    src = cms.InputTag("pfParticleNetAK4LastJetTagInfos")
)
process.pfParticleNetAK4LastJetTags.flav_names = cms.vstring(output_nodes);
process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/preprocess.json');
process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/particle-net.onnx')

from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParTFeatureEvaluator_cfi import ParTFeatureEvaluator
process.pfParTAK4LastJetTagInfos = ParTFeatureEvaluator.clone(
    muons = cms.InputTag("slimmedMuons"),
    electrons = cms.InputTag("slimmedElectrons"),
    photons = cms.InputTag("slimmedPhotons"),
    taus = cms.InputTag("slimmedTaus"),
    vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
    secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
    jets = cms.InputTag("slimmedJetsWithUserData"),
    losttracks = cms.InputTag("lostTracks"),
    jet_radius = cms.double(0.4),
    min_jet_pt = cms.double(options.jetPtMin),
    min_jet_eta = cms.double(options.jetEtaMin),
    max_jet_eta = cms.double(options.jetEtaMax),
    min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                                
    min_pt_for_track_properties = cms.double(-1),
    min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
    max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
    min_pt_for_taus = cms.double(options.tauPtMin),
    max_eta_for_taus = cms.double(2.5),
)

process.pfParTAK4LastJetTags = boostedJetONNXJetTagsProducer.clone(
    src = cms.InputTag("pfParTAK4LastJetTagInfos"),
    flav_names = cms.vstring(output_nodes),
    preprocess_json = cms.string("ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/preprocess_base.json"),
    model_path = cms.FileInPath("ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/particle-transformer-base.onnx"),
)


import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2017v2p1"]) #other tauIDs can be added in parallel.                                                                                       
tauIdEmbedder.runTauID()

from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = "slimmedJetsWithUserData",
    addJetCorrFactors = False,
    discriminatorSources = cms.VInputTag(
        cms.InputTag("pfParticleNetAK4LastJetTags:probmu"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probele"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaup1h0p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaup1h1p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaup1h2p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaup3h0p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaup3h1p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaum1h0p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaum1h1p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaum1h2p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaum3h0p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probtaum3h1p"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probb"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probc"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probuds"),
        cms.InputTag("pfParticleNetAK4LastJetTags:probg"),
        cms.InputTag("pfParticleNetAK4LastJetTags:ptcorr"),
        cms.InputTag("pfParticleNetAK4LastJetTags:ptreslow"),
        cms.InputTag("pfParticleNetAK4LastJetTags:ptreshigh"),
        cms.InputTag("pfParticleNetAK4LastJetTags:ptnu"),
        cms.InputTag("pfParTAK4LastJetTags:probmu"),
        cms.InputTag("pfParTAK4LastJetTags:probele"),
        cms.InputTag("pfParTAK4LastJetTags:probtaup1h0p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaup1h1p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaup1h2p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaup3h0p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaup3h1p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaum1h0p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaum1h1p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaum1h2p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaum3h0p"),
        cms.InputTag("pfParTAK4LastJetTags:probtaum3h1p"),
        cms.InputTag("pfParTAK4LastJetTags:probb"),
        cms.InputTag("pfParTAK4LastJetTags:probc"),
        cms.InputTag("pfParTAK4LastJetTags:probuds"),
        cms.InputTag("pfParTAK4LastJetTags:probg"),
        cms.InputTag("pfParTAK4LastJetTags:ptcorr"),
        cms.InputTag("pfParTAK4LastJetTags:ptreslow"),
        cms.InputTag("pfParTAK4LastJetTags:ptreshigh"),
        cms.InputTag("pfParTAK4LastJetTags:ptnu")
    )
)
process.slimmedJetsUpdated.userData.userFloats.src = cms.VInputTag(
    cms.InputTag("bjetRegressionNN:corr"),
    cms.InputTag("bjetRegressionNN:res"),
    cms.InputTag("cjetRegressionNN:corr"),
    cms.InputTag("cjetRegressionNN:res")
)

process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

process.path = cms.Path(
    process.slimmedJetsUpdated+
    process.slimmedTausUpdated,process.edTask
)

process.output = cms.OutputModule( "PoolOutputModule",
    fileName = cms.untracked.string( options.outputName ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(4),
    eventAutoFlushCompressedSize = cms.untracked.int32(31457280),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string( 'RECO' ),
        filterName = cms.untracked.string( '' )
    ),
    overrideBranchesSplitLevel = cms.untracked.VPSet(),
    outputCommands = cms.untracked.vstring(
        'drop *',
        'keep *_*slimmedJetsUpdated*_*_*',
    )
)

process.endpath = cms.EndPath(process.output);
process.schedule = cms.Schedule(process.path,process.endpath);
