## CMSSW command line parameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register (
    'era',"2018",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'era that identifies the data taking period: 2016PreVFP, 2016PostVBF, 2017, 2018, 2022, 2023, 2024');

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'muonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'photonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for photons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtMin',25.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for AK4 jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for AK4 jets');

options.register (
    'usePuppiJets',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'use puppi jets instead of CHS one');

options.register(
    'reRunPuppi',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True run last puppi tune');

options.register (
    'dumpOnlyJetMatchedToGen', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'minimum pt for reco PF candidates');

options.register (
    'lostTrackPtMin',1.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for lost tracks');

options.register (
    'dRLostTrackJet',0.2,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum dR between lost track and jet axis');

options.register (
    'dRJetGenMatch',0.4,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets');

options.register (
    'applyJECs', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply updated JECs on the fly via pat modules');

options.register (
    'sqliteJECs', "", VarParsing.multiplicity.singleton, VarParsing.varType.string,
    'sqlite files with JECs to override GT');


options.register(
    'evaluatePNETModels','base',VarParsing.multiplicity.list, VarParsing.varType.string,
    'Name of the PNET models to be considered. Possible values are: default, base, k0p01, k0p1, k0p25w2, k0p5, k0p35w3, k0p35a2p0w3, k0p5emu, k0p5mtau, k0p35splitw3'
);

options.register(
    'evaluateParTModels','base',VarParsing.multiplicity.list, VarParsing.varType.string,
    'Name of the ParT models to be considered. Possible values are: base, k0p35a2p0w3, k0p35splitw3, k0p35, k0p25, k0p25adjv4, k0p25adjv4, k0p25adjv4fgsm'
);

options.register (
    'evaluateForwardPNETTraining', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'evalaute the forward PNET network instead of the central one')

options.parseArguments()

if options.era not in ['2016PreVFP','2016PostVFP','2017','2018','2022','2023', '2024']:
    sys.exit("Invalid era abort program");

if not options.evaluatePNETModels or not options.evaluateParTModels:
    sys.exit("No PNET or ParT models given to evaluate --> abort the execution");

for model in options.evaluatePNETModels:
    if model not in ['default','base', 'k0p01', 'k0p1', 'k0p25w2', 'k0p5', 'k0p35w3', 'k0p35a2p0w3', 'k0p5emu', 'k0p5mtau', 'k0p35splitw3']:
        warnings.warn("pnet model to evaluate not known  --> model-key "+model+" --> skip it");
        options.evaluatePNETModels.remove(model)
print("Evaluate PNET for the following models:",options.evaluatePNETModels);

for model in options.evaluateParTModels:
    if model not in ['base', 'k0p35a2p0w3', 'k0p35splitw3', 'k0p35', 'k0p25', 'k0p25adjv3', 'k0p25adjv4', 'k0p25adjv4fgsm']:
        warnings.warn("ParT model to evaluate not known  --> model-key "+model+" --> skip it");
        options.evaluateParTModels.remove(model)
print("Evaluate ParT for the following models:",options.evaluateParTModels);

        
# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    if options.era == "2024":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2024_realistic', '')
    elif options.era == "2023":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2023_realistic', '')
    elif options.era == "2022EE":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic_postEE', '')
    elif options.era == "2022":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic', '')
    elif options.era == "2018":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
    elif options.era == "2017":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2017_realistic', '')
    elif options.era == "2016PreVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc_pre_vfp', '')
    elif options.era == "2016PostVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc', '')
else:
    if options.era in ['2023','2022EE','2022']:
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run3_data','')
    else:
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')

#### add gen jets with neutrino
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
process.genParticlesForJets = genParticlesForJets.clone(
    src = cms.InputTag("packedGenParticles")
)

from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
process.ak4GenJetsWithNu = ak4GenJets.clone( 
    src = "genParticlesForJets" 
)

## deep tau evaluation
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.
tauIdEmbedder.runTauID()


#### Apply JECs
if options.applyJECs:
    print("Calibration of jets with: local file ",options.sqliteJECs,"  GT for era ",options.era);
    if options.sqliteJECs:
        from CondCore.CondDB.CondDB_cfi import CondDB
        CondDB.__delattr__('connect')
        process.jec = cms.ESSource("PoolDBESSource",
                CondDB,
                connect = cms.string("sqlite_file:"+options.sqliteJECs),
                toGet =  cms.VPSet(
                    cms.PSet(
                        record = cms.string("JetCorrectionsRecord"),
                        tag = cms.string("JetCorrectorParametersCollection_Summer20UL18_V2_MC_AK4PFPuppi"),
                        label= cms.untracked.string("AK4PFPuppi")
                    ),
                    cms.PSet(
                    record = cms.string("JetCorrectionsRecord"),
                        tag = cms.string("JetCorrectorParametersCollection_Summer20UL18_V2_MC_AK4PFchs"),
                        label= cms.untracked.string("AK4PFchs")
                    )
            )
        )
        process.es_prefer_jec = cms.ESPrefer("PoolDBESSource",'jec')

    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    process.patJetCorrFactorsProducer = JetCorrFactorsProducer.clone();
    process.patJetCorrFactorsProducer.extraJPTOffset = cms.string('L1FastJet');
    if options.isMC:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute');        
    else:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute','L2L3Residual');
    process.patJetCorrFactorsProducer.payload = cms.string('AK4PFPuppi' if options.usePuppiJets else 'AK4PFchs');
    process.patJetCorrFactorsProducer.primaryVertices = cms.InputTag("offlineSlimmedPrimaryVertices");
    process.patJetCorrFactorsProducer.rho  = cms.InputTag("fixedGridRhoFastjetAll");
    process.patJetCorrFactorsProducer.useNPV = cms.bool(True);
    process.patJetCorrFactorsProducer.useRho = cms.bool(True);
    process.patJetCorrFactorsProducer.src = cms.InputTag('slimmedJetsPuppi' if options.usePuppiJets else 'slimmedJets');

    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    process.slimmedJetsCalibrated = updatedPatJets.clone(
        jetSource = 'slimmedJetsPuppi' if options.usePuppiJets else 'slimmedJets',
        addJetCorrFactors = (True if options.applyJECs else False),
        jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsProducer"))
    )
    

from RecoJets.JetProducers.PileupJetID_cfi import pileupJetId
process.pileupJetIdUpdated = pileupJetId.clone(
    jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets")),
    inputIsCorrected = True,
    applyJec = False,
    vertexes = cms.InputTag("offlineSlimmedPrimaryVertices"),
)

if options.era == "2018":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL18
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL18)
elif options.era == "2017":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL17
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL17)
elif options.era == "2016PostVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16)
elif options.era == "2016PreVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16APV
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16APV)

process.bJetVars = cms.EDProducer("JetRegressionVarProducer",
        pvsrc = cms.InputTag("offlineSlimmedPrimaryVertices"),
        src   = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs  else ('slimmedJetsPuppi' if options.usePuppiJets else 'slimmedJets')),
        svsrc = cms.InputTag("slimmedSecondaryVertices")
)


process.slimmedJetsWithUserData = cms.EDProducer("PATJetUserDataEmbedder",
        src = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs  else ('slimmedJetsPuppi' if options.usePuppiJets else 'slimmedJets')),
        userFloats = cms.PSet(
            leadTrackPt = cms.InputTag("bJetVars:leadTrackPt"),
            leptonPtRelv0 = cms.InputTag("bJetVars:leptonPtRelv0"), 
            leptonPtRelInvv0 = cms.InputTag("bJetVars:leptonPtRelInvv0"),
            leptonDeltaR = cms.InputTag("bJetVars:leptonDeltaR"),
            vtxPt = cms.InputTag("bJetVars:vtxPt"),
            vtxMass = cms.InputTag("bJetVars:vtxMass"),
            vtx3dL = cms.InputTag("bJetVars:vtx3dL"),
            vtx3deL = cms.InputTag("bJetVars:vtx3deL"),
            ptD = cms.InputTag("bJetVars:ptD"),
            puIdNanoDisc = cms.InputTag('pileupJetIdUpdated:fullDiscriminant'),
        ),
        userInts = cms.PSet(
            vtxNtrk = cms.InputTag("bJetVars:vtxNtrk"),
            leptonPdgId = cms.InputTag("bJetVars:leptonPdgId"),
            puIdNanoId = cms.InputTag('pileupJetIdUpdated:fullId'),
        ),
)

process.bjetRegressionNN = cms.EDProducer("BJetEnergyRegressionMVA",
        backend = cms.string("ONNX"),
        src = cms.InputTag("slimmedJetsWithUserData"),
        pvsrc = cms.InputTag("offlineSlimmedPrimaryVertices"),
        svsrc = cms.InputTag("slimmedSecondaryVertices"),
        rhosrc = cms.InputTag("fixedGridRhoFastjetAll"),
        weightFile =  cms.FileInPath("PhysicsTools/NanoAOD/data/breg_training_2018.onnx"),
        name = cms.string("JetRegNN"),
        isClassifier = cms.bool(False),
        variablesOrder = cms.vstring([
            "Jet_pt","Jet_eta","rho","Jet_mt","Jet_leadTrackPt","Jet_leptonPtRel","Jet_leptonDeltaR","Jet_neHEF",
            "Jet_neEmEF","Jet_vtxPt","Jet_vtxMass","Jet_vtx3dL","Jet_vtxNtrk","Jet_vtx3deL",
            "Jet_numDaughters_pt03","Jet_energyRing_dR0_em_Jet_rawEnergy","Jet_energyRing_dR1_em_Jet_rawEnergy",
            "Jet_energyRing_dR2_em_Jet_rawEnergy","Jet_energyRing_dR3_em_Jet_rawEnergy","Jet_energyRing_dR4_em_Jet_rawEnergy",
            "Jet_energyRing_dR0_neut_Jet_rawEnergy","Jet_energyRing_dR1_neut_Jet_rawEnergy","Jet_energyRing_dR2_neut_Jet_rawEnergy",
            "Jet_energyRing_dR3_neut_Jet_rawEnergy","Jet_energyRing_dR4_neut_Jet_rawEnergy","Jet_energyRing_dR0_ch_Jet_rawEnergy",
            "Jet_energyRing_dR1_ch_Jet_rawEnergy","Jet_energyRing_dR2_ch_Jet_rawEnergy","Jet_energyRing_dR3_ch_Jet_rawEnergy",
            "Jet_energyRing_dR4_ch_Jet_rawEnergy","Jet_energyRing_dR0_mu_Jet_rawEnergy","Jet_energyRing_dR1_mu_Jet_rawEnergy",
            "Jet_energyRing_dR2_mu_Jet_rawEnergy","Jet_energyRing_dR3_mu_Jet_rawEnergy","Jet_energyRing_dR4_mu_Jet_rawEnergy",
            "Jet_chHEF","Jet_chEmEF","Jet_leptonPtRelInv","isEle","isMu","isOther","Jet_mass","Jet_ptd"
        ]),
        variables = cms.PSet(
           Jet_pt = cms.string("pt*jecFactor('Uncorrected')"),
           Jet_mt = cms.string("mt*jecFactor('Uncorrected')"),
           Jet_eta = cms.string("eta"),
           Jet_mass = cms.string("mass*jecFactor('Uncorrected')"),
           Jet_ptd = cms.string("userFloat('ptD')"),
           Jet_leadTrackPt = cms.string("userFloat('leadTrackPt')"),
           Jet_vtxNtrk = cms.string("userInt('vtxNtrk')"),
           Jet_vtxMass = cms.string("userFloat('vtxMass')"),
           Jet_vtx3dL = cms.string("userFloat('vtx3dL')"),
           Jet_vtx3deL = cms.string("userFloat('vtx3deL')"),
           Jet_vtxPt = cms.string("userFloat('vtxPt')"),
           Jet_leptonPtRel = cms.string("userFloat('leptonPtRelv0')"),
           Jet_leptonPtRelInv = cms.string("userFloat('leptonPtRelInvv0')*jecFactor('Uncorrected')"),
           Jet_leptonDeltaR = cms.string("userFloat('leptonDeltaR')"),
           Jet_neHEF = cms.string("neutralHadronEnergyFraction()"),
           Jet_neEmEF = cms.string("neutralEmEnergyFraction()"),
           Jet_chHEF = cms.string("chargedHadronEnergyFraction()"),
           Jet_chEmEF = cms.string("chargedEmEnergyFraction()"),
           isMu = cms.string("?abs(userInt('leptonPdgId'))==13?1:0"),
           isEle = cms.string("?abs(userInt('leptonPdgId'))==11?1:0"),
           isOther = cms.string("?userInt('leptonPdgId')==0?1:0"),
        ),
        inputTensorName = cms.string("ffwd_inp:0"),
        outputTensorName = cms.string("ffwd_out/BiasAdd:0"),
        outputNames = cms.vstring(["corr","res"]),
        outputFormulas = cms.vstring(["at(0)*0.27912887930870056+1.0545977354049683","0.5*(at(2)-at(1))*0.27912887930870056"])
)


process.cjetRegressionNN = process.bjetRegressionNN.clone()
process.cjetRegressionNN.weightFile = cms.FileInPath('PhysicsTools/NanoAOD/data/creg_training_2018.onnx')
process.cjetRegressionNN.variablesOrder = cms.vstring([
    "Jet_pt","Jet_eta","rho","Jet_mt","Jet_leadTrackPt","Jet_leptonPtRel","Jet_leptonDeltaR",
    "Jet_neHEF","Jet_neEmEF","Jet_vtxPt","Jet_vtxMass","Jet_vtx3dL","Jet_vtxNtrk","Jet_vtx3deL",
    "Jet_numDaughters_pt03","Jet_chEmEF","Jet_chHEF", "Jet_ptd","Jet_mass",
    "Jet_energyRing_dR0_em_Jet_rawEnergy","Jet_energyRing_dR1_em_Jet_rawEnergy",
    "Jet_energyRing_dR2_em_Jet_rawEnergy","Jet_energyRing_dR3_em_Jet_rawEnergy","Jet_energyRing_dR4_em_Jet_rawEnergy",
    "Jet_energyRing_dR0_neut_Jet_rawEnergy","Jet_energyRing_dR1_neut_Jet_rawEnergy","Jet_energyRing_dR2_neut_Jet_rawEnergy",
    "Jet_energyRing_dR3_neut_Jet_rawEnergy","Jet_energyRing_dR4_neut_Jet_rawEnergy","Jet_energyRing_dR0_ch_Jet_rawEnergy",
    "Jet_energyRing_dR1_ch_Jet_rawEnergy","Jet_energyRing_dR2_ch_Jet_rawEnergy","Jet_energyRing_dR3_ch_Jet_rawEnergy",
    "Jet_energyRing_dR4_ch_Jet_rawEnergy","Jet_energyRing_dR0_mu_Jet_rawEnergy","Jet_energyRing_dR1_mu_Jet_rawEnergy",
    "Jet_energyRing_dR2_mu_Jet_rawEnergy","Jet_energyRing_dR3_mu_Jet_rawEnergy","Jet_energyRing_dR4_mu_Jet_rawEnergy"]
);
process.cjetRegressionNN.outputFormulas = cms.vstring(["at(0)*0.24325256049633026+0.993854820728302","0.5*(at(2)-at(1))*0.24325256049633026"]);

    

### Evaluate the version of particleNet
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer

process.pfParticleNetAK4LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
    muons = cms.InputTag("slimmedMuons"),                                                            
    electrons = cms.InputTag("slimmedElectrons"),                                                            
    photons = cms.InputTag("slimmedPhotons"),                                                            
    taus = cms.InputTag("slimmedTaus"),                                                            
    vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
    secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
    jets = cms.InputTag("slimmedJetsWithUserData"),
    losttracks = cms.InputTag("lostTracks"),                                                         
    jet_radius = cms.double(0.4),
    min_jet_pt = cms.double(options.jetPtMin),
    min_jet_eta = cms.double(options.jetEtaMin),
    max_jet_eta = cms.double(options.jetEtaMax),
    min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                            
    min_pt_for_track_properties = cms.double(-1),
    min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
    max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
    min_pt_for_taus = cms.double(options.tauPtMin),
    max_eta_for_taus = cms.double(2.5),
    dump_feature_tree = cms.bool(False)
)
process.pfParticleNetAK4LastNegativeJetTagInfos = process.pfParticleNetAK4LastJetTagInfos.clone()
process.pfParticleNetAK4LastNegativeJetTagInfos.flip_ip_sign = cms.bool(True)

pnetDiscriminatorLabels = [];
pnetDiscriminatorNames = [];
for pnet_version in options.evaluatePNETModels:
    output_nodes = ['probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p','probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu']

    if "base" in pnet_version:
        if not options.usePuppiJets and not options.evaluateForwardPNETTraining:
            model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/"
        elif not options.usePuppiJets and options.evaluateForwardPNETTraining:
            model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/"
        elif options.usePuppiJets and not options.evaluateForwardPNETTraining:
            model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/"
        elif options.usePuppiJets and options.evaluateForwardPNETTraining:
            model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/"

        setattr(process,"pfParticleNetAK4JetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParticleNetAK4LastJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess.json"),
            model_path = cms.FileInPath(model_directory+"/particle-net.onnx")
        ))
        setattr(process,"pfParticleNetAK4NegativeJetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParticleNetAK4LastNegativeJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess.json"),
            model_path = cms.FileInPath(model_directory+"/particle-net.onnx")
        ))
        for node in output_nodes:
            pnetDiscriminatorLabels.append(node);
            pnetDiscriminatorNames.append("pfParticleNetAK4JetTags"+":"+node);
        for node in output_nodes:
            pnetDiscriminatorLabels.append("neg"+node);
            pnetDiscriminatorNames.append("pfParticleNetAK4NegativeJetTags"+":"+node);
    else:
        model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegDomain/"
        
        setattr(process,"pfParticleNetAK4"+pnet_version+"JetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParticleNetAK4LastJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+pnet_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-net-"+pnet_version+".onnx")
        ))
        setattr(process,"pfParticleNetAK4"+pnet_version+"NegativeJetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParticleNetAK4LastNegativeJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+pnet_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-net-"+pnet_version+".onnx")
        ))
        for node in output_nodes:
            pnetDiscriminatorLabels.append(pnet_version+"_"+node);
            pnetDiscriminatorNames.append("pfParticleNetAK4"+pnet_version+"JetTags"+":"+node);
        for node in output_nodes:
            pnetDiscriminatorLabels.append(pnet_version+"_neg"+node);
            pnetDiscriminatorNames.append("pfParticleNetAK4"+pnet_version+"NegativeJetTags"+":"+node);

## ParT discriminators                                                                                                                                                                              
parTDiscriminatorNames = [];
parTDiscriminatorLabels = [];

if options.evaluateParTModels:
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParTFeatureEvaluator_cfi import ParTFeatureEvaluator
    process.pfParTAK4LastJetTagInfos = ParTFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsWithUserData"),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.4),
        min_jet_pt = cms.double(options.jetPtMin),
        min_jet_eta = cms.double(options.jetEtaMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                            
        min_pt_for_track_properties = cms.double(-1),
        min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
        max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
        min_pt_for_taus = cms.double(options.tauPtMin),
        max_eta_for_taus = cms.double(2.5),
    )
    process.pfParTAK4LastNegativeJetTagInfos = process.pfParTAK4LastJetTagInfos.clone();
    process.pfParTAK4LastNegativeJetTagInfos.flip_ip_sign = cms.bool(True);
    
    model_directory = "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/"
    output_nodes = ['probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p',
                    'probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu']

    for parT_version in options.evaluateParTModels:
        setattr(process,"pfParTAK4"+parT_version+"JetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParTAK4LastJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+parT_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-transformer-"+parT_version+".onnx"),
            debugMode = cms.untracked.bool(False)
        ))
        setattr(process,"pfParTAK4"+parT_version+"NegativeJetTags",boostedJetONNXJetTagsProducer.clone(
            src = cms.InputTag("pfParTAK4LastNegativeJetTagInfos"),
            flav_names = cms.vstring(output_nodes),
            preprocess_json = cms.string(model_directory+"/preprocess_"+parT_version+".json"),
            model_path = cms.FileInPath(model_directory+"/particle-transformer-"+parT_version+".onnx"),
            debugMode = cms.untracked.bool(False)
        ))
        for node in output_nodes:
            parTDiscriminatorLabels.append(parT_version+"_"+node);
            parTDiscriminatorNames.append("pfParTAK4"+parT_version+"JetTags"+":"+node);
        for node in output_nodes:
            parTDiscriminatorLabels.append(parT_version+"_neg"+node);
            parTDiscriminatorNames.append("pfParTAK4"+parT_version+"NegativeJetTags"+":"+node);
            
## Update final jet collection                                                                                                                                                                 
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = "slimmedJetsWithUserData",
    addJetCorrFactors = False,
)
if pnetDiscriminatorNames:
    process.slimmedJetsUpdated.discriminatorSources = pnetDiscriminatorNames;
if parTDiscriminatorNames:
    process.slimmedJetsUpdated.discriminatorSources += parTDiscriminatorNames;

process.slimmedJetsUpdated.userData.userFloats.src = cms.VInputTag(
    cms.InputTag("bjetRegressionNN:corr"),
    cms.InputTag("bjetRegressionNN:res"),
    cms.InputTag("cjetRegressionNN:corr"),
    cms.InputTag("cjetRegressionNN:res")
)

### re-run puppi 
if options.reRunPuppi and options.usePuppiJets:

    process.load('CommonTools/PileupAlgos/Puppi_cff')
    process.puppi.candName = cms.InputTag('packedPFCandidates')
    process.puppi.vertexName = cms.InputTag('offlineSlimmedPrimaryVertices')
    process.puppi.clonePackedCands   = cms.bool(True)
    process.puppi.useExistingWeights = cms.bool(False)

    from RecoJets.JetProducers.ak4PFJets_cfi import ak4PFJets
    process.ak4PuppiJets = ak4PFJets.clone (
        src = 'puppi',
        doAreaFastjet = True,
        jetPtMin = 5.
    )

    from PhysicsTools.PatAlgos.tools.jetTools import addJetCollection
    addJetCollection(process,
                     labelName = 'Puppi',
                     jetSource = cms.InputTag('ak4PuppiJets'),
                     algo = 'AK',
                     rParam=0.4,
                     genJetCollection=cms.InputTag('slimmedGenJets'),
                     jetCorrections = ('AK4PFPuppi', ['L2Relative', 'L3Absolute'], 'None'),
                     pfCandidates = cms.InputTag('packedPFCandidates'),
                     pvSource = cms.InputTag('offlineSlimmedPrimaryVertices'),
                     svSource = cms.InputTag('slimmedSecondaryVertices'),
                     muSource =cms.InputTag( 'slimmedMuons'),
                     elSource = cms.InputTag('slimmedElectrons')
    )
    process.patJetsPuppi.addGenPartonMatch = cms.bool(options.isMC)
    process.patJetsPuppi.addGenJetMatch = cms.bool(options.isMC)
    process.patJetPartons.particles = cms.InputTag("prunedGenParticles")
    process.patJetPartonMatchPuppi.matched = cms.InputTag("prunedGenParticles")

    process.pileupJetIdUpdated.jets = cms.InputTag("patJetsPuppi");
    process.bJetVars.src = cms.InputTag("patJetsPuppi");
    process.slimmedJetsWithUserData.src = cms.InputTag("patJetsPuppi");
    
    from PhysicsTools.PatAlgos.patPuppiJetSpecificProducer_cfi import patPuppiJetSpecificProducer 
    process.patPuppiJetSpecificProducer = patPuppiJetSpecificProducer.clone(
        src=cms.InputTag("slimmedJetsWithUserData")
    )

    process.slimmedJetsUpdated.userData.userFloats.src += ['patPuppiJetSpecificProducer:puppiMultiplicity', 'patPuppiJetSpecificProducer:neutralPuppiMultiplicity', 'patPuppiJetSpecificProducer:neutralHadronPuppiMultiplicity', 'patPuppiJetSpecificProducer:photonPuppiMultiplicity', 'patPuppiJetSpecificProducer:HFHadronPuppiMultiplicity', 'patPuppiJetSpecificProducer:HFEMPuppiMultiplicity']


#### Final dumper
process.dnntree = cms.EDAnalyzer('ValidationTreeMakerAK4',
    #### General flags
    xsec  = cms.double(options.xsec),
    dumpOnlyJetMatchedToGen = cms.bool(options.dumpOnlyJetMatchedToGen),
    usePuppiJets = cms.bool(options.usePuppiJets),
    pnetDiscriminatorLabels = cms.vstring(),                                 
    pnetDiscriminatorNames  = cms.vstring(),
    parTDiscriminatorLabels = cms.vstring(),
    parTDiscriminatorNames  = cms.vstring(),
    ### Object selection 
    muonPtMin         = cms.double(options.muonPtMin),
    electronPtMin     = cms.double(options.electronPtMin),
    photonPtMin       = cms.double(options.photonPtMin),
    tauPtMin          = cms.double(options.tauPtMin),
    jetPtMin          = cms.double(options.jetPtMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    dRJetGenMatch     = cms.double(options.dRJetGenMatch),
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    genParticles      = cms.InputTag("prunedGenParticles"),
    lheInfo           = cms.InputTag("externalLHEProducer"),
    ### miniAOD objects
    triggerResults    = cms.InputTag("TriggerResults","", "HLT"),
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    stageL1Trigger    = cms.uint32(2),
    leptonPairs       = cms.InputTag(""),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    taus              = cms.InputTag("slimmedTausUpdated"),
    photons           = cms.InputTag("slimmedPhotons"),
    jets              = cms.InputTag("slimmedJetsUpdated"),
    met               = cms.InputTag("slimmedMETs"),
    genJets           = cms.InputTag("slimmedGenJets"),                                 
    genJetsWithNu     = cms.InputTag("ak4GenJetsWithNu"),                                 
    genJetsFlavour    = cms.InputTag("slimmedGenJetsFlavourInfos")
)

for element in pnetDiscriminatorLabels:
    process.dnntree.pnetDiscriminatorLabels.append(element);
for element in pnetDiscriminatorNames:
    process.dnntree.pnetDiscriminatorNames.append(element);
for element in parTDiscriminatorNames:
    process.dnntree.parTDiscriminatorNames.append(element);
for element in parTDiscriminatorLabels:
    process.dnntree.parTDiscriminatorLabels.append(element);

########
process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

########
process.dnnTreePath = cms.Path(process.dnntree,process.edTask)

#processDumpFile = open('processDump.py', 'w')
#print (processDumpFile, process.dumpPython())
