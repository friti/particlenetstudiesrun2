### CMSSW command line arameter parser                                                                                                                                                               
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
import os, sys

options = VarParsing ('python')

options.register (
    'outputName',"tree.root",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the outputfile');

options.register (
    'processName',"DNNTREE",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'name for the process');

options.register (
    'era',"2018",VarParsing.multiplicity.singleton,VarParsing.varType.string,
    'era that identifies the data taking period: 2016PreVFP, 2016PostVBF, 2017, 2018, 2022, 2023, 2024');

options.register(
    'xsec',1.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'external value for sample cross section');

options.register(
    'isMC',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'rules if running on data or MC');

options.register(
    'reRunPuppi',True,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'if True run last puppi tune');

options.register (
    'nThreads',1,VarParsing.multiplicity.singleton, VarParsing.varType.int,
    'default number of threads');

options.register (
    'muonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for muons');

options.register (
    'electronPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for electrons');

options.register (
    'photonPtMin',10.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for photons');

options.register (
    'tauPtMin',20.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for taus');

options.register (
    'jetPtMin',25.,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for jets');

options.register (
    'jetEtaMax',2.5,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum eta for jets');

options.register (
    'jetEtaMin',0.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum eta for jets');

options.register (
    'usePuppiJets',False,VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'use puppi jets instead of CHS one');

options.register (
    'jetPFCandidatePtMin',0.1,VarParsing.multiplicity.singleton,VarParsing.varType.float,
    'minimum pt for reco PF candidates');

options.register (
    'lostTrackPtMin',1.0,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'minimum pt for lost tracks');

options.register (
    'dRLostTrackJet',0.2,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'maximum dR between lost track and jet axis');

options.register (
    'dRJetGenMatch',0.4,VarParsing.multiplicity.singleton, VarParsing.varType.float,
    'deltaR for matching reco and gen jets');

options.register (
    'dumpOnlyJetMatchedToGen', True, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'dump only jet matched to generator level ones');

options.register (
    'applyJECs', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'apply updated JECs on the fly via pat modules');

options.register (
    'useForwardPNETTraining', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'evaluate forward PNET training instead of central one')

options.register (
    'evaluateParTTraining', False, VarParsing.multiplicity.singleton, VarParsing.varType.bool,
    'evaluate ParT model and save it to the training trees')

options.parseArguments()

if options.era not in ['2016PreVFP','2016PostVFP','2017','2018','2022','2023', '2024']:
    sys.exit("Invalid era abort program");

# Define the CMSSW process
process = cms.Process(options.processName)

# Message Logger settings
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 250

# Define the input source
process.source = cms.Source("PoolSource", 
    fileNames = cms.untracked.vstring(options.inputFiles)                            
)

# Output file
process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputName)
)

# Processing setup
process.options = cms.untracked.PSet( 
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(True),
    numberOfThreads  = cms.untracked.uint32(options.nThreads),
    numberOfStreams = cms.untracked.uint32(options.nThreads)
)

process.maxEvents = cms.untracked.PSet( 
    input = cms.untracked.int32(options.maxEvents)
)

### Conditions
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load('Configuration.StandardSequences.GeometryDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('TrackingTools.TransientTrack.TransientTrackBuilder_cfi')

from Configuration.AlCa.GlobalTag import GlobalTag
if options.isMC:
    if options.era == "2024":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2024_realistic', '')
    elif options.era == "2023":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2023_realistic', '')
    elif options.era == "2022EE":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic_postEE', '')
    elif options.era == "2022":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2022_realistic', '')
    elif options.era == "2018":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')
    elif options.era == "2017":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2017_realistic', '')
    elif options.era == "2016PreVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc_pre_vfp', '')
    elif options.era == "2016PostVFP":
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_mc', '')
else:
    if options.era in ['2023','2022EE','2022']:
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run3_data','')
    else:
        process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data','')


#####
if options.applyJECs and not (options.reRunPuppi and options.usePuppiJets):
    from PhysicsTools.PatAlgos.JetCorrFactorsProducer_cfi import JetCorrFactorsProducer
    process.patJetCorrFactorsProducer = JetCorrFactorsProducer.clone();
    process.patJetCorrFactorsProducer.extraJPTOffset = cms.string('L1FastJet');
    if options.isMC:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute');
    else:
        process.patJetCorrFactorsProducer.levels = cms.vstring('L1FastJet','L2Relative','L3Absolute','L2L3Residual');
    if options.usePuppiJets:
        process.patJetCorrFactorsProducer.levels.remove('L1FastJet')
    process.patJetCorrFactorsProducer.payload = cms.string('AK4PFPuppi' if options.usePuppiJets else "AK4PFchs");
    process.patJetCorrFactorsProducer.primaryVertices = cms.InputTag("offlineSlimmedPrimaryVertices");
    process.patJetCorrFactorsProducer.rho  = cms.InputTag("fixedGridRhoFastjetAll");
    process.patJetCorrFactorsProducer.useNPV = cms.bool(True);
    process.patJetCorrFactorsProducer.useRho = cms.bool(True);
    process.patJetCorrFactorsProducer.src = cms.InputTag("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets");

    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    process.slimmedJetsCalibrated = updatedPatJets.clone(
        jetSource = 'slimmedJetsPuppi' if options.usePuppiJets else "slimmedJets",
        addJetCorrFactors = ( True if options.applyJECs else False),
        jetCorrFactorsSource = cms.VInputTag(cms.InputTag("patJetCorrFactorsProducer"))
    )

    from PhysicsTools.PatUtils.tools.runMETCorrectionsAndUncertainties import runMetCorAndUncFromMiniAOD
    runMetCorAndUncFromMiniAOD(process,
       isData  = not options.isMC,
       postfix = "Updated"
    )

### Evaluate particle-net regression + classification                    
pnetDiscriminatorNames = [];
pnetDiscriminatorLabels = [];

from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParticleNetFeatureEvaluator_cfi import ParticleNetFeatureEvaluator
process.pfParticleNetAK4LastJetTagInfos = ParticleNetFeatureEvaluator.clone(
    muons = cms.InputTag("slimmedMuons"),
    electrons = cms.InputTag("slimmedElectrons"),
    photons = cms.InputTag("slimmedPhotons"),
    taus = cms.InputTag("slimmedTaus"),
    vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
    secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
    jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if (options.usePuppiJets and not options.reRunPuppi) else ("patJetsPuppi" if (options.usePuppiJets and options.reRunPuppi) else "slimmedJets"))),
    losttracks = cms.InputTag("lostTracks"),
    jet_radius = cms.double(0.4),
    min_jet_pt = cms.double(options.jetPtMin),
    max_jet_eta = cms.double(options.jetEtaMax),
    min_jet_eta = cms.double(options.jetEtaMin),
    min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin),
    min_pt_for_track_properties = cms.double(-1),
    min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
    max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
    min_pt_for_taus = cms.double(options.tauPtMin),
    max_eta_for_taus = cms.double(2.5),
    dump_feature_tree = cms.bool(False),
    use_puppiP4 = cms.bool(False),
    puppi_weights = cms.InputTag(""),
)    

process.pfParticleNetAK4LastNegativeJetTagInfos = process.pfParticleNetAK4LastJetTagInfos.clone(
    flip_ip_sign = cms.bool(True)
)

from RecoBTag.ONNXRuntime.boostedJetONNXJetTagsProducer_cfi import boostedJetONNXJetTagsProducer
process.pfParticleNetAK4LastJetTags = boostedJetONNXJetTagsProducer.clone();
process.pfParticleNetAK4LastJetTags.src = cms.InputTag("pfParticleNetAK4LastJetTagInfos");
process.pfParticleNetAK4LastJetTags.flav_names = cms.vstring('probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p','probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu');

if options.usePuppiJets and not options.useForwardPNETTraining:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/particle-net.onnx');
elif options.usePuppiJets and options.useForwardPNETTraining:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegPuppi/particle-net.onnx');
elif not options.usePuppiJets and options.useForwardPNETTraining:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/particle-net.onnx');
elif not options.usePuppiJets and not options.useForwardPNETTraining:
    process.pfParticleNetAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/preprocess.json');
    process.pfParticleNetAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParticleNetAK4/PNETUL/ClassRegTwoTargets/particle-net.onnx');
process.pfParticleNetAK4LastJetTags.debugMode = cms.untracked.bool(False)

process.pfParticleNetAK4LastNegativeJetTags = process.pfParticleNetAK4LastJetTags.clone();
process.pfParticleNetAK4LastNegativeJetTags.src = cms.InputTag("pfParticleNetAK4LastNegativeJetTagInfos");

pnetDiscriminatorNames.extend([
    "pfParticleNetAK4LastJetTags:probmu",
    "pfParticleNetAK4LastJetTags:probele",
    "pfParticleNetAK4LastJetTags:probtaup1h0p",
    "pfParticleNetAK4LastJetTags:probtaup1h1p",
    "pfParticleNetAK4LastJetTags:probtaup1h2p",
    "pfParticleNetAK4LastJetTags:probtaup3h0p",
    "pfParticleNetAK4LastJetTags:probtaup3h1p",
    "pfParticleNetAK4LastJetTags:probtaum1h0p",
    "pfParticleNetAK4LastJetTags:probtaum1h1p",
    "pfParticleNetAK4LastJetTags:probtaum1h2p",
    "pfParticleNetAK4LastJetTags:probtaum3h0p",
    "pfParticleNetAK4LastJetTags:probtaum3h1p",
    "pfParticleNetAK4LastJetTags:probb",
    "pfParticleNetAK4LastJetTags:probc",
    "pfParticleNetAK4LastJetTags:probuds",
    "pfParticleNetAK4LastJetTags:probg",
    "pfParticleNetAK4LastJetTags:ptcorr",
    "pfParticleNetAK4LastJetTags:ptreslow",
    "pfParticleNetAK4LastJetTags:ptreshigh",
    "pfParticleNetAK4LastJetTags:ptnu",
    "pfParticleNetAK4LastNegativeJetTags:probmu",
    "pfParticleNetAK4LastNegativeJetTags:probele",
    "pfParticleNetAK4LastNegativeJetTags:probtaup1h0p",
    "pfParticleNetAK4LastNegativeJetTags:probtaup1h1p",
    "pfParticleNetAK4LastNegativeJetTags:probtaup1h2p",
    "pfParticleNetAK4LastNegativeJetTags:probtaup3h0p",
    "pfParticleNetAK4LastNegativeJetTags:probtaup3h1p",
    "pfParticleNetAK4LastNegativeJetTags:probtaum1h0p",
    "pfParticleNetAK4LastNegativeJetTags:probtaum1h1p",
    "pfParticleNetAK4LastNegativeJetTags:probtaum1h2p",
    "pfParticleNetAK4LastNegativeJetTags:probtaum3h0p",
    "pfParticleNetAK4LastNegativeJetTags:probtaum3h1p",
    "pfParticleNetAK4LastNegativeJetTags:probb",
    "pfParticleNetAK4LastNegativeJetTags:probc",
    "pfParticleNetAK4LastNegativeJetTags:probuds",
    "pfParticleNetAK4LastNegativeJetTags:probg",
    "pfParticleNetAK4LastNegativeJetTags:ptcorr",
    "pfParticleNetAK4LastNegativeJetTags:ptreslow",
    "pfParticleNetAK4LastNegativeJetTags:ptreshigh",
    "pfParticleNetAK4LastNegativeJetTags:ptnu",
    
])

pnetDiscriminatorLabels = [name.replace("pfParticleNetAK4LastJetTags:","").replace("pfParticleNetAK4LastNegativeJetTags:","neg") for name in pnetDiscriminatorNames]

## ParT training
parTDiscriminatorNames = [];
parTDiscriminatorLabels = [];

if options.evaluateParTTraining:
    
    from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.ParTFeatureEvaluator_cfi import ParTFeatureEvaluator
    process.pfParTAK4LastJetTagInfos = ParTFeatureEvaluator.clone(
        muons = cms.InputTag("slimmedMuons"),
        electrons = cms.InputTag("slimmedElectrons"),
        photons = cms.InputTag("slimmedPhotons"),
        taus = cms.InputTag("slimmedTaus"),
        vertices = cms.InputTag("offlineSlimmedPrimaryVertices"),
        secondary_vertices = cms.InputTag("slimmedSecondaryVertices"),
        jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets")),
        losttracks = cms.InputTag("lostTracks"),
        jet_radius = cms.double(0.4),
        min_jet_pt = cms.double(options.jetPtMin),
        min_jet_eta = cms.double(options.jetEtaMin),
        max_jet_eta = cms.double(options.jetEtaMax),
        min_pt_for_pfcandidates = cms.double(options.jetPFCandidatePtMin), ## arbitrary                                                                                                            
        min_pt_for_track_properties = cms.double(-1),
        min_pt_for_losttrack = cms.double(options.lostTrackPtMin),
        max_dr_for_losttrack = cms.double(options.dRLostTrackJet),
        min_pt_for_taus = cms.double(options.tauPtMin),
        max_eta_for_taus = cms.double(2.5),
    )
    process.pfParTAK4LastNegativeJetTagInfos = process.pfParTAK4LastJetTagInfos.clone();
    process.pfParTAK4LastNegativeJetTagInfos.flip_ip_sign = cms.bool(True);
    
    process.pfParTAK4LastJetTags = boostedJetONNXJetTagsProducer.clone();
    process.pfParTAK4LastJetTags.src = cms.InputTag("pfParTAK4LastJetTagInfos");
    process.pfParTAK4LastJetTags.flav_names = cms.vstring('probmu','probele','probtaup1h0p','probtaup1h1p','probtaup1h2p','probtaup3h0p','probtaup3h1p','probtaum1h0p','probtaum1h1p','probtaum1h2p','probtaum3h0p','probtaum3h1p','probb','probc','probuds','probg','ptcorr','ptreshigh','ptreslow','ptnu');
    process.pfParTAK4LastJetTags.preprocess_json = cms.string('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/preprocess_base.json');
    process.pfParTAK4LastJetTags.model_path = cms.FileInPath('ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/ParTAK4/particle-transformer-base.onnx');

    process.pfParTAK4LastNegativeJetTags = process.pfParTAK4LastJetTags.clone()
    process.pfParTAK4LastNegativeJetTags.src = cms.InputTag("pfParTAK4LastNegativeJetTagInfos");
    
    parTDiscriminatorNames.extend([
        "pfParTAK4LastJetTags:probmu",
        "pfParTAK4LastJetTags:probele",
        "pfParTAK4LastJetTags:probtaup1h0p",
        "pfParTAK4LastJetTags:probtaup1h1p",
        "pfParTAK4LastJetTags:probtaup1h2p",
        "pfParTAK4LastJetTags:probtaup3h0p",
        "pfParTAK4LastJetTags:probtaup3h1p",
        "pfParTAK4LastJetTags:probtaum1h0p",
        "pfParTAK4LastJetTags:probtaum1h1p",
        "pfParTAK4LastJetTags:probtaum1h2p",
        "pfParTAK4LastJetTags:probtaum3h0p",
        "pfParTAK4LastJetTags:probtaum3h1p",
        "pfParTAK4LastJetTags:probb",
        "pfParTAK4LastJetTags:probc",
        "pfParTAK4LastJetTags:probuds",
        "pfParTAK4LastJetTags:probg",
        "pfParTAK4LastJetTags:ptcorr",
        "pfParTAK4LastJetTags:ptreslow",
        "pfParTAK4LastJetTags:ptreshigh",
        "pfParTAK4LastJetTags:ptnu",
        "pfParTAK4LastNegativeJetTags:probmu",
        "pfParTAK4LastNegativeJetTags:probele",
        "pfParTAK4LastNegativeJetTags:probtaup1h0p",
        "pfParTAK4LastNegativeJetTags:probtaup1h1p",
        "pfParTAK4LastNegativeJetTags:probtaup1h2p",
        "pfParTAK4LastNegativeJetTags:probtaup3h0p",
        "pfParTAK4LastNegativeJetTags:probtaup3h1p",
        "pfParTAK4LastNegativeJetTags:probtaum1h0p",
        "pfParTAK4LastNegativeJetTags:probtaum1h1p",
        "pfParTAK4LastNegativeJetTags:probtaum1h2p",
        "pfParTAK4LastNegativeJetTags:probtaum3h0p",
        "pfParTAK4LastNegativeJetTags:probtaum3h1p",
        "pfParTAK4LastNegativeJetTags:probb",
        "pfParTAK4LastNegativeJetTags:probc",
        "pfParTAK4LastNegativeJetTags:probuds",
        "pfParTAK4LastNegativeJetTags:probg",
        "pfParTAK4LastNegativeJetTags:ptcorr",
        "pfParTAK4LastNegativeJetTags:ptreslow",
        "pfParTAK4LastNegativeJetTags:ptreshigh",
        "pfParTAK4LastNegativeJetTags:ptnu"
    ])
    
    parTDiscriminatorLabels = [name.replace("pfParTAK4LastJetTags:","").replace("pfParTAK4LastNegativeJetTags:","neg") for name in parTDiscriminatorNames]
    

## Update final jet collection                                                                                                                                                    
from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
process.slimmedJetsUpdated = updatedPatJets.clone(
    jetSource = "slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets"),
    addJetCorrFactors = False,
)

process.slimmedJetsUpdated.discriminatorSources += pnetDiscriminatorNames        
process.slimmedJetsUpdated.discriminatorSources += parTDiscriminatorNames        
        
###
from RecoJets.JetProducers.PileupJetID_cfi import pileupJetId
process.pileupJetIdUpdated = pileupJetId.clone( 
    jets = cms.InputTag("slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets")),
    inputIsCorrected = True,
    applyJec = False,
    vertexes = cms.InputTag("offlineSlimmedPrimaryVertices"),
)

if options.era == "2018":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL18
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL18)
elif options.era == "2017":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL17
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL17)
elif options.era == "2016PostVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16)
elif options.era == "2016PreVFP":
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16APV 
    process.pileupJetIdUpdated.algos = cms.VPSet(_chsalgos_106X_UL16APV)

if not hasattr(process,"slimmedJetsUpdated"):
    from PhysicsTools.PatAlgos.producersLayer1.jetUpdater_cfi import updatedPatJets
    process.slimmedJetsUpdated = updatedPatJets.clone(
        jetSource = "slimmedJetsCalibrated" if options.applyJECs else ("slimmedJetsPuppi" if options.usePuppiJets else "slimmedJets"),
        addJetCorrFactors = False
    )
    process.slimmedJetsUpdated.userData.userInts.src += ['pileupJetIdUpdated:fullId'];


### re-run puppi 
if options.reRunPuppi and options.usePuppiJets:
    process.load('CommonTools/PileupAlgos/Puppi_cff')
    process.puppi.candName = cms.InputTag('packedPFCandidates')
    process.puppi.vertexName = cms.InputTag('offlineSlimmedPrimaryVertices')
    process.puppi.clonePackedCands   = cms.bool(True)
    process.puppi.useExistingWeights = cms.bool(False)

    from RecoJets.JetProducers.ak4PFJets_cfi import ak4PFJets
    process.ak4PuppiJets = ak4PFJets.clone (
        src = 'puppi',
        doAreaFastjet = True,
        jetPtMin = 1.
    )

    from PhysicsTools.PatAlgos.tools.jetTools import addJetCollection
    addJetCollection(process,
                     labelName = 'Puppi',
                     jetSource = cms.InputTag('ak4PuppiJets'),
                     algo = 'AK',
                     rParam=0.4,
                     genJetCollection=cms.InputTag('slimmedGenJets'),
                     jetCorrections = ('AK4PFPuppi', ['L2Relative', 'L3Absolute'], 'None'),
                     pfCandidates = cms.InputTag('packedPFCandidates'),
                     pvSource = cms.InputTag('offlineSlimmedPrimaryVertices'),
                     svSource = cms.InputTag('slimmedSecondaryVertices'),
                     muSource =cms.InputTag( 'slimmedMuons'),
                     elSource = cms.InputTag('slimmedElectrons')
    )
    process.patJetsPuppi.addGenPartonMatch = cms.bool(options.isMC)
    process.patJetsPuppi.addGenJetMatch = cms.bool(options.isMC)
    process.patJetPartons.particles = cms.InputTag("prunedGenParticles")
    process.patJetPartonMatchPuppi.matched = cms.InputTag("prunedGenParticles")

    from PhysicsTools.PatAlgos.patPuppiJetSpecificProducer_cfi import patPuppiJetSpecificProducer 
    process.patPuppiJetSpecificProducer = patPuppiJetSpecificProducer.clone(
        src=cms.InputTag("patJetsPuppi")
    )

    process.pfParticleNetAK4LastJetTagInfos.jets = cms.InputTag("patJetsPuppi");
    process.pileupJetIdUpdated.jets = cms.InputTag("patJetsPuppi");

    process.slimmedJetsUpdated.jetSource = cms.InputTag("patJetsPuppi");
    process.slimmedJetsUpdated.userData.userInts.src += ['pileupJetIdUpdated:fullId'];
    process.slimmedJetsUpdated.userData.userFloats.src = ['patPuppiJetSpecificProducer:puppiMultiplicity', 'patPuppiJetSpecificProducer:neutralPuppiMultiplicity', 'patPuppiJetSpecificProducer:neutralHadronPuppiMultiplicity', 'patPuppiJetSpecificProducer:photonPuppiMultiplicity', 'patPuppiJetSpecificProducer:HFHadronPuppiMultiplicity', 'patPuppiJetSpecificProducer:HFEMPuppiMultiplicity']

### produce GEN jets with neutrinos
from RecoJets.Configuration.GenJetParticles_cff import genParticlesForJets
process.genParticlesForJets = genParticlesForJets.clone(
    src = cms.InputTag("packedGenParticles")
)

from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets
process.ak4GenJetsWithNu = ak4GenJets.clone(
    src = "genParticlesForJets"
)

## deep tau evaluation
import RecoTauTag.RecoTau.tools.runTauIdMVA as tauIdConfig
tauIdEmbedder = tauIdConfig.TauIDEmbedder(
    process,
    cms,
    originalTauName = "slimmedTaus",
    updatedTauName  = "slimmedTausUpdated",
    postfix = "",
    toKeep = [ "deepTau2018v2p5"]) #other tauIDs can be added in parallel.                                                                        
tauIdEmbedder.runTauID()

#### Final Dumper
process.dnntree = cms.EDAnalyzer('TrainingTreeMakerAK4',
    #### General flags
    xsec              = cms.double(options.xsec),
    isMC              = cms.bool(options.isMC),
    usePuppiJets      = cms.bool(options.usePuppiJets),
    dumpOnlyJetMatchedToGen  = cms.bool(options.dumpOnlyJetMatchedToGen),
    pnetDiscriminatorLabels = cms.vstring(),
    pnetDiscriminatorNames  = cms.vstring(),
    parTDiscriminatorLabels = cms.vstring(),
    parTDiscriminatorNames  = cms.vstring(),
    ### Object selection (applied on miniAOD, HLT and Gen objects for jets)
    muonPtMin         = cms.double(options.muonPtMin),
    muonEtaMax        = cms.double(2.4),
    electronPtMin     = cms.double(options.electronPtMin),
    electronEtaMax    = cms.double(2.5),
    photonPtMin       = cms.double(options.photonPtMin),
    phootonEtaMax     = cms.double(2.5),
    tauPtMin          = cms.double(options.tauPtMin),
    tauEtaMax         = cms.double(2.5),
    jetPtMin          = cms.double(options.jetPtMin),
    jetEtaMax         = cms.double(options.jetEtaMax),
    jetEtaMin         = cms.double(options.jetEtaMin),
    jetPFCandidatePtMin = cms.double(options.jetPFCandidatePtMin),
    lostTrackPtMin    = cms.double(options.lostTrackPtMin),
    dRLostTrackJet    = cms.double(options.dRLostTrackJet),
    dRJetGenMatch     = cms.double(options.dRJetGenMatch),
    ### Simulation quantities from miniAOD
    pileUpInfo        = cms.InputTag("slimmedAddPileupInfo"),
    genEventInfo      = cms.InputTag("generator"),
    genParticles      = cms.InputTag("prunedGenParticles"),
    lheInfo           = cms.InputTag("externalLHEProducer"),
    ### miniAOD objects
    triggerResults    = cms.InputTag("TriggerResults","", "HLT"),                                 
    stageL1Trigger    = cms.uint32(2),
    filterResults     = cms.InputTag("TriggerResults","", "PAT"),
    rho               = cms.InputTag("fixedGridRhoFastjetAll"),
    pVertices         = cms.InputTag("offlineSlimmedPrimaryVertices"),
    sVertices         = cms.InputTag("slimmedSecondaryVertices"),
    muons             = cms.InputTag("slimmedMuons"),
    electrons         = cms.InputTag("slimmedElectrons"),
    taus              = cms.InputTag("slimmedTausUpdated"),
    photons           = cms.InputTag("slimmedPhotons"),
    jets              = cms.InputTag("slimmedJetsUpdated"),
    met               = cms.InputTag("slimmedMETsUpdated") if options.applyJECs else cms.InputTag("slimmedMETs"),
    leptonPairs       = cms.InputTag(""),                                
    lostTracks        = cms.InputTag("lostTracks"),
    ### Gen jets  
    genJetsWnu        = cms.InputTag("ak4GenJetsWithNu"),
    genJets           = cms.InputTag("slimmedGenJets"),
    genJetsFlavour    = cms.InputTag("slimmedGenJetsFlavourInfos"),                                 
)

for element in pnetDiscriminatorNames:
    process.dnntree.pnetDiscriminatorNames.append(element);
for element in pnetDiscriminatorLabels:
    process.dnntree.pnetDiscriminatorLabels.append(element);
for element in parTDiscriminatorNames:
    process.dnntree.parTDiscriminatorNames.append(element);
for element in parTDiscriminatorLabels:
    process.dnntree.parTDiscriminatorLabels.append(element);

process.edTask = cms.Task()
for key in process.__dict__.keys():
    if(type(getattr(process,key)).__name__=='EDProducer' or type(getattr(process,key)).__name__=='EDFilter') :
        process.edTask.add(getattr(process,key))

process.dnnTreePath = cms.Path(process.dnntree,process.edTask)
