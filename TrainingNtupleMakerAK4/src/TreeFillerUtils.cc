#include "ParticleNetStudiesRun2/TrainingNtupleMakerAK4/interface/TreeFillerUtils.h"
#include <regex>


///// ----
void fillPDFVariations(const LHERunInfoProduct & myLHERunInfoProduct, std::vector<PdfVariation> & pdfvariation){

  using namespace boost::algorithm;

  int  default_pdf_order = -1; // 0 = LO, 1 = NLO, 2 = NNLO

  for (auto iter = myLHERunInfoProduct.headers_begin(); iter != myLHERunInfoProduct.headers_end(); iter++){
    std::vector<std::string> lines = iter->lines();

    for (unsigned int iLine = 0; iLine<lines.size(); iLine++) {
      std::vector<std::string> tokens;
      std::vector<std::string> tokens_alt;

      if(lines.at(iLine).find("MUR=\"1.0\" MUF=\"1.0\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("MUR=\"1\" MUF=\"1\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("MUF=\"1.0\" MUR=\"1.0\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("MUF=\"1\" MUR=\"1\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("muR=\"1.0\" muF=\"1.0\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("muR=\"1\" muF=\"1\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("muF=\"1.0\" muR=\"1.0\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("muF=\"1\" muR=\"1\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("mur=\"1.0\" muf=\"1.0\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("mur=\"1\" muf=\"1\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("muf=\"1.0\" mur=\"1.0\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("muf=\"1\" mur=\"1\" PDF=") != std::string::npos or // MG standard
	 lines.at(iLine).find("PDF=  ") != std::string::npos // MG standard
	 ){ // convention used in some NLO MG files

	
	TString line_string(lines.at(iLine));
	if(line_string.Contains("dyn_scale_choice") and line_string.Contains("DYN_SCALE")) continue;
	std::string token_line_string = std::string(line_string);
	split(tokens,token_line_string,is_any_of("\",\t "),boost::token_compress_on);
	tokens.erase(std::remove(tokens.begin(), tokens.end(),""), tokens.end());    
	tokens.erase(std::remove(tokens.begin(), tokens.end()," "), tokens.end());    
	
	int pos_muR = -1;
	int pos_muF = -1;
	int pos_id  = -1;
	int pos_pdfid = -1;
	int pos = 0;
	for(auto tok : tokens){ 
	  if((TString(tok).Contains("MUR") or TString(tok).Contains("muR") or TString(tok).Contains("mur")) and pos_muR == -1) pos_muR = pos;
	  if((TString(tok).Contains("MUF") or TString(tok).Contains("muF") or TString(tok).Contains("muf")) and pos_muF == -1) pos_muF = pos;
	  if(TString(tok).Contains("id=")  and pos_id == -1) pos_id = pos;
	  if(TString(tok).Contains("PDF=") and pos_pdfid == -1) pos_pdfid = pos;
	  pos++;
	}      

	int lhaid = std::stoi(tokens.at(pos_pdfid+1));
	if(default_pdf_order == -1)
	  default_pdf_order = getPDFOrder(lhaid);
	if(getPDFOrder(lhaid) == -1) continue;
	if(getPDFOrder(lhaid) != default_pdf_order) continue;
	int lhaid_base = getLHAID(lhaid);
	if(lhaid_base == -1) continue;
	PdfVariation apdf;
	apdf.lhaid = lhaid_base;
	
	if(find(pdfvariation.begin(),pdfvariation.end(),apdf) == pdfvariation.end()){
	  pdfvariation.push_back(apdf);
	}
	if(find(pdfvariation.back().pdfvar.begin(),pdfvariation.back().pdfvar.end(),std::stoi(tokens.at(pos_id+1))) == pdfvariation.back().pdfvar.end()){
	  pdfvariation.back().pdfvar.push_back(std::stoi(tokens.at(pos_id+1)));
	  pdfvariation.back().weight.push_back(0.);
	}    
      }
      else if(lines.at(iLine).find("lhapdf=") != std::string::npos or
	      lines.at(iLine).find("PDF set =") != std::string::npos){
	
	if(lines.at(iLine).find("rensfact=") != std::string::npos or
	   lines.at(iLine).find("facscfact=") !=std::string::npos) continue; // don't look at QCD scale block
	
        split(tokens,lines.at(iLine),is_any_of("\",\t "),boost::token_compress_on);
        tokens.erase(std::remove(tokens.begin(), tokens.end(),""), tokens.end());
        tokens.erase(std::remove(tokens.begin(), tokens.end()," "), tokens.end());

	int pos_pdfid = -1;
	int pos_id = -1;
	int pos = 0;
	for(auto tok : tokens){
	  if(TString(tok).Contains("lhapdf=")) pos_pdfid = pos;
	  else if(TString(tok).Contains("PDF set =")) pos_pdfid = pos;
	  if(TString(tok).Contains("id=")) pos_id = pos;
	  pos++;
	}
	
	PdfVariation apdf;
	TString lhapdf_token (tokens.at(pos_pdfid));
	lhapdf_token.ReplaceAll("lhapdf=","");
	lhapdf_token.ReplaceAll("PDF set =","");
	int lhaid = std::stoi(lhapdf_token.Data());
	if(default_pdf_order == -1)
	  default_pdf_order = getPDFOrder(lhaid);
	if(getPDFOrder(lhaid) == -1) continue;
	if(getPDFOrder(lhaid) != default_pdf_order) continue;
	int lhaid_base = getLHAID(lhaid);
	if(lhaid_base == -1) continue;
	apdf.lhaid = lhaid_base;
	
	if(find(pdfvariation.begin(),pdfvariation.end(),apdf) == pdfvariation.end()){
	  pdfvariation.push_back(apdf);
	}
	if(find(pdfvariation.back().pdfvar.begin(),pdfvariation.back().pdfvar.end(),std::stoi(tokens.at(pos_id+1))) == pdfvariation.back().pdfvar.end()){
	  pdfvariation.back().pdfvar.push_back(std::stoi(tokens.at(pos_id+1)));
	  pdfvariation.back().weight.push_back(0.);
	}
      }      
    }
  }
}


///// ---
void fillQCDScaleVariations(const LHERunInfoProduct & myLHERunInfoProduct, std::vector<QCDScaleVariation> & qcdscale){

  using namespace boost::algorithm;

  for (auto iter = myLHERunInfoProduct.headers_begin(); iter != myLHERunInfoProduct.headers_end(); iter++){
    std::vector<std::string> lines = iter->lines();

    for (unsigned int iLine = 0; iLine<lines.size(); iLine++) {
      std::vector<std::string> tokens;
      std::vector<std::string> tokens_alt;

      /////////////// Magdraph
      TString line_string(lines.at(iLine));

      if(line_string.Contains("systematics_arguments")) continue;

      if((line_string.Contains("MUR") and line_string.Contains("MUF") and 
	  not line_string.Contains("MUR=\"1.0\" MUF=\"1.0\" PDF=") and 
	  not line_string.Contains("MUR=\"1\" MUF=\"1\" PDF=") and  
	  not line_string.Contains("MUF=\"1.0\" MUR=\"1.0\" PDF=") and 
	  not line_string.Contains("MUF=\"1\" MUR=\"1\" PDF=")) or
	 (line_string.Contains("mur=") and line_string.Contains("muf=") and 
	  not line_string.Contains("mur=\"1.0\" muf=\"1.0\"") and
	  not line_string.Contains("mur=\"1\" muf=\"1\"") and
	  not line_string.Contains("muf=\"1.0\" mur=\"1.0\"") and
	  not line_string.Contains("muf=\"1\" mur=\"1\"")) or
	 (line_string.Contains("muR=") and line_string.Contains("muF=") and 
	  not line_string.Contains("muR=\"1.0\" muF=\"1.0\"") and
	  not line_string.Contains("muR=0.10000E+01 muF=0.10000E+01") and
	  not line_string.Contains("muR=\"1\" muF=\"1\"") and
	  not line_string.Contains("muF=\"1.0\" muR=\"1.0\"") and
	  not line_string.Contains("muF=\"1\" muR=\"1\""))){ 

	if(line_string.Contains("dyn_scale_choice") and line_string.Contains("DYN_SCALE")) continue;

	std::string token_line_string = std::string(line_string);
	split(tokens,token_line_string,is_any_of("\",\t "),boost::token_compress_on);
        tokens.erase(std::remove(tokens.begin(), tokens.end(),""), tokens.end());
        tokens.erase(std::remove(tokens.begin(), tokens.end()," "), tokens.end());

	int pos_muR = -1;
	int pos_muF = -1;
	int pos_id  = -1;
	int pos = 0;
	for(auto tok : tokens){
	  if(TString(tok).Contains("MUR") and pos_muR == -1) pos_muR = pos;
	  else if(TString(tok).Contains("muR") and pos_muR == -1) pos_muR = pos;
	  else if(TString(tok).Contains("mur") and pos_muR == -1) pos_muR = pos;
	  if(TString(tok).Contains("MUF") and pos_muF == -1) pos_muF = pos;
	  else if(TString(tok).Contains("muF") and pos_muF == -1) pos_muF = pos;
	  else if(TString(tok).Contains("muf") and pos_muF == -1) pos_muF = pos;
	  if(TString(tok).Contains("id") and pos_id == -1) pos_id = pos;
	  pos++;
	}
	
	if(pos_muR == -1 or 
	      pos_muF == -1 or
	   pos_id == -1){
	  std::cerr<<"Problem in parsing QCD-scale block --> MUR or MUF or id not found "<<std::endl;
	}

	QCDScaleVariation a;
	TString token_line_muR(tokens.at(pos_muR));
	TString token_line_muF(tokens.at(pos_muF));
	if(not token_line_muR.Contains("muR") and not token_line_muF.Contains("muF")){
	  a.muR = std::stof(tokens.at(pos_muR+1));
	  a.muF = std::stof(tokens.at(pos_muF+1));
	  a.scalevar = std::stoi(tokens.at(pos_id+1));

	  if(find(qcdscale.begin(),qcdscale.end(),a) == qcdscale.end())
	    qcdscale.push_back(a);
	}
	else{
	  token_line_muR.ReplaceAll("muR=","");
	  token_line_muF.ReplaceAll("muF=","");
	  a.muR = std::stof(token_line_muR.Data());
	  a.muF = std::stof(token_line_muF.Data());
	  a.scalevar = std::stoi(tokens.at(pos_id+1));
	    
	  if(find(qcdscale.begin(),qcdscale.end(),a) == qcdscale.end())
	    qcdscale.push_back(a);
	}
      }      
      /////////////////////////////
      else if(line_string.Contains("renscfact") and line_string.Contains("facscfact")){ // POWHEG
	
	std::string token_line_string = std::string(line_string);
        split(tokens,token_line_string,is_any_of("\",\t "),boost::token_compress_on);
        tokens.erase(std::remove(tokens.begin(), tokens.end(),""), tokens.end());
        tokens.erase(std::remove(tokens.begin(), tokens.end()," "), tokens.end());
	
	int pos_muR = -1;
	int pos_muF = -1;
	int pos_id  = -1;
	int pos = 0;
	for(auto tok : tokens){
	  if(TString(tok).Contains("renscfact=")) pos_muR = pos;
	  if(TString(tok).Contains("facscfact=")) pos_muF = pos;
	  if(TString(tok).Contains("id=")) pos_id = pos;
	  pos++;
	}
	
	QCDScaleVariation a;
	a.muR = 1.0;
	a.muF = 1.0;
	a.scalevar = std::stoi(tokens.at(pos_id+1));
	TString token_line_muR (tokens.at(pos_muR));
	TString token_line_muF (tokens.at(pos_muF));
	token_line_muR.ReplaceAll("renscfact=","");
	token_line_muF.ReplaceAll("facscfact=","");
	if(token_line_muR == "0.5d0") a.muR = 0.5;
	else if(token_line_muR == "1.0d0") a.muR = 1.0;
	else if(token_line_muR == "1d0") a.muR = 1.0;
	else if(token_line_muR == "2.0d0") a.muR = 2.0;
	else if(token_line_muR == "2d0") a.muR = 2.0;
	if(token_line_muF == "0.5d0") a.muF = 0.5;
	else if(token_line_muF == "1.0d0") a.muF = 1.0;
	else if(token_line_muF == "1d0") a.muF = 1.0;
	else if(token_line_muF == "2.0d0") a.muF = 2.0;
	else if(token_line_muF == "2d0") a.muF = 2.0;
	
	if(a.muR > 10 or a.muF > 10) continue; // probably one is reading PDF info
	if(find(qcdscale.begin(),qcdscale.end(),a) == qcdscale.end())
	  qcdscale.push_back(a);  
      }
    }      
  }
}


// PS weights
void fillPSVariations(const GenLumiInfoHeader & myGenLumiInfoHeader, std::vector<PSVariation> & psvariation){
  for(size_t iweight = 0; iweight < myGenLumiInfoHeader.weightNames().size(); iweight++){
    if(myGenLumiInfoHeader.weightNames()[iweight].empty()) continue;
    PSVariation a;
    a.pos = int(iweight);
    a.variation = myGenLumiInfoHeader.weightNames()[iweight];
    psvariation.push_back(a);
  }  
}

/////
bool isCleanWithMuons(const pat::JetRef & jet, const std::vector<pat::MuonRef> & muons, const float & dR){
  bool goodJet = true;
  for(auto muon : muons){
    if(deltaR(jet->eta(),jet->phi(),muon->eta(),muon->phi()) < dR) goodJet = false;    
  }
  return goodJet;
}

bool isCleanWithElectrons(const pat::JetRef & jet, const std::vector<pat::ElectronRef> & electrons, const float & dR){
  bool goodJet = true;
  for(auto ele : electrons){
    if(deltaR(jet->eta(),jet->phi(),ele->eta(),ele->phi()) < dR) goodJet = false;    
  }
  return goodJet;
}

bool isCleanWithTaus(const pat::JetRef & jet, const std::vector<pat::TauRef> & taus, const float & dR){
  bool goodJet = true;
  for(auto tau : taus){
    if(deltaR(jet->eta(),jet->phi(),tau->eta(),tau->phi()) < dR) goodJet = false;    
  }
  return goodJet;
}

/// https://lhapdf.hepforge.org/pdfsets.html
int getLHAID(const std::string & pdfSetName){

  TString name (pdfSetName);

  if(name.Contains("CT10.LHgrid") or name.Contains("PDF_variation CT10") or name.Contains("name=\"CT10")) return 10800;
  else if(name.Contains("CT10nlo.LHgrid") or name.Contains("PDF_variation CT10nlo") or name.Contains("name=\"CT10nlo")) return 11000;
  else if(name.Contains("CT10nlo_as_0112.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0112") or name.Contains("name=\"CT10nlo_as_0112")) return 11062;
  else if(name.Contains("CT10nlo_as_0113.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0113") or name.Contains("name=\"CT10nlo_as_0113")) return 11063;
  else if(name.Contains("CT10nlo_as_0114.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0114") or name.Contains("name=\"CT10nlo_as_0114")) return 11064;
  else if(name.Contains("CT10nlo_as_0115.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0115") or name.Contains("name=\"CT10nlo_as_0115")) return 11065;
  else if(name.Contains("CT10nlo_as_0116.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0116") or name.Contains("name=\"CT10nlo_as_0116")) return 11066;
  else if(name.Contains("CT10nlo_as_0117.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0117") or name.Contains("name=\"CT10nlo_as_0117")) return 11067;
  else if(name.Contains("CT10nlo_as_0118.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0118") or name.Contains("name=\"CT10nlo_as_0118")) return 11068;
  else if(name.Contains("CT10nlo_as_0119.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0119") or name.Contains("name=\"CT10nlo_as_0119")) return 11069;
  else if(name.Contains("CT10nlo_as_0120.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0120") or name.Contains("name=\"CT10nlo_as_0120")) return 11070;
  else if(name.Contains("CT10nlo_as_0121.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0121") or name.Contains("name=\"CT10nlo_as_0121")) return 11071;
  else if(name.Contains("CT10nlo_as_0122.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0122") or name.Contains("name=\"CT10nlo_as_0122")) return 11072;
  else if(name.Contains("CT10nlo_as_0123.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0123") or name.Contains("name=\"CT10nlo_as_0123")) return 11073;
  else if(name.Contains("CT10nlo_as_0124.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0124") or name.Contains("name=\"CT10nlo_as_0124")) return 11074;
  else if(name.Contains("CT10nlo_as_0125.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0125") or name.Contains("name=\"CT10nlo_as_0125")) return 11075;
  else if(name.Contains("CT10nlo_as_0126.LHgrid") or name.Contains("PDF_variation CT10nlo_as_0126") or name.Contains("name=\"CT10nlo_as_0126")) return 11076;
  else if(name.Contains("CT10nlo_as_0127.LHgrid") or name.Contains("PDF_variation CT10nlo_as_127") or name.Contains("name=\"CT10nlo_as_127")) return 11077;
  else if(name.Contains("CT10nnlo.LHgrid") or name.Contains("PDF_variation CT10nnlo") or name.Contains("name=\"CT10nnlo")) return 11200;
  else if(name.Contains("CT10nnlo_as_0112.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0112") or name.Contains("name=\"CT10nnlo_as_0112")) return 11262;
  else if(name.Contains("CT10nnlo_as_0113.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0113") or name.Contains("name=\"CT10nnlo_as_0113")) return 11263;
  else if(name.Contains("CT10nnlo_as_0114.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0114") or name.Contains("name=\"CT10nnlo_as_0114")) return 11264;
  else if(name.Contains("CT10nnlo_as_0115.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0115") or name.Contains("name=\"CT10nnlo_as_0115")) return 11265;
  else if(name.Contains("CT10nnlo_as_0116.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0116") or name.Contains("name=\"CT10nnlo_as_0116")) return 11266;
  else if(name.Contains("CT10nnlo_as_0117.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0117") or name.Contains("name=\"CT10nnlo_as_0117")) return 11267;
  else if(name.Contains("CT10nnlo_as_0118.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0118") or name.Contains("name=\"CT10nnlo_as_0118")) return 11268;
  else if(name.Contains("CT10nnlo_as_0119.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0119") or name.Contains("name=\"CT10nnlo_as_0119")) return 11269;
  else if(name.Contains("CT10nnlo_as_0120.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0120") or name.Contains("name=\"CT10nnlo_as_0120")) return 11270;
  else if(name.Contains("CT10nnlo_as_0121.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0121") or name.Contains("name=\"CT10nnlo_as_0121")) return 11271;
  else if(name.Contains("CT10nnlo_as_0122.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0122") or name.Contains("name=\"CT10nnlo_as_0122")) return 11272;
  else if(name.Contains("CT10nnlo_as_0123.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0123") or name.Contains("name=\"CT10nnlo_as_0123")) return 11273;
  else if(name.Contains("CT10nnlo_as_0124.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0124") or name.Contains("name=\"CT10nnlo_as_0124")) return 11274;
  else if(name.Contains("CT10nnlo_as_0125.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0125") or name.Contains("name=\"CT10nnlo_as_0125")) return 11275;
  else if(name.Contains("CT10nnlo_as_0126.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0126") or name.Contains("name=\"CT10nnlo_as_0126")) return 11276;
  else if(name.Contains("CT10nnlo_as_0127.LHgrid") or name.Contains("PDF_variation CT10nnlo_as_0127") or name.Contains("name=\"CT10nnlo_as_0127")) return 11277;
  else if(name.Contains("CT14nnlo.LHgrid") or name.Contains("PDF_variation CT14nnlo") or name.Contains("name=\"CT14nnlo")) return 13000;
  else if(name.Contains("CT14nnlo_as_0112.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0112") or name.Contains("name=\"CT14nnlo_as_0112")) return 12061;
  else if(name.Contains("CT14nnlo_as_0113.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0113") or name.Contains("name=\"CT14nnlo_as_0113")) return 12062;
  else if(name.Contains("CT14nnlo_as_0114.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0114") or name.Contains("name=\"CT14nnlo_as_0114")) return 12063;
  else if(name.Contains("CT14nnlo_as_0115.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0115") or name.Contains("name=\"CT14nnlo_as_0115")) return 12064;
  else if(name.Contains("CT14nnlo_as_0116.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0116") or name.Contains("name=\"CT14nnlo_as_0116")) return 12065;
  else if(name.Contains("CT14nnlo_as_0117.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0117") or name.Contains("name=\"CT14nnlo_as_0117")) return 12066;
  else if(name.Contains("CT14nnlo_as_0118.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0118") or name.Contains("name=\"CT14nnlo_as_0118")) return 12067;
  else if(name.Contains("CT14nnlo_as_0119.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0119") or name.Contains("name=\"CT14nnlo_as_0119")) return 12068;
  else if(name.Contains("CT14nnlo_as_0120.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0120") or name.Contains("name=\"CT14nnlo_as_0120")) return 12069;
  else if(name.Contains("CT14nnlo_as_0121.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0121") or name.Contains("name=\"CT14nnlo_as_0121")) return 12070;
  else if(name.Contains("CT14nnlo_as_0122.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0122") or name.Contains("name=\"CT14nnlo_as_0122")) return 12071;
  else if(name.Contains("CT14nnlo_as_0123.LHgrid") or name.Contains("PDF_variation CT14nnlo_as_0123") or name.Contains("name=\"CT14nnlo_as_0123")) return 12072;
  else if(name.Contains("CT14nlo.LHgrid") or name.Contains("PDF_variation CT14nlo") or name.Contains("name=\"CT14nlo")) return 13100;
  else if(name.Contains("CT14nlo_as_0112.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0112") or name.Contains("name=\"CT14nlo_as_0112")) return 13159;
  else if(name.Contains("CT14nlo_as_0113.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0113") or name.Contains("name=\"CT14nlo_as_0113")) return 13160;
  else if(name.Contains("CT14nlo_as_0114.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0114") or name.Contains("name=\"CT14nlo_as_0114")) return 13161;
  else if(name.Contains("CT14nlo_as_0115.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0115") or name.Contains("name=\"CT14nlo_as_0115")) return 13162;
  else if(name.Contains("CT14nlo_as_0116.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0116") or name.Contains("name=\"CT14nlo_as_0116")) return 13163;
  else if(name.Contains("CT14nlo_as_0117.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0117") or name.Contains("name=\"CT14nlo_as_0117")) return 13164;
  else if(name.Contains("CT14nlo_as_0118.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0118") or name.Contains("name=\"CT14nlo_as_0118")) return 13165;
  else if(name.Contains("CT14nlo_as_0119.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0119") or name.Contains("name=\"CT14nlo_as_0119")) return 13166;
  else if(name.Contains("CT14nlo_as_0120.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0120") or name.Contains("name=\"CT14nlo_as_0120")) return 13167;
  else if(name.Contains("CT14nlo_as_0121.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0121") or name.Contains("name=\"CT14nlo_as_0121")) return 13168;
  else if(name.Contains("CT14nlo_as_0122.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0122") or name.Contains("name=\"CT14nlo_as_0122")) return 13169;
  else if(name.Contains("CT14nlo_as_0123.LHgrid") or name.Contains("PDF_variation CT14nlo_as_0123") or name.Contains("name=\"CT14nlo_as_0123")) return 13170;
  else if(name.Contains("CT14qed_inc_proton.LHgrid") or name.Contains("CT14qed_inc_proton") or name.Contains("name=\"CT14qed_inc_proton")) return 13400;
  else if(name.Contains("MMHT2014lo68cl.LHgrid") or name.Contains("PDF_variation MMHT2014lo68cl") or name.Contains("name=\"MMHT2014lo68cl")) return 25000;
  else if(name.Contains("MMHT2014nlo68cl.LHgrid") or name.Contains("PDF_variation MMHT2014nlo68cl") or name.Contains("name=\"MMHT2014nlo68cl")) return 25100;
  else if(name.Contains("MMHT2014nnlo68cl.LHgrid") or name.Contains("PDF_variation MMHT2014nnlo68cl") or name.Contains("name=\"MMHT2014nnlo68cl")) return 25300;
  else if(name.Contains("HERAPDF15NNLO_EIG.LHgrid") or name.Contains("PDF_variation HERAPDF15NNLO_EIG") or name.Contains("name=\"HERAPDF15NNLO_EIG")) return 60600;
  else if(name.Contains("HERAPDF15NLO_EIG.LHgrid") or name.Contains("PDF_variation HERAPDF15NLO_EIG") or name.Contains("name=\"HERAPDF15NLO_EIG")) return 60700;
  else if(name.Contains("HERAPDF15LO_EIG.LHgrid") or name.Contains("PDF_variation HERAPDF15LO_EIG") or name.Contains("name=\"HERAPDF15LO_EIG")) return 60800;
  else if(name.Contains("HERAPDF20NNLO_EIG.LHgrid") or name.Contains("PDF_variation HERAPDF20NNLO_EIG") or name.Contains("name=\"HERAPDF20NNLO_EIG")) return 61200;
  else if(name.Contains("HERAPDF20NLO_EIG.LHgrid") or name.Contains("PDF_variation HERAPDF20NLO_EIG") or name.Contains("name=\"HERAPDF20NLO_EIG")) return 61100;
  else if(name.Contains("HERAPDF20LO_EIG.LHgrid") or name.Contains("PDF_variation HERAPDF20LO_EIG") or name.Contains("name=\"HERAPDF20LO_EIG")) return 61000;
  else if(name.Contains("NNPDF30_nlo_as_0118.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_as_0118") or name.Contains("name=\"NNPDF30_nlo_as_0118")) return 260000;
  else if(name.Contains("NNPDF30_nnlo_as_0118.LHgrid") or name.Contains("PDF_variation NNPDF30_nnlo_as_0118") or name.Contains("name=\"NNPDF30_nnlo_as_0118")) return 261000;
  else if(name.Contains("NNPDF30_lo_as_0118.LHgrid") or name.Contains("PDF_variation NNPDF30_lo_as_0118") or name.Contains("name=\"NNPDF30_lo_as_0118")) return 262000;
  else if(name.Contains("NNPDF30_lo_as_0130.LHgrid") or name.Contains("PDF_variation NNPDF30_lo_as_0130") or name.Contains("name=\"NNPDF30_lo_as_0130")) return 263000;
  else if(name.Contains("NNPDF30_nlo_as_0120.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_as_0115") or name.Contains("name=\"NNPDF30_nlo_as_0115")) return 264000;
  else if(name.Contains("NNPDF30_nlo_as_0117.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_as_0117") or name.Contains("name=\"NNPDF30_nlo_as_0117")) return 265000;
  else if(name.Contains("NNPDF30_nlo_as_0119.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_as_0119") or name.Contains("name=\"NNPDF30_nlo_as_0119")) return 266000;
  else if(name.Contains("NNPDF30_nlo_as_0121.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_as_0121") or name.Contains("name=\"NNPDF30_nlo_as_0121")) return 267000;
  else if(name.Contains("NNPDF30_nnlo_as_0115.LHgrid") or name.Contains("PDF_variation NNPDF30_nnlo_as_0115") or name.Contains("name=\"NNPDF30_nnlo_as_0115")) return 268000;
  else if(name.Contains("NNPDF30_nnlo_as_0117.LHgrid") or name.Contains("PDF_variation NNPDF30_nnlo_as_0117") or name.Contains("name=\"NNPDF30_nnlo_as_0117")) return 269000;
  else if(name.Contains("NNPDF30_nnlo_as_0119.LHgrid") or name.Contains("PDF_variation NNPDF30_nnlo_as_0119") or name.Contains("name=\"NNPDF30_nnlo_as_0119")) return 270000;
  else if(name.Contains("NNPDF30_nnlo_as_0121.LHgrid") or name.Contains("PDF_variation NNPDF30_nnlo_as_0121") or name.Contains("name=\"NNPDF30_nnlo_as_0121")) return 271000;
  else if(name.Contains("NNPDF30_nlo_nf_4_pdfas.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_nf_4_pdfas") or name.Contains("name=\"NNPDF30_nlo_nf_4_pdfas")) return 292000;
  else if(name.Contains("NNPDF30_nlo_nf_5_pdfas.LHgrid") or name.Contains("PDF_variation NNPDF30_nlo_nf_5_pdfas") or name.Contains("name=\"NNPDF30_nlo_nf_5_pdfas")) return 292200;
  else if(name.Contains("NNPDF30_nnlo_nf_5_pdfas.LHgrid") or name.Contains("PDF_variation NNPDF30_nnlo_nf_5_pdfas") or name.Contains("name=\"NNPDF30_nnlo_nf_5_pdfas")) return 292600;
  else if(name.Contains("NNPDF31_nlo_as_0118.LHgrid") or name.Contains("PDF_variation NNPDF31_nlo_as_0118") or name.Contains("name=\"NNPDF31_nlo_as_0118")) return 303400;
  else if(name.Contains("NNPDF31_nnlo_as_0118.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0118") or name.Contains("name=\"NNPDF31_nnlo_as_0118")) return 303600;
  else if(name.Contains("NNPDF31_nlo_hessian_pdfas.LHgrid") or name.Contains("PDF_variation NNPDF31_nlo_hessian_pdfas") or name.Contains("name=\"NNPDF31_nlo_hessian_pdfas")) return 305800;
  else if(name.Contains("NNPDF31_nnlo_hessian_pdfas.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_hessian_pdfas") or name.Contains("name=\"NNPDF31_nnlo_hessian_pdfas")) return 306000;
  else if(name.Contains("NNPDF31_lo_as_0118.LHgrid") or name.Contains("PDF_variation NNPDF31_lo_as_0118") or name.Contains("name=\"NNPDF31_lo_as_0118")) return 315000;
  else if(name.Contains("NNPDF31_lo_as_0130.LHgrid") or name.Contains("PDF_variation NNPDF31_lo_as_0130") or name.Contains("name=\"NNPDF31_lo_as_0130")) return 315200;
  else if(name.Contains("NNPDF31_nlo_as_0116.LHgrid") or name.Contains("PDF_variation NNPDF31_nlo_as_0116") or name.Contains("name=\"NNPDF31_nlo_as_0116")) return 318900;
  else if(name.Contains("NNPDF31_nlo_as_0120.LHgrid") or name.Contains("PDF_variation NNPDF31_nlo_as_0120") or name.Contains("name=\"NNPDF31_nlo_as_0120")) return 319100;
  else if(name.Contains("NNPDF31_nnlo_as_0116.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0116") or name.Contains("name=\"NNPDF31_nnlo_as_0116")) return 319300;
  else if(name.Contains("NNPDF31_nnlo_as_0120.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0120") or name.Contains("name=\"NNPDF31_nnlo_as_0120")) return 319500;
  else if(name.Contains("NNPDF31_nnlo_as_0118_nf_4.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0118_nf_4") or name.Contains("name=\"NNPDF31_nnlo_as_0118_nf_4")) return 320900;
  else if(name.Contains("NNPDF31_nnlo_as_0108.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0108") or name.Contains("name=\"NNPDF31_nnlo_as_0108")) return 322500;
  else if(name.Contains("NNPDF31_nnlo_as_0110.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0110") or name.Contains("name=\"NNPDF31_nnlo_as_0110")) return 322700;
  else if(name.Contains("NNPDF31_nnlo_as_0112.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0112") or name.Contains("name=\"NNPDF31_nnlo_as_0112")) return 322900;
  else if(name.Contains("NNPDF31_nnlo_as_0114.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0114") or name.Contains("name=\"NNPDF31_nnlo_as_0114")) return 323100;
  else if(name.Contains("NNPDF31_nnlo_as_0117.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0117") or name.Contains("name=\"NNPDF31_nnlo_as_0117")) return 323300;
  else if(name.Contains("NNPDF31_nnlo_as_0119.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0119") or name.Contains("name=\"NNPDF31_nnlo_as_0119")) return 323500;
  else if(name.Contains("NNPDF31_nnlo_as_0122.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0122") or name.Contains("name=\"NNPDF31_nnlo_as_0122")) return 323700;
  else if(name.Contains("NNPDF31_nnlo_as_0124.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0124") or name.Contains("name=\"NNPDF31_nnlo_as_0124")) return 323900;
  else if(name.Contains("NNPDF31_nlo_as_0118_luxqed.LHgrid") or name.Contains("PDF_variation NNPDF31_nlo_as_0118_luxqed") or name.Contains("name=\"NNPDF31_nlo_as_0118_luxqed")) return 324900;
  else if(name.Contains("NNPDF31_nnlo_as_0118_luxqed.LHgrid") or name.Contains("PDF_variation NNPDF31_nnlo_as_0118_luxqed") or name.Contains("name=\"NNPDF31_nnlo_as_0118_luxqed")) return 325100;
  else if(name.Contains("CT14MC1nlo.LHgrid") or name.Contains("PDF_variation CT14MC1nlo") or name.Contains("name=\"CT14MC1nlo")) return 500000;
  else if(name.Contains("CT14MC2nlo.LHgrid") or name.Contains("PDF_variation CT14MC2nlo") or name.Contains("name=\"CT14MC2nlo")) return 502000;
  else if(name.Contains("CT14MC1nnlo.LHgrid") or name.Contains("PDF_variation CT14MC1nnlo") or name.Contains("name=\"CT14MC1nnlo")) return 504000;
  else if(name.Contains("CT14MC2nnlo.LHgrid") or name.Contains("PDF_variation CT14MC2nnlo") or name.Contains("name=\"CT14MC2nnlo")) return 506000;
  else if(name.Contains("LUXqed17_plus_PDF4LHC15_nnlo_100.LHgrid") or name.Contains("PDF_variation LUXqed17_plus_PDF4LHC15_nnlo_100") or name.Contains("name=\"LUXqed17_plus_PDF4LHC15_nnlo_100")) return 82200;
  else if(name.Contains("LUXqed17_plus_PDF4LHC15_nnlo_30.LHgrid") or name.Contains("PDF_variation LUXqed17_plus_PDF4LHC15_nnlo_30") or name.Contains("name=\"LUXqed17_plus_PDF4LHC15_nnlo_30")) return 82350;
  else if(name.Contains("PDF4LHC15_nlo_100_pdfas.LHgrid") or name.Contains("PDF_variation PDF4LHC15_nlo_100_pdfas") or name.Contains("name=\"PDF4LHC15_nlo_100_pdfas")) return 90200;
  else if(name.Contains("PDF4LHC15_nnlo_100_pdfas.LHgrid") or name.Contains("PDF_variation PDF4LHC15_nnlo_100_pdfas") or name.Contains("name=\"PDF4LHC15_nnlo_100_pdfas")) return 91200;
  else return -1;
}

int getLHAID(const int & lhaid){  
  if(lhaid >= 10800 and lhaid < 11000) return 10800;
  else if(lhaid >= 11000 and lhaid < 11100) return 11000;
  else if(lhaid >= 11200 and lhaid < 12000) return 11200;
  else if(lhaid >= 13000 and lhaid < 13100) return 13000;
  else if(lhaid >= 13100 and lhaid < 13200) return 13100;
  else if(lhaid >= 13300 and lhaid < 13350) return 13300;
  else if(lhaid >= 25000 and lhaid < 25050) return 25000;
  else if(lhaid >= 25100 and lhaid < 25200) return 25100;
  else if(lhaid >= 25300 and lhaid < 25400) return 25300;
  else if(lhaid >= 60600 and lhaid < 60650) return 60600;
  else if(lhaid >= 60700 and lhaid < 60750) return 60700;
  else if(lhaid >= 60800 and lhaid < 61000) return 60800;
  else if(lhaid >= 61000 and lhaid < 61100) return 61000;
  else if(lhaid >= 61100 and lhaid < 61200) return 61100;
  else if(lhaid >= 61200 and lhaid < 61300) return 61200;
  else if(lhaid >= 260000 and lhaid < 261000) return 260000;
  else if(lhaid >= 261000 and lhaid < 262000) return 261000;
  else if(lhaid >= 262000 and lhaid < 263000) return 262000;
  else if(lhaid >= 263000 and lhaid < 264000) return 263000;
  else if(lhaid >= 264000 and lhaid < 265000) return 264000;
  else if(lhaid >= 265000 and lhaid < 266000) return 265000;
  else if(lhaid >= 266000 and lhaid < 267000) return 266000;
  else if(lhaid >= 267000 and lhaid < 268000) return 267000;
  else if(lhaid >= 268000 and lhaid < 269000) return 268000;
  else if(lhaid >= 269000 and lhaid < 270000) return 269000;
  else if(lhaid >= 270000 and lhaid < 271000) return 270000;
  else if(lhaid >= 271000 and lhaid < 272000) return 271000;
  else if(lhaid >= 292000 and lhaid < 292200) return 292000;
  else if(lhaid >= 292200 and lhaid < 292400) return 292200;
  else if(lhaid >= 292600 and lhaid < 293000) return 292600; 
  else if(lhaid >= 303400 and lhaid < 303600) return 303400;
  else if(lhaid >= 303600 and lhaid < 305000) return 303600;
  else if(lhaid >= 305000 and lhaid < 306000) return 305000;
  else if(lhaid >= 306000 and lhaid < 307000) return 306000;
  else if(lhaid >= 315000 and lhaid < 315200) return 315000;
  else if(lhaid >= 315200 and lhaid < 315400) return 315200;
  else if(lhaid >= 318900 and lhaid < 319100) return 318900;
  else if(lhaid >= 319100 and lhaid < 319300) return 319100;
  else if(lhaid >= 319300 and lhaid < 319500) return 319300;
  else if(lhaid >= 319500 and lhaid < 319700) return 319500;
  else if(lhaid >= 320900 and lhaid < 321100) return 320900;
  else if(lhaid >= 322500 and lhaid < 322700) return 322500;
  else if(lhaid >= 322700 and lhaid < 322900) return 322700;
  else if(lhaid >= 322900 and lhaid < 323100) return 322900;
  else if(lhaid >= 323100 and lhaid < 323300) return 323100;
  else if(lhaid >= 323300 and lhaid < 323500) return 323300;
  else if(lhaid >= 323500 and lhaid < 323700) return 323500;
  else if(lhaid >= 323700 and lhaid < 323900) return 323700;
  else if(lhaid >= 323900 and lhaid < 324100) return 323900;
  else if(lhaid >= 324900 and lhaid < 325100) return 324900;
  else if(lhaid >= 325100 and lhaid < 325300) return 325100;
  else if(lhaid >= 500000 and lhaid < 502000) return 500000;
  else if(lhaid >= 502000 and lhaid < 504000) return 502000;
  else if(lhaid >= 504000 and lhaid < 506000) return 504000;
  else if(lhaid >= 506000 and lhaid < 508000) return 506000;
  else if(lhaid >= 82200 and lhaid < 82350) return 82200;
  else if(lhaid >= 82350 and lhaid < 90000) return 82350;
  else if(lhaid >= 90200 and lhaid < 91200) return 90200;
  else if(lhaid >= 91200 and lhaid < 91400) return 91200;
  else return -1;  
}


/// https://lhapdf.hepforge.org/pdfsets.html
int getPDFOrder(const std::string & pdfSetName){
  TString name (pdfSetName);
  if(name.Contains("nnlo") or name.Contains("NNLO")) return 2;
  else if(name.Contains("nlo") or name.Contains("NLO")) return 1;
  else return 0;
}

/// https://lhapdf.hepforge.org/pdfsets.html
int getPDFOrder(const int & lhaid){  
  if(lhaid >= 10800 and lhaid < 11000) return 0;
  else if(lhaid >= 11000 and lhaid < 12000) return 1;
  else if(lhaid >= 12000 and lhaid < 13000) return 2;
  else if(lhaid >= 13000 and lhaid < 13100) return 2;
  else if(lhaid >= 13100 and lhaid < 13400) return 1;
  else if(lhaid >= 13400 and lhaid < 13500) return 0;
  else if(lhaid >= 25000 and lhaid < 25100) return 0;
  else if(lhaid >= 25100 and lhaid < 25300) return 1;
  else if(lhaid >= 25300 and lhaid < 25500) return 2;
  else if(lhaid >= 60600 and lhaid < 60700) return 2;
  else if(lhaid >= 60700 and lhaid < 60800) return 1;
  else if(lhaid >= 60800 and lhaid < 60900) return 0;
  else if(lhaid >= 61000 and lhaid < 61100) return 0;
  else if(lhaid >= 61100 and lhaid < 61200) return 1;
  else if(lhaid >= 61200 and lhaid < 61300) return 2;
  else if(lhaid >= 260000 and lhaid < 261000) return 1;
  else if(lhaid >= 261000 and lhaid < 262000) return 2;
  else if(lhaid >= 262000 and lhaid < 264000) return 0;
  else if(lhaid >= 264000 and lhaid < 268000) return 1;
  else if(lhaid >= 268000 and lhaid < 271000) return 2;
  else if(lhaid >= 292000 and lhaid < 292200) return 1;
  else if(lhaid >= 292200 and lhaid < 292400) return 1;
  else if(lhaid >= 292600 and lhaid < 293000) return 2;
  else if(lhaid >= 303400 and lhaid < 303600) return 1;
  else if(lhaid >= 303600 and lhaid < 303800) return 2;
  else if(lhaid >= 305800 and lhaid < 306000) return 1;
  else if(lhaid >= 306000 and lhaid < 306200) return 2;
  else if(lhaid >= 315000 and lhaid < 315400) return 0;
  else if(lhaid >= 318900 and lhaid < 319300) return 1;
  else if(lhaid >= 319300 and lhaid < 324100) return 2;
  else if(lhaid >= 324900 and lhaid < 325100) return 1;
  else if(lhaid >= 325100 and lhaid < 325300) return 2;
  else if(lhaid >= 500000 and lhaid < 504000) return 1;
  else if(lhaid >= 504000 and lhaid < 508000) return 2;
  else if(lhaid >= 82200 and lhaid < 90000) return 2;
  else if(lhaid >= 90200 and lhaid < 90500) return 1;
  else if(lhaid >= 91200 and lhaid < 91500) return 2;
  else return -1;
}

