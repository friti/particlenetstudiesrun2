def AddDYSamples(samples):

    samples['DYJetsToLL'] = [
        '/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6.401e+03','isMC=True','nThreads=2','jetPtMin=25','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]


def AddHiggsSamples(samples):

    samples['ZH_HToBB_ZToLL_M-125'] = [
        '/ZH_HToBB_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.01736','isMC=True','nThreads=2','jetPtMin=25','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]

    samples['GluGluHToTauTau_M-125'] = [
        '/GluGluHToTauTau_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.047','isMC=True','nThreads=2','jetPtMin=25','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]

    samples['ZH_HToCC_ZToLL_M-125'] = [
        '/ZH_HToCC_ZToLL_M-125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=0.0008357','isMC=True','nThreads=2','jetPtMin=25','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]


def AddTTbarSamples(samples):

    samples['TTToSemiLeptonic'] = [
        '/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=384.26','isMC=True','nThreads=2','jetPtMin=25','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','applyJECs=False','useFastSVFit=True'],
        'EventAwareLumiBased',
        50000,
        '',
        5000000
    ]
            
def AddAllSamples(samples):
    AddDYSamples(samples)
    AddHiggsSamples(samples)
    AddTTbarSamples(samples)
