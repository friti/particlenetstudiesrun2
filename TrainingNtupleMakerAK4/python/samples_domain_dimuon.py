def AddDYSamples(samples):

    samples['DYJetsToLL'] = [
        '/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=6077.22','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dimuon','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        500000,
        '',
        -1
    ]

def AddDataSamples(samples):

    samples['SingleMuon2018D'] = [
        '/SingleMuon/Run2018D-UL2018_MiniAODv2-v3/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dimuon','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        125,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

def AddAllSamples(samples):
    AddDYSamples(samples)
    AddDataSamples(samples)
    
