import os, sys
import argparse
import glob
import shutil
import numpy

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='',help='mother directory in which all samples of a given domain are stored')
parser.add_argument('-o', '--output-dir', type=str, default='',help='output path where the various subdirectories and files will be created')
parser.add_argument('-n', '--nfiles', type=int, default=1, help='number of files that will merged together in every job')
parser.add_argument('-t', '--nthreads', type=int, default=1, help='number of threads to be run and requested via HTCondor')
parser.add_argument('-g', '--grep-name', type=str, default="", help='directory names to be grep when search for files')
parser.add_argument('-d', '--domain-type', type=str, default="none", help='type of domain region for the training', 
                    choices=['none','dimuon','dielectron','emu','mutau','etau','dijet','ttcharm'])
parser.add_argument('-m', '--is-mc', action='store_true', default=False, help='when true run in mc mode')
parser.add_argument('-sj', '--split-jobs', action='store_true', default=False, help='one job every nfiles')
parser.add_argument('-j', '--job-dir', type=str, default='',help='directory where files for HTCondor jobs have to be created')
parser.add_argument('-q', '--queque', type=str, default="longlunch", help='queque for condorHT')
parser.add_argument('-s', '--submit', action='store_true', default=False, help='submit jobs to condorHT scherduler')

if __name__ == '__main__':
    
    args = parser.parse_args()
    jwd  = os.getcwd();

    ## create input file list
    for directory in glob.iglob(args.input_dir+"/*/"):
        if args.grep_name in directory:
            os.chdir(jwd);
            output_name = list(filter(None,directory.split("/")))[-1];
            ## prepare job directory            
            jdir = os.path.join(jwd,args.job_dir+"/"+args.domain_type+"/"+output_name);
            if os.path.exists(jdir) and os.path.isdir(jdir):        
                print("Job directory already exists --> remove its content");
                shutil.rmtree(jdir);
                
            print("Create job directory = ",jdir);
            os.makedirs(jdir);
            os.chdir(jdir);

            print("Write job exectution file for directory ",directory);
            job_script = open("condor_job.sh","w");
            job_script.write("#!/bin/bash\n");    
            job_script.write('cd '+jwd+'\n');
            job_script.write('source /cvmfs/cms.cern.ch/cmsset_default.sh'+'\n');
            job_script.write('export EOS_MGM_URL='+os.getenv('EOS_MGM_URL')+'\n');            
            job_script.write('cmsenv \n')
            job_script.write("cd -\n");    
            job_script.write("mkdir -p "+args.output_dir+"\n");            
            njobs = 0;
            if not args.split_jobs:
                njobs = 1;
                output_filename = "tree_"+output_name+".root";
                job_script.write("if [ $1 -eq 0 ]; then\n");            
                if args.is_mc:
                    job_script.write(" makeDomainSumOfWeights --inputDirectory "+directory+" --outputDIR ./ --outputFileName "+output_filename+" --isMC true --nFilesToAnalyze "+str(args.nfiles)+" --domain-type domain_type::"+args.domain_type+" --nThreads "+str(args.nthreads)+" \n");
                else:
                    job_script.write(" makeDomainSumOfWeights --inputDirectory "+directory+" --outputDIR ./ --outputFileName "+output_filename+" --isMC false --nFilesToAnalyze "+str(args.nfiles)+" --domain-type domain_type::"+args.domain_type+" --nThreads "+str(args.nthreads)+" \n");
                job_script.write(" xrdcp -f "+output_filename.replace(".root","*.root")+" "+args.output_dir+"\n")
                job_script.write("fi\n");
                job_script.close();            
            else:
                inputFileList = [];
                for ifile in glob.iglob(directory+"/**/*.root",recursive=True):
                    inputFileList.append(ifile);
                nchunk = int(len(inputFileList)/args.nfiles) if len(inputFileList) % args.nfiles == 0 else int(len(inputFileList)/args.nfiles)+1
                inputFileListSplit = numpy.array_split(numpy.array(inputFileList),nchunk);
                for idx in range(0,len(inputFileListSplit)):
                    njobs = njobs +1;
                    offset = idx*args.nfiles;
                    job_script.write("if [ $1 -eq "+str(idx)+" ]; then\n");            
                    output_filename = "tree_"+output_name+"_"+str(idx)+".root";
                    if args.is_mc:                        
                        job_script.write(" makeDomainSumOfWeights --inputDirectory "+directory+" --outputDIR ./ --outputFileName "+output_filename+" --isMC true --nFilesToAnalyze "+str(args.nfiles)+" --domain-type domain_type::"+args.domain_type+" --nThreads "+str(args.nthreads)+" --nFilesOffset "+str(offset)+" \n");
                    else:
                        job_script.write(" makeDomainSumOfWeights --inputDirectory "+directory+" --outputDIR ./ --outputFileName "+output_filename+" --isMC false --nFilesToAnalyze "+str(args.nfiles)+" --domain-type domain_type::"+args.domain_type+" --nThreads "+str(args.nthreads)+" --nFilesOffset "+str(offset)+" \n");
                    job_script.write(" xrdcp -f "+output_filename.replace(".root","*.root")+" "+args.output_dir+"\n")
                    job_script.write("fi\n");

            job_script.close();
            os.system("chmod +x condor_job.sh");
                    
            print("Create HTCondor submission file");
            condor_job = open("condor_job.sub","w");
            condor_job.write("universe = vanilla\n");
            if "el9" in os.environ['SCRAM_ARCH']:
                condor_job.write("MY.WantOS = \"el9\""+"\n");
            elif "el8" in os.environ['SCRAM_ARCH']:
                condor_job.write("MY.WantOS = \"el8\""+"\n");
            elif "el7" in os.environ['SCRAM_ARCH']:
                condor_job.write("MY.WantOS = \"el7\""+"\n");
            condor_job.write("request_cpus   = "+str(args.nthreads)+"\n");
            condor_job.write("request_memory = "+str(2000*args.nthreads)+"\n");
            condor_job.write("executable = %s/condor_job.sh\n"%(jdir));
            condor_job.write("arguments = $(ProcId)\n");
            condor_job.write("log = %s/condor_job_$(ProcId).log\n"%(jdir));
            condor_job.write("output = %s/condor_job_$(ProcId).out\n"%(jdir));
            condor_job.write("error  = %s/condor_job_$(ProcId).err\n"%(jdir));
            condor_job.write("should_transfer_files = YES\n");
            condor_job.write("when_to_transfer_output = ON_EXIT\n");
            condor_job.write("on_exit_remove  = (ExitBySignal == False) && (ExitCode == 0)\n");
            condor_job.write("transfer_output_files = \"\"\n");
            condor_job.write("+JobFlavour = \""+args.queque+"\"\n");
            condor_job.write("queue "+str(njobs)+" \n");
            condor_job.close();
            
            if args.submit:
                os.system("condor_submit condor_job.sub");
            else:
                sys.exit("No jobs submitted cause submit option was not specified")
