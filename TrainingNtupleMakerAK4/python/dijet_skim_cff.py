import FWCore.ParameterSet.Config as cms

## trigger selection
triggerResultFilter = cms.EDFilter('TriggerResultsFilter',
        hltResults = cms.InputTag('TriggerResults','',"HLT"),
        l1tResults = cms.InputTag(''),
        l1tIgnoreMaskAndPrescale = cms.bool(False),
        throw = cms.bool(False),
        triggerConditions = cms.vstring(
            'HLT_ZeroBias_v*',
            'HLT_ZeroBias_part*_v*'
        )
);

## muon selection
vetoMuons = cms.EDFilter("PATMuonSelector",
        src = cms.InputTag("correctedMuons"),
        cut = cms.string("pt > 15 && abs(eta) < 2.4 && passed('CutBasedIdTight') && passed('PFIsoLoose')"),
        filter = cms.bool(False)
);

filterVetoMuons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("vetoMuons"),
        minNumber = cms.uint32(0),
        maxNumber = cms.uint32(0)
);

## electron selection
vetoElectrons = cms.EDFilter("PATElectronSelector",
        src = cms.InputTag("slimmedElectrons"),
        cut = cms.string("pt*(userFloat('ecalTrkEnergyPostCorr')/energy) > 15 && abs(eta) < 2.5 && electronID('mvaEleID-Fall17-iso-V2-wp90')"),
        filter = cms.bool(False)
);

filterVetoElectrons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("vetoElectrons"),
        minNumber = cms.uint32(0),
        maxNumber = cms.uint32(0),
);

## final sequence
leptonSelection = cms.Sequence(
      triggerResultFilter+
      vetoMuons+
      filterVetoMuons+
      vetoElectrons+
      filterVetoElectrons
)

## jet selection (keep only the first two jets --> leading pt if present)
from PhysicsTools.PatAlgos.selectionLayer1.jetSelector_cfi import selectedPatJets
selectedJets = selectedPatJets.clone();
selectedJets.src = cms.InputTag("slimmedJetsUpdated");
selectedJets.cut = cms.string("correctedJet('Uncorrected').pt() > 30 && abs(eta) < 2.5");
selectedJets.filter = cms.bool(False);

filterSelectedJets = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("selectedJets"),
        minNumber = cms.uint32(2),
        maxNumber = cms.uint32(99)
);

tagJets = selectedJets.clone();
tagJets.src = cms.InputTag("selectedJets");
tagJets.cut = cms.string("correctedJet('Uncorrected').pt() > 30 && abs(eta) < 1.3");
tagJets.filter = cms.bool(False);

filterTagJets = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("tagJets"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(99)
);

selectedHighestPtJets = cms.EDFilter("LargestPtCandViewSelector",
   src = cms.InputTag("selectedJets"),
   maxNumber = cms.uint32(2)
)

## dijet pairs                                                                                                                                                                                       
dijetPairs = cms.EDProducer("CandViewShallowCloneCombiner",
    decay = cms.string("selectedHighestPtJets@+ selectedHighestPtJets@-"),
    cut = cms.string("abs(deltaPhi(daughter(0).phi,daughter(1).phi)) > 2.7 && abs(daughter(0).pt-daughter(1).pt)/(daughter(0).pt+daughter(1).pt) < 0.70"),
    checkCharge = cms.bool(False)
);

filterDijetPairs = cms.EDFilter("PATCandViewCountFilter",
    src = cms.InputTag("dijetPairs"),
    minNumber = cms.uint32(1),
    maxNumber = cms.uint32(99)
);

jetSelection = cms.Sequence(
    selectedJets +
    filterSelectedJets +
    tagJets +
    filterTagJets +
    selectedHighestPtJets + 
    dijetPairs +
    filterDijetPairs    
)
