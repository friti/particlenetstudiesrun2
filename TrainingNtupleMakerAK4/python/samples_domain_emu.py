def AddSTSamples(samples):

    samples['ST_tW_antitop'] = [
        '/ST_tW_antitop_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.251e+01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        100000,
        '',
        -1
    ]

    samples['ST_tW_top'] = [
	'/ST_tW_top_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.245e+01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        100000,
        '',
        -1
    ]

    samples['ST_t_channel'] = [
        '/ST_t-channel_muDecays_TuneCP5_13TeV-comphep-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.955e+01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        100000,
        '',
        -1
    ]

    samples['ST_s_channel'] = [
        '/ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.549e+00 ','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        100000,
        '',
        -1
    ]

def AddVVSamples(samples):

 samples['WWTo2L'] = [
        '/WWTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.109e+01','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        100000,
        '',
        -1
    ]


def AddTTbarSamples(samples):

 samples['TTTo2L'] = [
        '/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=86.46','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'EventAwareLumiBased',
        500000,
        '',
        -1
    ]

def AddDataSamples(samples):

    samples['SingleMuon2018A'] = [
        '/SingleMuon/Run2018A-UL2018_MiniAODv2-v3/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        150,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

    samples['SingleMuon2018B'] = [
        '/SingleMuon/Run2018B-UL2018_MiniAODv2-v2/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        150,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

    samples['SingleMuon2018C'] = [
        '/SingleMuon/Run2018C-UL2018_MiniAODv2-v2/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        150,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

    samples['SingleMuon2018D'] = [
        '/SingleMuon/Run2018D-UL2018_MiniAODv2-v3/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=emu','evaluateParTTraining=True','selectOnParTScore=True'],
        'LumiBased',
        150,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

def AddAllSamples(samples):
    AddTTbarSamples(samples)
    AddDataSamples(samples)
    AddSTSamples(samples)
    AddVVSamples(samples)
