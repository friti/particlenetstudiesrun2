def AddTTbarSamples(samples):

    samples['TTToSemiLeptonic'] = [
        '/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=384.26','isMC=True','jetPtMin=10','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','dRJetGenMatch=0.4','evaluateParTTraining=True'],
        'EventAwareLumiBased',
        50000,
        '',
        10000000
    ]


def signalBsTauTauSamples(samples):

    samples['BsToTauTau'] = [
        '/ttbarToBsToTauTau_BsFilter_TauTauFilter_TuneCP5_13TeV-pythia8-evtgen/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.','isMC=True','jetPtMin=10','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True','dRJetGenMatch=0.4','evaluateParTTraining=True'],
                'EventAwareLumiBased',
        50000,
        '',
        10000000
    ]

def AddAllSamples(samples):
    AddTTbarSamples(samples)
    signalBsTauTauSamples(samples)
    
