import os
import sys
import glob
import argparse
import subprocess
import shutil
from importlib import import_module

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--sample-config', type=str, default='samples_training.py',help='python file containing list of samples')
parser.add_argument('-t', '--sample-type', nargs="+", default=[], help='list of sample type to be considered')
parser.add_argument('-c', '--crab-config', type=str, default='../crab/crabConfig_training.py',help='python file that dives the overall crabDescription')
parser.add_argument('-e', '--crab-script', type=str, default='',help='bash script to be executed by crab (optional)')
parser.add_argument('-j', '--crab-job-dir', type=str, default='../crab/',help='directory where crab jobs folder will be created')
parser.add_argument('-f', '--force-copy-python-dir',action='store_true', default=False, help='force copy of $CMSSW_BASE/src/*/*/python into $CMSSW_BASE/python')
parser.add_argument('-m', '--command', type=str, default='submit', help="possible commands are: submit, resubmit, kill, status, getlog, erase,", choices=['submit','resubmit','kill','status','getlog','erase']);
parser.add_argument('-o', '--options', nargs="+", default=[], help="options to each specific crab command");
 
if __name__ == '__main__':

    args = parser.parse_args()

    if args.force_copy_python_dir:
        basedir = os.getcwd();
        for directory in glob.glob(os.environ["CMSSW_BASE"]+"/src/**/python/",recursive=True):
            dire = os.path.relpath(directory,start=os.environ["CMSSW_BASE"]+"/src")
            dire = dire.replace("/python","")
            dire = dire.split("/");
            os.makedirs(os.environ["CMSSW_BASE"]+"/python/",exist_ok=True);
            slink_path = os.environ["CMSSW_BASE"]+"/python/"+''.join(dire[:1]);
            slink_rpath = os.path.relpath(directory,slink_path);
            slink_name = ''.join(dire[-1:])
            os.chdir(slink_path);
            if os.path.islink(slink_name):
                os.unlink(slink_name)
            elif os.path.isfile(slink_name) or os.path.isdir(slink_name):
                shutil.rmtree(slink_name)
            os.symlink(slink_rpath,slink_name)
            os.chdir(basedir);

    ## import the sample config
    samples_dict = {};
    sample_module = import_module(args.sample_config.replace('.py', '').replace('/', '.'))
    if not args.sample_type or "All" in args.sample_type:
        sample_module.AddAllSamples(samples_dict);
    else:
        if "DY" in args.sample_type:
            sample_module.AddDYSamples(samples_dict);
        if "ST" in args.sample_type:
            sample_module.AddSTSamples(samples_dict);
        if "WJet" in args.sample_type:
            sample_module.AddWJetSamples(samples_dict);
        if "Higgs" in args.sample_type:
            sample_module.AddHiggsSamples(samples_dict);
        if "QCD" in args.sample_type:
            sample_module.AddQCDSamples(samples_dict);
        if "QCDHT" in args.sample_type:
            sample_module.AddQCDHTSamples(samples_dict);
        if "QCDb" in args.sample_type:
            sample_module.AddQCDbEnrichedSamples(samples_dict);
        if "TTbar" in args.sample_type:
            sample_module.AddTTbarSamples(samples_dict);
        if "VV" in args.sample_type:
            sample_module.AddVVSamples(samples_dict);
        if "HH" in args.sample_type:
            sample_module.AddHHNonResSamples(samples_dict);
        if "BulkGrav" in args.sample_type:
            sample_module.AddBulkGravSamples(samples_dict);
        if "BoostedH" in args.sample_type:
            sample_module.AddBoostedHSamples(samples_dict);
        if "BoostedZ" in args.sample_type:
            sample_module.AddBoostedZSamples(samples_dict);
        if "XtoHH" in args.sample_type:
            sample_module.AddXtoHHSamples(samples_dict);
        if "Data" in args.sample_type:
            sample_module.AddDataSamples(samples_dict);

    # adjusting the command to submit
    if args.options:
        args.options = ["--"+s for s in args.options];

    ## loop over values and create directories or erase them                                                                                                                                          
    for key,value in samples_dict.items():    
        path = os.path.join(os.getcwd(),args.crab_job_dir,key);
        ## create directories for crab
        if (os.path.exists(path) and os.path.isdir(path)):
            print (key+" directory exists in "+os.path.join(os.getcwd(),args.crab_job_dir));
            if args.command == "erase":
                print ("Removing directory");
                shutil.rmtree(path);
        else :
            if args.command == "submit":
                print (key+" directory does not exists --> create it since command is submit");
                os.makedirs(path); 
   
    if args.command == "erase":
        sys.exit();
    else:
        mother_dir = os.getcwd();
        for key,value in samples_dict.items():    
            os.chdir(mother_dir);
            path = os.path.join(os.getcwd(),args.crab_job_dir,key);
            if not os.path.exists(path): continue;
            if not os.path.isdir(path): continue;
            if args.command == "submit":
                shutil.copy2(args.crab_config,path) 
                if args.crab_script:
                    shutil.copy2(args.crab_script,path) 
            os.chdir(path);
            if args.command == "submit":
                crab_query = subprocess.Popen("crab submit -c "+os.path.basename(args.crab_config)+" "+" ".join(args.options),shell=True);
                crab_query.wait();
            else:
                ## other crab actions to be performed on the subfolders where the .crabcache file is located
                for dirname in os.listdir(os.getcwd()):                
                    if not os.path.isdir(dirname): continue;
                    if "__pycache__" in dirname: continue;
                    if args.command == "status":
                        print("crab status -d "+dirname+" "+" --".join(args.options))
                        crab_query = subprocess.Popen("crab status -d "+dirname+" "+" ".join(args.options),shell=True);
                        crab_query.wait();
                    elif args.command == "kill":
                        crab_query = subprocess.Popen("crab kill -d "+dirname+" "+" ".join(args.options),shell=True);
                        crab_query.wait();
                    elif args.command == "resubmit":
                        crab_query = subprocess.Popen("crab resubmit -d "+dirname+" "+" ".join(args.options),shell=True);
                        crab_query.wait();
                    elif args.command == "getlog":
                        crab_query = subprocess.Popen("crab getlog -d "+dirname+" "+" ".join(args.options),shell=True);
                        crab_query.wait();
