import os
import argparse
import subprocess
from importlib import import_module

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--sample-config', type=str, default='samples_training.py',help='python file containing list of samples')
parser.add_argument('-m', '--max-number-events', type=int, default=25000, help='max number of events to be used for xsec evaluation')
parser.add_argument('-f', '--max-number-files', type=int, default=10, help='max number of files to be processed for xsec evaluation')
parser.add_argument('-c', '--cmssw-config', type=str, default='../test/makeGenXsecAnalyzer_cfg.py', help='cmssw job to be executed to evaluate the xsec')
parser.add_argument('-t', '--sample-type', nargs="+", default=[], help='list of sample type to be considered')

if __name__ == '__main__':
    args = parser.parse_args()
    samples_dict = {};
    sample_module = import_module(args.sample_config.replace('.py', '').replace('/', '.'))
    if not args.sample_type:
        sample_module.AddAllSamples(samples_dict);
    else:
        if "DY" in args.sample_type:
            sample_module.AddDYSamples(samples_dict);
        if "ST" in args.sample_type:
            sample_module.AddSTSamples(samples_dict);
        if "VV" in args.sample_type:
            sample_module.AddVVSamples(samples_dict);
        if "WJet" in args.sample_type:
            sample_module.AddWJetSamples(samples_dict);
        if "Higgs" in args.sample_type:
            sample_module.AddHiggsSamples(samples_dict);
        if "QCD" in args.sample_type:
            sample_module.AddQCDSamples(samples_dict);
        if "QCDHT" in args.sample_type:
            sample_module.AddQCDHTSamples(samples_dict);
        if "QCDb" in args.sample_type:
            sample_module.AddQCDbEnrichedSamples(samples_dict);
        if "TTbar" in args.sample_type:
            sample_module.AddTTbarSamples(samples_dict);
        if "HH" in args.sample_type:
            sample_module.AddHHNonResSamples(samples_dict);
        if "BulkGrav" in args.sample_type:
            sample_module.AddBulkGravSamples(samples_dict);
        if "BoostedH" in args.sample_type:
            sample_module.AddBoostedHSamples(samples_dict);
        if "BoostedZ" in args.sample_type:
            sample_module.AddBoostedZSamples(samples_dict);
        if "XtoHH" in args.sample_type:
            sample_module.AddXtoHHSamples(samples_dict);
        if "Data" in args.sample_type:
            sample_module.AddDataSamples(samples_dict);
            
    ## loop over values
    for key,value in samples_dict.items():
        dataset = value[0];
        command = 'dasgoclient --query "file dataset='+dataset+'" | tail -'+str(args.max_number_files);
        das_query = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE);
        das_query.wait();
        das_files = das_query.stdout.read().decode().splitlines();
        ## submit cmsRun
        command = "cmsRun "+args.cmssw_config+" maxEvents="+str(args.max_number_events)+" inputFiles="+",".join(das_files);
        cmssw_job = subprocess.Popen(command,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE);
        cmssw_job.wait();
        cmssw_output = cmssw_job.stderr.read().decode().splitlines();
        xsec_line = [line for line in cmssw_output if "final cross section" in line]
        print(key," --> ",xsec_line);
