def AddQCDSamples(samples):

    samples['QCD_Inclusive'] = [
        '/QCD_Pt-15to7000_TuneCP5_Flat2018_13TeV_pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=1.359e+09','isMC=True','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluatePNETModels=base,k0p35a2p0w3,k0p5emu,k0p5mtau,k0p35splitw3','evaluateParTModels=base,k0p35a2p0w3,k0p35splitw3,k0p25,k0p25adjv3,k0p25adjv4','selectOnParTScore=True'],
        'EventAwareLumiBased',
        300000,
        '',
        -1
    ]
    
def AddDataSamples(samples):

    samples['ZeroBias2018D'] = [
        '/ZeroBias/Run2018D-UL2018_MiniAODv2-v1/MINIAOD',
        ['xsec=-1','isMC=False','jetPtMin=25','jetEtaMax=2.5','analysisRegion=dijet','evaluatePNETModels=base,k0p35a2p0w3,k0p5emu,k0p5mtau,k0p35splitw3','evaluateParTModels=base,k0p35a2p0w3,k0p35splitw3,k0p25,k0p25adjv3,k0p25adjv4','selectOnParTScore=True'],
        'LumiBased',
        125,
        '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/Legacy_2018/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt',
        -1
    ]

def AddAllSamples(samples):
    AddQCDSamples(samples)
    AddDataSamples(samples)
    
