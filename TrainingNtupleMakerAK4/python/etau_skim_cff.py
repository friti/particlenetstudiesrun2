import FWCore.ParameterSet.Config as cms

def etauSelection(process,scoreLabel="pfParticleNetAK4base"):

    ## trigger selection
    process.triggerResultFilter = cms.EDFilter('TriggerResultsFilter',
        hltResults = cms.InputTag('TriggerResults','',"HLT"),
        l1tResults = cms.InputTag(''),
        l1tIgnoreMaskAndPrescale = cms.bool(False),
        throw = cms.bool(False),
        triggerConditions = cms.vstring(
            'HLT_Ele32_WPTight_Gsf_v*'
        )
    );

    ## electron selection
    process.tagElectrons = cms.EDFilter("PATElectronSelector",
        src = cms.InputTag("slimmedElectrons"),
        cut = cms.string("pt*(userFloat('ecalTrkEnergyPostCorr')/energy) > 35 && abs(eta) < 2.5 && electronID('mvaEleID-Fall17-iso-V2-wp80')"),
        filter = cms.bool(False)
    );
    
    process.filterTagElectrons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("tagElectrons"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(1)
    );

    ## lepton veto
    process.vetoElectrons = cms.EDFilter("PATElectronSelector",
        src = cms.InputTag("slimmedElectrons"),
        cut = cms.string("pt*(userFloat('ecalTrkEnergyPostCorr')/energy) > 15 && abs(eta) < 2.5 && electronID('mvaEleID-Fall17-iso-V2-wp90')"),
        filter = cms.bool(False)
    );

    process.filterVetoElectrons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("vetoElectrons"),
        minNumber = cms.uint32(0),
        maxNumber = cms.uint32(0)
    );

    process.vetoMuons = cms.EDFilter("PATMuonSelector",
        src = cms.InputTag("correctedMuons"),
        cut = cms.string("pt > 15 && abs(eta) < 2.4 && passed('CutBasedIdLoose') && passed('PFIsoLoose')"),
        filter = cms.bool(False)
    );

    process.filterVetoMuons = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("vetoMuons"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(1)
    );

    ## tau selection
    tauvsjet = "(bDiscriminator('"+scoreLabel+":probtaup1h0p')+bDiscriminator('"+scoreLabel+":probtaup1h1p')+bDiscriminator('"+scoreLabel+":probtaup1h2p')+bDiscriminator('"+scoreLabel+":probtaup3h0p')+bDiscriminator('"+scoreLabel+":probtaup3h1p')+bDiscriminator('"+scoreLabel+":probtaum1h0p')+bDiscriminator('"+scoreLabel+":probtaum1h1p')+bDiscriminator('"+scoreLabel+":probtaum1h2p')+bDiscriminator('"+scoreLabel+":probtaum3h0p')+bDiscriminator('"+scoreLabel+":probtaum3h1p'))/(1-bDiscriminator('"+scoreLabel+":probele')-bDiscriminator('"+scoreLabel+":probmu'))";
    tauvsele = "(bDiscriminator('"+scoreLabel+":probtaup1h0p')+bDiscriminator('"+scoreLabel+":probtaup1h1p')+bDiscriminator('"+scoreLabel+":probtaup1h2p')+bDiscriminator('"+scoreLabel+":probtaup3h0p')+bDiscriminator('"+scoreLabel+":probtaup3h1p')+bDiscriminator('"+scoreLabel+":probtaum1h0p')+bDiscriminator('"+scoreLabel+":probtaum1h1p')+bDiscriminator('"+scoreLabel+":probtaum1h2p')+bDiscriminator('"+scoreLabel+":probtaum3h0p')+bDiscriminator('"+scoreLabel+":probtaum3h1p'))/(1-(bDiscriminator('"+scoreLabel+":probb')+bDiscriminator('"+scoreLabel+":probc')+bDiscriminator('"+scoreLabel+":probuds')+bDiscriminator('"+scoreLabel+":probg')+bDiscriminator('"+scoreLabel+":probmu')))";
    tauvsmu = "(bDiscriminator('"+scoreLabel+":probtaup1h0p')+bDiscriminator('"+scoreLabel+":probtaup1h1p')+bDiscriminator('"+scoreLabel+":probtaup1h2p')+bDiscriminator('"+scoreLabel+":probtaup3h0p')+bDiscriminator('"+scoreLabel+":probtaup3h1p')+bDiscriminator('"+scoreLabel+":probtaum1h0p')+bDiscriminator('"+scoreLabel+":probtaum1h1p')+bDiscriminator('"+scoreLabel+":probtaum1h2p')+bDiscriminator('"+scoreLabel+":probtaum3h0p')+bDiscriminator('"+scoreLabel+":probtaum3h1p'))/(1-(bDiscriminator('"+scoreLabel+":probb')+bDiscriminator('"+scoreLabel+":probc')+bDiscriminator('"+scoreLabel+":probuds')+bDiscriminator('"+scoreLabel+":probg')+bDiscriminator('"+scoreLabel+":probele')))";

    ## tau selection
    process.tagTaus = cms.EDFilter("PATJetSelector",
        src = cms.InputTag("slimmedJetsUpdated"),
        cut = cms.string("correctedJet('Uncorrected').pt() > 25 && abs(eta) < 2.5 && "+tauvsjet+" > 0.90 && "+tauvsele+" > 0.95 && "+tauvsmu+" > 0.50"),
        filter = cms.bool(False)
    );
    
    process.filterTagTaus = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("tagTaus"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(1),
    );

    ##  etau pair selection
    process.etauPairs = cms.EDProducer("CandViewShallowCloneCombiner",
        decay = cms.string("tagElectrons@+ tagTaus@-"),
        cut = cms.string("mass > 50 && mass < 90 && deltaR(daughter(0).eta,daughter(0).phi,daughter(1).eta,daughter(1).phi) > 0.4"),
        checkCharge = cms.bool(False)
    );

    process.filterETauPairs = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("etauPairs"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(1)
    );
                                    

    ## final sequence
    process.leptonSelection = cms.Sequence(
        process.triggerResultFilter+
        process.tagElectrons+
        process.filterTagElectrons+
        process.vetoElectrons+
        process.filterVetoElectrons+
        process.vetoMuons+
        process.filterVetoMuons+
        process.tagTaus+
        process.filterTagTaus+
        process.etauPairs+
        process.filterETauPairs
    )

    from PhysicsTools.PatAlgos.cleaningLayer1.jetCleaner_cfi import cleanPatJets
    process.cleanJets = cms.EDProducer("PATJetCleaner",
        src = cms.InputTag("slimmedJetsUpdated"),
        preselection = cms.string(''),
        checkOverlaps = cms.PSet(
            electrons = cms.PSet(
                src          = cms.InputTag("tagElectrons"),
                algorithm    = cms.string("byDeltaR"),
                preselection = cms.string(""),
                deltaR       = cms.double(0.4),
                checkRecoComponents = cms.bool(False),
                pairCut             = cms.string(""),
                requireNoOverlaps   = cms.bool(True)
            )
        ),
      finalCut = cms.string('')
    )

    
    ## require at least one jet                                                                                                                                                                        
    from PhysicsTools.PatAlgos.selectionLayer1.jetSelector_cfi import selectedPatJets
    process.selectedCleanJets = selectedPatJets.clone();
    process.selectedCleanJets.src = cms.InputTag("cleanJets");
    process.selectedCleanJets.cut = cms.string("correctedJet('Uncorrected').pt() > 25 && abs(eta) < 2.5");
    process.selectedCleanJets.filter = cms.bool(False)

    process.filterCleanJets = cms.EDFilter("PATCandViewCountFilter",
        src = cms.InputTag("selectedCleanJets"),
        minNumber = cms.uint32(1),
        maxNumber = cms.uint32(99)
    );

    process.jetSelection = cms.Sequence(
        process.cleanJets +
        process.selectedCleanJets +
        process.filterCleanJets
    )

    return process;
