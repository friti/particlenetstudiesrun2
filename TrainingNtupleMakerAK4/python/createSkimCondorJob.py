import os, sys
import argparse
import glob
import shutil

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='',help='path to the location of input files (CERN EOS typically)')
parser.add_argument('-l', '--input-files-list', type=str, default='',help='path to the location of input files (CERN EOS typically)')
parser.add_argument('-o', '--output-dir', type=str, default='',help='output path where to store the skimmed files')
parser.add_argument('-f', '--output-file-name', type=str, default='tree.root',help='name of the output file that will be produced')
parser.add_argument('-t', '--nthreads', type=int, default=1, help='number of threads to be run and requested via HTCondor')
parser.add_argument('-n', '--nfiles', type=int, default=1, help='number of files per job to be processed')
parser.add_argument('-m', '--merge', action='store_true', default=False, help='merge the output files produced by the various threads .. only when num of output files > 1')
parser.add_argument('-jpt', '--minjpt', type=float, default=25, help='min jet pt')
parser.add_argument('-je', '--maxjeta', type=float, default=2.5, help='max jet eta')
parser.add_argument('-jem', '--minjeta', type=float, default=0.0, help='min jet eta')
parser.add_argument('-pfpt', '--minpfpt', type=float, default=0., help='min pf candidate pt')
parser.add_argument('-pfpw', '--minpfpw', type=float, default=0., help='min pf candidate puppi weight')
parser.add_argument('-a', '--sample-type', type=str, default="none", help='type of sample to be stored as a integer in the output file', 
                    choices=['none','dy','wjet','ttbar','qcd','ggH','vbfH','ggHH','bulkG','VH','data'])
parser.add_argument('-d', '--domain-type', type=str, default="none", help='type of domain region for the training', 
                    choices=['none','dimuon','dielectron','emu','mutau','etau','dijet','ttcharm'])
parser.add_argument('--pnet-labels', nargs="+", default=["base"], help='list of pnet labels only for validation jobs')
parser.add_argument('--parT-labels', nargs="+", default=["base"], help='list of parT labels only for validation jobs')
parser.add_argument('-g', '--save-only-gen', action='store_true', default=False, help='save only gen jets')
parser.add_argument('-p', '--select-on-part-score', action='store_true', default=False, help='select based on ParT score rather than pnet')
parser.add_argument('-e', '--jet-id', action='store_true', default=False, help='apply jet id to jets not matched to gen-truth leptions')
parser.add_argument('-ch', '--split-charge', action='store_true', default=False, help='split neutral and charged PF into two different set of branches')
parser.add_argument('-v', '--validation', action='store_true', default=False, help='run on validation trees')
parser.add_argument('-c', '--compress', action='store_true', default=False, help='compress output file') 
parser.add_argument('-j', '--job-dir', type=str, default='condor_jobs',help='directory where files for HTCondor jobs have to be created')
parser.add_argument('-q', '--queque', type=str, default="microcentury", help='queque for condorHT')
parser.add_argument('-s', '--submit', action='store_true', default=False, help='submit jobs to condorHT scherduler')

if __name__ == '__main__':
    
    args = parser.parse_args()


    ## prepare job directory
    jwd  = os.getcwd();
    jdir = os.path.join(jwd,args.job_dir);

    if os.path.exists(jdir) and os.path.isdir(jdir):        
        print("Job directory already exists --> remove its content");
        #shutil.rmtree(jdir);
        
    print("Create job directory = ",jdir);
    os.makedirs(jdir);

    ## create input file list
    inputFileList = [];
    if args.input_files_list == '':
        for element in glob.iglob(args.input_dir+"/**/*.root",recursive=True):
            inputFileList.append(element);
        print("inputFileList size ",len(inputFileList));
    else:
        for element in open(args.input_files_list).readlines():
            inputFileList.append(element);
        print("inputFileList size ",len(inputFileList));
            
    
    ## random shuffling of the list (useful for DA data)                                                                                                                                              
    import os,random
    random.seed = os.urandom(1000)
    random.shuffle(inputFileList);

    ## Loop over input files and split per nfiles
    print("Create job files lists");
    os.chdir(jdir);
    njob = 0;
    jobfile = [];
    for i,name in enumerate(inputFileList):
        if ((i+1) % (args.nfiles) == 0 and i != 0) or ((i+1) == len(inputFileList)):            
            jobfile.append(name);
            ## create the list
            with open("list_job_"+str(njob)+".txt","w") as f:
                for ifile in jobfile:
                    f.write(ifile);
                f.close();
                njob = njob+1;
            jobfile.clear();
        else:
            jobfile.append(name);

    ## prepare the actual jobs
    print("Write job exectution file");
    job_script = open("condor_job.sh","w");
    job_script.write("#!/bin/bash\n");    
    job_script.write('cd '+jwd+'\n');
    job_script.write('source /cvmfs/cms.cern.ch/cmsset_default.sh'+'\n');
    job_script.write('export EOS_MGM_URL='+os.getenv('EOS_MGM_URL')+'\n');
    job_script.write('cmsenv \n')
    job_script.write("cd -\n");    
    job_script.write("mkdir -p "+args.output_dir+"\n");

    #####
    save_only_gen = False;
    if args.save_only_gen:
        save_only_gen = True;
    #####
    jet_id = False;
    if args.jet_id:
        jet_id = True;
    #####
    merge = False;
    if args.merge:
        merge = True;
    #####        
    compress = False;
    if args.compress:
        compress = True;
    #####        
    split = False;
    if args.split_charge:
        split = True;
    #####        
    select_on_parT_score = False;
    if args.select_on_part_score:
        select_on_parT_score = True;
        
    #####        
    for ijob in range(0,njob):
        output_name = args.output_file_name.replace(".root","_job_%d.root"%ijob);        
        job_script.write("if [ $1 -eq "+str(ijob)+" ]; then\n");
        job_script.write(" scp "+jdir+"/list_job_"+str(ijob)+".txt ./\n");
        if not args.validation:
            command = " makeSkimmedNtuplesForTraining --inputFileList list_job_"+str(ijob)+".txt --outputDIR ./ --outputFileName "+output_name+" --nThreads "+str(args.nthreads)+" --mergeThreadOutputFiles "+str(merge)+" --useXRootD false --compressOutputFile "+str(compress)+" --jetPtMin "+str(args.minjpt)+" --jetEtaMax "+str(args.maxjeta)+" --jetEtaMin "+str(args.minjeta)+"  --sample-type sample_type::"+args.sample_type+" --saveOnlyGenMatchedJets "+str(save_only_gen)+" --applyJetID "+str(jet_id)+" --pfCandPtMin "+str(args.minpfpt)+" --pfCandPuppiWeightMin "+str(args.minpfpw)+" --domain-type domain_type::"+args.domain_type+" --selectOnParTScore "+str(select_on_parT_score)+" --splitChargedPF "+str(split);
        else:
            command = "makeSkimmedNtuplesForValidation --inputFileList list_job_"+str(ijob)+".txt --outputDIR ./ --outputFileName "+output_name+" --nThreads "+str(args.nthreads)+" --mergeThreadOutputFiles "+str(merge)+" --useXRootD false --compressOutputFile "+str(compress)+" --jetPtMin "+str(args.minjpt)+" --jetEtaMax "+str(args.maxjeta)+" --jetEtaMin "+str(args.minjeta)+" --sample-type sample_type::"+args.sample_type+" --saveOnlyGenMatchedJets "+str(save_only_gen)+" --applyJetID "+str(jet_id)+" --domain-type domain_type::"+args.domain_type+" --selectOnParTScore "+str(select_on_parT_score)+" --splitChargedPF "+str(split);            
            if args.pnet_labels:
                command += " --pnetLabels "+','.join(args.pnet_labels)
            if args.parT_labels:
                command += " --parTLabels "+','.join(args.parT_labels)
        command += "\n";
        job_script.write(command);
        job_script.write(" rm -f "+args.output_dir+"/"+output_name.replace(".root","*.root")+"\n"); 
        job_script.write(" xrdcp -f "+output_name.replace(".root","*.root")+" "+args.output_dir+"\n")
        job_script.write("fi\n");

    job_script.close();

    os.system("chmod +x condor_job.sh");

    print("Create HTCondor submission file");
    condor_job = open("condor_job.sub","w");
    condor_job.write("universe = vanilla\n");
    if "el9" in os.environ['SCRAM_ARCH']:
        condor_job.write("MY.WantOS = \"el9\""+"\n");
    elif "el8" in os.environ['SCRAM_ARCH']:
        condor_job.write("MY.WantOS = \"el8\""+"\n");
    elif "el7" in os.environ['SCRAM_ARCH']:
        condor_job.write("MY.WantOS = \"el7\""+"\n");        
    condor_job.write("request_cpus   = "+str(args.nthreads)+"\n");
    condor_job.write("request_memory = "+str(2000*args.nthreads)+"\n");
    condor_job.write("executable = %s/condor_job.sh\n"%(jdir));
    condor_job.write("Proxy_filename = x509up_u121632\n");
    condor_job.write("Proxy_path              = /afs/cern.ch/user/f/friti/$(Proxy_filename)\n");
    condor_job.write("x509userproxy           = $(Proxy_path)\n");
    condor_job.write("use_x509userproxy       = true\n");
    
    
    condor_job.write("arguments = $(ProcId)\n");
    condor_job.write("should_transfer_files = YES\n");
    condor_job.write("log = %s/condor_job_$(ProcId).log\n"%(jdir));
    condor_job.write("output = %s/condor_job_$(ProcId).out\n"%(jdir));
    condor_job.write("error  = %s/condor_job_$(ProcId).err\n"%(jdir));
    condor_job.write("when_to_transfer_output = ON_EXIT\n");
    condor_job.write("on_exit_remove  = (ExitBySignal == False) && (ExitCode == 0)\n");
    condor_job.write("transfer_output_files = \"\"\n");
    condor_job.write("+JobFlavour = \""+args.queque+"\"\n");
    condor_job.write("queue "+str(njob)+"\n");
    condor_job.close();

    if args.submit:
        print("job submission");
        os.system("condor_submit condor_job.sub");
    else:
     sys.exit("No jobs submitted cause submit option was not specified")
