def AddHiggsSamples(samples):

    samples['GluGluHToTauTau_M-125'] = [
        '/GluGluHToTauTau_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM',
        ['xsec=3.047','isMC=True','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True'],
        'EventAwareLumiBased',
        70000,
        '',
        5000000
    ]
    
    samples['VBFHToTauTau_M125'] = [
        '/VBFHToTauTau_M125_TuneCP5_13TeV-powheg-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM',
        ['xsec=0.237','isMC=True','jetEtaMax=2.5','dumpOnlyJetMatchedToGen=True'],
        'EventAwareLumiBased',
        70000,
        '',
        5000000
    ]
        
def AddAllSamples(samples):
    AddHiggsSamples(samples)
    
