#include "commonTools.h"

void makeRegressionQuantiles(const string & fileNameStringToGrep, 
			     const string & dataConfigStringToGrep, //YAML weaver file
			     const string & outputDIR,
			     const bool   & useXRootD  = false,
			     const bool   & applyReweight  = false,
			     const vector<float> & resBinsJet = {0.050,0.065,0.080,0.100,0.125,0.150,0.175,0.200,0.250},
			     const vector<float> & resBinsTau = {0.010,0.030,0.050,0.065,0.075,0.090,0.110,0.130,0.150,0.175,0.200},
			     const bool   & requireMatchingHPS = false,
			     const bool   & requireMatchingLepton = false,
			     const vector<sample_type> & sample_selected = {},
			     const int    & nThreads = 4
			     ){

  string xrootd_eos = string(getenv("EOS_MGM_URL"))+"//";

  ROOT::GetROOT()->SetBatch(kTRUE);
  system(("mkdir -p "+outputDIR).c_str());
  setTDRStyle();
  gStyle->SetOptStat(0);

  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();

  // Open file and take tree
  cout<<"makeRegressionResponse --> starting code"<<endl;
  map<string,string> inputFileName;
  inputFileName["PNETNoJEC"] = "/media/Disk1/rgerosa/WeaverResults/ClassReg/trainingAK4_taumuel_classreg_charge_nojec_twotarget_2212022";
  inputFileName["ParTNoJEC"] = "/media/Disk1/rgerosa/WeaverResults/ClassReg/trainingAK4_taumuel_classreg_transformer_21052023";

  map<string,int> nEvents;
  for(auto imap: inputFileName){
    unique_ptr<TChain> inputTree (new TChain("Events"));
    if(useXRootD)
      inputTree->Add((xrootd_eos+imap.second+"/"+fileNameStringToGrep+"*.root").c_str());
    else
      inputTree->Add((imap.second+"/"+fileNameStringToGrep+"*.root").c_str());
    nEvents[imap.first] = inputTree->GetEntries();
    cout<<"makeRegressionResponse --> imap.first "<<imap.first<<" nevents "<<nEvents[imap.first] <<endl;
  }

  // Response binning
  float min_x = 0.30;
  float max_x = 2.00;
  float min_x_tau = 0.40;
  float max_x_tau = 1.80;
  int   nbins = 1000;
  int   nbins_tau = 1500;

  // Parse the DataConfig via python
  cout<<"makeRegressionResponse --> Parsing data config file"<<endl;
  map<string,shared_ptr<TH2F>> weights;
  for(auto imap: inputFileName){
    cout<<"makeRegressionResponse --> python3 parseDataConfig.py --data-config "+imap.second+"/"+dataConfigStringToGrep+" --output-file "+outputDIR+"/weight_"+imap.first+".root"<<endl;
    system(("python3 parseDataConfig.py --data-config "+imap.second+"/"+dataConfigStringToGrep+" --output-file "+outputDIR+"/weight_"+imap.first+".root").c_str());
    unique_ptr<TFile> inputFileWeights (TFile::Open((outputDIR+"/weight_"+imap.first+".root").c_str(),"READ"));
    if(inputFileWeights->Get("reweight_label_b"))   weights[imap.first+"_label_b"]   = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_b"));
    if(inputFileWeights->Get("reweight_label_c"))   weights[imap.first+"_label_c"]   = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_c"));
    if(inputFileWeights->Get("reweight_label_uds")) weights[imap.first+"_label_uds"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_uds"));
    if(inputFileWeights->Get("reweight_label_g"))   weights[imap.first+"_label_g"]   = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_g"));
    if(inputFileWeights->Get("reweight_label_taup_1h0p")) weights[imap.first+"_label_taup_1h0p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taup_1h0p"));
    if(inputFileWeights->Get("reweight_label_taup_1h1p")) weights[imap.first+"_label_taup_1h1p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taup_1h1p"));
    if(inputFileWeights->Get("reweight_label_taup_1h2p")) weights[imap.first+"_label_taup_1h2p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taup_1h2p"));
    if(inputFileWeights->Get("reweight_label_taup_3h0p")) weights[imap.first+"_label_taup_3h0p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taup_3h0p"));
    if(inputFileWeights->Get("reweight_label_taup_3h1p")) weights[imap.first+"_label_taup_3h1p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taup_3h1p"));
    if(inputFileWeights->Get("reweight_label_taum_1h0p")) weights[imap.first+"_label_taum_1h0p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taum_1h0p"));
    if(inputFileWeights->Get("reweight_label_taum_1h1p")) weights[imap.first+"_label_taum_1h1p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taum_1h1p"));
    if(inputFileWeights->Get("reweight_label_taum_1h2p")) weights[imap.first+"_label_taum_1h2p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taum_1h2p"));
    if(inputFileWeights->Get("reweight_label_taum_3h0p")) weights[imap.first+"_label_taum_3h0p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taum_3h0p"));
    if(inputFileWeights->Get("reweight_label_taum_3h1p")) weights[imap.first+"_label_taum_3h1p"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_taum_3h1p"));
    if(inputFileWeights->Get("reweight_label_b"))   weights[imap.first+"_label_b"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_c"))   weights[imap.first+"_label_c"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_uds")) weights[imap.first+"_label_uds"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_g"))   weights[imap.first+"_label_g"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taup_1h0p")) weights[imap.first+"_label_taup_1h0p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taup_1h1p")) weights[imap.first+"_label_taup_1h1p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taup_1h2p")) weights[imap.first+"_label_taup_1h2p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taup_3h0p")) weights[imap.first+"_label_taup_3h0p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taup_3h1p")) weights[imap.first+"_label_taup_3h1p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taum_1h0p")) weights[imap.first+"_label_taum_1h0p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taum_1h1p")) weights[imap.first+"_label_taum_1h1p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taum_1h2p")) weights[imap.first+"_label_taum_1h2p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taum_3h0p")) weights[imap.first+"_label_taum_3h0p"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_taum_3h1p")) weights[imap.first+"_label_taum_3h1p"]->SetDirectory(0);
 
    inputFileWeights->Close();
  }

  // histograms for signal and background
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_resolution_b;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_resolution_c;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_resolution_uds;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_resolution_g;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_resolution_tau;

  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_response_b;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_response_c;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_response_uds;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_response_g;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_response_tau;

  for(auto imap: inputFileName){
    for(size_t ibin = 0; ibin < resBinsJet.size()-1; ibin++){
      string name = imap.first+"_res_bin_"+to_string(ibin);
      hist_resolution_b[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_resolution_b_"+name).c_str(),"",nbins,resBinsJet.at(ibin),resBinsJet.at(ibin+1)));
      hist_resolution_c[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_resolution_c_"+name).c_str(),"",nbins,resBinsJet.at(ibin),resBinsJet.at(ibin+1)));
      hist_resolution_uds[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_resolution_uds_"+name).c_str(),"",nbins,resBinsJet.at(ibin),resBinsJet.at(ibin+1)));
      hist_resolution_g[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_resolution_g_"+name).c_str(),"",nbins,resBinsJet.at(ibin),resBinsJet.at(ibin+1)));
      hist_response_b[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_response_b_"+name).c_str(),"",nbins,min_x,max_x));
      hist_response_c[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_response_c_"+name).c_str(),"",nbins,min_x,max_x));
      hist_response_uds[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_response_uds_"+name).c_str(),"",nbins,min_x,max_x));
      hist_response_g[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_response_g_"+name).c_str(),"",nbins,min_x,max_x));
    }
    for(size_t ibin = 0; ibin < resBinsTau.size()-1; ibin++){
      string name = imap.first+"_res_bin_"+to_string(ibin);
      hist_resolution_tau[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_resolution_tau_"+name).c_str(),"",nbins,resBinsTau.at(ibin),resBinsTau.at(ibin+1)));
      hist_response_tau[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_response_tau_"+name).c_str(),"",nbins_tau,min_x_tau,max_x_tau));
    }
  }

  ////////////////
  auto fillHistogram = [&](int workerID,  string fileKey, int nevents){

    unique_ptr<TChain> chain (new TChain("Events"));
    if(useXRootD)
      chain->Add((xrootd_eos+inputFileName[fileKey]+"/"+fileNameStringToGrep+"*root").c_str());
    else
      chain->Add((inputFileName[fileKey]+"/"+fileNameStringToGrep+"*root").c_str());

    TTreeReader reader (chain.get());
    
    TTreeReaderValue<float> jet_pt   (reader,"jet_pt");
    TTreeReaderValue<float> jet_pt_raw   (reader,"jet_pt_raw");
    TTreeReaderValue<float> jet_eta  (reader,"jet_eta");
    TTreeReaderValue<float> jet_phi  (reader,"jet_phi");
    TTreeReaderValue<float> jet_mass (reader,"jet_mass");
    TTreeReaderValue<int> sample (reader,"sample");

    TTreeReaderValue<bool> label_b (reader,"label_b");
    TTreeReaderValue<bool> label_c (reader,"label_c");
    TTreeReaderValue<bool> label_uds (reader,"label_uds");
    TTreeReaderValue<bool> label_g (reader,"label_g");
    TTreeReaderValue<bool> label_mu (reader,"label_mu");
    TTreeReaderValue<bool> label_el (reader,"label_el");
    TTreeReaderValue<bool> label_taup_1h0p (reader,"label_taup_1h0p");
    TTreeReaderValue<bool> label_taup_1h1p (reader,"label_taup_1h1p");
    TTreeReaderValue<bool> label_taup_1h2p (reader,"label_taup_1h2p");
    TTreeReaderValue<bool> label_taup_3h0p (reader,"label_taup_3h0p");
    TTreeReaderValue<bool> label_taup_3h1p (reader,"label_taup_3h1p");
    TTreeReaderValue<bool> label_taum_1h0p (reader,"label_taum_1h0p");
    TTreeReaderValue<bool> label_taum_1h1p (reader,"label_taum_1h1p");
    TTreeReaderValue<bool> label_taum_1h2p (reader,"label_taum_1h2p");
    TTreeReaderValue<bool> label_taum_3h0p (reader,"label_taum_3h0p");
    TTreeReaderValue<bool> label_taum_3h1p (reader,"label_taum_3h1p");
    
    TTreeReaderValue<float> jet_taumatch_pt (reader,"jet_taumatch_pt");    

    TTreeReaderValue<float> target_pt (reader,"target_pt");
    TTreeReaderValue<float> score_target_pt (reader,"score_target_pt");

    TTreeReaderValue<float>* score_target_pt_q16 = NULL;
    if(chain->GetBranch("score_target_pt_q16"))
      score_target_pt_q16 = new TTreeReaderValue<float> (reader,"score_target_pt_q16");
    else
      score_target_pt_q16 = new TTreeReaderValue<float> (reader,"score_target_pt_16");
    TTreeReaderValue<float>* score_target_pt_q84 = NULL;
    if(chain->GetBranch("score_target_pt_q84"))
      score_target_pt_q84 = new TTreeReaderValue<float> (reader,"score_target_pt_q84");
    else
      score_target_pt_q84 = new TTreeReaderValue<float> (reader,"score_target_pt_84");
    
    reader.SetTree(chain.get());
    auto beginEntry = (Long64_t) nevents*workerID/nThreads;
    auto endEntry   = (Long64_t) nevents*min(nThreads,workerID+1)/nThreads-1;
    reader.SetEntriesRange(beginEntry,endEntry);

    bool useRawPt = false;
    if(TString(fileKey).Contains("NoJEC"))
      useRawPt = true;
   
    while(reader.Next()){

      // sample selection                                                                                                                                                                             
      bool passing_sample = false;
      for(auto const & s : sample_selected){
        if(int(*sample) == static_cast<int>(s))
          passing_sample = true;
      }
      if(sample_selected.empty())
        passing_sample = true;
      if(not passing_sample) continue;

      // skip electrons and muons
      if(*label_mu or
	 *label_el) continue;

      // identify resolution bin
      float resolution = abs(**score_target_pt_q84-**score_target_pt_q16)/2;      
      float jetpt = *jet_pt;
      if(useRawPt) jetpt = *jet_pt_raw;

      //////////
      int resbin = -1;
      if(*label_b or 
	 *label_c or
	 *label_g or
	 *label_uds){
	for(size_t ires = 0; ires < resBinsJet.size()-1; ires++){
	  if(resolution >= resBinsJet.at(ires) and resolution < resBinsJet.at(ires+1)){
	    resbin = ires;
	    break;
	  }
	}
      }
      else if(*label_taup_1h0p or *label_taup_1h1p or *label_taup_1h2p or *label_taup_3h0p or *label_taup_3h1p or *label_taum_1h0p or *label_taum_1h1p or *label_taum_1h2p or *label_taum_3h0p or *label_taum_3h1p){
	for(size_t ires = 0; ires < resBinsTau.size()-1; ires++){
	  if(resolution >= resBinsTau.at(ires) and resolution < resBinsTau.at(ires+1)){
	    resbin = ires;
	    break;
	  }
	}
      }
      if(resbin == -1) continue;
      
      // name of the histogram
      string name = fileKey+"_res_bin_"+to_string(resbin);
      float weight = 1;
      if(applyReweight){	
	if(*label_b)
	  weight = weights[fileKey+"_label_b"]->GetBinContent(weights[fileKey+"_label_b"]->GetXaxis()->FindBin(jetpt),
							      weights[fileKey+"_label_b"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_c) 
	  weight = weights[fileKey+"_label_c"]->GetBinContent(weights[fileKey+"_label_c"]->GetXaxis()->FindBin(jetpt),
							      weights[fileKey+"_label_c"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_uds) 
	  weight = weights[fileKey+"_label_uds"]->GetBinContent(weights[fileKey+"_label_uds"]->GetXaxis()->FindBin(jetpt),
								weights[fileKey+"_label_uds"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_g) 
	  weight = weights[fileKey+"_label_g"]->GetBinContent(weights[fileKey+"_label_g"]->GetXaxis()->FindBin(jetpt),
							      weights[fileKey+"_label_g"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taup_1h0p)
          weight = weights[fileKey+"_label_taup_1h0p"]->GetBinContent(weights[fileKey+"_label_taup_1h0p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taup_1h0p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taup_1h1p)
          weight = weights[fileKey+"_label_taup_1h1p"]->GetBinContent(weights[fileKey+"_label_taup_1h1p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taup_1h1p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taup_1h2p)
          weight = weights[fileKey+"_label_taup_1h2p"]->GetBinContent(weights[fileKey+"_label_taup_1h2p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taup_1h2p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taup_3h0p)
          weight = weights[fileKey+"_label_taup_3h0p"]->GetBinContent(weights[fileKey+"_label_taup_3h0p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taup_3h0p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taup_3h1p)
          weight = weights[fileKey+"_label_taup_3h1p"]->GetBinContent(weights[fileKey+"_label_taup_3h1p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taup_3h1p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taum_1h0p)
          weight = weights[fileKey+"_label_taum_1h0p"]->GetBinContent(weights[fileKey+"_label_taum_1h0p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taum_1h0p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taum_1h1p)
          weight = weights[fileKey+"_label_taum_1h1p"]->GetBinContent(weights[fileKey+"_label_taum_1h1p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taum_1h1p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taum_1h2p)
          weight = weights[fileKey+"_label_taum_1h2p"]->GetBinContent(weights[fileKey+"_label_taum_1h2p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taum_1h2p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taum_3h0p)
          weight = weights[fileKey+"_label_taum_3h0p"]->GetBinContent(weights[fileKey+"_label_taum_3h0p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taum_3h0p"]->GetYaxis()->FindBin(*jet_eta));
        else if(*label_taum_3h1p)
          weight = weights[fileKey+"_label_taum_3h1p"]->GetBinContent(weights[fileKey+"_label_taum_3h1p"]->GetXaxis()->FindBin(jetpt),weights[fileKey+"_label_taum_3h1p"]->GetYaxis()->FindBin(*jet_eta));
      }
      
      if(*label_b){
	(*hist_response_b[name])->Fill(*target_pt/(*score_target_pt),weight);
	(*hist_resolution_b[name])->Fill(resolution,weight);
      }
      else if(*label_c){
	(*hist_response_c[name])->Fill(*target_pt/(*score_target_pt),weight);
	(*hist_resolution_c[name])->Fill(resolution,weight);
      }
      else if(*label_uds){
	(*hist_response_uds[name])->Fill(*target_pt/(*score_target_pt),weight);
	(*hist_resolution_uds[name])->Fill(resolution,weight);
      }
      else if(*label_g){
	(*hist_response_g[name])->Fill(*target_pt/(*score_target_pt),weight);
	(*hist_resolution_g[name])->Fill(resolution,weight);
      }
      if(*label_taup_1h0p or *label_taup_1h1p or *label_taup_1h2p or *label_taup_3h0p or *label_taup_3h1p or *label_taum_1h0p or *label_taum_1h1p or *label_taum_1h2p or *label_taum_3h0p or *label_taum_3h1p){
	if(requireMatchingHPS and *jet_taumatch_pt <= 0) continue;
	(*hist_response_tau[name])->Fill(*target_pt/(*score_target_pt),weight);
	(*hist_resolution_tau[name])->Fill(resolution,weight);
      }
    }
  };
  
  
  for(auto imap: inputFileName){
    std::cout<<"makeRegressionResponse --> start looping for "<<imap.first<<" with nThreads = "<<nThreads<<std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::vector<std::thread> threads;
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(fillHistogram,workerID,imap.first,nEvents[imap.first]);
    for (auto && worker : threads) worker.join();
    auto stop  = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> duration = stop-start;
    std::cout<<"makeRegressionResponse --> duration loop is "<<duration.count()<<std::endl;
  }

  // Final plot
  cout<<"makeRegressionResponse --> Merge and scale"<<endl;
  map<string,shared_ptr<TH1F> > hist_response_b_merged;
  map<string,shared_ptr<TH1F> > hist_response_c_merged;
  map<string,shared_ptr<TH1F> > hist_response_uds_merged;
  map<string,shared_ptr<TH1F> > hist_response_g_merged;
  map<string,shared_ptr<TH1F> > hist_response_tau_merged;

  map<string,shared_ptr<TH1F> > hist_resolution_b_merged;
  map<string,shared_ptr<TH1F> > hist_resolution_c_merged;
  map<string,shared_ptr<TH1F> > hist_resolution_uds_merged;
  map<string,shared_ptr<TH1F> > hist_resolution_g_merged;
  map<string,shared_ptr<TH1F> > hist_resolution_tau_merged;

  for(auto & hist : hist_response_b) hist_response_b_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_response_c) hist_response_c_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_response_uds) hist_response_uds_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_response_g) hist_response_g_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_response_tau) hist_response_tau_merged[hist.first] = (*hist.second).Merge();

  for(auto & hist : hist_resolution_b) hist_resolution_b_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_resolution_c) hist_resolution_c_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_resolution_uds) hist_resolution_uds_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_resolution_g) hist_resolution_g_merged[hist.first] = (*hist.second).Merge();
  for(auto & hist : hist_resolution_tau) hist_resolution_tau_merged[hist.first] = (*hist.second).Merge();

  for(auto & hist : hist_response_b_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_response_c_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_response_uds_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_response_g_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_response_tau_merged) hist.second->Scale(1./hist.second->Integral());

  for(auto & hist : hist_resolution_b_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_resolution_c_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_resolution_uds_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_resolution_g_merged) hist.second->Scale(1./hist.second->Integral());
  for(auto & hist : hist_resolution_tau_merged) hist.second->Scale(1./hist.second->Integral());

  cout<<"makeRegressionResponse --> make final plots"<<endl;  
  map<string,vector<float> > response_b_parameters;
  map<string,vector<float> > response_c_parameters;
  map<string,vector<float> > response_uds_parameters;
  map<string,vector<float> > response_g_parameters;
  map<string,vector<float> > response_tau_parameters;

  for(size_t ires = 0 ; ires < resBinsJet.size()-1; ires++){

      vector<shared_ptr<TH1F> > response_to_plot_b;
      vector<shared_ptr<TH1F> > response_to_plot_c;
      vector<shared_ptr<TH1F> > response_to_plot_uds;
      vector<shared_ptr<TH1F> > response_to_plot_g;

      vector<string>  legends;
      string binlabel = "res_bin_"+to_string(ires);

      for(auto map_key: inputFileName){
        string name = map_key.first+"_"+binlabel;
	response_to_plot_b.push_back(hist_response_b_merged[name]);
	response_to_plot_c.push_back(hist_response_c_merged[name]);
	response_to_plot_uds.push_back(hist_response_uds_merged[name]);
	response_to_plot_g.push_back(hist_response_g_merged[name]);
        legends.push_back(map_key.first); 
	response_b_parameters[name] = vector<float>();
	response_c_parameters[name] = vector<float>();
	response_uds_parameters[name] = vector<float>();
	response_g_parameters[name] = vector<float>();
      }

      string binname = Form("%.3f < #sigma_{target} < %.3f",resBinsJet.at(ires),resBinsJet.at(ires+1));
      string xaxis_title = "p_{T}^{gen}/p_{T}}";

      buildRegResponse(response_to_plot_b,legends,response_b_parameters,xaxis_title,"b-jet",binname,outputDIR,"response_b_",binlabel,false);
      buildRegResponse(response_to_plot_c,legends,response_c_parameters,xaxis_title,"c-jet",binname,outputDIR,"response_c_",binlabel,false);
      buildRegResponse(response_to_plot_uds,legends,response_uds_parameters,xaxis_title,"uds-jet",binname,outputDIR,"response_uds_",binlabel,false);
      buildRegResponse(response_to_plot_g,legends,response_g_parameters,xaxis_title,"g-jet",binname,outputDIR,"response_g_",binlabel,false);

  }

  for(size_t ires = 0 ; ires < resBinsTau.size()-1; ires++){

      vector<shared_ptr<TH1F> > response_to_plot_tau;
      vector<string>  legends;
      string binlabel = "res_bin_"+to_string(ires);

      for(auto map_key: inputFileName){
        string name = map_key.first+"_"+binlabel;
        legends.push_back(map_key.first); 
	response_to_plot_tau.push_back(hist_response_tau_merged[name]);
	response_tau_parameters[name] = vector<float>();
      }

      string binname = Form("%.3f < #sigma_{target} < %.3f",resBinsTau.at(ires),resBinsTau.at(ires+1));
      string xaxis_title = "p_{T}^{gen}/p_{T}";      
      buildRegResponse(response_to_plot_tau,legends,response_tau_parameters,xaxis_title,"#tau-jet",binname,outputDIR,"response_tau_",binlabel,false);
  }


  // Observables vs pT
  vector<TGraph*> sigma_correlation_b;
  vector<TGraph*> sigma_correlation_c;
  vector<TGraph*> sigma_correlation_uds;
  vector<TGraph*> sigma_correlation_g;
  vector<TGraph*> sigma_correlation_tau;

  vector<string>  legends;

  for(auto imap: inputFileName){

    legends.push_back(imap.first);

    sigma_correlation_b.push_back(new TGraph());
    sigma_correlation_c.push_back(new TGraph());
    sigma_correlation_uds.push_back(new TGraph());
    sigma_correlation_g.push_back(new TGraph());
    sigma_correlation_tau.push_back(new TGraph());

    sigma_correlation_b.back()->SetName(Form("%s_sigma_correlation_b",imap.first.c_str()));
    sigma_correlation_c.back()->SetName(Form("%s_sigma_correlation_c",imap.first.c_str()));
    sigma_correlation_uds.back()->SetName(Form("%s_sigma_correlation_uds",imap.first.c_str()));
    sigma_correlation_g.back()->SetName(Form("%s_sigma_correlation_g",imap.first.c_str()));
    sigma_correlation_tau.back()->SetName(Form("%s_sigma_correlation_tau",imap.first.c_str()));
    
    double x_med = 0, x_q16 = 0, x_q84 = 0;
    double q_med = 0.5, q_16 = 0.16, q_84 = 0.84;
    for(size_t ires = 0 ; ires < resBinsJet.size()-1; ires++){      

      string binlabel = imap.first+"_res_bin_"+to_string(ires);
      sigma_correlation_b.back()->SetPoint(ires,response_b_parameters[binlabel].back(),hist_resolution_b_merged[binlabel]->GetMean());
      sigma_correlation_c.back()->SetPoint(ires,response_c_parameters[binlabel].back(),hist_resolution_c_merged[binlabel]->GetMean());
      sigma_correlation_uds.back()->SetPoint(ires,response_uds_parameters[binlabel].back(),hist_resolution_uds_merged[binlabel]->GetMean());
      sigma_correlation_g.back()->SetPoint(ires,response_g_parameters[binlabel].back(),hist_resolution_g_merged[binlabel]->GetMean());
    }

    for(size_t ires = 0 ; ires < resBinsTau.size()-1; ires++){      
      string binlabel = imap.first+"_res_bin_"+to_string(ires);
      sigma_correlation_tau.back()->SetPoint(ires,response_tau_parameters[binlabel].back(),hist_resolution_tau_merged[binlabel]->GetMean());
    }
  }
					     					     
  string xaxis_title = "Measured #sigma_{eff} [GeV]";
  string yaxis_title = "Predicted <#sigma_{eff}> [GeV]";

  plotRegressionQuantile(sigma_correlation_b,legends,xaxis_title,yaxis_title,outputDIR,"sigma_correlation_b","b-jet");
  plotRegressionQuantile(sigma_correlation_c,legends,xaxis_title,yaxis_title,outputDIR,"sigma_correlation_c","c-jet");
  plotRegressionQuantile(sigma_correlation_uds,legends,xaxis_title,yaxis_title,outputDIR,"sigma_correlation_uds","uds-jet");
  plotRegressionQuantile(sigma_correlation_g,legends,xaxis_title,yaxis_title,outputDIR,"sigma_correlation_g","g-jet");
  plotRegressionQuantile(sigma_correlation_tau,legends,xaxis_title,yaxis_title,outputDIR,"sigma_correlation_tau","#tau_{h}-jet");
}
