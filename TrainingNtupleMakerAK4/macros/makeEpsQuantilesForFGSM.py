
import os
import sys
import glob
import argparse
import subprocess
import shutil
import yaml
from importlib import import_module

import ROOT
import numpy as np
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-dir', type=str, default='', help='directory with input files');
parser.add_argument('-s', '--string-to-grep', type=str, default='', help='string in the file names to grep');
parser.add_argument('-d', '--data-config', type=str, default='', help="data config file of weaver");
parser.add_argument('-x', '--use-xrootd', action='store_true', help='use xrootd instead of local eos mount');
parser.add_argument('-n', '--nthreads', type=int, default=4, help='number of threads');
parser.add_argument('-q', '--quantile', type=float, default=0.95, help='one sided quantile of the distribution');
parser.add_argument('-l', '--quantileLep', type=float, default=0.995, help='one sided quantile of the distribution');
parser.add_argument('-o', '--output-file', type=str, default='', help='output file');
parser.add_argument('-c', '--clip-val', type=float, default=5, help='clipping value to be applied as default');
parser.add_argument('-v', '--var-exclude', nargs="+", default=["charge","frompv","track_qual","hit","layers","id","tau_signal","isglobal","station","ntrack","mask"],
                    help='list of variables for which no eps are stored --> fixed to 0');

if __name__ == '__main__':

    args = parser.parse_args()
    xrootd_eos = os.getenv("EOS_MGM_URL")+"//";

    ROOT.EnableImplicitMT(args.nthreads);
    ROOT.EnableThreadSafety();
    ## parse yaml file
    with open(args.data_config, 'r') as file:
        data_config = yaml.safe_load(file)

    ## take selections, new variables, and inputs
    selection = data_config['selection'];
    new_variables = data_config['new_variables'];
    inputs = data_config['inputs'];

    ## exclude masks    
    new_variables = {key: value for key, value in new_variables.items() if "mask" not in key };
    inputs = {key: value for key, value in inputs.items() if "mask" not in key };

    ## load the input dataframe
    if not args.use_xrootd:
        input_files = args.input_dir+"/*"+args.string_to_grep+"*root" if args.string_to_grep else args.input_dir+"/*root";
    else:
        input_files = xrootd_eos+"/"+args.input_dir+"/*"+args.string_to_grep+"*root" if args.string_to_grep else xrootd_eos+"/"+args.input_dir+"/*root";

    print("###### Create RDataFrame for ",input_files);
    data_frame = ROOT.RDataFrame("tree",input_files);
        
    ## add new colums
    print("###### Add new colums to DataFrame");
    for key,var in new_variables.items():        
        var = var.replace("np.","").replace("&","&&").replace("~","!").replace("_p4_from_ptetaphie","ROOT::VecOps::Construct<ROOT::Math::PtEtaPhiEVector>")
        if ".px" in var or ".py" in var or ".pz" in var:
            var1 = var.split(".")[0];
            var2 = var.split(".")[1];
            var = "return ROOT::VecOps::Map("+var1+",[](const ROOT::Math::PtEtaPhiEVector &v){return v."+var2+";})";
            var = var.replace("px","Px()").replace("py","Py()").replace("pz","Pz()")
        print(key," ",var);
        data_frame = data_frame.Define(key,var);
        
    ## edit selection string
    selection = selection.replace("&","&&").replace("~","!").replace("np.","")
    print("###### Add filter to DataFrame ",selection);
    data_frame = data_frame.Filter(selection);
    ## variables with transformation
    print("###### Create histograms for input features --> after transforming when needed");
    histograms = {}
    for key,var in inputs.items():
        for idx,element in enumerate(var['vars']):
            ## all elements are written in the yaml file
            if len(element) >= 5 and element[1] != None and element[2] != None and element[3] != None and element[4] != None:
                print("Transform ",element[0]," as ","("+element[0]+"+"+str(element[1])+")*"+str(element[2])," defined in [",element[3],",",element[4],"] range");
                h = data_frame.Define(element[0]+"_trans","("+element[0]+"-"+str(element[1])+")*"+str(element[2])).Histo1D((element[0]+"_trans","",25000,element[3],element[4]),element[0]+"_trans");
            ## min and max not written, we put it as cip-val
            elif len(element) >= 3 and element[1] != None and element[2] != None:
                print("Transform ",element[0]," as ","("+element[0]+"+"+str(element[1])+")*"+str(element[2])," defined in [-"+str(np.round(args.clip_val,2))+","+str(np.round(args.clip_val,2))+"] range");
                h = data_frame.Define(element[0]+"_trans","("+element[0]+"-"+str(element[1])+")*"+str(element[2])).Histo1D((element[0]+"_trans","",25000,-args.clip_val,args.clip_val),element[0]+"_trans");
            else:
                print("No transformation for ",element[0]);
                h = data_frame.Histo1D((element[0],"",25000,-1000,1000),element[0]);                                    
            histograms[element[0]] = h;

    ## compute the quantiles
    print("###### Looping on events and fill histograms");
    output_file_root = ROOT.TFile(args.output_file.replace(".yaml",".root"),"RECREATE");
    output_file_root.cd();
    for key,histo in histograms.items():
        x_q = np.array([0.,0.]);
        if "photon" in key or "muon" in key or "electron" in key or "tau" in key:
            y_q = np.array([1-args.quantile,args.quantile]);
        else:
            y_q = np.array([1-args.quantileLep,args.quantileLep]);            
        histo.GetQuantiles(len(x_q),x_q,y_q);
        x_q = np.around(x_q,5);
        print("Variable ",key," quantile ",args.quantile," x_q ",x_q," y_q ",y_q," f-underflow ",np.around(histo.GetBinContent(-1)/histo.Integral(),5)," f-overflow ",np.around(histo.GetBinContent(histo.GetNbinsX()+1)/histo.Integral(),5));
        histo.Write();
        for k,v in inputs.items():
            for idx,entry in enumerate(v['vars']):                
              if entry[0] == key:
                  ## quantile infos
                  eps_list= [];
                  if any(vars in key for vars in args.var_exclude):
                      eps_list.extend([float(0.),float(0.)]);
                  else:
                      eps_list.extend([float(x_q[0]),float(x_q[1])]);
                  ## if there are already 5 elements just add eps
                  if len(entry) >= 5:
                      data_config['inputs'][k]['vars'][idx].extend(eps_list);
                  elif len(entry) == 3:
                      data_config['inputs'][k]['vars'][idx].extend([float(-args.clip_val),float(args.clip_val)]);
                      data_config['inputs'][k]['vars'][idx].extend(eps_list);
                  else:
                      data_config['inputs'][k]['vars'][idx].extend([None,None,None]);
                      data_config['inputs'][k]['vars'][idx].extend(eps_list);
                  print("Writing --> ",data_config['inputs'][k]['vars'][idx]);
    ## save output files
    print("###### Save output files");
    output_file_root.Close();    
    with open(args.output_file, 'w') as outfile:
        yaml.preserve_quotes = False
        yaml.safe_dump(data_config,outfile,sort_keys=False)
    
