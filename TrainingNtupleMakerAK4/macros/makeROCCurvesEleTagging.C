#include "commonTools.h"

void makeROCCurvesEleTagging( const string & fileNameStringToGrep,
			      const string & dataConfigStringToGrep,
			      const string & outputDIR,
			      const bool   & useXRootD      = false,
			      const bool   & applyReweight  = true,
			      const vector<float> & ptBins  = {30,1000},
			      const vector<float> & etaBins = {0,2.5},
			      const bool addMatchingWithRecoEle = true,
			      const vector<sample_type> & sample_selected = {},
			      const vector<float> & sigEffTargetEle = {0.90,0.85,0.80},
			      const int    & nThreads = 4
			      ){

  cout<<"makeROCCurvesEleTagging --> Open program "<<endl;
  map<string,string> inputFileName;
  inputFileName["PNETULQuant"] = "/media/Disk1/rgerosa/WeaverResults/ClassReg/trainingAK4_taumuel_classreg_hps_quantile_10082022";
  inputFileName["PNETULLast"] = "/media/Disk1/rgerosa/WeaverResults/ClassReg/trainingAK4_taumuel_classreg_hps_lrnew_10082022/";
  inputFileName["PNETUL"] = "/media/Disk1/rgerosa/WeaverResults/ClassReg/trainingAK4_taumuel_classreg_tauhps_13072022/";
  //inputFileName["EGammaID"] = "/media/Disk1/rgerosa/WeaverResults/ClassReg/trainingAK4_taumuel_classreg_tauhps_13072022/";
  
  ROOT::GetROOT()->SetBatch(kTRUE);
  system(("mkdir -p "+outputDIR).c_str());
  setTDRStyle();
  gStyle->SetOptStat(0);

  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();

  string xrootd_eos = string(getenv("EOS_MGM_URL"))+"//";
    
  // Open file and take tree
  cout<<"makeROCCurvesEleTagging --> Retrive number of events "<<endl;
  map<string,int> nEvents;
  for(auto imap: inputFileName){
    unique_ptr<TChain> inputTree (new TChain("Events"));
    if(useXRootD)
      inputTree->Add((xrootd_eos+imap.second+"/"+fileNameStringToGrep+"*.root").c_str());
    else
      inputTree->Add((imap.second+"/"+fileNameStringToGrep+"*.root").c_str());
    nEvents[imap.first] = inputTree->GetEntries();    
    cout<<"makeROCCurvesEleTagging --> imap.first "<<imap.first<<" nevents "<<nEvents[imap.first] <<endl;
  }

  // Parse the DataConfig via python3
  cout<<"makeROCCurvesEleTagging --> Parsing data config file"<<endl;
  map<string,shared_ptr<TH2F>> weights;
  for(auto imap: inputFileName){    
    cout<<"makeROCCurvesEleTagging --> python3 parseDataConfig.py --data-config "+imap.second+"/"+dataConfigStringToGrep+" --output-file "+outputDIR+"/weight_"+imap.first+".root"<<endl;
    system(("python3 parseDataConfig.py --data-config "+imap.second+"/"+dataConfigStringToGrep+" --output-file "+outputDIR+"/weight_"+imap.first+".root").c_str());   
    // Save weights in pT - mass plane
    unique_ptr<TFile> inputFileWeights (TFile::Open((outputDIR+"/weight_"+imap.first+".root").c_str(),"READ"));
    if(inputFileWeights->Get("reweight_label_b"))   weights[imap.first+"_label_b"]   = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_b"));
    if(inputFileWeights->Get("reweight_label_c"))   weights[imap.first+"_label_c"]   = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_c"));
    if(inputFileWeights->Get("reweight_label_uds")) weights[imap.first+"_label_uds"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_uds"));
    if(inputFileWeights->Get("reweight_label_g"))   weights[imap.first+"_label_g"]   = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_g"));
    if(inputFileWeights->Get("reweight_label_el"))  weights[imap.first+"_label_el"] = shared_ptr<TH2F>((TH2F*) inputFileWeights->Get("reweight_label_el"));
    if(inputFileWeights->Get("reweight_label_b"))   weights[imap.first+"_label_b"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_c"))   weights[imap.first+"_label_c"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_uds")) weights[imap.first+"_label_uds"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_g"))   weights[imap.first+"_label_g"]->SetDirectory(0);
    if(inputFileWeights->Get("reweight_label_el"))  weights[imap.first+"_label_el"]->SetDirectory(0);
    inputFileWeights->Close();
  }

  // histograms for signal and background
  cout<<"makeROCCurvesEleTagging --> Create histograms"<<endl;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_sig_el_vs_b;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_sig_el_vs_c;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_sig_el_vs_uds;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_sig_el_vs_g;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_sig_el_vs_jet;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_bkg_el_vs_b;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_bkg_el_vs_c;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_bkg_el_vs_uds;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_bkg_el_vs_g;
  map<string,shared_ptr<ROOT::TThreadedObject<TH1F>>> hist_bkg_el_vs_jet;

  for(auto imap: inputFileName){
    for(size_t ipt = 0; ipt < ptBins.size()-1; ipt++){
      for(size_t ieta = 0; ieta < etaBins.size()-1; ieta++){
	string name = imap.first+"_pt_"+to_string(ipt)+"_eta_"+to_string(ieta);
	hist_sig_el_vs_b[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_sig_el_vs_b_"+name).c_str(),"",10000,0.,14.));
	hist_sig_el_vs_c[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_sig_el_vs_c_"+name).c_str(),"",10000,0.,14.));
	hist_sig_el_vs_uds[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_sig_el_vs_uds_"+name).c_str(),"",10000,0.,14.));
	hist_sig_el_vs_g[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_sig_el_vs_g_"+name).c_str(),"",10000,0.,14.));
	hist_sig_el_vs_jet[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_sig_el_vs_jet_"+name).c_str(),"",10000,0.,14.));
	hist_bkg_el_vs_b[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_bkg_el_vs_b_"+name).c_str(),"",10000,0.,14.));
	hist_bkg_el_vs_c[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_bkg_el_vs_c_"+name).c_str(),"",10000,0.,14.));
	hist_bkg_el_vs_uds[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_bkg_el_vs_uds_"+name).c_str(),"",10000,0.,14.));
	hist_bkg_el_vs_g[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_bkg_el_vs_g_"+name).c_str(),"",10000,0.,14.));
	hist_bkg_el_vs_jet[name] = shared_ptr<ROOT::TThreadedObject<TH1F>>(new ROOT::TThreadedObject<TH1F>(("hist_bkg_el_vs_jet_"+name).c_str(),"",10000,0.,14.));
      }
    }
  }
  
  ////////////////
  auto fillHistogram = [&](int workerID, string fileKey, int nevents){

    unique_ptr<TChain> chain (new TChain("Events"));
    if(useXRootD)
      chain->Add((xrootd_eos+inputFileName[fileKey]+"/"+fileNameStringToGrep+"*root").c_str());
    else
      chain->Add((inputFileName[fileKey]+"/"+fileNameStringToGrep+"*root").c_str());

    TTreeReader reader (chain.get());
    
    TTreeReaderValue<float> jet_pt  (reader,"jet_pt");
    TTreeReaderValue<float> jet_pt_raw  (reader,"jet_pt_raw");
    TTreeReaderValue<float> jet_eta (reader,"jet_eta");
    TTreeReaderValue<int> sample  (reader,"sample");

    TTreeReaderValue<float> score_label_b (reader,"score_label_b");
    TTreeReaderValue<float> score_label_c (reader,"score_label_c");
    TTreeReaderValue<float> score_label_uds (reader,"score_label_uds");
    TTreeReaderValue<float> score_label_g (reader,"score_label_g");
    TTreeReaderValue<float> score_label_el (reader,"score_label_el");
    TTreeReaderValue<float> score_label_mu (reader,"score_label_mu");

    TTreeReaderValue<bool> label_b (reader,"label_b");
    TTreeReaderValue<bool> label_c (reader,"label_c");
    TTreeReaderValue<bool> label_uds (reader,"label_uds");
    TTreeReaderValue<bool> label_g (reader,"label_g");
    TTreeReaderValue<bool> label_mu (reader,"label_mu");
    TTreeReaderValue<bool> label_el (reader,"label_el");

    TTreeReaderValue<float> score_label_taup_1h0p (reader,"score_label_taup_1h0p");
    TTreeReaderValue<float> score_label_taup_1h1p (reader,"score_label_taup_1h1p");
    TTreeReaderValue<float> score_label_taup_1h2p (reader,"score_label_taup_1h2p");
    TTreeReaderValue<float> score_label_taup_3h0p (reader,"score_label_taup_3h0p");
    TTreeReaderValue<float> score_label_taup_3h1p (reader,"score_label_taup_3h1p");
    TTreeReaderValue<float> score_label_taum_1h0p (reader,"score_label_taum_1h0p");
    TTreeReaderValue<float> score_label_taum_1h1p (reader,"score_label_taum_1h1p");
    TTreeReaderValue<float> score_label_taum_1h2p (reader,"score_label_taum_1h2p");
    TTreeReaderValue<float> score_label_taum_3h0p (reader,"score_label_taum_3h0p");
    TTreeReaderValue<float> score_label_taum_3h1p (reader,"score_label_taum_3h1p");

    TTreeReaderValue<bool> label_taup_1h0p (reader,"label_taup_1h0p");
    TTreeReaderValue<bool> label_taup_1h1p (reader,"label_taup_1h1p");
    TTreeReaderValue<bool> label_taup_1h2p (reader,"label_taup_1h2p");
    TTreeReaderValue<bool> label_taup_3h0p (reader,"label_taup_3h0p");
    TTreeReaderValue<bool> label_taup_3h1p (reader,"label_taup_3h1p");
    TTreeReaderValue<bool> label_taum_1h0p (reader,"label_taum_1h0p");
    TTreeReaderValue<bool> label_taum_1h1p (reader,"label_taum_1h1p");
    TTreeReaderValue<bool> label_taum_1h2p (reader,"label_taum_1h2p");
    TTreeReaderValue<bool> label_taum_3h0p (reader,"label_taum_3h0p");
    TTreeReaderValue<bool> label_taum_3h1p (reader,"label_taum_3h1p");

    TTreeReaderValue<float> jet_elematch_pt (reader,"jet_elematch_pt");
    TTreeReaderValue<float> jet_elematch_eta (reader,"jet_elematch_eta");
    TTreeReaderValue<float> jet_elematch_idscore (reader,"jet_elematch_idscore");

    reader.SetTree(chain.get());
    auto beginEntry = (Long64_t) nevents*workerID/nThreads;
    auto endEntry   = (Long64_t) nevents*min(nThreads,workerID+1)/nThreads-1;
    reader.SetEntriesRange(beginEntry,endEntry);

    bool useRawPt = false;
    if(TString(fileKey).Contains("NoJEC"))
      useRawPt = true;

    bool useEGID = false;
    if(TString(fileKey).Contains("EGID"))
      useEGID = true;
    
    while(reader.Next()){
      
      // sample selection
      bool passing_sample = false;
      for(auto const & s : sample_selected){
	if(int(*sample) == static_cast<int>(s))
	  passing_sample = true;
      } 
      if(sample_selected.empty()) 
	passing_sample = true;
      if(not passing_sample) continue;

      // filter out muons
      if(*label_mu) continue;

      // filter out taus
      if(*label_taup_1h0p or *label_taup_1h1p or *label_taup_1h2p or *label_taup_3h0p or *label_taup_3h1p or *label_taum_1h0p or *label_taum_1h1p or *label_taum_1h2p or *label_taum_3h0p or *label_taum_3h1p)
        continue;

      // identify pt and eta bin                                                                                                                                                                
      float jetpt = *jet_pt;
      if(useRawPt)
        jetpt = *jet_pt_raw;

      // identify pt and eta bin
      int ptbin = -1;
      for(size_t ipt = 0; ipt < ptBins.size()-1; ipt++){
	if(jetpt >= ptBins.at(ipt) and jetpt < ptBins.at(ipt+1)){
	  ptbin = ipt;
	  break;
	}
      }
      if(ptbin == -1) continue;

      int etabin = -1;
      for(size_t ieta = 0; ieta < etaBins.size()-1; ieta++){
	if(fabs(*jet_eta) >= etaBins.at(ieta) and fabs(*jet_eta) < etaBins.at(ieta+1)){
	  etabin = ieta;
	  break;
	}
      }
      if(etabin == -1) continue;
      
      // electron selection
      if(addMatchingWithRecoEle and (*jet_elematch_pt <= minElePt or fabs(*jet_elematch_eta) > maxEleEta)) continue;
      if(addMatchingWithRecoEle and fabs(*jet_elematch_eta) > 1.4442  and fabs(*jet_elematch_eta) < 1.56) continue;

      // weights
      string name = fileKey+"_pt_"+to_string(ptbin)+"_eta_"+to_string(etabin);
      float weight = 1;
      if(applyReweight){	
	if(*label_b)
	  weight = weights[fileKey+"_label_b"]->GetBinContent(weights[fileKey+"_label_b"]->GetXaxis()->FindBin(*jet_pt),weights[fileKey+"_label_b"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_c)
	  weight = weights[fileKey+"_label_c"]->GetBinContent(weights[fileKey+"_label_c"]->GetXaxis()->FindBin(*jet_pt),weights[fileKey+"_label_c"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_uds)
	  weight = weights[fileKey+"_label_uds"]->GetBinContent(weights[fileKey+"_label_uds"]->GetXaxis()->FindBin(*jet_pt),weights[fileKey+"_label_uds"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_g)
	  weight = weights[fileKey+"_label_g"]->GetBinContent(weights[fileKey+"_label_g"]->GetXaxis()->FindBin(*jet_pt),weights[fileKey+"_label_g"]->GetYaxis()->FindBin(*jet_eta));
	else if(*label_el)
	  weight = weights[fileKey+"_label_el"]->GetBinContent(weights[fileKey+"_label_el"]->GetXaxis()->FindBin(*jet_pt),weights[fileKey+"_label_el"]->GetYaxis()->FindBin(*jet_eta));
      }

      // score from ele-id
      float score_eg = atanh((*jet_elematch_idscore+1)/2);
      if(score_eg > (*hist_bkg_el_vs_jet[name])->GetXaxis()->GetBinLowEdge((*hist_bkg_el_vs_jet[name])->GetNbinsX()+1))
	score_eg = (*hist_bkg_el_vs_jet[name])->GetBinCenter((*hist_bkg_el_vs_jet[name])->GetNbinsX());
      
      // Fill histograms for true b-jets
      if(*label_b){
	if(useEGID){
	  (*hist_bkg_el_vs_b[name])->Fill(score_eg,weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(score_eg,weight);
	}
	else{
	  (*hist_bkg_el_vs_b[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b)),weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b+*score_label_c+*score_label_uds+*score_label_g)),weight);	      
	}
      }
    
      // Fill histograms for true c-jets
      else if(*label_c){	
	if(useEGID){
	  (*hist_bkg_el_vs_c[name])->Fill(score_eg,weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(score_eg,weight);
	}
	else{
	  (*hist_bkg_el_vs_c[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_c)),weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b+*score_label_c+*score_label_uds+*score_label_g)),weight);	      
	}
      }

      // Fill histograms for true uds-jets
      else if(*label_uds){
	if(useEGID){
	  (*hist_bkg_el_vs_uds[name])->Fill(score_eg,weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(score_eg,weight);
	}
	else{
	  (*hist_bkg_el_vs_uds[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_uds)),weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b+*score_label_c+*score_label_uds+*score_label_g)),weight);	      
	}
      }

      // Fill histograms for true gluon-jets
      else if(*label_g){
	if(useEGID){
	  (*hist_bkg_el_vs_g[name])->Fill(score_eg,weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(score_eg,weight);
	}
	else{
	  (*hist_bkg_el_vs_g[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_g)),weight);
	  (*hist_bkg_el_vs_jet[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b+*score_label_c+*score_label_uds+*score_label_g)),weight);	      
	}
      }
      
      // Fill histograms for e-jets
      else if (*label_el){
	if(useEGID){
          (*hist_sig_el_vs_b[name])->Fill(score_eg,weight);
          (*hist_sig_el_vs_c[name])->Fill(score_eg,weight);
          (*hist_sig_el_vs_uds[name])->Fill(score_eg,weight);
          (*hist_sig_el_vs_g[name])->Fill(score_eg,weight);
          (*hist_sig_el_vs_jet[name])->Fill(score_eg,weight);
	}
	else{
          (*hist_sig_el_vs_b[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b)),weight);
          (*hist_sig_el_vs_c[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_c)),weight);
	  (*hist_sig_el_vs_uds[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_uds)),weight);
          (*hist_sig_el_vs_g[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_g)),weight);
          (*hist_sig_el_vs_jet[name])->Fill(atanh((*score_label_el)/(*score_label_el+*score_label_b+*score_label_c+*score_label_uds+*score_label_g)),weight);
	}
      }
    }
  };

  for(auto imap: inputFileName){
    cout<<"makeROCCurvesEleTagging --> Event loop for "<<imap.first<<" with nthreads = "<<nThreads<<endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::vector<std::thread> threads;
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(fillHistogram,workerID,imap.first,nEvents[imap.first]);
    for (auto && worker : threads) worker.join();
    auto stop  = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> duration = stop-start;
    std::cout<<"makeROCCurvesEleTagging --> Duration loop is "<<duration.count()<<std::endl;
  }

  // Final plot
  map<string,shared_ptr<TH1F> > hist_sig_el_vs_b_merged;
  map<string,shared_ptr<TH1F> > hist_sig_el_vs_c_merged;
  map<string,shared_ptr<TH1F> > hist_sig_el_vs_uds_merged;
  map<string,shared_ptr<TH1F> > hist_sig_el_vs_g_merged;
  map<string,shared_ptr<TH1F> > hist_sig_el_vs_jet_merged;
  map<string,shared_ptr<TH1F> > hist_bkg_el_vs_b_merged;
  map<string,shared_ptr<TH1F> > hist_bkg_el_vs_c_merged;
  map<string,shared_ptr<TH1F> > hist_bkg_el_vs_uds_merged;
  map<string,shared_ptr<TH1F> > hist_bkg_el_vs_g_merged;
  map<string,shared_ptr<TH1F> > hist_bkg_el_vs_jet_merged;

  cout<<"makeROCCurvesEleTagging --> Merge and rescale"<<endl;
  for(auto & ihist : hist_sig_el_vs_c) hist_sig_el_vs_c_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_sig_el_vs_g) hist_sig_el_vs_g_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_sig_el_vs_uds) hist_sig_el_vs_uds_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_sig_el_vs_b) hist_sig_el_vs_b_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_sig_el_vs_jet) hist_sig_el_vs_jet_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_bkg_el_vs_c) hist_bkg_el_vs_c_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_bkg_el_vs_g) hist_bkg_el_vs_g_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_bkg_el_vs_uds) hist_bkg_el_vs_uds_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_bkg_el_vs_b) hist_bkg_el_vs_b_merged[ihist.first] = (*ihist.second).Merge();
  for(auto & ihist : hist_bkg_el_vs_jet) hist_bkg_el_vs_jet_merged[ihist.first] = (*ihist.second).Merge();

  for(auto & ihist : hist_sig_el_vs_b_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_sig_el_vs_c_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_sig_el_vs_uds_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_sig_el_vs_g_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_sig_el_vs_jet_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_bkg_el_vs_b_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_bkg_el_vs_c_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_bkg_el_vs_uds_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_bkg_el_vs_jet_merged) ihist.second->Scale(1./ihist.second->Integral());
  for(auto & ihist : hist_bkg_el_vs_g_merged) ihist.second->Scale(1./ihist.second->Integral());

  cout<<"makeROCCurvesEleTagging --> Build final ROCs"<<endl;

  map<string,TGraph*> roc_el_vs_b;
  map<string,TGraph*> roc_el_vs_c;
  map<string,TGraph*> roc_el_vs_uds;
  map<string,TGraph*> roc_el_vs_g;
  map<string,TGraph*> roc_el_vs_jet;

  for(auto map_key : hist_sig_el_vs_b){
    roc_el_vs_b[map_key.first] = buildROC(hist_sig_el_vs_b_merged[map_key.first],hist_bkg_el_vs_b_merged[map_key.first]);
    roc_el_vs_c[map_key.first] = buildROC(hist_sig_el_vs_c_merged[map_key.first],hist_bkg_el_vs_c_merged[map_key.first]);
    roc_el_vs_uds[map_key.first] = buildROC(hist_sig_el_vs_uds_merged[map_key.first],hist_bkg_el_vs_uds_merged[map_key.first]);
    roc_el_vs_g[map_key.first] = buildROC(hist_sig_el_vs_g_merged[map_key.first],hist_bkg_el_vs_g_merged[map_key.first]);
    roc_el_vs_jet[map_key.first] = buildROC(hist_sig_el_vs_jet_merged[map_key.first],hist_bkg_el_vs_jet_merged[map_key.first]);
  }

  map<string,TF1*> roc_el_vs_b_f;
  map<string,TF1*> roc_el_vs_c_f;
  map<string,TF1*> roc_el_vs_uds_f;
  map<string,TF1*> roc_el_vs_g_f;
  map<string,TF1*> roc_el_vs_jet_f;
  
  for(auto & roc : roc_el_vs_c) roc_el_vs_c_f[roc.first] = new TF1(Form("%s_f",roc.second->GetName()),[&](double*x, double *p){ return roc.second->Eval(x[0]);},0,15,0);
  for(auto & roc : roc_el_vs_uds) roc_el_vs_uds_f[roc.first] = new TF1(Form("%s_f",roc.second->GetName()),[&](double*x, double *p){ return roc.second->Eval(x[0]);},0,15,0);
  for(auto & roc : roc_el_vs_g) roc_el_vs_g_f[roc.first] = new TF1(Form("%s_f",roc.second->GetName()),[&](double*x, double *p){ return roc.second->Eval(x[0]);},0,15,0);
  for(auto & roc : roc_el_vs_jet) roc_el_vs_jet_f[roc.first] = new TF1(Form("%s_f",roc.second->GetName()),[&](double*x, double *p){ return roc.second->Eval(x[0]);},0,15,0);
  for(auto & roc : roc_el_vs_b) roc_el_vs_b_f[roc.first] = new TF1(Form("%s_f",roc.second->GetName()),[&](double*x, double *p){ return roc.second->Eval(x[0]);},0,15,0);
  
  cout<<"makeROCCurvesEleTagging --> Plot ROC curves"<<endl;  

  for(size_t ipt = 0 ; ipt < ptBins.size()-1; ipt++){
    for(size_t ieta = 0 ; ieta < etaBins.size()-1; ieta++){

      vector<TF1*> rocs_to_plot_el_vs_b;
      vector<TF1*> rocs_to_plot_el_vs_c;
      vector<TF1*> rocs_to_plot_el_vs_uds;
      vector<TF1*> rocs_to_plot_el_vs_g;
      vector<TF1*> rocs_to_plot_el_vs_jet;

      vector<string>  legends;
      string binlabel = "pt_"+to_string(ipt)+"_eta_"+to_string(ieta);
      for(auto map_key: inputFileName){
	string name = map_key.first+"_"+binlabel;
	rocs_to_plot_el_vs_b.push_back(roc_el_vs_b_f[name]);
	rocs_to_plot_el_vs_c.push_back(roc_el_vs_c_f[name]);
	rocs_to_plot_el_vs_uds.push_back(roc_el_vs_uds_f[name]);
	rocs_to_plot_el_vs_g.push_back(roc_el_vs_g_f[name]);
	rocs_to_plot_el_vs_jet.push_back(roc_el_vs_jet_f[name]);
	legends.push_back(map_key.first);
      }          
      string binname = Form("%d < p_{T} < %d GeV, %.1f < |#eta| < %.1f",int(ptBins.at(ipt)),int(ptBins.at(ipt+1)),etaBins.at(ieta),etaBins.at(ieta+1));      

      vector<string>  labels;
      labels.push_back("ele vs uds");
      labels.push_back("ele vs g");
      labels.push_back("ele vs c");
      labels.push_back("ele vs b");
      plotROC(rocs_to_plot_el_vs_uds,rocs_to_plot_el_vs_g,rocs_to_plot_el_vs_c,rocs_to_plot_el_vs_b,{},{},
	      legends,labels,binname,outputDIR,"roc_eltag_exclusive_"+binlabel,0.7,1,0.001);
            
      labels.clear();
      labels.push_back("ele vs jet");
      plotROC(rocs_to_plot_el_vs_jet,{},{},{},{},{},
	      legends,labels,binname,outputDIR,"roc_eltag_vs_jet_"+binlabel,0.7,1,0.001);
      
    }
  }  

  cout<<"makeROCCurvesCTagging --> make efficiency vs pT and eta curves"<<endl;

  if(ptBins.size() > 2 and etaBins.size() > 2){
    std::cerr<<"ptBins and etaBins have size > 2 .. we cannot plot 2D trends but only 1D therefore abort and choose either just pT or eta binning"<<std::endl;
    return;
  }

  map<string,TGraph*> eff_el_vs_b;
  map<string,TGraph*> eff_el_vs_uds;
  map<string,TGraph*> eff_el_vs_g;
  map<string,TGraph*> eff_el_vs_c;
  map<string,TGraph*> eff_el_vs_jet;

  for(auto imap: inputFileName){

    for(size_t ieff = 0; ieff < sigEffTargetEle.size(); ieff++){
      float  eff  = sigEffTargetEle.at(ieff);
      eff_el_vs_b[Form("eff_el_vs_b_%s_%.2f",imap.first.c_str(),eff)] = new TGraph();
      eff_el_vs_c[Form("eff_el_vs_c_%s_%.2f",imap.first.c_str(),eff)] = new TGraph();
      eff_el_vs_uds[Form("eff_el_vs_uds_%s_%.2f",imap.first.c_str(),eff)] = new TGraph();
      eff_el_vs_g[Form("eff_el_vs_g_%s_%.2f",imap.first.c_str(),eff)] = new TGraph();
      eff_el_vs_jet[Form("eff_el_vs_jet_%s_%.2f",imap.first.c_str(),eff)] = new TGraph();

      if(ptBins.size() > 2){
        for (size_t ipt = 0; ipt < ptBins.size()-1; ipt++){
          string name = imap.first+"_pt_"+to_string(ipt)+"_eta_0";
          eff_el_vs_b[Form("eff_el_vs_b_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ipt,(ptBins.at(ipt+1)+ptBins.at(ipt))/2,roc_el_vs_b_f[name]->Eval(eff));
          eff_el_vs_c[Form("eff_el_vs_c_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ipt,(ptBins.at(ipt+1)+ptBins.at(ipt))/2,roc_el_vs_c_f[name]->Eval(eff));
          eff_el_vs_uds[Form("eff_el_vs_uds_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ipt,(ptBins.at(ipt+1)+ptBins.at(ipt))/2,roc_el_vs_uds_f[name]->Eval(eff));
          eff_el_vs_g[Form("eff_el_vs_g_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ipt,(ptBins.at(ipt+1)+ptBins.at(ipt))/2,roc_el_vs_g_f[name]->Eval(eff));
          eff_el_vs_jet[Form("eff_el_vs_jet_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ipt,(ptBins.at(ipt+1)+ptBins.at(ipt))/2,roc_el_vs_jet_f[name]->Eval(eff));
	}
      }
      else if(etaBins.size() > 2){
        for (size_t ieta = 0; ieta < etaBins.size()-1; ieta++){
          string name = imap.first+"_pt_0_eta_"+to_string(ieta);
          eff_el_vs_b[Form("eff_el_vs_b_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ieta,(etaBins.at(ieta+1)+etaBins.at(ieta))/2,roc_el_vs_b_f[name]->Eval(eff));
          eff_el_vs_c[Form("eff_el_vs_c_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ieta,(etaBins.at(ieta+1)+etaBins.at(ieta))/2,roc_el_vs_c_f[name]->Eval(eff));
          eff_el_vs_uds[Form("eff_el_vs_uds_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ieta,(etaBins.at(ieta+1)+etaBins.at(ieta))/2,roc_el_vs_uds_f[name]->Eval(eff));
          eff_el_vs_g[Form("eff_el_vs_g_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ieta,(etaBins.at(ieta+1)+etaBins.at(ieta))/2,roc_el_vs_g_f[name]->Eval(eff));
          eff_el_vs_jet[Form("eff_el_vs_jet_%s_%.2f",imap.first.c_str(),eff)]->SetPoint(ieta,(etaBins.at(ieta+1)+etaBins.at(ieta))/2,roc_el_vs_jet_f[name]->Eval(eff));
	}
      }   
    }
  }

  for(size_t ieff = 0; ieff < sigEffTargetEle.size(); ieff++){

    vector<TGraph*> eff_el_vs_b_to_plot;
    vector<TGraph*> eff_el_vs_c_to_plot;
    vector<TGraph*> eff_el_vs_uds_to_plot;
    vector<TGraph*> eff_el_vs_g_to_plot;
    vector<TGraph*> eff_el_vs_jet_to_plot;

    vector<string>  legends;

    for(auto imap: inputFileName){
      eff_el_vs_b_to_plot.push_back(eff_el_vs_b[Form("eff_el_vs_b_%s_%.2f",imap.first.c_str(),sigEffTargetEle.at(ieff))]);
      eff_el_vs_c_to_plot.push_back(eff_el_vs_c[Form("eff_el_vs_c_%s_%.2f",imap.first.c_str(),sigEffTargetEle.at(ieff))]);
      eff_el_vs_uds_to_plot.push_back(eff_el_vs_uds[Form("eff_el_vs_uds_%s_%.2f",imap.first.c_str(),sigEffTargetEle.at(ieff))]);
      eff_el_vs_g_to_plot.push_back(eff_el_vs_g[Form("eff_el_vs_g_%s_%.2f",imap.first.c_str(),sigEffTargetEle.at(ieff))]);
      eff_el_vs_jet_to_plot.push_back(eff_el_vs_jet[Form("eff_el_vs_jet_%s_%.2f",imap.first.c_str(),sigEffTargetEle.at(ieff))]);
      legends.push_back(imap.first);
    }

    string binname  = Form("ele efficiency = %.2f",sigEffTargetEle.at(ieff));
    string binlabel = Form("eff_%.2f",sigEffTargetEle.at(ieff));

    vector<string>  labels;
    labels.push_back("ele vs uds");
    labels.push_back("ele vs g");
    labels.push_back("ele vs c");
    labels.push_back("ele vs b");
    float xmax = (ptBins.back()+ptBins.at(ptBins.size()-2))/2.;
    if(etaBins.size() > 2)
      xmax = (etaBins.back()+etaBins.at(etaBins.size()-2))/2.;
    if(ptBins.size() > 2) 
      plotEfficiency(eff_el_vs_uds_to_plot,eff_el_vs_g_to_plot,eff_el_vs_c_to_plot,eff_el_vs_b_to_plot,legends,labels,binname,outputDIR,"eff_eletag_vs_pt_"+binlabel,ptBins.front(),xmax);
    else if(etaBins.size() > 2) 
      plotEfficiency(eff_el_vs_uds_to_plot,eff_el_vs_g_to_plot,eff_el_vs_c_to_plot,eff_el_vs_b_to_plot,legends,labels,binname,outputDIR,"eff_eletag_vs_eta_"+binlabel,etaBins.front(),xmax);

    labels.clear();
    labels.push_back("ele vs jet");
    if(ptBins.size() > 2) plotEfficiency(eff_el_vs_jet_to_plot,{},{},{},legends,labels,binname,outputDIR,"eff_eletag_vs_pt_"+binlabel,ptBins.front(),xmax);
    else if(etaBins.size() > 2) plotEfficiency(eff_el_vs_jet_to_plot,{},{},{},legends,labels,binname,outputDIR,"eff_eletag_vs_eta_"+binlabel,etaBins.front(),xmax);
  }
}
