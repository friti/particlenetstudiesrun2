#ifndef commonDataMCTools_h
#define commonDataMCTools_h

enum class observable {
  llmass, llpt, l1pt, l1ptreg, l1eta, l1pmu, l1pele, l1mt, l2pt, l2ptreg, l2eta, l2mt, l2pmu, l2pele, l2ptauhvsj, l2ptauhvsjtrans, l2ptauhvsmu, l2ptauhvsmutrans, l2ptauhvsele, l2ptauhvseletrans, detall, dphill, drll, met, njets, npv, j1pt, j1eta, j1ptreg, jprobb, jprobbtrans, jprobc, jprobctrans, jprobuds, jprobg, jprobtauh, jprobtauhtrans, jprobtauhvsmu, jprobmu, jprobele, j2pt, j2ptreg, j2eta, mjj, detajj, ptjj, dphijj};


void getObservableBinning(const observable & obs, const domain_type & region, float & min, float & max, int & nbins){

  if(obs == observable::llmass and (region == domain_type::dimuon or region == domain_type::dielectron)){
    min = 75; max = 105; nbins = 50;  
  }
  else if(obs == observable::llmass and (region == domain_type::mutau or region == domain_type::etau)){
    min = 30; max = 130; nbins = 60;
  }
  else if(obs == observable::llmass and (region == domain_type::emu or region == domain_type::ttcharm or region == domain_type::dijet)){
    min = 30; max = 250; nbins = 75;
  }
  else if(obs == observable::llpt){
    min = 0; max = 250; nbins = 100;
  }
  else if(obs == observable::l1pt or obs == observable::l2pt or obs == observable::l1ptreg or obs == observable::l2ptreg){
    min = 20; max = 200; nbins = 75;
  }
  else if(obs == observable::l1mt or obs == observable::l2mt){
    min = 0; max = 100; nbins = 50;
  }
  else if(obs == observable::l1eta or obs == observable::l2eta){
    min = -2.5; max = 2.5; nbins = 40;
  }
  else if(obs == observable::l1pmu or obs == observable::l2pmu){
    min = 0; max = 1; nbins = 60;
  }
  else if(obs == observable::l1pele or obs == observable::l2pele){
    min = 0; max = 1; nbins = 60;
  }
  else if(obs == observable::l2ptauhvsj or obs == observable::l2ptauhvsmu){
    min = 0.9; max = 1; nbins = 30;
  }
  else if(obs == observable::l2ptauhvsele){
    min = 0.5; max = 1; nbins = 60;
  }
  else if(obs == observable::l2ptauhvsjtrans){
    min = 0; max = 8; nbins = 70;
  }
  else if(obs == observable::l2ptauhvsmutrans or obs == observable::l2ptauhvseletrans){
    min = 0; max = 12; nbins = 70;
  }
  else if(obs == observable::detall){
    min = 0; max = 6; nbins = 60;
  }
  else if(obs == observable::dphill){
    min = 0; max = 3.14; nbins = 60;
  }
  else if(obs == observable::drll){
    min = 0; max = 6; nbins = 60;
  }
  else if(obs == observable::met){
    min = 0; max = 150; nbins = 50;
  }
  else if(obs == observable::npv){
    min = 0; max = 80; nbins = 40;
  }
  else if(obs == observable::njets){
    min = 0; max = 8; nbins = 8;
  }
  else if(obs == observable::j1pt or obs == observable::j1ptreg){
    min = 25; max = 250; nbins = 75;
  }
  else if(obs == observable::j2pt or obs == observable::j2ptreg){
    min = 25; max = 150; nbins = 50;
  }
  else if(obs == observable::j1eta or obs == observable::j2eta){
    min = -2.5; max = 2.5; nbins = 40;
  }
  else if(obs == observable::jprobb or obs == observable::jprobc or obs == observable::jprobuds or obs == observable::jprobg or 
	  obs == observable::jprobtauh or obs == observable::jprobmu or obs == observable::jprobele){
    min = 0; max = 1; nbins = 60;
  }
  else if(obs == observable::jprobbtrans or obs == observable::jprobtauhtrans or obs == observable::jprobctrans){
    min = 0; max = 8; nbins = 70;
  }
  else if(obs == observable::mjj){
    min = 0; max = 1000; nbins = 100;
  }
  else if(obs == observable::detajj){
    min = 0; max = 7; nbins = 50;
  }
  else if(obs == observable::dphijj){
    min = 0; max = 3.14; nbins = 50;
  }
  else if(obs == observable::ptjj){
    min = 0; max = 500; nbins = 60;
  }
}

void getObservableName(const observable & obs, string & obsName, string & obsLabel){

  if(obs == observable::llmass){
    obsName  = "llmass";
    obsLabel = "m_{ll} (GeV)";
  }
  else if(obs == observable::llpt){
    obsName  = "llpt";
    obsLabel = "p_{T}^{ll} (GeV)";
  }
  else if(obs == observable::l1pt){
    obsName  = "l1pt";
    obsLabel = "p_{T}^{l_{1}} (GeV)";
  }
  else if(obs == observable::l1ptreg){
    obsName  = "l1ptreg";
    obsLabel = "p_{T}^{l_{1}} (regressed) (GeV)";
  }
  else if(obs == observable::l2pt){
    obsName  = "l2pt";
    obsLabel = "p_{T}^{l_{2}} (GeV)";
  }
  else if(obs == observable::l2ptreg){
    obsName  = "l2ptreg";
    obsLabel = "p_{T}^{l_{2}} (regressed) (GeV)";
  }
  else if(obs == observable::l1eta){
    obsName  = "l1eta";
    obsLabel = "#eta_{l_{1}} (GeV)";
  }
  else if(obs == observable::l2eta){
    obsName  = "l2eta";
    obsLabel = "#eta_{l_{2}}";
  }
  else if(obs == observable::l1mt){
    obsName  = "l1mt";
    obsLabel = "m_{T}(l_{1}) (GeV)";
  }
  else if(obs == observable::l2mt){
    obsName  = "l2mt";
    obsLabel = "m_{T}(l_{2}) (GeV)";
  }
  else if(obs == observable::l1pmu){
    obsName  = "l1pmu";
    obsLabel = " P(#mu) (l_{1})";
  }
  else if(obs == observable::l1pele){
    obsName  = "l1pele";
    obsLabel = " P(e) (l_{1})";
  }
  else if(obs == observable::l2pmu){
    obsName  = "l2pmu";
    obsLabel = " P(#mu) (l_{2})";
  }
  else if(obs == observable::l2pele){
    obsName  = "l2pele";
    obsLabel = " P(e) (l_{2})";
  }
  else if(obs == observable::detall){
    obsName  = "detall";
    obsLabel = "#Delta#eta_{ll}";
  }
  else if(obs == observable::dphill){
    obsName  = "dphill";
    obsLabel = "#Delta#phi_{ll}";
  }
  else if(obs == observable::drll){
    obsName  = "drll";
    obsLabel = "#DeltaR_{ll}";
  }
  else if(obs == observable::l2ptauhvsj){
    obsName  = "l2ptauhvsj";
    obsLabel = " P(#tau_{h})/P(jet) (l_{2})";
  }
  else if(obs == observable::l2ptauhvsjtrans){
    obsName  = "l2ptauhvsjtrans";
    obsLabel = "Transformed PNET P(#tau_{h})/P(jet) (l_{2})";
  }
  else if(obs == observable::l2ptauhvsmu){
    obsName  = "l2ptauhvsmu";
    obsLabel = " P(#tau_{h})/P(#mu) (l_{2})";
  }
  else if(obs == observable::l2ptauhvsmutrans){
    obsName  = "l2ptauhvsmutrans";
    obsLabel = "Transformed PNET P(#tau_{h})/P(#mu) (l_{2})";
  }
  else if(obs == observable::l2ptauhvsele){
    obsName  = "l2ptauhvsele";
    obsLabel = " P(#tau_{h})/P(ele) (l_{2})";
  }
  else if(obs == observable::l2ptauhvseletrans){
    obsName  = "l2ptauhvseletrans";
    obsLabel = "Transformed PNET P(#tau_{h})/P(ele) (l_{2})";
  }
  else if(obs == observable::met){
    obsName  = "met";
    obsLabel = "p_{T}^{miss} (GeV)";
  }
  else if(obs == observable::npv){
    obsName  = "npv";
    obsLabel = "N_{vtx}";
  }
  else if(obs == observable::njets){
    obsName  = "njets";
    obsLabel = "N_{jets}";
  }
  else if(obs == observable::j1pt){
    obsName  = "j1pt";
    obsLabel = "p_{T}^{j1} (GeV)";
  }
  else if(obs == observable::j1ptreg){
    obsName  = "j1ptreg";
    obsLabel = "p_{T}^{j1}(regressed) (GeV)";
  }
  else if(obs == observable::j1eta){
    obsName  = "j1eta";
    obsLabel = "#eta_{j1}";
  }
  else if(obs == observable::j2pt){
    obsName  = "j2pt";
    obsLabel = "p_{T}^{j2} (GeV)";
  }
  else if(obs == observable::j2ptreg){
    obsName  = "j2ptreg";
    obsLabel = "p_{T}^{j2}(regressed) (GeV)";
  }
  else if(obs == observable::j2eta){
    obsName  = "j2eta";
    obsLabel = "#eta_{j2}";
  }
  else if(obs == observable::mjj){
    obsName  = "mjj";
    obsLabel = "m_{jj} (GeV)";
  }
  else if(obs == observable::ptjj){
    obsName  = "ptjj";
    obsLabel = "p_{T}^{jj} (GeV)";
  }
  else if(obs == observable::dphijj){
    obsName  = "dphijj";
    obsLabel = "#Delta#phi_{jj}";
  }
  else if(obs == observable::detajj){
    obsName  = "detajj";
    obsLabel = "#Delta#eta_{jj}";
  }
  else if(obs == observable::jprobb){
    obsName  = "jprobb";
    obsLabel = "PNET P(b)/P(b+c+uds+g)";
  }
  else if(obs == observable::jprobc){
    obsName  = "jprobc";
    obsLabel = "PNET P(c)/P(b+c+uds+g)";
  }
  else if(obs == observable::jprobuds){
    obsName  = "jprobuds";
    obsLabel = "PNET P(uds)/P(b+c+uds+g)";
  }
  else if(obs == observable::jprobg){
    obsName  = "jprobg";
    obsLabel = "PNET P(g)/P(b+c+uds+g)";
  }
  else if(obs == observable::jprobtauh){
    obsName  = "jprobtauh";
    obsLabel = "PNET P(#tau_{h})/P(#tau_{h}+b+c+uds+g)";
  }
  else if(obs == observable::jprobmu){
    obsName  = "jprobmu";
    obsLabel = "PNET P(#mu)/P(#mu+b+c+uds+g)";
  }
  else if(obs == observable::jprobele){
    obsName  = "jprobele";
    obsLabel = "PNET P(ele)/P(ele+b+c+uds+g)";
  }
  else if(obs == observable::jprobbtrans){
    obsName  = "jprobbtrans";
    obsLabel = "Transformed PNET P(b)/P(b+c+uds+g)";
  }
  else if(obs == observable::jprobctrans){
    obsName  = "jprobctrans";
    obsLabel = "Transformed PNET P(c)/P(b+c+uds+g)";
  }
  else if(obs == observable::jprobtauhtrans){
    obsName  = "jprobtauhtrans";
    obsLabel = "Transformed PNET P(#tau_{h})/P(#tau_{h}+b+c+uds+g)";
  }
}

#endif
