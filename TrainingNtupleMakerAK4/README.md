# Training Ntuples for ParticleNet AK4

* Before submitting any of the tasks, please consider that the PNET models are present in the `data` directory and they will be used for inference in some of the workflows. The data directory will be part of crab job sandbox, hence there is no need to copy the models in other places like into `$CMSSW_BASE/external`.

* The `TauES` files have been taken from the github repository of TAU POG [[Link]](https://github.com/cms-tau-pog/TauIDSFs/tree/master/data)

## Producing Training Ntuples

The CMSSW config run in order to produce the training ntuples is located at `test/makeTrainingNtuple_cfg.py`. Various options can be set at runtime through command line parameters, the default values are those used for the standard crab-based production. Example on how to run it on a `MINIAODSIM` input file is:

```sh
cmsRun makeTrainingNtuple_cfg.py maxEvents=100 inputFiles=/store/mc/RunIISummer20UL18MiniAODv2/GluGluHToBB_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/2DA3D33B-3D34-A04C-8A16-656A62DAF289.root
```

**Available command line options**:
* `outputName`: name of the output ROOT file
* `processName`: name of the current cmsRun process
* `xsec`: xsec value of the MC sample under-processing (in pb)
* `era`: string that identifies the era in order to load the right conditions [2016PreVFP, 2016PostVBF, 2017, 2018].
* `isMC`: is data or MC
* `reRunPuppi`: if true runs the latest version of puppi available/default of the release (for puppi v17).
* `nThreads`: mumber of threads used by the job (EDAnalyzer runs by stopping threads)
* `muonPtMin`: pt requirement applied to dump muons belonging to `slimmedMuons` collection
* `electronPtMin`: pt requirements applied to dump electrons belonging to `slimmedElectrons` collection
* `tauPtMin`: pt requirements applied to dump taus belonging to `slimmedTaus` collection
* `photonPtMin`: pt requirements applied to dump photons belonging to `slimmedPhotons` collection
* `jetPtMin`, `jetEtaMax`, and `jetEtaMin`: pt and eta requirements applied to dump jets belonging to `slimmedJets` collection
* `usePuppiJets`: consider/dump puppi-jets instead of regular AK4PFCHS.
* `jetPFCandidatePtMin`: minimum pT threshold applied to PF-candidates inside the jet
* `lostTrackPtMin`: minimum pT threshold applied to the lost-tracks
* `dRLostTrackJet`: maximum dR threshold applied to the lost-tracks w.r.t. the jet axis
* `dRJetGenMatch`: cone-sizde to match GEN jets with RECO ones.
* `dumpOnlyJetMatchedToGen`: dump only or not jets matched with GEN level jets when running on MC
* `applyJECs`: if set to True apply JECs to jets `slimmedJets` and `slimmedMETs` on the fly on the basis of the GT content
* `evaluateLastPNETTraining`: if set to True re-run the last version of PNET training. Its model needs to be exported in ONNX, copied to the external folder, and properties need to be modified in the cmsRun configuration file.

**Crab submission**
* List of samples (grouped per type) and running options used for the Ntuple production are listed into the `python/samples_training.py` file. A function for every type of sample add information to a dictionary whose entries are built according to the following nomenclature: 
  * Key is the sample-name identifier, arguments are instead provided as a list.
  * First argument is the DAS sample name.
  * Second argument is a list of options that should be inserted in the crab argument parser.
  * Third argument is the splitting mode for crab.
  * Fourth argument is the splitting units chosen for crab.
  * Fifth argument is a lumiMask (used only in data to parse certified run and lumisections).
  * Sixth argument is the total number of units (used in MC to express total number of events to process or in data the total number of lumi-sections)
* The `python/createCrabJob.py` script is used to create crab-jobs on the basis of:
  * `--sample-config`: list of sample identifier that should map the name of the files in the `python/samples_training.py` directory.
  * `--sample-type`: samples type for which jobs need to be submitted.
  * `--crab-config`: is the crab submission template file that is used for the job generation `crab/crabConfig_training.py`. By default a maximum of total units, number of events, is set for each sample.
  * `--crab-job-dir`: directory where crab jobs will be created
  * `--command`: crab command that needs to be executed. Only a set of predefined ones can be used.
  * `--options`: additional options to the crab command 

**Compute sample xsec**
* Use the `GenXSecAnalyzer` provided by the GEN-GROUP in an automated way. All samples apart from ttbar and SM Higgs for which more-accurate predictions are available.
* In order to run the `GenXSecAnalyzer` please refer to the CMSSW config in `test/makeGenXsecAnalyzer_cfg.py`
* In order to run it automaticallay for the list of samples in ``python/samples_training.py`, the `python/evaluateSampleCrossSection.py` can be used (set the proxy via `voms-proxy-init -voms cms` before running it):
  * `--sample-config`: file with sample list to be imported
  * `-m`: number of events to be processed for xsec evaluation
  * `-f`: maximum number of files to be considered
  * `--sample-type`: samples type for which the xsec evaluation has to be run
  * `--cmssw-config`: cmssw config file to run GenXSecAnalyzer i.e. `test/makeGenXsecAnalyzer_cfg.py`
  * The code prints at screen for each sample entry in the sample list the value of `After filter: final cross section`. This numerical value has to be copied by end in the sample list to use it when submitting crab jobs.

## Skim Training Ntuples

Once the crab jobs for the Training Ntuples are done, these can be further skimmed in order to produce a set of jet level tress that will be used for the particle-net training. In order to run the skim process, the following c++ code needs to be used `bin/makeSkimmedNtuplesForTraining.cpp`. The code is compiled while doing `scram b` and it has to be re-compiled everytime a change is applied. In order to visualize the running options you could use `makeSkimmedNtuplesForTraining --help`; this list is reported below:
* `--inputFileList`: provide a file containing the list of .root files that needs to be processed.
* `--inputFileDIR `: provide the name of the directory containing the files that needs to be processed (you can either give a list or a directory).
* `--outputDIR`: location of the output directory that could be located either on a local machine or on CERN EOS.
* `--outputFileName`: name of the output file. It has to end with ".root" extension.
* `--nThreads`: number of threads to be run when looping on the events.
* `--maxNumberOfFilesToBeProcessed`: when a directory is provided, this parameters defines the number of files to be processed each time.
* `--mergeThreadOutputFiles`: merge or not, via hadd, the output files produced by each thread into a single one.
* `--jetPtMin`: pT threshold to be applied on the AK4 jets.
* `--jetEtaMax`: max eta of jets to be considered.
* `--jetEtaMin`: min eta of jets to be considered.
* `--pfCandPtMin`: minimum pt threshold applied on PF candidates.
* `--pfCandPuppiWeightMin`: minimum puppi weight threshold applied on PF candidates.
* `--losttrackPtMin`: minimum transverse momentum selection for the lost tracks.
* `--sample-type`: type of sample (DY,WJet,QCD,TTbar,ggH ...etc) defined inside the code.
* `--domain-type`: type of domain region (mumu,ee,emu,mutau) defined inside the code. When turned on, this endcodes phase-space selection determined by the thresholds sets at the beginning of the cpp file.
* `--saveOnlyGenMatchedJets`: whether or not only stored jets matched with GEN jets.
* `--applyJetID`: whether or not to apply jet-id on the jets not in overlap with truth leptons.
* `--compressOutputFile`: compress or not the output files. By default compression level 4 and LZ4 algo are used, instead compression level 6 and LZMA.

Example on how to run, giving an input folder with the files, the skimming step:
```sh
makeSkimmedNtuplesForTraining --inputFileDIR /eos/cms/store/group/phys_exotica/monojet/rgerosa/ParticleNetUL/NtupleTrainingAK4/DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8/ --outputDIR /eos/cms/store/group/phys_exotica/monojet/rgerosa/ParticleNetUL/NtupleTrainingAK4Skimmed/ --outputFileName tree_DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8.root --maxNumberOfFilesToBeProcessed 10 --nThreads 4
```

In order to submit the `skim production` on the CERN HTCondor system the following python script can be used `python/createSkimCondorJob.py`. These are its options:
* `--input-dir`: input directory with the files.
* `--output-dir`: output directory where the files will be copied.
* `--output-file-name`: output file name (needs to end with .root).
* `--nthreads`: number of threads to use and request on the job node.
* `--nfiles`: number of files per job to process.
* `--merge`: merge or not output files created by each thread.
* `--minjpt`, `--maxjeta`, `--minjeta`, `--jet-id`, `--save-only-gen`: min jet pT, max and min jet eta, apply jet-id selection, save only jets matched to GEN.
* `--compress`: compress or not the output file.
* `--sample-type`: needs to be a string of (none,qcd,dy,wjet,ttbar,ggH,vbfH,VH,ggHH,bulkG).
* `--domain-type`: needs to be a string of (none,dimuon,dielectron,emu,mutau,etau).
* `--validation`: in case the skim of validation ntuples, not training ones, needs to be submited. The executable for training is `bin/makeSkimmedNtuplesForTraining.cpp`, for validation is `bin/makeSkimmedNtuplesForValidation.cpp`.
* `--job-dir`: directory where jobs need to be created.
* `--debug-mode`: to decide if .err and .out files are copied to the job directory or not.
* `--queque`: HTCondor job flavour that will be used.
* `--submit`: if set jobs are submitted to the scheduler.
* `--minpfpt`: minimum pt threshold applied on PF candidates.
* `--minpfpw`: minimum puppi weight threshold applied on PF candidates.

In order to submit all jobs for samples inside the same mother EOS directory you can used sth like:
```sh
ls /eos/cms/store/group/phys_exotica/monojet/rgerosa/ParticleNetUL/NtupleTrainingAK4/ | grep dy | awk '{print "python3 createSkimCondorJob.py --input-dir /eos/cms/store/group/phys_exotica/monojet/rgerosa/ParticleNetUL/NtupleTrainingAK4/"$1" --output-dir /eos/cms/store/group/phys_exotica/monojet/rgerosa/ParticleNetUL/NtupleTrainingAK4Skimmed/ --output-file-name tree_"$1".root --nthreads 4 --nfiles 20 --minjpt 30 --maxjeta 2.5 --sample-type dy --job-dir jobs/"$1" --submit"}' | /bin/sh
```

Examples for latest puppi productions are:
* `python/skim_training_puppi_submit.sh`: central region with threshold at 15 GeV and no jet-id applied.
* `python/skim_training_puppi_forward_submit.sh`: forward region with threshold at 15 GeV and no jet-id applied.

Before submitting jobs to the CERN HTCondor scheduler, you can select the best one by running `myschedd bump`. To check the scheduler status, you can run `condor_status --schedd`. The ntuple size remains pretty big because of the low compression level applied by default. In case one compresses them, at least a factor 2 in space will be saved.

## Run trainings via Weaver

The particle-net trainings are run on top of the skimmed ntuples via the weaver framework made by Huilin Qu. A customized version of weaver (forking) is used which stores: (1) the right configuration files and PNET models for the trianings, (2) some little changes in order to run a simultaneous classification+regression training or multiple-target regressions. The installation recipe is reported below:

* *Preparation of conda environment*: a conda environment (pyhton 3.9 is chosen cause onnx-runtime is not available yet for higher versions), containing also other packages needed to run weaver into a Docker Container, needs to be prepared as follows:
```sh
curl -LJO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
conda config --set auto_activate_base false
conda config --set channel_priority strict
conda update -n base -c defaults conda
conda create -n weaver python=3.11
conda activate weaver
pip3 install numpy 
pip3 install scikit-learn scipy matplotlib tqdm
pip3 install PyYAML beautifulsoup4 lz4 xxhash tables 
pip3 install vector tensorboard
pip3 install uproot awkward awkward0
pip3 install onnx onnxruntime-gpu onnxruntime
pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
pip3 cache purge
conda clean --tarballs
conda deactivate
```
* After this you can checkout weaver in your favourite local area as:
```sh
git clone git@github.com:rgerosa/weaver-core.git -b domain_fgsm
git clone git@github.com:rgerosa/weaver-benchmark.git -b domain_fgsm
```
* In order to create a Docker image that can be used in Kubernetes based clusters (GPU clusters running containers), the recipe expressed in the `weaver/docker/Dockerfile` need to be executed and kept updated to use the best docker-base images available. 
  * The docker is created starting from the centos7+cuda base version, installing all the packages (including pip ones) present in the `weaver` conda environment. 
  * In this way, once the container is launched, one can activate the conda environment and run weaver as it would be one on a local machine.
  * A detailed description of the options of weaver is reported in its `README.md` file or available by running `python3 train.py --help`.
  * The creation of the docker container is performed via:
  ```sh
  cd weaver/docker;
  conda activate weaver
  conda env export > weaver-environment.yaml
  conda deactivate
  sudo docker build -t <name of the container> .
  # to display created images
  sudo docker images
  # to run the image locally into a container
  sudo docker run -i -t <name of the container> /bin/bash
  # to display containers run recently
  sudo docker container ls -a
  ```
  * By default the docker images and corresponding containers are created on the local machine file system in ``/var/lib/docker/``, hence sudo priviledges are needed.
  * Once created, it is common to load the docker container on a github/gitlab repository and delete it from the local machine to save space. This can be done as follows:
  ```sh
  sudo docker login <git repository> -u <username> -p <token>
  sudo docker push <git repository/account name/name of the container>
  sudo docker system prune --all --force
  ```
  * Here a real example:
  ```sh
  sudo docker login gitlab-registry.cern.ch 
  sudo docker build -t gitlab-registry.cern.ch/rgerosa/particlenetstudiesrun2/weaver:latest .
  sudo docker push gitlab-registry.cern.ch/rgerosa/particlenetstudiesrun2/weaver:latest     
  ```
* Please check the recipe contained in the ``Docker`` file and periodically update

## Submit training jobs on GPU cluster PRP
* First build the docker image of weaver and upload it to the nautilus gitlab repository:
  ```sh
  sudo docker login gitlab-registry.nrp-nautilus.io
  sudo docker build -t gitlab-registry.nrp-nautilus.io/rgerosa/particlenetrun2ul/weaver:latest . 
  sudo docker push gitlab-registry.nrp-nautilus.io/rgerosa/particlenetrun2ul/weaver:latest
  sudo docker system prune --all --force
  ```
* Checkout the gitlab natuilus PRP repository where the yaml configuration files for running jobs on PRP kubernetes cluster are stored:
  ```sh
  git clone ssh://git@gitlab-ssh.nrp-nautilus.io:30622/rgerosa/particlenetrun2ul.git 
  cd particlenetrun2ul
  ```
* Description of the PRP cluster provided in the nautilus manual [NautilusPRP](https://ucsd-prp.gitlab.io/)

## Macros for validation purposes on the testing files

## Validation of PNET AK4 in CMSSW

The CMSSW config run in order to produce the so-called validation ntuples is located at `test/makeValidationNtuple_cfg.py`. Various options can be set at runtime through command line parameters, the default values are those used for the standard crab-based production. Example on how to run it on a `MINIAODSIM` input file is:

```sh
cmsRun makeValidationNtuple_cfg.py maxEvents=100 inputFiles=/store/mc/RunIISummer20UL18MiniAODv2/GluGluHToBB_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/2DA3D33B-3D34-A04C-8A16-656A62DAF289.root
```

This workflow is used to compare performance of the last version of PNET against: (1) DeepJet and default PNET for AK4 jet-tagging, (2) DeepTau for hadronic tau-tagging, (3) b/c DNN regressions (if PNET has a regression output), (4) SVFit applied to standard-taus and PNET regressed one (only visible pT). For these studies, SVFit is involved so please consult [SVFit](https://github.com/SVfit/ClassicSVfit).

**Available command line options**:
* `outputName`: name of the output ROOT file
* `processName`: name of the current cmsRun process
* `xsec`: xsec value of the MC sample under-processing (in pb)
* `isMC`: is data or MC
* `nThreads`: mumber of threads used by the job (EDAnalyzer runs by stopping threads)
* `muonPtMin`: pt requirement applied to dump muons belonging to `slimmedMuons` collection
* `electronPtMin`: pt requirements applied to dump electrons belonging to `slimmedElectrons` collection
* `tauPtMin`: pt requirements applied to dump taus belonging to `slimmedTaus` collection
* `jetPtMin` and `jetEtaMax`: pt and eta requirements applied to dump jets belonging to `slimmedJets` collection
* `jetPFCandidatePtMin`: minimum pT threshold applied to PF-candidates inside the jet
* `dumpOnlyJetMatchedToGen`: dump only or not jets matched with GEN level jets when running on MC
* `applyJECs`: if set to True apply JECs to jets `slimmedJets` and `slimmedMETs` on the fly on the basis of the GT content
* `evaluatePNETModels`: give a set of strings indicating which PNET models trained can/should be inferred and saved
* `evaluateForwardPNETTraining`: evaluate the forward training instead of central on

Warning are genereated by running SVFit using AK4 jets corrected by the PNET regression instead of the HPS taus. Their mass can be higher than the tau-invariant mass, in that case it is constrained to be (fixed) equal to the tau-invariant mass. This is the origin of the warnings. Please discard them for this round of checks.

**Crab submission**
* List of samples (grouped per type) and running options used for the Ntuple production are listed into the `python/samples_validation.py` file. A function for every type of sample add information to a dictionary whose entries are built according to the same nomenclature explained above
* The `python/createCrabJob.py` script is used to create crab-jobs on the basis of:
  * `--sample-config`: list of sample identifier that should map the name of the files in the `python/samples_validation.py` directory
  * `--crab-config`: is the crab submission template file that is used for the job generation `crab/crabConfig_validation.py`
  * All the remaining options have been already described above

## Inference time of PNET AK4 in CMSSW

The CMSSW config run in order to quickly test the inference performance (also comparing with DeepJet and DeepTau) is located at `test/makePNETInferenceTime_cfg.py`. Some options can be set at runtime through command line parameters, please check them. In order to run it and assess the timing performance via the TimingReport.

```sh
cmsRun makePNETInferenceTime_cfg.py maxEvents=10000 inputFiles=/store/mc/RunIISummer20UL18MiniAODv2/GluGluHToBB_M-125_TuneCP5_13TeV-amcatnloFXFX-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/40000/2DA3D33B-3D34-A04C-8A16-656A62DAF289.root,... more files if needed
```
