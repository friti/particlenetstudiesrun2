#include "TFile.h"
#include "TChain.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TH1F.h"
#include "TSystem.h"
#include "TROOT.h"
#include "ROOT/TSeq.hxx"
#include "ROOT/TThreadedObject.hxx"
#include "ROOT/RDataFrame.hxx"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <thread>
#include <atomic>
#include <fstream>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>

using namespace std;

enum class domain_type {
     none=-1,dimuon=0,dielectron=1,emu=2,mutau=3,etau=4,ttcharm=5,dijet=6
};

domain_type convertDomainType (const std::string & domain_name){
  if(domain_name == "domain_type::none") return domain_type::none;
  else if(domain_name == "domain_type::dimuon") return domain_type::dimuon;
  else if(domain_name == "domain_type::dielectron") return domain_type::dielectron;
  else if(domain_name == "domain_type::emu") return domain_type::emu;
  else if(domain_name == "domain_type::mutau") return domain_type::mutau;
  else if(domain_name == "domain_type::etau") return domain_type::etau;
  else if(domain_name == "domain_type::ttcharm") return domain_type::ttcharm;
  else if(domain_name == "domain_type::dijet") return domain_type::dijet;
  else return domain_type::none;
}

string inputDirectory;
string outputDIR;
string outputFileName;
string domain_name;
domain_type domain;
bool   isMC;
bool   useXRootD;
int    nFilesOffset;
int    nFilesToAnalyze;
int    nThreads;

void fillWeights (const int & workerID,		  
		  const std::vector<std::string> & fileList,
		  const int & nevents,
		  const std::atomic_int & nthreads,
		  const shared_ptr<ROOT::TThreadedObject<TH1F>> & histoPU,
		  const shared_ptr<ROOT::TThreadedObject<TH1D>> & histoWGT
		  ){
    
  std::shared_ptr<TChain> tree_in  (new TChain("gentree/tree","gentree/tree"));
  for(size_t ifile = 0; ifile < fileList.size(); ifile++)
    tree_in->Add(fileList.at(ifile).c_str());

  TTreeReader reader;
  reader.SetTree(tree_in.get());
  auto beginEntry = (Long64_t) nevents*workerID/nthreads;
  auto endEntry   = (Long64_t) nevents*std::min(nthreads.load(std::memory_order_relaxed),workerID+1)/nthreads-1;
  reader.SetEntriesRange(beginEntry,endEntry);
  std::cout<<"fillWeights --> thread "<<workerID<<" starting from entry "<<beginEntry<<" to entry "<<endEntry<<std::endl;

  TTreeReaderValue<float> wgt (reader,"wgt");
  TTreeReaderValue<int> putrue (reader,"putrue");

  while(reader.Next()){
    (*histoPU)->Fill(*putrue);
    (*histoWGT)->Fill(1,*wgt);
  }
  std::cout<<"fillWeights --> thread "<<workerID<<" stopping "<<std::endl;
  
}    

    
int main(int argc, char **argv){

  boost::program_options::options_description desc("Main options");
  desc.add_options()
    ("inputDirectory,i", boost::program_options::value<std::string>(&inputDirectory)->default_value(""), "Input directory where files needs to be taken")
    ("outputDIR,o", boost::program_options::value<std::string>(&outputDIR)->default_value(""), "Output directory where files need to be created")
    ("outputFileName,f", boost::program_options::value<std::string>(&outputFileName)->default_value(""), "Base name for the output file")
    ("isMC,s", boost::program_options::value<bool>(&isMC)->default_value(true), "Indicate if it is data or MC")
    ("useXRootD,x", boost::program_options::value<bool>(&useXRootD)->default_value(true), "Indicate if read data via xrootd or local mountpoint")
    ("nFilesOffset,fo", boost::program_options::value<int>(&nFilesOffset)->default_value(-1), "When !=-1, start from the a given point along the file list")
    ("nFilesToAnalyze,a", boost::program_options::value<int>(&nFilesToAnalyze)->default_value(10), "Indicate number of files to be processed each time")
    ("nThreads,n", boost::program_options::value<int>(&nThreads)->default_value(1), "Number of internal threads to be used in certain operations")
    ("domain-type,s", boost::program_options::value<std::string>(&domain_name)->default_value(""), "Type of domain region")
    ("help,h", "Produce help message interface");

  boost::program_options::variables_map options;
  
  try{
    boost::program_options::store(boost::program_options::command_line_parser(argc,argv).options(desc).run(),options);
    boost::program_options::notify(options);
  }
  catch(std::exception &ex) {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cerr << "Use makeSkimmedNtuplesForTraining --help to get a list of all the allowed options"  << std::endl;
    return 999;
  }
  catch(...) {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help                                                                                                                                                                              
  if(options.count("help")) {
    std::cout << "Usage: makeSkimmedNtuplesForTraining [options]\n";
    std::cout << desc;
    return 0;
  }

  domain = convertDomainType(domain_name);

  cout<<"makeDomainSumOfWeights.cpp Parameter Summary"<<endl;
  cout<<"inputDirectory --> "<<inputDirectory<<endl;
  cout<<"outputDIR --> "<<outputDIR<<endl;
  cout<<"outputFileName --> "<<outputFileName<<endl;
  cout<<"isMC --> "<<isMC<<endl;
  cout<<"nFilesOffset --> "<<nFilesOffset<<endl;
  cout<<"nFilesToAnalyze --> "<<nFilesToAnalyze<<endl;
  cout<<"domain_type --> "<<domain_name<<endl;
  
  gSystem->Exec(("mkdir -p "+outputDIR).c_str());

  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();
  
  std::string xrootd_eos = string(getenv("EOS_MGM_URL"))+"//";

  // Data pileup
  string filePileupData = "$CMSSW_BASE/src/ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/PileUp/PileupHistogram-goldenJSON-13tev-2018D-72400ub-99bins.root";
  if(domain == domain_type::mutau or domain == domain_type::etau or domain == domain_type::emu or domain == domain_type::ttcharm)  
    filePileupData = "$CMSSW_BASE/src/ParticleNetStudiesRun2/TrainingNtupleMakerAK4/data/PileUp/PileupHistogram-goldenJSON-13tev-2018-72400ub-99bins.root";
  
  string postfix;
  if(domain == domain_type::dimuon) postfix = "dimuon";
  else if(domain == domain_type::dielectron) postfix = "dielectron";
  else if(domain == domain_type::emu) postfix = "emu";
  else if(domain == domain_type::mutau) postfix = "mutau";
  else if(domain == domain_type::etau) postfix = "etau";
  else if(domain == domain_type::ttcharm) postfix = "ttcharm";
  else if(domain == domain_type::dijet) postfix = "dijet";

  // Build the list of files
  cout<<"makeDomainSumOfWeights --> inputDirectory "<<inputDirectory<<endl;
  system(("find "+inputDirectory+" -name \"*root\" > file_"+postfix+".list").c_str());  
  std::ifstream infileList (("file_"+postfix+".list").c_str());  
  vector<string> inputfileList;
  if(infileList.is_open()){
    string token;
    while(!infileList.eof()){
      getline(infileList,token);
      if(not TString(token.c_str()).Contains(".root")) continue;
      struct stat info;
      stat(token.c_str(), &info);
      struct passwd *pw = getpwuid(info.st_uid);
      if(string(pw->pw_name) == "phedex") continue;
      if(useXRootD)
	inputfileList.push_back(xrootd_eos+token);
      else
	inputfileList.push_back(token);
    }
  }
  system(("rm file_"+postfix+".list").c_str());
  sort(inputfileList.begin(),inputfileList.end());
  cout<<"makeDomainSumOfWeights --> file list made nfiles = "<<inputfileList.size()<<endl;

  // Get Sum of weights and PU  
  double wgtsum = 0;
  auto start = std::chrono::high_resolution_clock::now();  
  shared_ptr<TFile> fileInputData (TFile::Open(filePileupData.c_str()));
  shared_ptr<TH1F> histoPUData ((TH1F*) ((TH1F*) fileInputData->Get("pileup"))->Clone("histoPUData"));
  histoPUData->Scale(1./histoPUData->Integral());    
  histoPUData->SetDirectory(0);

  shared_ptr<TH1F> histoPUWeight ((TH1F*) histoPUData->Clone("histoPUWeight"));  
  shared_ptr<ROOT::TThreadedObject<TH1F>> histoPU  = shared_ptr<ROOT::TThreadedObject<TH1F>> (new ROOT::TThreadedObject<TH1F>("histoPUMC","",histoPUData->GetNbinsX(),histoPUData->GetXaxis()->GetXmin(),histoPUData->GetXaxis()->GetXmax()));
  shared_ptr<ROOT::TThreadedObject<TH1D>> histoWGT = shared_ptr<ROOT::TThreadedObject<TH1D>> (new ROOT::TThreadedObject<TH1D>("histoWgt","",1,0,2));

  if(isMC){
    ROOT::RDataFrame rdf_gentree ("gentree/tree",inputfileList);
    long int nevents = *rdf_gentree.Count();
    auto stop  = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> duration = stop-start;
    cout<<"makeDomainSumOfWeights --> getting total number of events "<<nevents<<" in "<<duration.count()<<"s"<<endl;    
    std::vector<std::thread> threads;  
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(fillWeights, workerID, inputfileList, nevents, nThreads, histoPU, histoWGT);
    for (auto && worker : threads) worker.join();
    stop  = std::chrono::high_resolution_clock::now();
    threads.clear();
    shared_ptr<TH1D> histoWGT_merged = histoWGT->Merge();
    wgtsum = histoWGT_merged->Integral();
    shared_ptr<TH1F> histoPU_merged = histoPU->Merge();
    histoPU_merged->Scale(1./histoPU_merged->Integral());    
    histoPUWeight->Divide(histoPU_merged.get());
  }

  auto stop = std::chrono::high_resolution_clock::now();
  std::chrono::duration<float> duration = stop-start;
  cout<<"makeDomainSumOfWeights --> Time getting total PU weights and sum of weights --> "<<duration.count()<<"s"<<" wgtsum = "<<wgtsum<<endl;

  // Fill outout trees
  if(nFilesOffset==-1){
    int noutput = ((inputfileList.size() % nFilesToAnalyze) == 0) ? int(inputfileList.size() / nFilesToAnalyze) : int(inputfileList.size() / nFilesToAnalyze)+1;
    int ifile = 0;
    cout<<"makeDomainSumOfWeights --> Produce noutput files "<<noutput<<endl;
    for(int iout = 0; iout < noutput; iout++){
      cout<<"Producing output: iout "<<iout<<endl;
      start = std::chrono::high_resolution_clock::now();
      shared_ptr<TChain> inputTree (new TChain("dnntree/tree"));    
      for(int itemp = 0; itemp < nFilesToAnalyze; itemp++){
	inputTree->Add(inputfileList.at(ifile+itemp).c_str());
	ifile += itemp;
      }   

      // skip the pfcandidates and sv
      inputTree->SetBranchStatus("jet_pfcandidate*",0);
      inputTree->SetBranchStatus("jet_sv*",0);
      inputTree->SetBranchStatus("jet_losttrack*",0);
      if(isMC){
	inputTree->SetBranchStatus("*gen_particle_pt*",0);
	inputTree->SetBranchStatus("*gen_particle_eta*",0);
	inputTree->SetBranchStatus("*gen_particle_phi*",0);
	inputTree->SetBranchStatus("*gen_particle_mass*",0);
	inputTree->SetBranchStatus("*gen_particle_status*",0);
	inputTree->SetBranchStatus("*gen_particle_daughters_status*",0);
	inputTree->SetBranchStatus("*gen_particle_daughters_charge*",0);
      }
      inputTree->SetBranchStatus("*trigger*",0);
      
      // copy the tree
      shared_ptr<TFile> outputFile (new TFile((outputDIR+"/"+outputFileName+"_"+to_string(iout)+".root").c_str(),"RECREATE"));
      shared_ptr<TTree> newtree (inputTree->CopyTree(""));
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      cout<<"makeDomainSumOfWeights: tree copied in duration =  "<<duration.count()<<"s"<<endl;

      // loop and add the branches
      start = std::chrono::high_resolution_clock::now();

      TTreeReader reader (newtree.get());
      TTreeReaderValue<unsigned int> putrue (reader, (isMC) ? "putrue" : "npv");
      float puweight  = 1.;
      shared_ptr<TBranch> bpuweight (newtree->Branch("puweight", &puweight, "puweight/F"));
      shared_ptr<TBranch> bwgtsum (newtree->Branch("wgtsum",  &wgtsum,  "wgtsum/D"));

      while(reader.Next()){
	if(isMC)
	  puweight = histoPUWeight->GetBinContent(histoPUWeight->FindBin(*putrue));
	else
	  puweight = 1;	
	bpuweight->Fill();
	bwgtsum->Fill();
      }
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      cout<<"makeDomainSumOfWeights: fill wgtsum and puweight duration = "<<duration.count()<<"s"<<endl;
      start = std::chrono::high_resolution_clock::now();
      outputFile->cd();
      newtree->Write(0,TObject::kOverwrite);
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      cout<<"makeDomainSumOfWeights: write output file = "<<duration.count()<<"s"<<endl;
      ifile ++;
    }
  }
  else{   
    cout<<"makeDomainSumOfWeights --> Produce output file, nFilesOffset="<<nFilesOffset<<endl;
    start = std::chrono::high_resolution_clock::now();

    shared_ptr<TFile> outputFile (new TFile((outputDIR+"/"+outputFileName).c_str(),"RECREATE"));
    shared_ptr<TChain> inputTree (new TChain("dnntree/tree"));    
    for(int ifile = nFilesOffset; ifile < TMath::Min(int(inputfileList.size()),nFilesOffset+nFilesToAnalyze) ; ifile++)
      inputTree->Add(inputfileList.at(ifile).c_str());

    // skip the pfcandidates and sv
    inputTree->SetBranchStatus("jet_pfcandidate*",0);
    inputTree->SetBranchStatus("jet_sv*",0);
    if(isMC){
      inputTree->SetBranchStatus("*gen_particle_pt*",0);
      inputTree->SetBranchStatus("*gen_particle_eta*",0);
      inputTree->SetBranchStatus("*gen_particle_phi*",0);
      inputTree->SetBranchStatus("*gen_particle_mass*",0);
      inputTree->SetBranchStatus("*gen_particle_status*",0);
      inputTree->SetBranchStatus("*gen_particle_daughters_status*",0);
      inputTree->SetBranchStatus("*gen_particle_daughters_charge*",0);
    }
    inputTree->SetBranchStatus("*trigger*",0);

    // copy the tree
    shared_ptr<TTree> newtree (inputTree->CopyTree(""));
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    cout<<"makeDomainSumOfWeights: tree copied in duration =  "<<duration.count()<<"s"<<endl;

    // loop and add the branches
    start = std::chrono::high_resolution_clock::now();
    TTreeReader reader (newtree.get());
    TTreeReaderValue<unsigned int> putrue (reader,(isMC) ? "putrue" : "npv");
    float puweight  = 1.;
    shared_ptr<TBranch> bpuweight (newtree->Branch("puweight", &puweight, "puweight/F"));
    shared_ptr<TBranch> bwgtsum (newtree->Branch("wgtsum",  &wgtsum,  "wgtsum/D"));

    while(reader.Next()){
      if(isMC)
	puweight = histoPUWeight->GetBinContent(histoPUWeight->FindBin(*putrue));
      else
	puweight = 1;
      bpuweight->Fill();
      bwgtsum->Fill();
    }
    
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    cout<<"makeDomainSumOfWeights: fill wgtsum and puweight duration = "<<duration.count()<<"s"<<endl;
    start = std::chrono::high_resolution_clock::now();
    outputFile->cd();
    newtree->Write(0,TObject::kOverwrite);
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    cout<<"makeDomainSumOfWeights: write output file = "<<duration.count()<<"s"<<endl;
  }
}
