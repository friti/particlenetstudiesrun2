#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <thread>
#include <atomic>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "TFile.h"
#include "TChain.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TString.h"
#include "TTreeReader.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "ROOT/TSeq.hxx"
#include "Compression.h"

/// Simple object to store resonance and daughter GEN kinematics
class Resonance {
public:
  Resonance(){};
  ~Resonance(){};
  TLorentzVector p4;
  std::vector<int> daugid;
  std::vector<TLorentzVector> daugp4;
};


/// thresholds for matching
static float dRCone        = 0.2;
static float dRMatchingPF  = 0.1;
static float ptGenLeptonMin = 8;
static float ptGenTauVisibleMin = 15;
// Dilepton domain
static float minLep1PtDilepton = 28;
static float minLep2PtDilepton = 20;
static unsigned int maxNjetDilepton = 2;
static unsigned int minNjetDilepton = 1;
static float maxMET = 80;
// EMu domain
static float minLep1PtEMu = 28;
static float minLep2PtEMu = 20;
static unsigned int maxNjetEMu = 99;
static unsigned int minNjetEMu = 2;
static float minPBVsJet = 0.10;
// Dijet domain
static float minLep1PtDijet = 45;
static float minLep2PtDijet = 35;
static unsigned int maxNjetDijet = 99;
static unsigned int minNjetDijet = 2;
// ttcharm
static float minLep1PtTTcharm = 28;
static float minLep2PtTTcharm = 30;
static unsigned int maxNjetTTcharm = 99;
static unsigned int minNjetTTcharm = 4;
static float minPCVsJet = 0.50;
// mutau
static float minLep1PtMuTau = 28;
static float minLep2PtMuTau = 20;
static float minTauPt = 20;
static float maxTauEta = 2.5;
static float minMassVisible = 55;
static float maxMassVisible = 85;
static float minTransverseMass = 50;
static unsigned int maxNjetMuTau = 2;
static unsigned int minNjetMuTau = 1;
static bool  matchHPSTauCandidate = false;
static float minPTauVsJet = 0.90;
static float minPTauVsMu  = 0.98;
static float minPTauVsEle = 0.80;
// jet to be saved for domain
static unsigned int maxNjetDileptonToStore = 2;
static unsigned int maxNjetEMuToStore = 2;
static unsigned int maxNjetMuTauToStore = 1;
static unsigned int maxNjetDijetToStore = 2;
static unsigned int maxNjetTTcharmToStore = 1;
/// atomic counters
std::atomic_uint nJetsTotal = 0;
std::atomic_uint nJetsRejectedBaseCuts = 0;
std::atomic_uint nJetsRejectedMuTau = 0;
std::atomic_uint nJetsRejectedEMu = 0;
std::atomic_uint nJetsRejectedTTcharm = 0;
std::atomic_uint nJetsRejectedLowGenLepton = 0;
std::atomic_uint nEventsRejectedDomainKinematics = 0;
std::atomic_uint nJetsTraining = 0;

/// sample type definition
enum class sample_type {none=-1,qcd=0,dy=1,wjet=2,ttbar=3,ggH=4,vbfH=5,VH=6,ggHH=7,bulkG=8,data=9};

sample_type convertSampleType (const std::string & sample_name){
  if(sample_name == "sample_type::none") return sample_type::none;
  else if(sample_name == "sample_type::qcd") return sample_type::qcd;
  else if(sample_name == "sample_type::dy") return sample_type::dy;
  else if(sample_name == "sample_type::wjet") return sample_type::wjet;
  else if(sample_name == "sample_type::ttbar") return sample_type::ttbar;
  else if(sample_name == "sample_type::ggH") return sample_type::ggH;
  else if(sample_name == "sample_type::vbfH") return sample_type::vbfH;
  else if(sample_name == "sample_type::VH") return sample_type::VH;
  else if(sample_name == "sample_type::ggHH") return sample_type::ggHH;
  else if(sample_name == "sample_type::bulkG") return sample_type::bulkG;
  else if(sample_name == "sample_type::data") return sample_type::data;
  else return sample_type::none;
}

// Domain definition
enum class domain_type {none=-1,dimuon=0,dielectron=1,emu=2,mutau=3,etau=4,dijet=5,ttcharm=6};

domain_type convertDomainType (const std::string & domain_name){
  if(domain_name == "domain_type::none") return domain_type::none;
  else if(domain_name == "domain_type::dimuon") return domain_type::dimuon;
  else if(domain_name == "domain_type::dielectron") return domain_type::dielectron;
  else if(domain_name == "domain_type::emu") return domain_type::emu;
  else if(domain_name == "domain_type::mutau") return domain_type::mutau;
  else if(domain_name == "domain_type::etau") return domain_type::etau;
  else if(domain_name == "domain_type::dijet") return domain_type::dijet;
  else if(domain_name == "domain_type::ttcharm") return domain_type::ttcharm;
  else return domain_type::none;
}

// Delta phi between objects
float deltaPhi (float phi1, float phi2){
  if(fabs(phi1-phi2) < TMath::Pi())
    return fabs(phi1-phi2);
  else
    return 2*TMath::Pi()-fabs(phi1-phi2);
}

/// Global parameters that are parsed from command line
std::string inputFileList;
std::string inputFileDIR;
std::string outputDIR;
std::string outputFileName;
unsigned int nThreads;
unsigned int maxNumberOfFilesToBeProcessed;
bool mergeThreadOutputFiles;
bool useXRootD;
bool splitChargedPF;
float jetPtMin;
float jetEtaMax;
float jetEtaMin;
float pfCandPtMin;
float pfCandPuppiWeightMin;
float losttrackPtMin;
sample_type sample;
std::string sample_name ;
domain_type domain;
std::string domain_name ;
bool saveOnlyGenMatchedJets;
bool applyJetID;
bool selectOnParTScore;
bool compressOutputFile;

/// creation of output tree
void ntupleCreation (const int & workerID, 
		     const std::vector<std::string> & fileList, 
		     const std::vector<std::shared_ptr<TTree> > & trees_out, 
		     const int & nevents, 
		     const std::atomic_int & nthreads
		     ){


  auto tree_out = trees_out.at(workerID);

  std::unique_ptr<TChain> tree_in (new TChain("dnntree/tree","dnntree/tree"));
  for(size_t ifile = 0; ifile < fileList.size(); ifile++)
    tree_in->Add(fileList.at(ifile).c_str());

  TTreeReader reader (tree_in.get());
  
  TTreeReaderValue<unsigned int> run   (reader,"run");
  TTreeReaderValue<unsigned int> lumi  (reader,"lumi");
  TTreeReaderValue<unsigned int> event (reader,"event");

  TTreeReaderValue<unsigned int>* putrue = NULL;
  TTreeReaderValue<float>*        wgt = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_pt = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_eta = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_phi = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_mass = NULL;
  TTreeReaderValue<std::vector<int> >*    gen_particle_id = NULL;
  TTreeReaderValue<std::vector<unsigned int> >* gen_particle_daughters_igen = NULL;
  TTreeReaderValue<std::vector<unsigned int> >* gen_particle_daughters_status = NULL;
  TTreeReaderValue<std::vector<int> >*    gen_particle_daughters_charge = NULL;
  TTreeReaderValue<std::vector<int> >*    gen_particle_daughters_id = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_pt = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_eta = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_phi = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_mass = NULL;

  if(not (sample == sample_type::data)){
    putrue = new TTreeReaderValue<unsigned int>(reader,"putrue");    
    wgt = new TTreeReaderValue<float>(reader,"wgt");
    gen_particle_pt  = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_pt");
    gen_particle_eta = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_eta");
    gen_particle_phi = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_phi");
    gen_particle_mass = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_mass");
    gen_particle_id  = new TTreeReaderValue<std::vector<int> > (reader,"gen_particle_id");
    gen_particle_daughters_igen = new TTreeReaderValue<std::vector<unsigned int> > (reader,"gen_particle_daughters_igen");
    gen_particle_daughters_status = new TTreeReaderValue<std::vector<unsigned int> > (reader,"gen_particle_daughters_status");
    gen_particle_daughters_charge = new TTreeReaderValue<std::vector<int> > (reader,"gen_particle_daughters_charge");
    gen_particle_daughters_id = new TTreeReaderValue<std::vector<int> > (reader,"gen_particle_daughters_id");
    gen_particle_daughters_pt = new TTreeReaderValue<std::vector<float> >  (reader,"gen_particle_daughters_pt");
    gen_particle_daughters_eta = new TTreeReaderValue<std::vector<float> >  (reader,"gen_particle_daughters_eta");
    gen_particle_daughters_phi = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_daughters_phi");
    gen_particle_daughters_mass = new TTreeReaderValue<std::vector<float> >  (reader,"gen_particle_daughters_mass");
  }

  TTreeReaderValue<float> rho (reader,"rho");
  TTreeReaderValue<unsigned int> npv (reader,"npv");
  TTreeReaderValue<unsigned int> nsv (reader,"nsv");
  TTreeReaderValue<float> met (reader,"met");
  TTreeReaderValue<float> met_phi (reader,"met_phi");

  TTreeReaderValue<std::vector<float> > *leppair_pt1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_eta1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_phi1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_mass1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_pt2 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_eta2 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_phi2 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_mass2 = NULL;
  TTreeReaderValue<unsigned int > *flags = NULL;

  if(domain != domain_type::none){
    flags = new TTreeReaderValue<unsigned int > (reader,"flags");
    leppair_pt1 = new TTreeReaderValue<std::vector<float>>  (reader,"leppair_pt1");                                        
    leppair_eta1 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_eta1");
    leppair_phi1 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_phi1");
    leppair_mass1 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_mass1");
    leppair_pt2 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_pt2");
    leppair_eta2 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_eta2");
    leppair_phi2 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_phi2");
    leppair_mass2 = new TTreeReaderValue<std::vector<float>> (reader,"leppair_mass2");
  }

  TTreeReaderValue<std::vector<float> > jet_pt (reader,"jet_pt");
  TTreeReaderValue<std::vector<float> > jet_pt_raw (reader,"jet_pt_raw");
  TTreeReaderValue<std::vector<float> > jet_eta (reader,"jet_eta");
  TTreeReaderValue<std::vector<float> > jet_phi (reader,"jet_phi");
  TTreeReaderValue<std::vector<float> > jet_mass (reader,"jet_mass");
  TTreeReaderValue<std::vector<float> > jet_mass_raw (reader,"jet_mass_raw");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probb (reader,"jet_deepjet_probb");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probbb (reader,"jet_deepjet_probbb");
  TTreeReaderValue<std::vector<float> > jet_deepjet_problepb (reader,"jet_deepjet_problepb");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probc (reader,"jet_deepjet_probc");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probuds (reader,"jet_deepjet_probuds");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probg (reader,"jet_deepjet_probg");
  TTreeReaderValue<std::vector<float> > jet_pnet_probb (reader,"jet_pnet_probb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probbb (reader,"jet_pnet_probbb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probc (reader,"jet_pnet_probc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probcc (reader,"jet_pnet_probcc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probuds (reader,"jet_pnet_probuds");
  TTreeReaderValue<std::vector<float> > jet_pnet_probg (reader,"jet_pnet_probg");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probb (reader,"jet_pnetlast_probb");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probc (reader,"jet_pnetlast_probc");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probuds (reader,"jet_pnetlast_probuds");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probg (reader,"jet_pnetlast_probg");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probmu (reader,"jet_pnetlast_probmu");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probele (reader,"jet_pnetlast_probele");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaup1h0p (reader,"jet_pnetlast_probtaup1h0p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaup1h1p (reader,"jet_pnetlast_probtaup1h1p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaup1h2p (reader,"jet_pnetlast_probtaup1h2p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaup3h0p (reader,"jet_pnetlast_probtaup3h0p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaup3h1p (reader,"jet_pnetlast_probtaup3h1p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaum1h0p (reader,"jet_pnetlast_probtaum1h0p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaum1h1p (reader,"jet_pnetlast_probtaum1h1p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaum1h2p (reader,"jet_pnetlast_probtaum1h2p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaum3h0p (reader,"jet_pnetlast_probtaum3h0p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_probtaum3h1p (reader,"jet_pnetlast_probtaum3h1p");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_ptcorr (reader,"jet_pnetlast_ptcorr");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_ptnu (reader,"jet_pnetlast_ptnu");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_ptreshigh (reader,"jet_pnetlast_ptreshigh");
  TTreeReaderValue<std::vector<float> > jet_pnetlast_ptreslow (reader,"jet_pnetlast_ptreslow");

  TTreeReaderValue<std::vector<float> > jet_parTlast_probb (reader,"jet_parTlast_probb");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probc (reader,"jet_parTlast_probc");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probuds (reader,"jet_parTlast_probuds");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probg (reader,"jet_parTlast_probg");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probmu (reader,"jet_parTlast_probmu");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probele (reader,"jet_parTlast_probele");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaup1h0p (reader,"jet_parTlast_probtaup1h0p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaup1h1p (reader,"jet_parTlast_probtaup1h1p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaup1h2p (reader,"jet_parTlast_probtaup1h2p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaup3h0p (reader,"jet_parTlast_probtaup3h0p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaup3h1p (reader,"jet_parTlast_probtaup3h1p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaum1h0p (reader,"jet_parTlast_probtaum1h0p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaum1h1p (reader,"jet_parTlast_probtaum1h1p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaum1h2p (reader,"jet_parTlast_probtaum1h2p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaum3h0p (reader,"jet_parTlast_probtaum3h0p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_probtaum3h1p (reader,"jet_parTlast_probtaum3h1p");
  TTreeReaderValue<std::vector<float> > jet_parTlast_ptcorr (reader,"jet_parTlast_ptcorr");
  TTreeReaderValue<std::vector<float> > jet_parTlast_ptnu (reader,"jet_parTlast_ptnu");
  TTreeReaderValue<std::vector<float> > jet_parTlast_ptreshigh (reader,"jet_parTlast_ptreshigh");
  TTreeReaderValue<std::vector<float> > jet_parTlast_ptreslow (reader,"jet_parTlast_ptreslow");

  TTreeReaderValue<std::vector<unsigned int> > jet_id (reader,"jet_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_puid (reader,"jet_puid");
  TTreeReaderValue<std::vector<unsigned int> > jet_hflav (reader,"jet_hflav");
  TTreeReaderValue<std::vector<int> > jet_pflav (reader,"jet_pflav");
  TTreeReaderValue<std::vector<unsigned int> > jet_nbhad (reader,"jet_nbhad");
  TTreeReaderValue<std::vector<unsigned int> > jet_nchad (reader,"jet_nchad");    
  TTreeReaderValue<std::vector<unsigned int> > jet_ncand (reader,"jet_ncand");
  TTreeReaderValue<std::vector<float> > jet_chf (reader,"jet_chf");
  TTreeReaderValue<std::vector<float> > jet_nhf (reader,"jet_nhf");
  TTreeReaderValue<std::vector<float> > jet_elf (reader,"jet_elf");
  TTreeReaderValue<std::vector<float> > jet_phf (reader,"jet_phf");
  TTreeReaderValue<std::vector<float> > jet_muf (reader,"jet_muf");
  
  TTreeReaderValue<std::vector<float> >* jet_genmatch_pt = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_eta = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_phi = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_mass = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_pt = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_eta = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_phi = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_mass = NULL;

  if(not (sample == sample_type::data)){
    jet_genmatch_pt = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_pt");
    jet_genmatch_eta = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_eta");
    jet_genmatch_phi = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_phi");
    jet_genmatch_mass = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_mass");
    jet_genmatch_wnu_pt = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_pt");
    jet_genmatch_wnu_eta = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_eta");
    jet_genmatch_wnu_phi = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_phi");
    jet_genmatch_wnu_mass = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_mass");
  }

  TTreeReaderValue<std::vector<float> > tau_pt (reader,"tau_pt");
  TTreeReaderValue<std::vector<float> > tau_eta (reader,"tau_eta");
  TTreeReaderValue<std::vector<float> > tau_phi (reader,"tau_phi");
  TTreeReaderValue<std::vector<float> > tau_mass (reader,"tau_mass");
  TTreeReaderValue<std::vector<float> > tau_dz (reader,"tau_dz");
  TTreeReaderValue<std::vector<float> > tau_dxy (reader,"tau_dxy");
  TTreeReaderValue<std::vector<unsigned int> > tau_decaymode (reader,"tau_decaymode");
  TTreeReaderValue<std::vector<int> > tau_charge (reader,"tau_charge");
  TTreeReaderValue<std::vector<float> > tau_idjet (reader,"tau_idjet");
  TTreeReaderValue<std::vector<float> > tau_idele (reader,"tau_idele");
  TTreeReaderValue<std::vector<float> > tau_idmu (reader,"tau_idmu");
  TTreeReaderValue<std::vector<unsigned int> > tau_idjet_wp (reader,"tau_idjet_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_idmu_wp (reader,"tau_idmu_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_idele_wp (reader,"tau_idele_wp");

  TTreeReaderValue<std::vector<float> > muon_pt (reader,"muon_pt");
  TTreeReaderValue<std::vector<float> > muon_eta (reader,"muon_eta");
  TTreeReaderValue<std::vector<float> > muon_phi (reader,"muon_phi");
  TTreeReaderValue<std::vector<float> > muon_mass (reader,"muon_mass");
  TTreeReaderValue<std::vector<float> > muon_energy (reader,"muon_energy");
  TTreeReaderValue<std::vector<int> > muon_charge (reader,"muon_charge");
  TTreeReaderValue<std::vector<unsigned int> > muon_id (reader,"muon_id");
  TTreeReaderValue<std::vector<unsigned int> > muon_iso (reader,"muon_iso");
  TTreeReaderValue<std::vector<float> > muon_d0 (reader,"muon_d0");
  TTreeReaderValue<std::vector<float> > muon_dz (reader,"muon_dz");

  TTreeReaderValue<std::vector<float> > electron_pt (reader,"electron_pt");
  TTreeReaderValue<std::vector<float> > electron_pt_corr (reader,"electron_pt_corr");
  TTreeReaderValue<std::vector<float> > electron_eta (reader,"electron_eta");
  TTreeReaderValue<std::vector<float> > electron_phi (reader,"electron_phi");
  TTreeReaderValue<std::vector<float> > electron_mass (reader,"electron_mass");
  TTreeReaderValue<std::vector<float> > electron_energy (reader,"electron_energy");
  TTreeReaderValue<std::vector<int> > electron_charge (reader,"electron_charge");
  TTreeReaderValue<std::vector<unsigned int> > electron_id (reader,"electron_id");
  TTreeReaderValue<std::vector<float> > electron_idscore (reader,"electron_idscore");
  TTreeReaderValue<std::vector<float> > electron_d0 (reader,"electron_d0");
  TTreeReaderValue<std::vector<float> > electron_dz (reader,"electron_dz");

  TTreeReaderValue<std::vector<float> > photon_pt (reader,"photon_pt");
  TTreeReaderValue<std::vector<float> > photon_eta (reader,"photon_eta");
  TTreeReaderValue<std::vector<float> > photon_phi (reader,"photon_phi");
  TTreeReaderValue<std::vector<float> > photon_mass (reader,"photon_mass");
  TTreeReaderValue<std::vector<float> > photon_energy (reader,"photon_energy");
  TTreeReaderValue<std::vector<unsigned int> > photon_id (reader,"photon_id");
  TTreeReaderValue<std::vector<float> > photon_idscore (reader,"photon_idscore");

  TTreeReaderValue<std::vector<float> > jet_pfcand_pt (reader,"jet_pfcandidate_pt");
  TTreeReaderValue<std::vector<float> > jet_pfcand_eta (reader,"jet_pfcandidate_eta");
  TTreeReaderValue<std::vector<float> > jet_pfcand_phi (reader,"jet_pfcandidate_phi");
  TTreeReaderValue<std::vector<float> > jet_pfcand_mass (reader,"jet_pfcandidate_mass");
  TTreeReaderValue<std::vector<float> > jet_pfcand_energy (reader,"jet_pfcandidate_energy");
  TTreeReaderValue<std::vector<float> > jet_pfcand_calofraction (reader,"jet_pfcandidate_calofraction");
  TTreeReaderValue<std::vector<float> > jet_pfcand_hcalfraction (reader,"jet_pfcandidate_hcalfraction");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dxy (reader,"jet_pfcandidate_dxy");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dz (reader,"jet_pfcandidate_dz");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dxysig (reader,"jet_pfcandidate_dxysig");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dzsig (reader,"jet_pfcandidate_dzsig");
  TTreeReaderValue<std::vector<float> > jet_pfcand_pperp_ratio (reader,"jet_pfcandidate_candjet_pperp_ratio");
  TTreeReaderValue<std::vector<float> > jet_pfcand_ppara_ratio (reader,"jet_pfcandidate_candjet_ppara_ratio");
  TTreeReaderValue<std::vector<float> > jet_pfcand_deta (reader,"jet_pfcandidate_candjet_deta");
  TTreeReaderValue<std::vector<float> > jet_pfcand_dphi (reader,"jet_pfcandidate_candjet_dphi");
  TTreeReaderValue<std::vector<float> > jet_pfcand_etarel (reader,"jet_pfcandidate_candjet_etarel");
  TTreeReaderValue<std::vector<float> > jet_pfcand_puppiw (reader,"jet_pfcandidate_puppiw");
  TTreeReaderValue<std::vector<int> > jet_pfcand_charge (reader,"jet_pfcandidate_charge");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_frompv (reader,"jet_pfcandidate_frompv");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_id (reader,"jet_pfcandidate_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_ijet (reader,"jet_pfcandidate_ijet");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nhits (reader,"jet_pfcandidate_nhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_npixhits (reader,"jet_pfcandidate_npixhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nstriphits (reader,"jet_pfcandidate_nstriphits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nlosthits (reader,"jet_pfcandidate_nlosthits");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_npixlayers (reader,"jet_pfcandidate_npixlayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_nstriplayers (reader,"jet_pfcandidate_nstriplayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_track_qual (reader,"jet_pfcandidate_track_qual");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_track_chi2 (reader,"jet_pfcandidate_track_chi2");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcand_track_algo (reader,"jet_pfcandidate_track_algo");
  TTreeReaderValue<std::vector<float> > jet_pfcand_track_pterr (reader,"jet_pfcandidate_track_pterr");
  TTreeReaderValue<std::vector<float> > jet_pfcand_track_etaerr (reader,"jet_pfcandidate_track_etaerr");
  TTreeReaderValue<std::vector<float> > jet_pfcand_track_phierr (reader,"jet_pfcandidate_track_phierr");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_d3d (reader,"jet_pfcandidate_trackjet_d3d");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_d3dsig (reader,"jet_pfcandidate_trackjet_d3dsig");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_dist (reader,"jet_pfcandidate_trackjet_dist");
  TTreeReaderValue<std::vector<float> > jet_pfcand_trackjet_decayL (reader,"jet_pfcandidate_trackjet_decayL");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_tau_signal (reader,"jet_pfcandidate_tau_signal");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_id (reader,"jet_pfcandidate_muon_id");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_muon_chi2 (reader,"jet_pfcandidate_muon_chi2");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_muon_segcomp (reader,"jet_pfcandidate_muon_segcomp");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_isglobal (reader,"jet_pfcandidate_muon_isglobal");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_nvalidhit (reader,"jet_pfcandidate_muon_nvalidhit");
  TTreeReaderValue<std::vector<unsigned int> > jet_pfcandidate_muon_nstation (reader,"jet_pfcandidate_muon_nstation");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_eOverP (reader,"jet_pfcandidate_electron_eOverP");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_detaIn (reader,"jet_pfcandidate_electron_detaIn");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_dphiIn (reader,"jet_pfcandidate_electron_dphiIn");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_r9 (reader,"jet_pfcandidate_electron_r9");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_sigIetaIeta (reader,"jet_pfcandidate_electron_sigIetaIeta");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_sigIphiIphi (reader,"jet_pfcandidate_electron_sigIphiIphi");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_electron_convProb (reader,"jet_pfcandidate_electron_convProb");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_photon_r9 (reader,"jet_pfcandidate_photon_r9");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_photon_sigIetaIeta (reader,"jet_pfcandidate_photon_sigIetaIeta");
  TTreeReaderValue<std::vector<float> > jet_pfcandidate_photon_eVeto (reader,"jet_pfcandidate_photon_eVeto");

  TTreeReaderValue<std::vector<float> > jet_losttrack_pt (reader,"jet_losttrack_pt");
  TTreeReaderValue<std::vector<float> > jet_losttrack_eta (reader,"jet_losttrack_eta");
  TTreeReaderValue<std::vector<float> > jet_losttrack_phi (reader,"jet_losttrack_phi");
  TTreeReaderValue<std::vector<float> > jet_losttrack_mass (reader,"jet_losttrack_mass");
  TTreeReaderValue<std::vector<float> > jet_losttrack_energy (reader,"jet_losttrack_energy");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dxy (reader,"jet_losttrack_dxy");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dz (reader,"jet_losttrack_dz");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dxysig (reader,"jet_losttrack_dxysig");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dzsig (reader,"jet_losttrack_dzsig");
  TTreeReaderValue<std::vector<float> > jet_losttrack_deta (reader,"jet_losttrack_candjet_deta");
  TTreeReaderValue<std::vector<float> > jet_losttrack_dphi (reader,"jet_losttrack_candjet_dphi");
  TTreeReaderValue<std::vector<float> > jet_losttrack_etarel (reader,"jet_losttrack_candjet_etarel");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_frompv (reader,"jet_losttrack_frompv");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_ijet (reader,"jet_losttrack_ijet");
  TTreeReaderValue<std::vector<int> > jet_losttrack_charge (reader,"jet_losttrack_charge");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nhits (reader,"jet_losttrack_nhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_npixhits (reader,"jet_losttrack_npixhits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nstriphits (reader,"jet_losttrack_nstriphits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nlosthits (reader,"jet_losttrack_nlosthits");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_npixlayers (reader,"jet_losttrack_npixlayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_nstriplayers (reader,"jet_losttrack_nstriplayers");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_track_qual (reader,"jet_losttrack_track_qual");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_track_chi2 (reader,"jet_losttrack_track_chi2");
  TTreeReaderValue<std::vector<unsigned int> > jet_losttrack_track_algo (reader,"jet_losttrack_track_algo");
  TTreeReaderValue<std::vector<float> > jet_losttrack_track_pterr (reader,"jet_losttrack_track_pterr");
  TTreeReaderValue<std::vector<float> > jet_losttrack_track_etaerr (reader,"jet_losttrack_track_etaerr");
  TTreeReaderValue<std::vector<float> > jet_losttrack_track_phierr (reader,"jet_losttrack_track_phierr");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_d3d (reader,"jet_losttrack_trackjet_d3d");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_d3dsig (reader,"jet_losttrack_trackjet_d3dsig");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_dist (reader,"jet_losttrack_trackjet_dist");
  TTreeReaderValue<std::vector<float> > jet_losttrack_trackjet_decayL (reader,"jet_losttrack_trackjet_decayL");

  TTreeReaderValue<std::vector<float> > jet_sv_pt (reader,"jet_sv_pt");
  TTreeReaderValue<std::vector<float> > jet_sv_eta (reader,"jet_sv_eta");
  TTreeReaderValue<std::vector<float> > jet_sv_phi (reader,"jet_sv_phi");
  TTreeReaderValue<std::vector<float> > jet_sv_mass (reader,"jet_sv_mass");
  TTreeReaderValue<std::vector<float> > jet_sv_energy (reader,"jet_sv_energy");
  TTreeReaderValue<std::vector<float> > jet_sv_chi2 (reader,"jet_sv_chi2");
  TTreeReaderValue<std::vector<float> > jet_sv_dxy (reader,"jet_sv_dxy");
  TTreeReaderValue<std::vector<float> > jet_sv_dxysig (reader,"jet_sv_dxysig");
  TTreeReaderValue<std::vector<float> > jet_sv_d3d (reader,"jet_sv_d3d");
  TTreeReaderValue<std::vector<float> > jet_sv_d3dsig (reader,"jet_sv_d3dsig");
  TTreeReaderValue<std::vector<unsigned int> > jet_sv_ntrack (reader,"jet_sv_ntrack");
  TTreeReaderValue<std::vector<unsigned int> > jet_sv_ijet (reader,"jet_sv_ijet");

  reader.SetTree(tree_in.get());
  auto beginEntry = (Long64_t) nevents*workerID/nthreads;
  auto endEntry   = (Long64_t) nevents*std::min(nthreads.load(std::memory_order_relaxed),workerID+1)/nthreads-1;
  reader.SetEntriesRange(beginEntry,endEntry);
  std::cout<<"ntupleCreation --> thread "<<workerID<<" starting from entry "<<beginEntry<<" to entry "<<endEntry<<std::endl;

  // output ntupla
  unsigned int run_b, lumi_b, event_b, npv_b, nsv_b, npu_b;
  float wgt_b,rho_b,met_b;
  int sample_b, domain_b, isdata_b;
  unsigned int ijet_b;

  tree_out->Branch("run",&run_b,"run/i"); 
  tree_out->Branch("lumi",&lumi_b,"lumi/i"); 
  tree_out->Branch("event",&event_b,"event/i"); 
  tree_out->Branch("npv",&npv_b,"npv/i"); 
  tree_out->Branch("nsv",&nsv_b,"nsv/i"); 
  tree_out->Branch("npu",&npu_b,"npu/i"); 
  tree_out->Branch("wgt",&wgt_b,"wgt/F"); 
  tree_out->Branch("rho",&rho_b,"rho/F"); 
  tree_out->Branch("met",&met_b,"met/F"); 
  tree_out->Branch("sample",&sample_b,"sample/I");
  tree_out->Branch("domain",&domain_b,"domain/I");
  tree_out->Branch("isdata",&isdata_b,"isdata/I");
  tree_out->Branch("ijet",&ijet_b,"ijet/i");

  float jet_pt_b, jet_pt_raw_b, jet_eta_b,jet_phi_b,jet_mass_b, jet_mass_raw_b, jet_energy_b;
  float jet_deepjet_probb_b,jet_deepjet_probc_b,jet_deepjet_probuds_b,jet_deepjet_probg_b;
  float jet_pnet_probb_b,jet_pnet_probc_b,jet_pnet_probuds_b,jet_pnet_probg_b;
  float jet_pnetlast_probb_b,jet_pnetlast_probc_b,jet_pnetlast_probuds_b,jet_pnetlast_probg_b;
  float jet_pnetlast_probtaup1h0p_b, jet_pnetlast_probtaup1h1p_b, jet_pnetlast_probtaup1h2p_b, jet_pnetlast_probtaup3h0p_b, jet_pnetlast_probtaup3h1p_b;
  float jet_pnetlast_probtaum1h0p_b, jet_pnetlast_probtaum1h1p_b, jet_pnetlast_probtaum1h2p_b, jet_pnetlast_probtaum3h0p_b, jet_pnetlast_probtaum3h1p_b;
  float jet_pnetlast_probele_b, jet_pnetlast_probmu_b, jet_pnetlast_ptcorr_b, jet_pnetlast_ptnu_b, jet_pnetlast_ptreshigh_b, jet_pnetlast_ptreslow_b;
  float jet_genmatch_pt_b,jet_genmatch_eta_b,jet_genmatch_phi_b,jet_genmatch_mass_b, jet_genmatch_energy_b;
  float jet_genmatch_wnu_pt_b, jet_genmatch_wnu_eta_b, jet_genmatch_wnu_phi_b, jet_genmatch_wnu_mass_b, jet_genmatch_wnu_energy_b;
  float jet_genmatch_lep_pt_b, jet_genmatch_lep_eta_b, jet_genmatch_lep_phi_b, jet_genmatch_lep_mass_b, jet_genmatch_lep_energy_b;
  float jet_genmatch_lep_vis_pt_b, jet_genmatch_lep_vis_eta_b, jet_genmatch_lep_vis_phi_b, jet_genmatch_lep_vis_mass_b, jet_genmatch_lep_vis_energy_b;
  float jet_genmatch_nu_pt_b, jet_genmatch_nu_eta_b, jet_genmatch_nu_phi_b, jet_genmatch_nu_mass_b, jet_genmatch_nu_energy_b;
  unsigned int jet_ncand_b, jet_nbhad_b, jet_nchad_b, jet_hflav_b, jet_nch_b, jet_nneu_b;
  float jet_chf_b, jet_nhf_b, jet_phf_b, jet_elf_b, jet_muf_b;
  int jet_pflav_b, jet_tauflav_b, jet_muflav_b, jet_elflav_b, jet_taudecaymode_b, jet_lepflav_b;
  int jet_taucharge_b, jet_mucharge_b, jet_elcharge_b;
  // di -tau variables
  int jet_ditauflav_b;

  tree_out->Branch("jet_pt",&jet_pt_b,"jet_pt/F");
  tree_out->Branch("jet_pt_raw",&jet_pt_raw_b,"jet_pt_raw/F");
  tree_out->Branch("jet_eta",&jet_eta_b,"jet_eta/F");
  tree_out->Branch("jet_phi",&jet_phi_b,"jet_phi/F");
  tree_out->Branch("jet_mass",&jet_mass_b,"jet_mass/F");
  tree_out->Branch("jet_mass_raw",&jet_mass_raw_b,"jet_mass_raw/F");
  tree_out->Branch("jet_energy",&jet_energy_b,"jet_energy/F");
  tree_out->Branch("jet_deepjet_probb",&jet_deepjet_probb_b,"jet_deepjet_probb/F");
  tree_out->Branch("jet_deepjet_probc",&jet_deepjet_probc_b,"jet_deepjet_probc/F");
  tree_out->Branch("jet_deepjet_probuds",&jet_deepjet_probuds_b,"jet_deepjet_probuds/F");
  tree_out->Branch("jet_deepjet_probg",&jet_deepjet_probg_b,"jet_deepjet_probg/F");
  tree_out->Branch("jet_pnet_probb",&jet_pnet_probb_b,"jet_pnet_probb/F");
  tree_out->Branch("jet_pnet_probc",&jet_pnet_probc_b,"jet_pnet_probc/F");
  tree_out->Branch("jet_pnet_probuds",&jet_pnet_probuds_b,"jet_pnet_probuds/F");
  tree_out->Branch("jet_pnet_probg",&jet_pnet_probg_b,"jet_pnet_probg/F");
  tree_out->Branch("jet_pnetlast_probb",&jet_pnetlast_probb_b,"jet_pnetlast_probb/F");
  tree_out->Branch("jet_pnetlast_probc",&jet_pnetlast_probc_b,"jet_pnetlast_probc/F");
  tree_out->Branch("jet_pnetlast_probuds",&jet_pnetlast_probuds_b,"jet_pnetlast_probuds/F");
  tree_out->Branch("jet_pnetlast_probg",&jet_pnetlast_probg_b,"jet_pnetlast_probg/F");
  tree_out->Branch("jet_pnetlast_probmu",&jet_pnetlast_probmu_b,"jet_pnetlast_probmu/F");
  tree_out->Branch("jet_pnetlast_probele",&jet_pnetlast_probele_b,"jet_pnetlast_probele/F");
  tree_out->Branch("jet_pnetlast_probtaup1h0p",&jet_pnetlast_probtaup1h0p_b,"jet_pnetlast_probtaup1h0p/F");
  tree_out->Branch("jet_pnetlast_probtaup1h1p",&jet_pnetlast_probtaup1h1p_b,"jet_pnetlast_probtaup1h1p/F");
  tree_out->Branch("jet_pnetlast_probtaup1h2p",&jet_pnetlast_probtaup1h2p_b,"jet_pnetlast_probtaup1h2p/F");
  tree_out->Branch("jet_pnetlast_probtaup3h0p",&jet_pnetlast_probtaup3h0p_b,"jet_pnetlast_probtaup3h0p/F");
  tree_out->Branch("jet_pnetlast_probtaup3h1p",&jet_pnetlast_probtaup3h1p_b,"jet_pnetlast_probtaup3h1p/F");
  tree_out->Branch("jet_pnetlast_probtaum1h0p",&jet_pnetlast_probtaum1h0p_b,"jet_pnetlast_probtaum1h0p/F");
  tree_out->Branch("jet_pnetlast_probtaum1h1p",&jet_pnetlast_probtaum1h1p_b,"jet_pnetlast_probtaum1h1p/F");
  tree_out->Branch("jet_pnetlast_probtaum1h2p",&jet_pnetlast_probtaum1h2p_b,"jet_pnetlast_probtaum1h2p/F");
  tree_out->Branch("jet_pnetlast_probtaum3h0p",&jet_pnetlast_probtaum3h0p_b,"jet_pnetlast_probtaum3h0p/F");
  tree_out->Branch("jet_pnetlast_probtaum3h1p",&jet_pnetlast_probtaum3h1p_b,"jet_pnetlast_probtaum3h1p/F");
  tree_out->Branch("jet_pnetlast_ptcorr",&jet_pnetlast_ptcorr_b,"jet_pnetlast_ptcorr/F");
  tree_out->Branch("jet_pnetlast_ptnu",&jet_pnetlast_ptnu_b,"jet_pnetlast_ptnu/F");
  tree_out->Branch("jet_pnetlast_ptreshigh",&jet_pnetlast_ptreshigh_b,"jet_pnetlast_ptreshigh/F");
  tree_out->Branch("jet_pnetlast_ptreslow",&jet_pnetlast_ptreslow_b,"jet_pnetlast_ptreslow/F");

  tree_out->Branch("jet_chf",&jet_chf_b,"jet_chf/F");
  tree_out->Branch("jet_nhf",&jet_nhf_b,"jet_nhf/F");
  tree_out->Branch("jet_phf",&jet_phf_b,"jet_phf/F");
  tree_out->Branch("jet_elf",&jet_elf_b,"jet_elf/F");
  tree_out->Branch("jet_muf",&jet_muf_b,"jet_muf/F");
  tree_out->Branch("jet_ncand",&jet_ncand_b,"jet_ncand/i");
  tree_out->Branch("jet_nch",&jet_nch_b,"jet_nch/i");
  tree_out->Branch("jet_nneu",&jet_nneu_b,"jet_nneu/i");
  tree_out->Branch("jet_nbhad",&jet_nbhad_b,"jet_nbhad/i");
  tree_out->Branch("jet_nchad",&jet_nchad_b,"jet_nchad/i");
  tree_out->Branch("jet_hflav",&jet_hflav_b,"jet_hflav/i");
  tree_out->Branch("jet_pflav",&jet_pflav_b,"jet_pflav/I");
  tree_out->Branch("jet_tauflav",&jet_tauflav_b,"jet_tauflav/I");
  tree_out->Branch("jet_ditauflav",&jet_ditauflav_b,"jet_ditauflav/I");
  tree_out->Branch("jet_taudecaymode",&jet_taudecaymode_b,"jet_taudecaymode/I");
  tree_out->Branch("jet_muflav",&jet_muflav_b,"jet_muflav/I");
  tree_out->Branch("jet_elflav",&jet_elflav_b,"jet_elflav/I");
  tree_out->Branch("jet_lepflav",&jet_lepflav_b,"jet_lepflav/I");
  tree_out->Branch("jet_mucharge",&jet_mucharge_b,"jet_mucharge/I");
  tree_out->Branch("jet_elcharge",&jet_elcharge_b,"jet_elcharge/I");
  tree_out->Branch("jet_taucharge",&jet_taucharge_b,"jet_taucharge/I");

  tree_out->Branch("jet_genmatch_pt",&jet_genmatch_pt_b,"jet_genmatch_pt/F");
  tree_out->Branch("jet_genmatch_eta",&jet_genmatch_eta_b,"jet_genmatch_eta/F");
  tree_out->Branch("jet_genmatch_phi",&jet_genmatch_phi_b,"jet_genmatch_phi/F");
  tree_out->Branch("jet_genmatch_mass",&jet_genmatch_mass_b,"jet_genmatch_mass/F");
  tree_out->Branch("jet_genmatch_energy",&jet_genmatch_energy_b,"jet_genmatch_energy/F");
  tree_out->Branch("jet_genmatch_wnu_pt",&jet_genmatch_wnu_pt_b,"jet_genmatch_wnu_pt/F");
  tree_out->Branch("jet_genmatch_wnu_eta",&jet_genmatch_wnu_eta_b,"jet_genmatch_wnu_eta/F");
  tree_out->Branch("jet_genmatch_wnu_phi",&jet_genmatch_wnu_phi_b,"jet_genmatch_wnu_phi/F");
  tree_out->Branch("jet_genmatch_wnu_mass",&jet_genmatch_wnu_mass_b,"jet_genmatch_wnu_mass/F");
  tree_out->Branch("jet_genmatch_wnu_energy",&jet_genmatch_wnu_energy_b,"jet_genmatch_wnu_energy/F");
  tree_out->Branch("jet_genmatch_nu_pt",&jet_genmatch_nu_pt_b,"jet_genmatch_nu_pt/F");
  tree_out->Branch("jet_genmatch_nu_eta",&jet_genmatch_nu_eta_b,"jet_genmatch_nu_eta/F");
  tree_out->Branch("jet_genmatch_nu_phi",&jet_genmatch_nu_phi_b,"jet_genmatch_nu_phi/F");
  tree_out->Branch("jet_genmatch_nu_mass",&jet_genmatch_nu_mass_b,"jet_genmatch_nu_mass/F");
  tree_out->Branch("jet_genmatch_nu_energy",&jet_genmatch_nu_energy_b,"jet_genmatch_nu_energy/F");
  tree_out->Branch("jet_genmatch_lep_pt",&jet_genmatch_lep_pt_b,"jet_genmatch_lep_pt/F");
  tree_out->Branch("jet_genmatch_lep_eta",&jet_genmatch_lep_eta_b,"jet_genmatch_lep_eta/F");
  tree_out->Branch("jet_genmatch_lep_phi",&jet_genmatch_lep_phi_b,"jet_genmatch_lep_phi/F");
  tree_out->Branch("jet_genmatch_lep_mass",&jet_genmatch_lep_mass_b,"jet_genmatch_lep_mass/F");
  tree_out->Branch("jet_genmatch_lep_energy",&jet_genmatch_lep_energy_b,"jet_genmatch_lep_energy/F");
  tree_out->Branch("jet_genmatch_lep_vis_pt",&jet_genmatch_lep_vis_pt_b,"jet_genmatch_lep_vis_pt/F");
  tree_out->Branch("jet_genmatch_lep_vis_eta",&jet_genmatch_lep_vis_eta_b,"jet_genmatch_lep_vis_eta/F");
  tree_out->Branch("jet_genmatch_lep_vis_phi",&jet_genmatch_lep_vis_phi_b,"jet_genmatch_lep_vis_phi/F");
  tree_out->Branch("jet_genmatch_lep_vis_mass",&jet_genmatch_lep_vis_mass_b,"jet_genmatch_lep_vis_mass/F");
  tree_out->Branch("jet_genmatch_lep_vis_energy",&jet_genmatch_lep_vis_energy_b,"jet_genmatch_lep_vis_energy/F");

  float jet_parTlast_probb_b,jet_parTlast_probc_b,jet_parTlast_probuds_b,jet_parTlast_probg_b;
  float jet_parTlast_probtaup1h0p_b, jet_parTlast_probtaup1h1p_b, jet_parTlast_probtaup1h2p_b, jet_parTlast_probtaup3h0p_b, jet_parTlast_probtaup3h1p_b;
  float jet_parTlast_probtaum1h0p_b, jet_parTlast_probtaum1h1p_b, jet_parTlast_probtaum1h2p_b, jet_parTlast_probtaum3h0p_b, jet_parTlast_probtaum3h1p_b;
  float jet_parTlast_probele_b, jet_parTlast_probmu_b, jet_parTlast_ptcorr_b, jet_parTlast_ptnu_b, jet_parTlast_ptreshigh_b, jet_parTlast_ptreslow_b;

  tree_out->Branch("jet_parTlast_probtaup1h0p",&jet_parTlast_probtaup1h0p_b,"jet_parTlast_probtaup1h0p/F");
  tree_out->Branch("jet_parTlast_probtaup1h1p",&jet_parTlast_probtaup1h1p_b,"jet_parTlast_probtaup1h1p/F");
  tree_out->Branch("jet_parTlast_probtaup1h2p",&jet_parTlast_probtaup1h2p_b,"jet_parTlast_probtaup1h2p/F");
  tree_out->Branch("jet_parTlast_probtaup3h0p",&jet_parTlast_probtaup3h0p_b,"jet_parTlast_probtaup3h0p/F");
  tree_out->Branch("jet_parTlast_probtaup3h1p",&jet_parTlast_probtaup3h1p_b,"jet_parTlast_probtaup3h1p/F");
  tree_out->Branch("jet_parTlast_probtaum1h0p",&jet_parTlast_probtaum1h0p_b,"jet_parTlast_probtaum1h0p/F");
  tree_out->Branch("jet_parTlast_probtaum1h1p",&jet_parTlast_probtaum1h1p_b,"jet_parTlast_probtaum1h1p/F");
  tree_out->Branch("jet_parTlast_probtaum1h2p",&jet_parTlast_probtaum1h2p_b,"jet_parTlast_probtaum1h2p/F");
  tree_out->Branch("jet_parTlast_probtaum3h0p",&jet_parTlast_probtaum3h0p_b,"jet_parTlast_probtaum3h0p/F");
  tree_out->Branch("jet_parTlast_probtaum3h1p",&jet_parTlast_probtaum3h1p_b,"jet_parTlast_probtaum3h1p/F");
  tree_out->Branch("jet_parTlast_probb",&jet_parTlast_probb_b,"jet_parTlast_probb/F");
  tree_out->Branch("jet_parTlast_probc",&jet_parTlast_probc_b,"jet_parTlast_probc/F");
  tree_out->Branch("jet_parTlast_probuds",&jet_parTlast_probuds_b,"jet_parTlast_probuds/F");
  tree_out->Branch("jet_parTlast_probg",&jet_parTlast_probg_b,"jet_parTlast_probg/F");
  tree_out->Branch("jet_parTlast_probmu",&jet_parTlast_probmu_b,"jet_parTlast_probmu/F");
  tree_out->Branch("jet_parTlast_probele",&jet_parTlast_probele_b,"jet_parTlast_probele/F");
  tree_out->Branch("jet_parTlast_ptcorr",&jet_parTlast_ptcorr_b,"jet_parTlast_ptcorr/F");
  tree_out->Branch("jet_parTlast_ptnu",&jet_parTlast_ptnu_b,"jet_parTlast_ptnu/F");
  tree_out->Branch("jet_parTlast_ptreshigh",&jet_parTlast_ptreshigh_b,"jet_parTlast_ptreshigh/F");
  tree_out->Branch("jet_parTlast_ptreslow",&jet_parTlast_ptreslow_b,"jet_parTlast_ptreslow/F");
  
  float jet_taumatch_pt_b, jet_taumatch_eta_b, jet_taumatch_phi_b, jet_taumatch_mass_b, jet_taumatch_energy_b;
  float jet_taumatch_idjet_b, jet_taumatch_idele_b, jet_taumatch_idmu_b;
  int jet_taumatch_decaymode_b, jet_taumatch_charge_b;
  float jet_taumatch_dxy_b, jet_taumatch_dz_b;
  int jet_taumatch_idjet_wp_b, jet_taumatch_idmu_wp_b, jet_taumatch_idele_wp_b;

  tree_out->Branch("jet_taumatch_pt",&jet_taumatch_pt_b,"jet_taumatch_pt/F");
  tree_out->Branch("jet_taumatch_eta",&jet_taumatch_eta_b,"jet_taumatch_eta/F");
  tree_out->Branch("jet_taumatch_phi",&jet_taumatch_phi_b,"jet_taumatch_phi/F");
  tree_out->Branch("jet_taumatch_mass",&jet_taumatch_mass_b,"jet_taumatch_mass/F");
  tree_out->Branch("jet_taumatch_energy",&jet_taumatch_energy_b,"jet_taumatch_energy/F");
  tree_out->Branch("jet_taumatch_decaymode",&jet_taumatch_decaymode_b,"jet_taumatch_decaymode/I");
  tree_out->Branch("jet_taumatch_charge",&jet_taumatch_charge_b,"jet_taumatch_charge/I");
  tree_out->Branch("jet_taumatch_idjet",&jet_taumatch_idjet_b,"jet_taumatch_idjet/F");
  tree_out->Branch("jet_taumatch_idmu",&jet_taumatch_idmu_b,"jet_taumatch_idmu/F");
  tree_out->Branch("jet_taumatch_idele",&jet_taumatch_idele_b,"jet_taumatch_idele/F");
  tree_out->Branch("jet_taumatch_dxy",&jet_taumatch_dxy_b,"jet_taumatch_dxy/F");
  tree_out->Branch("jet_taumatch_dz",&jet_taumatch_dz_b,"jet_taumatch_dz/F");
  tree_out->Branch("jet_taumatch_idjet_wp",&jet_taumatch_idjet_wp_b,"jet_taumatch_idjet_wp/I");
  tree_out->Branch("jet_taumatch_idmu_wp",&jet_taumatch_idmu_wp_b,"jet_taumatch_idmu_wp/I");
  tree_out->Branch("jet_taumatch_idele_wp",&jet_taumatch_idele_wp_b,"jet_taumatch_idele_wp/I");

  float jet_mumatch_pt_b, jet_mumatch_eta_b, jet_mumatch_phi_b, jet_mumatch_mass_b, jet_mumatch_energy_b, jet_mumatch_dxy_b, jet_mumatch_dz_b;
  int jet_mumatch_id_b, jet_mumatch_iso_b;
      
  tree_out->Branch("jet_mumatch_pt",&jet_mumatch_pt_b,"jet_mumatch_pt/F");
  tree_out->Branch("jet_mumatch_eta",&jet_mumatch_eta_b,"jet_mumatch_eta/F");
  tree_out->Branch("jet_mumatch_phi",&jet_mumatch_phi_b,"jet_mumatch_phi/F");
  tree_out->Branch("jet_mumatch_mass",&jet_mumatch_mass_b,"jet_mumatch_mass/F");
  tree_out->Branch("jet_mumatch_energy",&jet_mumatch_energy_b,"jet_mumatch_energy/F");
  tree_out->Branch("jet_mumatch_dxy",&jet_mumatch_dxy_b,"jet_mumatch_dxy/F");
  tree_out->Branch("jet_mumatch_dz",&jet_mumatch_dz_b,"jet_mumatch_dz/F");
  tree_out->Branch("jet_mumatch_id",&jet_mumatch_id_b,"jet_mumatch_id/I");
  tree_out->Branch("jet_mumatch_iso",&jet_mumatch_iso_b,"jet_mumatch_iso/I");

  float jet_elematch_pt_b, jet_elematch_eta_b, jet_elematch_phi_b, jet_elematch_mass_b, jet_elematch_energy_b, jet_elematch_dxy_b, jet_elematch_dz_b;
  int jet_elematch_id_b;
  float jet_elematch_idscore_b;

  tree_out->Branch("jet_elematch_pt",&jet_elematch_pt_b,"jet_elematch_pt/F");
  tree_out->Branch("jet_elematch_eta",&jet_elematch_eta_b,"jet_elematch_eta/F");
  tree_out->Branch("jet_elematch_phi",&jet_elematch_phi_b,"jet_elematch_phi/F");
  tree_out->Branch("jet_elematch_mass",&jet_elematch_mass_b,"jet_elematch_mass/F");
  tree_out->Branch("jet_elematch_energy",&jet_elematch_energy_b,"jet_elematch_energy/F");
  tree_out->Branch("jet_elematch_dxy",&jet_elematch_dxy_b,"jet_elematch_dxy/F");
  tree_out->Branch("jet_elematch_dz",&jet_elematch_dz_b,"jet_elematch_dz/F");
  tree_out->Branch("jet_elematch_id",&jet_elematch_id_b,"jet_elematch_id/I");
  tree_out->Branch("jet_elematch_idscore",&jet_elematch_idscore_b,"jet_elematch_idscore/F");

  float jet_phomatch_pt_b, jet_phomatch_eta_b, jet_phomatch_phi_b, jet_phomatch_mass_b, jet_phomatch_energy_b;
  int jet_phomatch_id_b;
  float jet_phomatch_idscore_b;

  tree_out->Branch("jet_phomatch_pt",&jet_phomatch_pt_b,"jet_phomatch_pt/F");
  tree_out->Branch("jet_phomatch_eta",&jet_phomatch_eta_b,"jet_phomatch_eta/F");
  tree_out->Branch("jet_phomatch_phi",&jet_phomatch_phi_b,"jet_phomatch_phi/F");
  tree_out->Branch("jet_phomatch_mass",&jet_phomatch_mass_b,"jet_phomatch_mass/F");
  tree_out->Branch("jet_phomatch_energy",&jet_phomatch_energy_b,"jet_phomatch_energy/F");
  tree_out->Branch("jet_phomatch_id",&jet_phomatch_id_b,"jet_phomatch_id/I");
  tree_out->Branch("jet_phomatch_idscore",&jet_phomatch_idscore_b,"jet_phomatch_idscore/F");
   
  std::vector<float> jet_pfcand_pt_b, jet_pfcand_eta_b, jet_pfcand_phi_b, jet_pfcand_mass_b, jet_pfcand_energy_b, jet_pfcand_pt_log_b, jet_pfcand_energy_log_b, jet_pfcand_calofraction_b;
  std::vector<float> jet_pfcand_hcalfraction_b, jet_pfcand_dxy_b, jet_pfcand_dz_b, jet_pfcand_dxysig_b, jet_pfcand_dzsig_b, jet_pfcand_pperp_ratio_b, jet_pfcand_ppara_ratio_b;
  std::vector<float> jet_pfcand_deta_b, jet_pfcand_dphi_b, jet_pfcand_etarel_b, jet_pfcand_puppiw_b;
  std::vector<float> jet_pfcand_frompv_b, jet_pfcand_id_b, jet_pfcand_track_qual_b, jet_pfcand_track_chi2_b, jet_pfcand_track_pterr_b, jet_pfcand_track_etaerr_b, jet_pfcand_track_phierr_b;
  std::vector<float> jet_pfcand_nhits_b, jet_pfcand_npixhits_b, jet_pfcand_nstriphits_b, jet_pfcand_nlosthits_b, jet_pfcand_npixlayers_b, jet_pfcand_nstriplayers_b, jet_pfcand_track_algo_b;
  std::vector<float> jet_pfcand_tau_signal_b, jet_pfcand_charge_b;
  std::vector<float> jet_pfcand_muon_id_b, jet_pfcand_muon_chi2_b, jet_pfcand_muon_segcomp_b, jet_pfcand_muon_isglobal_b, jet_pfcand_muon_nvalidhit_b, jet_pfcand_muon_nstation_b;
  std::vector<float> jet_pfcand_electron_eOverP_b, jet_pfcand_electron_detaIn_b, jet_pfcand_electron_dphiIn_b, jet_pfcand_electron_r9_b, jet_pfcand_electron_sigIetaIeta_b;
  std::vector<float> jet_pfcand_electron_convProb_b, jet_pfcand_electron_sigIphiIphi_b;
  std::vector<float> jet_pfcand_trackjet_d3d_b, jet_pfcand_trackjet_d3dsig_b;
  std::vector<float> jet_pfcand_trackjet_dist_b, jet_pfcand_trackjet_decayL_b;

  std::vector<float> jet_pfcand_neu_pt_b, jet_pfcand_neu_eta_b, jet_pfcand_neu_phi_b, jet_pfcand_neu_mass_b, jet_pfcand_neu_energy_b, jet_pfcand_neu_pt_log_b, jet_pfcand_neu_energy_log_b, jet_pfcand_neu_calofraction_b;
  std::vector<float> jet_pfcand_neu_hcalfraction_b, jet_pfcand_neu_dxy_b, jet_pfcand_neu_dz_b, jet_pfcand_neu_pperp_ratio_b, jet_pfcand_neu_ppara_ratio_b;
  std::vector<float> jet_pfcand_neu_deta_b, jet_pfcand_neu_dphi_b, jet_pfcand_neu_etarel_b, jet_pfcand_neu_puppiw_b;
  std::vector<float> jet_pfcand_neu_frompv_b, jet_pfcand_neu_id_b;
  std::vector<float> jet_pfcand_neu_tau_signal_b, jet_pfcand_neu_charge_b;
  std::vector<float> jet_pfcand_neu_photon_sigIetaIeta_b, jet_pfcand_neu_photon_r9_b, jet_pfcand_neu_photon_eVeto_b;
  std::vector<float> jet_pfcand_photon_sigIetaIeta_b, jet_pfcand_photon_r9_b, jet_pfcand_photon_eVeto_b;

  if(not splitChargedPF){
    tree_out->Branch("jet_pfcand_pt","std::vector<float>",&jet_pfcand_pt_b);
    tree_out->Branch("jet_pfcand_eta","std::vector<float>",&jet_pfcand_eta_b);
    tree_out->Branch("jet_pfcand_phi","std::vector<float>",&jet_pfcand_phi_b);
    tree_out->Branch("jet_pfcand_mass","std::vector<float>",&jet_pfcand_mass_b);
    tree_out->Branch("jet_pfcand_energy","std::vector<float>",&jet_pfcand_energy_b);
    tree_out->Branch("jet_pfcand_pt_log","std::vector<float>",&jet_pfcand_pt_log_b);
    tree_out->Branch("jet_pfcand_energy_log","std::vector<float>",&jet_pfcand_energy_log_b);
    tree_out->Branch("jet_pfcand_calofraction","std::vector<float>",&jet_pfcand_calofraction_b);
    tree_out->Branch("jet_pfcand_hcalfraction","std::vector<float>",&jet_pfcand_hcalfraction_b);
    tree_out->Branch("jet_pfcand_dxy","std::vector<float>",&jet_pfcand_dxy_b);
    tree_out->Branch("jet_pfcand_dxysig","std::vector<float>",&jet_pfcand_dxysig_b);
    tree_out->Branch("jet_pfcand_dz","std::vector<float>",&jet_pfcand_dz_b);
    tree_out->Branch("jet_pfcand_dzsig","std::vector<float>",&jet_pfcand_dzsig_b);
    tree_out->Branch("jet_pfcand_pperp_ratio","std::vector<float>",&jet_pfcand_pperp_ratio_b);
    tree_out->Branch("jet_pfcand_ppara_ratio","std::vector<float>",&jet_pfcand_ppara_ratio_b);
    tree_out->Branch("jet_pfcand_deta","std::vector<float>",&jet_pfcand_deta_b);
    tree_out->Branch("jet_pfcand_dphi","std::vector<float>",&jet_pfcand_dphi_b);
    tree_out->Branch("jet_pfcand_etarel","std::vector<float>",&jet_pfcand_etarel_b);
    tree_out->Branch("jet_pfcand_puppiw","std::vector<float>",&jet_pfcand_puppiw_b);
    tree_out->Branch("jet_pfcand_frompv","std::vector<float>",&jet_pfcand_frompv_b);
    tree_out->Branch("jet_pfcand_id","std::vector<float>",&jet_pfcand_id_b);
    tree_out->Branch("jet_pfcand_charge","std::vector<float>",&jet_pfcand_charge_b);
    tree_out->Branch("jet_pfcand_track_qual","std::vector<float>",&jet_pfcand_track_qual_b);
    tree_out->Branch("jet_pfcand_track_chi2","std::vector<float>",&jet_pfcand_track_chi2_b);    
    tree_out->Branch("jet_pfcand_track_algo","std::vector<float>",&jet_pfcand_track_algo_b);    
    tree_out->Branch("jet_pfcand_track_pterr","std::vector<float>",&jet_pfcand_track_pterr_b);    
    tree_out->Branch("jet_pfcand_track_etaerr","std::vector<float>",&jet_pfcand_track_etaerr_b);    
    tree_out->Branch("jet_pfcand_track_phierr","std::vector<float>",&jet_pfcand_track_phierr_b);    
    tree_out->Branch("jet_pfcand_nhits","std::vector<float>",&jet_pfcand_nhits_b);    
    tree_out->Branch("jet_pfcand_npixhits","std::vector<float>",&jet_pfcand_npixhits_b);    
    tree_out->Branch("jet_pfcand_nstriphits","std::vector<float>",&jet_pfcand_nstriphits_b);    
    tree_out->Branch("jet_pfcand_nlosthits","std::vector<float>",&jet_pfcand_nlosthits_b);    
    tree_out->Branch("jet_pfcand_npixlayers","std::vector<float>",&jet_pfcand_npixlayers_b);    
    tree_out->Branch("jet_pfcand_nstriplayers","std::vector<float>",&jet_pfcand_nstriplayers_b);    
    tree_out->Branch("jet_pfcand_tau_signal","std::vector<float>",&jet_pfcand_tau_signal_b);
    tree_out->Branch("jet_pfcand_muon_id","std::vector<float>",&jet_pfcand_muon_id_b);
    tree_out->Branch("jet_pfcand_muon_chi2","std::vector<float>",&jet_pfcand_muon_chi2_b);
    tree_out->Branch("jet_pfcand_muon_segcomp","std::vector<float>",&jet_pfcand_muon_segcomp_b);
    tree_out->Branch("jet_pfcand_muon_isglobal","std::vector<float>",&jet_pfcand_muon_isglobal_b);
    tree_out->Branch("jet_pfcand_muon_nvalidhit","std::vector<float>",&jet_pfcand_muon_nvalidhit_b);
    tree_out->Branch("jet_pfcand_muon_nstation","std::vector<float>",&jet_pfcand_muon_nstation_b);
    tree_out->Branch("jet_pfcand_electron_eOverP","std::vector<float>",&jet_pfcand_electron_eOverP_b);
    tree_out->Branch("jet_pfcand_electron_detaIn","std::vector<float>",&jet_pfcand_electron_detaIn_b);
    tree_out->Branch("jet_pfcand_electron_dphiIn","std::vector<float>",&jet_pfcand_electron_dphiIn_b);
    tree_out->Branch("jet_pfcand_electron_r9","std::vector<float>",&jet_pfcand_electron_r9_b);
    tree_out->Branch("jet_pfcand_electron_sigIetaIeta","std::vector<float>",&jet_pfcand_electron_sigIetaIeta_b);
    tree_out->Branch("jet_pfcand_electron_sigIphiIphi","std::vector<float>",&jet_pfcand_electron_sigIphiIphi_b);
    tree_out->Branch("jet_pfcand_electron_convProb","std::vector<float>",&jet_pfcand_electron_convProb_b);
    tree_out->Branch("jet_pfcand_photon_sigIetaIeta","std::vector<float>",&jet_pfcand_photon_sigIetaIeta_b);
    tree_out->Branch("jet_pfcand_photon_r9","std::vector<float>",&jet_pfcand_photon_r9_b);
    tree_out->Branch("jet_pfcand_photon_eVeto","std::vector<float>",&jet_pfcand_photon_eVeto_b);
    tree_out->Branch("jet_pfcand_trackjet_d3d","std::vector<float>",&jet_pfcand_trackjet_d3d_b);
    tree_out->Branch("jet_pfcand_trackjet_d3dsig","std::vector<float>",&jet_pfcand_trackjet_d3dsig_b);
    tree_out->Branch("jet_pfcand_trackjet_dist","std::vector<float>",&jet_pfcand_trackjet_dist_b);
    tree_out->Branch("jet_pfcand_trackjet_decayL","std::vector<float>",&jet_pfcand_trackjet_decayL_b);
  }
  else{
    tree_out->Branch("jet_pfcand_ch_pt","std::vector<float>",&jet_pfcand_pt_b);
    tree_out->Branch("jet_pfcand_ch_eta","std::vector<float>",&jet_pfcand_eta_b);
    tree_out->Branch("jet_pfcand_ch_phi","std::vector<float>",&jet_pfcand_phi_b);
    tree_out->Branch("jet_pfcand_ch_mass","std::vector<float>",&jet_pfcand_mass_b);
    tree_out->Branch("jet_pfcand_ch_energy","std::vector<float>",&jet_pfcand_energy_b);
    tree_out->Branch("jet_pfcand_ch_pt_log","std::vector<float>",&jet_pfcand_pt_log_b);
    tree_out->Branch("jet_pfcand_ch_energy_log","std::vector<float>",&jet_pfcand_energy_log_b);
    tree_out->Branch("jet_pfcand_ch_calofraction","std::vector<float>",&jet_pfcand_calofraction_b);
    tree_out->Branch("jet_pfcand_ch_hcalfraction","std::vector<float>",&jet_pfcand_hcalfraction_b);
    tree_out->Branch("jet_pfcand_ch_dxy","std::vector<float>",&jet_pfcand_dxy_b);
    tree_out->Branch("jet_pfcand_ch_dxysig","std::vector<float>",&jet_pfcand_dxysig_b);
    tree_out->Branch("jet_pfcand_ch_dz","std::vector<float>",&jet_pfcand_dz_b);
    tree_out->Branch("jet_pfcand_ch_dzsig","std::vector<float>",&jet_pfcand_dzsig_b);
    tree_out->Branch("jet_pfcand_ch_pperp_ratio","std::vector<float>",&jet_pfcand_pperp_ratio_b);
    tree_out->Branch("jet_pfcand_ch_ppara_ratio","std::vector<float>",&jet_pfcand_ppara_ratio_b);
    tree_out->Branch("jet_pfcand_ch_deta","std::vector<float>",&jet_pfcand_deta_b);
    tree_out->Branch("jet_pfcand_ch_dphi","std::vector<float>",&jet_pfcand_dphi_b);
    tree_out->Branch("jet_pfcand_ch_etarel","std::vector<float>",&jet_pfcand_etarel_b);
    tree_out->Branch("jet_pfcand_ch_puppiw","std::vector<float>",&jet_pfcand_puppiw_b);
    tree_out->Branch("jet_pfcand_ch_frompv","std::vector<float>",&jet_pfcand_frompv_b);
    tree_out->Branch("jet_pfcand_ch_id","std::vector<float>",&jet_pfcand_id_b);
    tree_out->Branch("jet_pfcand_ch_charge","std::vector<float>",&jet_pfcand_charge_b);
    tree_out->Branch("jet_pfcand_ch_track_qual","std::vector<float>",&jet_pfcand_track_qual_b);
    tree_out->Branch("jet_pfcand_ch_track_chi2","std::vector<float>",&jet_pfcand_track_chi2_b);    
    tree_out->Branch("jet_pfcand_ch_track_algo","std::vector<float>",&jet_pfcand_track_algo_b);    
    tree_out->Branch("jet_pfcand_ch_track_pterr","std::vector<float>",&jet_pfcand_track_pterr_b);    
    tree_out->Branch("jet_pfcand_ch_track_etaerr","std::vector<float>",&jet_pfcand_track_etaerr_b);    
    tree_out->Branch("jet_pfcand_ch_track_phierr","std::vector<float>",&jet_pfcand_track_phierr_b);    
    tree_out->Branch("jet_pfcand_ch_nhits","std::vector<float>",&jet_pfcand_nhits_b);    
    tree_out->Branch("jet_pfcand_ch_npixhits","std::vector<float>",&jet_pfcand_npixhits_b);    
    tree_out->Branch("jet_pfcand_ch_nstriphits","std::vector<float>",&jet_pfcand_nstriphits_b);    
    tree_out->Branch("jet_pfcand_ch_nlosthits","std::vector<float>",&jet_pfcand_nlosthits_b);    
    tree_out->Branch("jet_pfcand_ch_npixlayers","std::vector<float>",&jet_pfcand_npixlayers_b);    
    tree_out->Branch("jet_pfcand_ch_nstriplayers","std::vector<float>",&jet_pfcand_nstriplayers_b);    
    tree_out->Branch("jet_pfcand_ch_tau_signal","std::vector<float>",&jet_pfcand_tau_signal_b);
    tree_out->Branch("jet_pfcand_ch_muon_id","std::vector<float>",&jet_pfcand_muon_id_b);
    tree_out->Branch("jet_pfcand_ch_muon_chi2","std::vector<float>",&jet_pfcand_muon_chi2_b);
    tree_out->Branch("jet_pfcand_ch_muon_segcomp","std::vector<float>",&jet_pfcand_muon_segcomp_b);
    tree_out->Branch("jet_pfcand_ch_muon_isglobal","std::vector<float>",&jet_pfcand_muon_isglobal_b);
    tree_out->Branch("jet_pfcand_ch_muon_nvalidhit","std::vector<float>",&jet_pfcand_muon_nvalidhit_b);
    tree_out->Branch("jet_pfcand_ch_muon_nstation","std::vector<float>",&jet_pfcand_muon_nstation_b);
    tree_out->Branch("jet_pfcand_ch_electron_eOverP","std::vector<float>",&jet_pfcand_electron_eOverP_b);
    tree_out->Branch("jet_pfcand_ch_electron_detaIn","std::vector<float>",&jet_pfcand_electron_detaIn_b);
    tree_out->Branch("jet_pfcand_ch_electron_dphiIn","std::vector<float>",&jet_pfcand_electron_dphiIn_b);
    tree_out->Branch("jet_pfcand_ch_electron_r9","std::vector<float>",&jet_pfcand_electron_r9_b);
    tree_out->Branch("jet_pfcand_ch_electron_sigIetaIeta","std::vector<float>",&jet_pfcand_electron_sigIetaIeta_b);
    tree_out->Branch("jet_pfcand_ch_electron_sigIphiIphi","std::vector<float>",&jet_pfcand_electron_sigIphiIphi_b);
    tree_out->Branch("jet_pfcand_ch_electron_convProb","std::vector<float>",&jet_pfcand_electron_convProb_b);
    tree_out->Branch("jet_pfcand_ch_trackjet_d3d","std::vector<float>",&jet_pfcand_trackjet_d3d_b);
    tree_out->Branch("jet_pfcand_ch_trackjet_d3dsig","std::vector<float>",&jet_pfcand_trackjet_d3dsig_b);
    tree_out->Branch("jet_pfcand_ch_trackjet_dist","std::vector<float>",&jet_pfcand_trackjet_dist_b);
    tree_out->Branch("jet_pfcand_ch_trackjet_decayL","std::vector<float>",&jet_pfcand_trackjet_decayL_b);
    
    tree_out->Branch("jet_pfcand_neu_pt","std::vector<float>",&jet_pfcand_neu_pt_b);
    tree_out->Branch("jet_pfcand_neu_eta","std::vector<float>",&jet_pfcand_neu_eta_b);
    tree_out->Branch("jet_pfcand_neu_phi","std::vector<float>",&jet_pfcand_neu_phi_b);
    tree_out->Branch("jet_pfcand_neu_mass","std::vector<float>",&jet_pfcand_neu_mass_b);
    tree_out->Branch("jet_pfcand_neu_energy","std::vector<float>",&jet_pfcand_neu_energy_b);
    tree_out->Branch("jet_pfcand_neu_pt_log","std::vector<float>",&jet_pfcand_neu_pt_log_b);
    tree_out->Branch("jet_pfcand_neu_energy_log","std::vector<float>",&jet_pfcand_neu_energy_log_b);
    tree_out->Branch("jet_pfcand_neu_calofraction","std::vector<float>",&jet_pfcand_neu_calofraction_b);
    tree_out->Branch("jet_pfcand_neu_hcalfraction","std::vector<float>",&jet_pfcand_neu_hcalfraction_b);
    tree_out->Branch("jet_pfcand_neu_dxy","std::vector<float>",&jet_pfcand_neu_dxy_b);
    tree_out->Branch("jet_pfcand_neu_dz","std::vector<float>",&jet_pfcand_neu_dz_b);
    tree_out->Branch("jet_pfcand_neu_pperp_ratio","std::vector<float>",&jet_pfcand_neu_pperp_ratio_b);
    tree_out->Branch("jet_pfcand_neu_ppara_ratio","std::vector<float>",&jet_pfcand_neu_ppara_ratio_b);
    tree_out->Branch("jet_pfcand_neu_deta","std::vector<float>",&jet_pfcand_neu_deta_b);
    tree_out->Branch("jet_pfcand_neu_dphi","std::vector<float>",&jet_pfcand_neu_dphi_b);
    tree_out->Branch("jet_pfcand_neu_etarel","std::vector<float>",&jet_pfcand_neu_etarel_b);
    tree_out->Branch("jet_pfcand_neu_puppiw","std::vector<float>",&jet_pfcand_neu_puppiw_b);
    tree_out->Branch("jet_pfcand_neu_frompv","std::vector<float>",&jet_pfcand_neu_frompv_b);
    tree_out->Branch("jet_pfcand_neu_id","std::vector<float>",&jet_pfcand_neu_id_b);
    tree_out->Branch("jet_pfcand_neu_charge","std::vector<float>",&jet_pfcand_neu_charge_b);
    tree_out->Branch("jet_pfcand_neu_tau_signal","std::vector<float>",&jet_pfcand_neu_tau_signal_b);
    tree_out->Branch("jet_pfcand_neu_photon_sigIetaIeta","std::vector<float>",&jet_pfcand_neu_photon_sigIetaIeta_b);
    tree_out->Branch("jet_pfcand_neu_photon_r9","std::vector<float>",&jet_pfcand_neu_photon_r9_b);
    tree_out->Branch("jet_pfcand_neu_photon_eVeto","std::vector<float>",&jet_pfcand_neu_photon_eVeto_b);
  }

  std::vector<float> jet_sv_pt_b, jet_sv_pt_log_b, jet_sv_eta_b, jet_sv_phi_b, jet_sv_mass_b, jet_sv_energy_b, jet_sv_energy_log_b;
  std::vector<float> jet_sv_deta_b, jet_sv_dphi_b, jet_sv_chi2_b, jet_sv_dxy_b, jet_sv_dxysig_b;
  std::vector<float> jet_sv_d3d_b, jet_sv_d3dsig_b;
  std::vector<float> jet_sv_ntrack_b;
  
  tree_out->Branch("jet_sv_pt","std::vector<float>",&jet_sv_pt_b);
  tree_out->Branch("jet_sv_pt_log","std::vector<float>",&jet_sv_pt_log_b);
  tree_out->Branch("jet_sv_eta","std::vector<float>",&jet_sv_eta_b);
  tree_out->Branch("jet_sv_phi","std::vector<float>",&jet_sv_phi_b);
  tree_out->Branch("jet_sv_mass","std::vector<float>",&jet_sv_mass_b);
  tree_out->Branch("jet_sv_energy","std::vector<float>",&jet_sv_energy_b);
  tree_out->Branch("jet_sv_energy_log","std::vector<float>",&jet_sv_energy_log_b);
  tree_out->Branch("jet_sv_deta","std::vector<float>",&jet_sv_deta_b);
  tree_out->Branch("jet_sv_dphi","std::vector<float>",&jet_sv_dphi_b);
  tree_out->Branch("jet_sv_chi2","std::vector<float>",&jet_sv_chi2_b);
  tree_out->Branch("jet_sv_dxy","std::vector<float>",&jet_sv_dxy_b);
  tree_out->Branch("jet_sv_dxysig","std::vector<float>",&jet_sv_dxysig_b);
  tree_out->Branch("jet_sv_d3d","std::vector<float>",&jet_sv_d3d_b);
  tree_out->Branch("jet_sv_d3dsig","std::vector<float>",&jet_sv_d3dsig_b);
  tree_out->Branch("jet_sv_ntrack","std::vector<float>",&jet_sv_ntrack_b);      

  std::vector<float> jet_losttrack_pt_b, jet_losttrack_eta_b, jet_losttrack_phi_b, jet_losttrack_mass_b, jet_losttrack_energy_b, jet_losttrack_pt_log_b, jet_losttrack_energy_log_b;
  std::vector<float> jet_losttrack_dxy_b, jet_losttrack_dz_b, jet_losttrack_dxysig_b, jet_losttrack_dzsig_b;
  std::vector<float> jet_losttrack_deta_b, jet_losttrack_dphi_b, jet_losttrack_etarel_b;
  std::vector<float> jet_losttrack_charge_b, jet_losttrack_frompv_b, jet_losttrack_id_b, jet_losttrack_track_qual_b, jet_losttrack_track_chi2_b;
  std::vector<float> jet_losttrack_track_pterr_b, jet_losttrack_track_etaerr_b, jet_losttrack_track_phierr_b;
  std::vector<float> jet_losttrack_nhits_b, jet_losttrack_npixhits_b, jet_losttrack_nstriphits_b, jet_losttrack_nlosthits_b;
  std::vector<float> jet_losttrack_npixlayers_b, jet_losttrack_nstriplayers_b, jet_losttrack_track_algo_b;
  std::vector<float> jet_losttrack_trackjet_d3d_b, jet_losttrack_trackjet_d3dsig_b;
  std::vector<float> jet_losttrack_trackjet_dist_b, jet_losttrack_trackjet_decayL_b;
        
  tree_out->Branch("jet_losttrack_pt","std::vector<float>",&jet_losttrack_pt_b);
  tree_out->Branch("jet_losttrack_eta","std::vector<float>",&jet_losttrack_eta_b);
  tree_out->Branch("jet_losttrack_phi","std::vector<float>",&jet_losttrack_phi_b);
  tree_out->Branch("jet_losttrack_mass","std::vector<float>",&jet_losttrack_mass_b);
  tree_out->Branch("jet_losttrack_energy","std::vector<float>",&jet_losttrack_energy_b);
  tree_out->Branch("jet_losttrack_pt_log","std::vector<float>",&jet_losttrack_pt_log_b);
  tree_out->Branch("jet_losttrack_energy_log","std::vector<float>",&jet_losttrack_energy_log_b);
  tree_out->Branch("jet_losttrack_dxy","std::vector<float>",&jet_losttrack_dxy_b);
  tree_out->Branch("jet_losttrack_dxysig","std::vector<float>",&jet_losttrack_dxysig_b);
  tree_out->Branch("jet_losttrack_dz","std::vector<float>",&jet_losttrack_dz_b);
  tree_out->Branch("jet_losttrack_dzsig","std::vector<float>",&jet_losttrack_dzsig_b);
  tree_out->Branch("jet_losttrack_deta","std::vector<float>",&jet_losttrack_deta_b);
  tree_out->Branch("jet_losttrack_dphi","std::vector<float>",&jet_losttrack_dphi_b);
  tree_out->Branch("jet_losttrack_etarel","std::vector<float>",&jet_losttrack_etarel_b);
  tree_out->Branch("jet_losttrack_frompv","std::vector<float>",&jet_losttrack_frompv_b);
  tree_out->Branch("jet_losttrack_id","std::vector<float>",&jet_losttrack_id_b);
  tree_out->Branch("jet_losttrack_charge","std::vector<float>",&jet_losttrack_charge_b);
  tree_out->Branch("jet_losttrack_track_qual","std::vector<float>",&jet_losttrack_track_qual_b);
  tree_out->Branch("jet_losttrack_track_chi2","std::vector<float>",&jet_losttrack_track_chi2_b);    
  tree_out->Branch("jet_losttrack_track_algo","std::vector<float>",&jet_losttrack_track_algo_b);    
  tree_out->Branch("jet_losttrack_track_pterr","std::vector<float>",&jet_losttrack_track_pterr_b);    
  tree_out->Branch("jet_losttrack_track_etaerr","std::vector<float>",&jet_losttrack_track_etaerr_b);    
  tree_out->Branch("jet_losttrack_track_phierr","std::vector<float>",&jet_losttrack_track_phierr_b);    
  tree_out->Branch("jet_losttrack_nhits","std::vector<float>",&jet_losttrack_nhits_b);    
  tree_out->Branch("jet_losttrack_npixhits","std::vector<float>",&jet_losttrack_npixhits_b);    
  tree_out->Branch("jet_losttrack_nstriphits","std::vector<float>",&jet_losttrack_nstriphits_b);    
  tree_out->Branch("jet_losttrack_nlosthits","std::vector<float>",&jet_losttrack_nlosthits_b);    
  tree_out->Branch("jet_losttrack_npixlayers","std::vector<float>",&jet_losttrack_npixlayers_b);    
  tree_out->Branch("jet_losttrack_nstriplayers","std::vector<float>",&jet_losttrack_nstriplayers_b);    
  tree_out->Branch("jet_losttrack_trackjet_d3d","std::vector<float>",&jet_losttrack_trackjet_d3d_b);
  tree_out->Branch("jet_losttrack_trackjet_d3dsig","std::vector<float>",&jet_losttrack_trackjet_d3dsig_b);
  tree_out->Branch("jet_losttrack_trackjet_dist","std::vector<float>",&jet_losttrack_trackjet_dist_b);
  tree_out->Branch("jet_losttrack_trackjet_decayL","std::vector<float>",&jet_losttrack_trackjet_decayL_b);

  int iteration = -1;
  while(reader.Next()){
    iteration++;
    //if(iteration==2) break; // FIX for DEBUG
    // Gen leptons from resonance decay 
    std::vector<TLorentzVector> genLepFromResonance4V;
    std::vector<TLorentzVector> genMuonsFromResonance4V;
    std::vector<TLorentzVector> genElectronsFromResonance4V;
    std::vector<int> genMuonsFromResonanceCharge;
    std::vector<int> genElectronsFromResonanceCharge;
    std::vector<TLorentzVector> tau_gen_visible;
    std::vector<TLorentzVector> tau_gen;
    std::vector<int> tau_gen_charge;
    std::vector<unsigned int> tau_gen_nch;
    std::vector<unsigned int> tau_gen_np0;
    std::vector<unsigned int> tau_gen_nnh;
    std::vector<Resonance> resonances;

    if(not (sample == sample_type::data)){
      for(size_t igen = 0; igen < (*gen_particle_pt)->size(); igen++){
	// select resonances like Higgs, W, Z, taus
	if(abs((*gen_particle_id)->at(igen)) == 25 or
	   abs((*gen_particle_id)->at(igen)) == 23 or
	   abs((*gen_particle_id)->at(igen)) == 24 or
	   abs((*gen_particle_id)->at(igen)) == 15){	
	  for(size_t idau = 0; idau < (*gen_particle_daughters_id)->size(); idau++){
	    // select electrons or muons from the resonance / tau decay
	    if((*gen_particle_daughters_igen)->at(idau) == igen and
	       (abs((*gen_particle_daughters_id)->at(idau)) == 11 or
		abs((*gen_particle_daughters_id)->at(idau)) == 13)){
	      TLorentzVector gen4V;
	      gen4V.SetPtEtaPhiM((*gen_particle_daughters_pt)->at(idau),(*gen_particle_daughters_eta)->at(idau),(*gen_particle_daughters_phi)->at(idau),(*gen_particle_daughters_mass)->at(idau));
	      if(std::find(genLepFromResonance4V.begin(),genLepFromResonance4V.end(),gen4V) == genLepFromResonance4V.end())
		genLepFromResonance4V.push_back(gen4V);
	      if(abs((*gen_particle_daughters_id)->at(idau)) == 13 and 
		 std::find(genMuonsFromResonance4V.begin(),genMuonsFromResonance4V.end(),gen4V) == genMuonsFromResonance4V.end()){
		genMuonsFromResonance4V.push_back(gen4V);
		genMuonsFromResonanceCharge.push_back((*gen_particle_daughters_charge)->at(idau));
	      }
	      if(abs((*gen_particle_daughters_id)->at(idau)) == 11 and 
		 std::find(genElectronsFromResonance4V.begin(),genElectronsFromResonance4V.end(),gen4V) == genElectronsFromResonance4V.end()){
		genElectronsFromResonance4V.push_back(gen4V);		
		genElectronsFromResonanceCharge.push_back((*gen_particle_daughters_charge)->at(idau));
	      }
	    }
	  }
	}
      }   

      // FIXME We want to save tau_h + tau_X, so I need both hadronic and non hadronic taus
      for(size_t igen = 0; igen < (*gen_particle_pt)->size(); igen++){
	if(abs((*gen_particle_id)->at(igen)) == 15){ // hadronic or leptonic tau
	  TLorentzVector tau_gen_tmp;
	  unsigned int tau_gen_nch_tmp = 0;
	  unsigned int tau_gen_np0_tmp = 0;
	  unsigned int tau_gen_nnh_tmp = 0;
	  for(size_t idau = 0; idau < (*gen_particle_daughters_pt)->size(); idau++){
	    if((*gen_particle_daughters_igen)->at(idau) == igen and
	       abs((*gen_particle_daughters_id)->at(idau)) != 11 and // no mu
	       abs((*gen_particle_daughters_id)->at(idau)) != 13 and // no el
	       abs((*gen_particle_daughters_id)->at(idau)) != 12 and // no neutrinos
	       abs((*gen_particle_daughters_id)->at(idau)) != 14 and
	       abs((*gen_particle_daughters_id)->at(idau)) != 16){ // we save only hadronic
	      TLorentzVector tmp4V; 
	      tmp4V.SetPtEtaPhiM((*gen_particle_daughters_pt)->at(idau),(*gen_particle_daughters_eta)->at(idau),(*gen_particle_daughters_phi)->at(idau),(*gen_particle_daughters_mass)->at(idau));
	      tau_gen_tmp += tmp4V;
	      if ((*gen_particle_daughters_charge)->at(idau) != 0 and (*gen_particle_daughters_status)->at(idau) == 1) tau_gen_nch_tmp ++; // charged particles
	      else if((*gen_particle_daughters_charge)->at(idau) == 0 and (*gen_particle_daughters_id)->at(idau) == 111) tau_gen_np0_tmp++;
	      else if((*gen_particle_daughters_charge)->at(idau) == 0 and (*gen_particle_daughters_id)->at(idau) != 111) tau_gen_nnh_tmp++;
	    }
	  }	
	  if(tau_gen_tmp.Pt() > 0){ // good hadronic tau
	    tau_gen_visible.push_back(tau_gen_tmp);
	    tau_gen_tmp.SetPtEtaPhiM((*gen_particle_pt)->at(igen),(*gen_particle_eta)->at(igen),(*gen_particle_phi)->at(igen),(*gen_particle_mass)->at(igen));
	    tau_gen_charge.push_back(((*gen_particle_id)->at(igen) > 0) ? -1 : 1);
	    tau_gen.push_back(tau_gen_tmp);
	    tau_gen_nch.push_back(tau_gen_nch_tmp);
	    tau_gen_np0.push_back(tau_gen_np0_tmp);
	    tau_gen_nnh.push_back(tau_gen_nnh_tmp);
	  }
	}
      }

      
      // https://gitlab.cern.ch/friti/particlenetstudiesrun2/-/blob/softditau/TrainingNtupleMakerAK8/bin/makeSkimmedNtuplesForTrainingAK8.cpp?ref_type=heads#L772
      // Build Bs-> di-tau resonance
      
      std::vector<unsigned int> skipTaus;
      for(size_t igen = 0; igen < (*gen_particle_pt)->size(); igen++){
	if(abs((*gen_particle_id)->at(igen)) == 531){ // gen particle is a bs
	  TLorentzVector resonance4V; // save resonance 4V
	  resonance4V.SetPtEtaPhiM((*gen_particle_pt)->at(igen),(*gen_particle_eta)->at(igen),(*gen_particle_phi)->at(igen),(*gen_particle_mass)->at(igen));
	  // find the daughters of the Bs and check they are taus
	  std::vector<unsigned int> daughters;
	  for(size_t idau = 0; idau < (*gen_particle_daughters_id)->size(); idau++){
	    if((*gen_particle_daughters_igen)->at(idau) == igen){ //check bs daughters
	      if(abs((*gen_particle_daughters_id)->at(idau)) == 15){ // it's a tau
		TLorentzVector daughter4V; // define the 4v vector to find the right tau in the already saved collection
		daughter4V.SetPtEtaPhiM((*gen_particle_daughters_pt)->at(idau),(*gen_particle_daughters_eta)->at(idau),(*gen_particle_daughters_phi)->at(idau),(*gen_particle_daughters_mass)->at(idau));
		int tauIndex = -1;
		daughters.push_back(idau);
		for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){ // find which hadronic tau 
		  if(tau_gen.at(itau).DeltaR(daughter4V) == 0 and std::find(skipTaus.begin(),skipTaus.end(),itau) == skipTaus.end()){
		    tauIndex = itau;
		    // add here something to save! I dont know....
		  }       	
		}
		if (tauIndex != -1){ // there was a match with an hadronic tau
		  skipTaus.push_back(tauIndex);
		}
	      }
	    }
	  }
	  // tau_h tau_X, therefore I require at least 1 tau_h, at max 2 taus
	  if(skipTaus.size()<1 or daughters.size()!=2){
	    continue;
	  }
	  // if bs-> tau tau then I can save it (maybe it's useful to also save the taus?)
	  resonances.push_back(Resonance()); // Class
	  resonances.back().p4 = resonance4V;
	}
      }
    }
    // apply per-event selection expressed by the domain type
    std::vector<unsigned int> jets_no_overlap_leptons_index;
    
    // Domain specific event selections
    if(domain != domain_type::none){

      TLorentzVector lep1_4v,lep2_4v;
      lep1_4v.SetPtEtaPhiM((*leppair_pt1)->front(),(*leppair_eta1)->front(),(*leppair_phi1)->front(),(*leppair_mass1)->front());
      lep2_4v.SetPtEtaPhiM((*leppair_pt2)->front(),(*leppair_eta2)->front(),(*leppair_phi2)->front(),(*leppair_mass2)->front());      
      if(lep1_4v.Pt() <= 0 or lep2_4v.Pt() <= 0){
	nEventsRejectedDomainKinematics++; 
	continue;
      }

      // matching leptons jets
      int lep1_index = -1;
      int lep2_index = -1;
      float minDR1 = 1000;
      float minDR2 = 1000;      
      for(size_t ijet = 0; ijet < jet_pt->size(); ijet++){
	TLorentzVector jet_4v; 
	jet_4v.SetPtEtaPhiM(jet_pt->at(ijet),jet_eta->at(ijet),jet_phi->at(ijet),jet_mass->at(ijet));
	float dR1 = jet_4v.DeltaR(lep1_4v);
	float dR2 = jet_4v.DeltaR(lep2_4v);
	if(dR1 <= dR2 and dR1 <= minDR1 and dR1 <= dRCone){
	  minDR1 = dR1;
	  lep1_index = ijet;
	}
	else if(dR2 < dR1 and dR2 <= minDR2 and dR2 <= dRCone){
	  minDR2 = dR2;
	  lep2_index = ijet;
	}
      }

      // first selections in the various regions
      if(domain == domain_type::dimuon or domain == domain_type::dielectron){ // dilepton
	if(**flags != 0){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(*met > maxMET) {	  
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(lep1_4v.Pt() < minLep1PtDilepton){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(lep2_4v.Pt() < minLep2PtDilepton){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
      }
      else if(domain == domain_type::emu){ // emu
	if(**flags != 0){
	  nEventsRejectedDomainKinematics++;
	  continue;
	}
	if(lep1_4v.Pt() < minLep1PtEMu){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(lep2_4v.Pt() < minLep2PtEMu){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
      }
      if(domain == domain_type::dijet){ // dijet
	if(**flags != 0){
	  nEventsRejectedDomainKinematics++;
	  continue;
	}
	if(lep1_4v.Pt() < minLep1PtDijet){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(lep2_4v.Pt() < minLep2PtDijet){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
      }
      if(domain == domain_type::ttcharm){ // ttcharm
	if(**flags != 0){
	  nEventsRejectedDomainKinematics++;
	  continue;
	}
	if(lep1_4v.Pt() < minLep1PtTTcharm){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(lep2_4v.Pt() < minLep2PtTTcharm){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
      }
      else if(domain == domain_type::mutau or domain == domain_type::etau){ // mutau

	if(**flags != 0){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}	
	if(lep1_4v.Pt() < minLep1PtMuTau){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(lep2_4v.Pt() < minLep2PtMuTau){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(sqrt(2*lep1_4v.Pt()*(*met)*(1-cos(deltaPhi(lep1_4v.Phi(),*met_phi)))) > minTransverseMass) {
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}	
	  
	int tau_index = -1;
	float minDR = 1000;      
	for(size_t itau = 0; itau < tau_pt->size(); itau++){
	  TLorentzVector tau_4v; tau_4v.SetPtEtaPhiM(tau_pt->at(itau),tau_eta->at(itau),tau_phi->at(itau),tau_mass->at(itau));
	  float dR = lep2_4v.DeltaR(tau_4v);
	  if(dR <= minDR and dR <= dRCone){
	    minDR = dR;
	    tau_index = itau;
	  }
	}

	if(matchHPSTauCandidate and tau_index == -1){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if(tau_index != -1)
	  lep2_4v.SetPtEtaPhiM(tau_pt->at(tau_index),tau_eta->at(tau_index),tau_phi->at(tau_index),tau_mass->at(tau_index));	

	if(lep2_4v.Pt() < minTauPt or fabs(lep2_4v.Eta()) > maxTauEta){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}
	if((lep1_4v+lep2_4v).M() < minMassVisible or (lep1_4v+lep2_4v).M() > maxMassVisible){
	  nEventsRejectedDomainKinematics++; 
	  continue;
	}

	// compute tau-charge via parT or PNET
	float tau_charge_score = 0.;
	if(selectOnParTScore)
	  tau_charge_score = (lep2_index==-1) ? 0 : (jet_parTlast_probtaup1h0p->at(lep2_index)+jet_parTlast_probtaup1h1p->at(lep2_index)+jet_parTlast_probtaup1h2p->at(lep2_index)+jet_parTlast_probtaup3h0p->at(lep2_index)+jet_parTlast_probtaup3h1p->at(lep2_index))/(jet_parTlast_probtaup1h0p->at(lep2_index)+jet_parTlast_probtaup1h1p->at(lep2_index)+jet_parTlast_probtaup1h2p->at(lep2_index)+jet_parTlast_probtaup3h0p->at(lep2_index)+jet_parTlast_probtaup3h1p->at(lep2_index)+jet_parTlast_probtaum1h0p->at(lep2_index)+jet_parTlast_probtaum1h1p->at(lep2_index)+jet_parTlast_probtaum1h2p->at(lep2_index)+jet_parTlast_probtaum3h0p->at(lep2_index)+jet_parTlast_probtaum3h1p->at(lep2_index));	
	else
	  tau_charge_score = (lep2_index==-1) ? 0 : (jet_pnetlast_probtaup1h0p->at(lep2_index)+jet_pnetlast_probtaup1h1p->at(lep2_index)+jet_pnetlast_probtaup1h2p->at(lep2_index)+jet_pnetlast_probtaup3h0p->at(lep2_index)+jet_pnetlast_probtaup3h1p->at(lep2_index))/(jet_pnetlast_probtaup1h0p->at(lep2_index)+jet_pnetlast_probtaup1h1p->at(lep2_index)+jet_pnetlast_probtaup1h2p->at(lep2_index)+jet_pnetlast_probtaup3h0p->at(lep2_index)+jet_pnetlast_probtaup3h1p->at(lep2_index)+jet_pnetlast_probtaum1h0p->at(lep2_index)+jet_pnetlast_probtaum1h1p->at(lep2_index)+jet_pnetlast_probtaum1h2p->at(lep2_index)+jet_pnetlast_probtaum3h0p->at(lep2_index)+jet_pnetlast_probtaum3h1p->at(lep2_index));	
        float tau_charge = (tau_charge_score-0.5)/fabs(tau_charge_score-0.5);	
	
	if(domain == domain_type::mutau){
	  int mu_index = -1;
	  float minDR = 1000;
	  for(size_t imu = 0; imu < muon_pt->size(); imu++){
	    TLorentzVector muon_4v; muon_4v.SetPtEtaPhiM(muon_pt->at(imu),muon_eta->at(imu),muon_phi->at(imu),muon_mass->at(imu));
	    float dR = lep1_4v.DeltaR(muon_4v);
	    if(dR <= minDR and dR <= dRCone){
	      minDR = dR;
	      mu_index = imu;
	    }
	  }
	  if(mu_index != -1 and muon_charge->at(mu_index)*tau_charge > 0) {
	    nEventsRejectedDomainKinematics++; 
	    continue;
	  }
	  if(mu_index == -1){
	    nEventsRejectedDomainKinematics++; 
	    continue;
	  }
	}
	else if(domain == domain_type::etau){
	  int ele_index = -1;
	  float minDR = 1000;
	  for(size_t iele = 0; iele < electron_pt_corr->size(); iele++){
	    TLorentzVector electron_4v; electron_4v.SetPtEtaPhiM(electron_pt->at(iele),electron_eta->at(iele),electron_phi->at(iele),electron_mass->at(iele));
	    float dR = lep1_4v.DeltaR(electron_4v);
	    if(dR <= minDR and dR <= dRCone){
	      minDR = dR;
	      ele_index = iele;
	    }
	  }
	  if(ele_index != -1 and electron_charge->at(ele_index)*tau_charge > 0) {
	    nEventsRejectedDomainKinematics++; 
	    continue;
	  }
	  if(ele_index == -1){
	    nEventsRejectedDomainKinematics++; 
	    continue;
	  }
	}
      }

      // save jets not in overlap with leptons
      for(size_t ijet = 0; ijet < jet_pt_raw->size(); ijet++){
	// basic kinematic selections
	if(jet_pt->at(ijet) < jetPtMin) continue;
	if(fabs(jet_eta->at(ijet)) > jetEtaMax) continue;
	if(fabs(jet_eta->at(ijet)) < jetEtaMin) continue;
	// based on index
	if(int(ijet) == lep1_index){
	  if(domain != domain_type::dijet) continue;
	  if(jet_id->at(ijet) == 0) continue;
	  if(jet_puid->at(ijet) == 0) continue;
	  jets_no_overlap_leptons_index.push_back(ijet);
	}
	else if(int(ijet) == lep2_index){
	  if(domain == domain_type::dimuon or domain == domain_type::dielectron or domain == domain_type::emu) continue; // not a jet
	  else if(domain == domain_type::dijet or domain == domain_type::ttcharm){ // regular jet
	    if(jet_id->at(ijet) == 0) continue;
	    if(jet_puid->at(ijet) == 0) continue;
	    jets_no_overlap_leptons_index.push_back(ijet);	    
	  }
	  else if(domain == domain_type::mutau or domain == domain_type::etau){ // tauh candidate
	    jets_no_overlap_leptons_index.push_back(ijet);
	  }
	}
	else{
	  if(jet_id->at(ijet) == 0) continue;
	  if(jet_puid->at(ijet) == 0) continue;
	  jets_no_overlap_leptons_index.push_back(ijet);
	}	
      }

      // impose requirements on the number of jets that needs to be considered in the analysis
      if(jets_no_overlap_leptons_index.empty()){
	nEventsRejectedDomainKinematics++; 
	continue;
      }

      // jet occupancy selections
      if((domain == domain_type::dimuon or domain == domain_type::dielectron) and (jets_no_overlap_leptons_index.size() > maxNjetDilepton or jets_no_overlap_leptons_index.size() < minNjetDilepton)){
	nEventsRejectedDomainKinematics++; 
	continue;
      }
      else if(domain == domain_type::emu and (jets_no_overlap_leptons_index.size() > maxNjetEMu or jets_no_overlap_leptons_index.size() < minNjetEMu)){ 
	nEventsRejectedDomainKinematics++; 
	continue;
      }
      else if((domain == domain_type::mutau or domain == domain_type::etau) and (jets_no_overlap_leptons_index.size() > maxNjetMuTau or jets_no_overlap_leptons_index.size() < minNjetMuTau)){
	nEventsRejectedDomainKinematics++; 
	continue;
      }
      else if(domain == domain_type::dijet and (jets_no_overlap_leptons_index.size() > maxNjetDijet or jets_no_overlap_leptons_index.size() < minNjetDijet)){
	nEventsRejectedDomainKinematics++; 
	continue;
      }
      else if(domain == domain_type::ttcharm and (jets_no_overlap_leptons_index.size() > maxNjetTTcharm or jets_no_overlap_leptons_index.size() < minNjetTTcharm)){
	nEventsRejectedDomainKinematics++; 
	continue;
      }

      // Sorting of jets
      if(domain == domain_type::emu){ // sort based on b vs jet
	std::vector<std::pair<int,float> > jet_b_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
	  jet_b_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probb->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probb->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg->at(jets_no_overlap_leptons_index.at(ijet)))));
	std::sort(jet_b_score.begin(),
                  jet_b_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_b_score.at(ijet).first;
      }
      else if(domain == domain_type::ttcharm){ // sort based on c vs jet
	std::vector<std::pair<int,float> > jet_c_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
	  jet_c_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probc->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probc->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg->at(jets_no_overlap_leptons_index.at(ijet)))));
	std::sort(jet_c_score.begin(),
                  jet_c_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_c_score.at(ijet).first;
      }
      else if(domain == domain_type::dimuon or domain == domain_type::dielectron){
	std::vector<std::pair<int,float> > jet_uds_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
	  jet_uds_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg->at(jets_no_overlap_leptons_index.at(ijet)))));
	std::sort(jet_uds_score.begin(),
                  jet_uds_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_uds_score.at(ijet).first;
      }
      else if(domain == domain_type::mutau or domain == domain_type::etau){
	std::vector<std::pair<int,float> > jet_tau_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
	  jet_tau_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? (jet_parTlast_probtaup1h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup1h1p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup1h2p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup3h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup3h1p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum1h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum1h1p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum1h2p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum3h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum3h1p->at(jets_no_overlap_leptons_index.at(ijet)))/(1-jet_parTlast_probmu->at(jets_no_overlap_leptons_index.at(ijet))-jet_parTlast_probele->at(jets_no_overlap_leptons_index.at(ijet))) : (jet_pnetlast_probtaup1h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup1h1p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup1h2p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup3h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup3h1p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum1h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum1h1p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum1h2p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum3h0p->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum3h1p->at(jets_no_overlap_leptons_index.at(ijet)))/(1-jet_pnetlast_probmu->at(jets_no_overlap_leptons_index.at(ijet))-jet_pnetlast_probele->at(jets_no_overlap_leptons_index.at(ijet)))));
	std::sort(jet_tau_score.begin(),
                  jet_tau_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_tau_score.at(ijet).first;
      }
      else if(domain == domain_type::dijet){
	std::vector<std::pair<int,float> > jet_g_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
	  jet_g_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probg->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probg->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg->at(jets_no_overlap_leptons_index.at(ijet)))));
	std::sort(jet_g_score.begin(),
                  jet_g_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_g_score.at(ijet).first;
      }    
      
      // dimuon --> take maxNjetDileptonToStore leading UDS jets
      if((domain == domain_type::dimuon or domain == domain_type::dielectron) and jets_no_overlap_leptons_index.size() > maxNjetDileptonToStore)      
	jets_no_overlap_leptons_index.resize(maxNjetDileptonToStore);
      // emu --> take maxNjetEMuToStore leading B jets
      else if(domain == domain_type::emu and jets_no_overlap_leptons_index.size() > maxNjetEMuToStore)
	jets_no_overlap_leptons_index.resize(maxNjetEMuToStore);
      // mutau --> take maxNjetMuTauToStore leading tau(h) jets
      else if((domain == domain_type::mutau or domain == domain_type::etau) and jets_no_overlap_leptons_index.size() > maxNjetMuTauToStore)      
	jets_no_overlap_leptons_index.resize(maxNjetMuTauToStore);      
      // dijet --> take maxNjetDijetToStore leading G jets
      else if(domain == domain_type::dijet and jets_no_overlap_leptons_index.size() > maxNjetDijetToStore)      
	jets_no_overlap_leptons_index.resize(maxNjetDijetToStore);      
      // ttcharm --> take maxNjetTTcharmToStore leading c jets
      else if(domain == domain_type::ttcharm and jets_no_overlap_leptons_index.size() > maxNjetTTcharmToStore)      
	jets_no_overlap_leptons_index.resize(maxNjetTTcharmToStore);      
    }

    // loop on jets
    for(size_t ijet = 0; ijet < jet_pt->size(); ijet++){      

      // skip jet in overlap with leptons only for domain
      nJetsTotal++;      
      
      // Jet 4V
      TLorentzVector jet4V;
      jet4V.SetPtEtaPhiM(jet_pt->at(ijet),jet_eta->at(ijet),jet_phi->at(ijet),jet_mass->at(ijet));
      
      // Add the selection and limit in number of jets in case of domain
      if(domain == domain_type::none){ 
	if(jet_pt_raw->at(ijet) < jetPtMin or
	   fabs(jet_eta->at(ijet)) > jetEtaMax or 
	   fabs(jet_eta->at(ijet)) < jetEtaMin or
	   (not (sample == sample_type::data) and saveOnlyGenMatchedJets and (*jet_genmatch_pt)->at(ijet) <= 0)
	   ){
	  nJetsRejectedBaseCuts++; 
	  continue;
	}
      }
      else{
	// selections for domain
	if(std::find(jets_no_overlap_leptons_index.begin(),jets_no_overlap_leptons_index.end(),ijet) == jets_no_overlap_leptons_index.end()){
	  nJetsRejectedBaseCuts++;
	  continue;
	}

	// selection for mutau
	if(domain == domain_type::mutau or domain == domain_type::etau){
	  if((selectOnParTScore) ?
	     ((jet_parTlast_probtaup1h0p->at(ijet)+jet_parTlast_probtaup1h1p->at(ijet)+jet_parTlast_probtaup1h2p->at(ijet)+jet_parTlast_probtaum1h0p->at(ijet)+jet_parTlast_probtaum1h1p->at(ijet)+jet_parTlast_probtaum1h2p->at(ijet)+jet_parTlast_probtaup3h0p->at(ijet)+jet_parTlast_probtaup3h1p->at(ijet)+jet_parTlast_probtaum3h0p->at(ijet)+jet_parTlast_probtaum3h1p->at(ijet))/(1-jet_parTlast_probmu->at(ijet)-jet_parTlast_probele->at(ijet)) <  minPTauVsJet) :
	     ((jet_pnetlast_probtaup1h0p->at(ijet)+jet_pnetlast_probtaup1h1p->at(ijet)+jet_pnetlast_probtaup1h2p->at(ijet)+jet_pnetlast_probtaum1h0p->at(ijet)+jet_pnetlast_probtaum1h1p->at(ijet)+jet_pnetlast_probtaum1h2p->at(ijet)+jet_pnetlast_probtaup3h0p->at(ijet)+jet_pnetlast_probtaup3h1p->at(ijet)+jet_pnetlast_probtaum3h0p->at(ijet)+jet_pnetlast_probtaum3h1p->at(ijet))/(1-jet_pnetlast_probmu->at(ijet)-jet_pnetlast_probele->at(ijet)) <  minPTauVsJet)){
	    nJetsRejectedMuTau++;
	    continue;
	  }
	  if((selectOnParTScore) ?
	     ((jet_parTlast_probtaup1h0p->at(ijet)+jet_parTlast_probtaup1h1p->at(ijet)+jet_parTlast_probtaup1h2p->at(ijet)+jet_parTlast_probtaum1h0p->at(ijet)+jet_parTlast_probtaum1h1p->at(ijet)+jet_parTlast_probtaum1h2p->at(ijet)+jet_parTlast_probtaup3h0p->at(ijet)+jet_parTlast_probtaup3h1p->at(ijet)+jet_parTlast_probtaum3h0p->at(ijet)+jet_parTlast_probtaum3h1p->at(ijet))/(1-jet_parTlast_probb->at(ijet)-jet_parTlast_probc->at(ijet)-jet_parTlast_probuds->at(ijet)-jet_parTlast_probg->at(ijet)-jet_parTlast_probele->at(ijet)) <  minPTauVsMu) :
	     ((jet_pnetlast_probtaup1h0p->at(ijet)+jet_pnetlast_probtaup1h1p->at(ijet)+jet_pnetlast_probtaup1h2p->at(ijet)+jet_pnetlast_probtaum1h0p->at(ijet)+jet_pnetlast_probtaum1h1p->at(ijet)+jet_pnetlast_probtaum1h2p->at(ijet)+jet_pnetlast_probtaup3h0p->at(ijet)+jet_pnetlast_probtaup3h1p->at(ijet)+jet_pnetlast_probtaum3h0p->at(ijet)+jet_pnetlast_probtaum3h1p->at(ijet))/(1-jet_pnetlast_probb->at(ijet)-jet_pnetlast_probc->at(ijet)-jet_pnetlast_probuds->at(ijet)-jet_pnetlast_probg->at(ijet)-jet_pnetlast_probele->at(ijet)) <  minPTauVsMu)){
	    nJetsRejectedMuTau++;
	    continue;
	  }
	  if((selectOnParTScore) ?
	     ((jet_parTlast_probtaup1h0p->at(ijet)+jet_parTlast_probtaup1h1p->at(ijet)+jet_parTlast_probtaup1h2p->at(ijet)+jet_parTlast_probtaum1h0p->at(ijet)+jet_parTlast_probtaum1h1p->at(ijet)+jet_parTlast_probtaum1h2p->at(ijet)+jet_parTlast_probtaup3h0p->at(ijet)+jet_parTlast_probtaup3h1p->at(ijet)+jet_parTlast_probtaum3h0p->at(ijet)+jet_parTlast_probtaum3h1p->at(ijet))/(1-jet_parTlast_probb->at(ijet)-jet_parTlast_probc->at(ijet)-jet_parTlast_probuds->at(ijet)-jet_parTlast_probg->at(ijet)-jet_parTlast_probmu->at(ijet)) <  minPTauVsEle) :
	     ((jet_pnetlast_probtaup1h0p->at(ijet)+jet_pnetlast_probtaup1h1p->at(ijet)+jet_pnetlast_probtaup1h2p->at(ijet)+jet_pnetlast_probtaum1h0p->at(ijet)+jet_pnetlast_probtaum1h1p->at(ijet)+jet_pnetlast_probtaum1h2p->at(ijet)+jet_pnetlast_probtaup3h0p->at(ijet)+jet_pnetlast_probtaup3h1p->at(ijet)+jet_pnetlast_probtaum3h0p->at(ijet)+jet_pnetlast_probtaum3h1p->at(ijet))/(1-jet_pnetlast_probb->at(ijet)-jet_pnetlast_probc->at(ijet)-jet_pnetlast_probuds->at(ijet)-jet_pnetlast_probg->at(ijet)-jet_pnetlast_probmu->at(ijet)) <  minPTauVsEle)){
	    nJetsRejectedMuTau++;
	    continue;
	  }
	}	   
	// selection for emu
	else if(domain == domain_type::emu){
	  if((selectOnParTScore) ? (jet_parTlast_probb->at(ijet)/(jet_parTlast_probb->at(ijet)+jet_parTlast_probc->at(ijet)+jet_parTlast_probuds->at(ijet)+jet_parTlast_probg->at(ijet)) < minPBVsJet) : (jet_pnetlast_probb->at(ijet)/(jet_pnetlast_probb->at(ijet)+jet_pnetlast_probc->at(ijet)+jet_pnetlast_probuds->at(ijet)+jet_pnetlast_probg->at(ijet)) < minPBVsJet)){
	    nJetsRejectedEMu++;
	    continue;
	  }
	}	    	
	// selection for ttcharm
	else if(domain == domain_type::ttcharm){
	  if((selectOnParTScore) ? (jet_parTlast_probc->at(ijet)/(jet_parTlast_probb->at(ijet)+jet_parTlast_probc->at(ijet)+jet_parTlast_probuds->at(ijet)+jet_parTlast_probg->at(ijet)) < minPCVsJet) : (jet_pnetlast_probc->at(ijet)/(jet_pnetlast_probb->at(ijet)+jet_pnetlast_probc->at(ijet)+jet_pnetlast_probuds->at(ijet)+jet_pnetlast_probg->at(ijet)) < minPCVsJet)){
	    nJetsRejectedTTcharm++;
	    continue;
	  }
	}	    	
      }




      // matching with gen-leptons ( taus and di-taus)
      float minDR = 1000;
      int nlep_in_cone  = 0;
      int pos_matched_genmu = -1;
      int pos_matched_genele = -1;
      int pos_matched_tauh = -1;
      int pos_matched_ditauh = -1; // for ditau label
      int gentau_decaymode = -1; 	  
      TLorentzVector genLepton4V;
      TLorentzVector genLeptonVis4V;
      //int match_index_dR_resonance_jet = -1;

      
      if(not (sample == sample_type::data)){
	// matching jet-bsresonance
	float mindR_jetres = 1000;
	for(size_t ipair = 0; ipair < resonances.size(); ipair++){ // find the bs that better matches with the reco ak4jet
	  float dRjres = jet4V.DeltaR(resonances.at(ipair).p4);
	  if(dRjres < dRCone and dRjres < mindR_jetres){
	    //match_index_dR_resonance_jet = ipair;
	    pos_matched_ditauh = 1;
	  }
	}

      /*
	for(size_t igen = 0; igen < genMuonsFromResonance4V.size(); igen++){
	  float dR = jet4V.DeltaR(genMuonsFromResonance4V.at(igen));	      
	  if(dR < dRCone) nlep_in_cone++;
	  if(dR < dRCone and dR < minDR){
	    pos_matched_genmu = igen;
	    minDR = dR;
	    genLepton4V = genMuonsFromResonance4V.at(igen);
	    genLeptonVis4V = genMuonsFromResonance4V.at(igen);
	  }
	}
	
	for(size_t igen = 0; igen < genElectronsFromResonance4V.size(); igen++){
	  float dR = jet4V.DeltaR(genElectronsFromResonance4V.at(igen));	      
	  if(dR < dRCone) nlep_in_cone++;
	  if(dR < dRCone and dR < minDR){
	    pos_matched_genmu  = -1;
	    pos_matched_genele = igen;
	    minDR = dR;
	    genLepton4V = genElectronsFromResonance4V.at(igen);
	    genLeptonVis4V = genElectronsFromResonance4V.at(igen);
	  }
	}

	
	// single-tau matching
	for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){
	  float dR = tau_gen_visible.at(itau).DeltaR(jet4V); 
	  //std::cout<<iteration<<" single - tau gen "<<tau_gen.at(itau).Pt()<<" jet pt "<<jet4V.Pt()<<" dR "<<dR<<" minDR "<<minDR<<std::endl;
	  if(dR < dRCone) nlep_in_cone++;
	  if(dR < dRCone and dR < minDR){
	    pos_matched_genmu  = -1;
	    pos_matched_genele = -1;
	    pos_matched_tauh = itau;
	    minDR = dR;
	    gentau_decaymode = 5*(tau_gen_nch.at(itau)-1)+tau_gen_np0.at(itau);
	    genLepton4V = tau_gen.at(itau);
	    genLeptonVis4V = tau_gen_visible.at(itau);
	  }
	}

	
	// di-tau from Bs


	for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){
	  float dR = tau_gen_visible.at(itau).DeltaR(jet4V); 
	  //std::cout<<iteration<<" di-tau gen "<<tau_gen.at(itau).Pt()<<" jet pt "<<jet4V.Pt()<<" dR "<<dR<<" minDR "<<minDR<<" pos_matched_tauh "<<pos_matched_tauh<<std::endl;
          //if(dR < dRCone) nlep_in_cone++; // this has already been filled in the single-tau scope, for both taus
          // MAYBE? Also require dR between the two taus to be less than 0.3/0.4
          if(pos_matched_tauh != -1 and  dR != minDR and dR < dRCone){ // second tau to be matched with the jet
            //std::cout<<"====> this is the second tau "<<tau_gen.at(itau).Pt()<<std::endl;
            pos_matched_ditauh = itau; // the other tau is saved in the loop with single-tau

	    // for now I am not saving any information on the second tau, to be done later
	    //gendiLepton4V = tau_gen.at(itau);
            //gendiLeptonVis4V = tau_gen_visible.at(itau);
	  }
	}
	*/



	
	// exclude, when a jet is matched with a lepton, those for which the matched lepton is below the chosen pt threshold
	if(domain == domain_type::none){
	  // Jet id applied only to jets not overlapping with gen-leptons
	  if(applyJetID and pos_matched_genmu  == -1 and pos_matched_genele == -1 and pos_matched_tauh == -1 and jet_id->at(ijet) == 0){
	    nJetsRejectedBaseCuts++;
	    continue;
	  }
	  if(pos_matched_genmu != -1 and genLeptonVis4V.Pt() < ptGenLeptonMin){
	    nJetsRejectedLowGenLepton++;
	    continue;
	  }
	  if(pos_matched_genele != -1 and genLeptonVis4V.Pt() < ptGenLeptonMin){
	    nJetsRejectedLowGenLepton++;
	    continue;
	  }
	  if(pos_matched_tauh != -1 and genLeptonVis4V.Pt() < ptGenTauVisibleMin){
	    nJetsRejectedLowGenLepton++;
	    continue;
	  } 
	}
      }
	
      // good jets for the analysis	  
      nJetsTraining++;	  

      /// Fill branches
      run_b = *run;
      lumi_b = *lumi;
      event_b = *event;
      npv_b = *npv;
      nsv_b = *nsv;
      if(not (sample == sample_type::data)){
	npu_b = **putrue;
	wgt_b = **wgt;
      }
      rho_b = *rho;
      met_b = *met;	
      sample_b = static_cast<int>(sample);
      domain_b = static_cast<int>(domain);
      isdata_b = (sample == sample_type::data) ? 1 : 0;
      ijet_b = ijet;

      // Basic jet quantities
      jet_pt_b   = jet_pt->at(ijet);
      jet_pt_raw_b   = jet_pt_raw->at(ijet);
      jet_eta_b  = jet_eta->at(ijet);
      jet_phi_b  = jet_phi->at(ijet);
      jet_mass_b = jet_mass->at(ijet);
      jet_mass_raw_b = jet_mass_raw->at(ijet);
      jet_energy_b = jet4V.E();

      jet_chf_b = jet_chf->at(ijet);
      jet_nhf_b = jet_nhf->at(ijet);
      jet_phf_b = jet_phf->at(ijet);
      jet_elf_b = jet_elf->at(ijet);
      jet_muf_b = jet_muf->at(ijet);

      if(not (sample == sample_type::data)){
	jet_nbhad_b = jet_nbhad->at(ijet);
	jet_nchad_b = jet_nchad->at(ijet);	
	jet_hflav_b = jet_hflav->at(ijet);
	jet_pflav_b = jet_pflav->at(ijet);
      }
      else{
	jet_nbhad_b = 0;
	jet_nchad_b = 0;
	jet_hflav_b = 0;
	jet_pflav_b = 0;
      }
      
      jet_deepjet_probb_b   = jet_deepjet_probb->at(ijet)+jet_deepjet_probbb->at(ijet)+jet_deepjet_problepb->at(ijet);
      jet_deepjet_probc_b   = jet_deepjet_probc->at(ijet);
      jet_deepjet_probuds_b = jet_deepjet_probuds->at(ijet);
      jet_deepjet_probg_b   = jet_deepjet_probg->at(ijet);

      jet_pnet_probb_b = (jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet))/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      jet_pnet_probc_b = (jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet))/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      jet_pnet_probuds_b = jet_pnet_probuds->at(ijet)/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      jet_pnet_probg_b = jet_pnet_probg->at(ijet)/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));

      jet_pnetlast_probb_b = jet_pnetlast_probb->at(ijet);
      jet_pnetlast_probc_b = jet_pnetlast_probc->at(ijet);
      jet_pnetlast_probuds_b = jet_pnetlast_probuds->at(ijet);
      jet_pnetlast_probg_b = jet_pnetlast_probg->at(ijet);
      jet_pnetlast_probmu_b = jet_pnetlast_probmu->at(ijet);
      jet_pnetlast_probele_b = jet_pnetlast_probele->at(ijet);
      jet_pnetlast_probtaup1h0p_b = jet_pnetlast_probtaup1h0p->at(ijet);
      jet_pnetlast_probtaup1h1p_b = jet_pnetlast_probtaup1h1p->at(ijet);
      jet_pnetlast_probtaup1h2p_b = jet_pnetlast_probtaup1h2p->at(ijet);
      jet_pnetlast_probtaup3h0p_b = jet_pnetlast_probtaup3h0p->at(ijet);
      jet_pnetlast_probtaup3h1p_b = jet_pnetlast_probtaup3h1p->at(ijet);
      jet_pnetlast_probtaum1h0p_b = jet_pnetlast_probtaum1h0p->at(ijet);
      jet_pnetlast_probtaum1h1p_b = jet_pnetlast_probtaum1h1p->at(ijet);
      jet_pnetlast_probtaum1h2p_b = jet_pnetlast_probtaum1h2p->at(ijet);
      jet_pnetlast_probtaum3h0p_b = jet_pnetlast_probtaum3h0p->at(ijet);
      jet_pnetlast_probtaum3h1p_b = jet_pnetlast_probtaum3h1p->at(ijet);
      jet_pnetlast_ptcorr_b = jet_pnetlast_ptcorr->at(ijet);
      jet_pnetlast_ptnu_b = jet_pnetlast_ptnu->at(ijet);
      jet_pnetlast_ptreshigh_b = jet_pnetlast_ptreshigh->at(ijet);
      jet_pnetlast_ptreslow_b = jet_pnetlast_ptreslow->at(ijet);

      jet_parTlast_probb_b = jet_parTlast_probb->at(ijet);
      jet_parTlast_probc_b = jet_parTlast_probc->at(ijet);
      jet_parTlast_probuds_b = jet_parTlast_probuds->at(ijet);
      jet_parTlast_probg_b = jet_parTlast_probg->at(ijet);
      jet_parTlast_probmu_b = jet_parTlast_probmu->at(ijet);
      jet_parTlast_probele_b = jet_parTlast_probele->at(ijet);
      jet_parTlast_probtaup1h0p_b = jet_parTlast_probtaup1h0p->at(ijet);
      jet_parTlast_probtaup1h1p_b = jet_parTlast_probtaup1h1p->at(ijet);
      jet_parTlast_probtaup1h2p_b = jet_parTlast_probtaup1h2p->at(ijet);
      jet_parTlast_probtaup3h0p_b = jet_parTlast_probtaup3h0p->at(ijet);
      jet_parTlast_probtaup3h1p_b = jet_parTlast_probtaup3h1p->at(ijet);
      jet_parTlast_probtaum1h0p_b = jet_parTlast_probtaum1h0p->at(ijet);
      jet_parTlast_probtaum1h1p_b = jet_parTlast_probtaum1h1p->at(ijet);
      jet_parTlast_probtaum1h2p_b = jet_parTlast_probtaum1h2p->at(ijet);
      jet_parTlast_probtaum3h0p_b = jet_parTlast_probtaum3h0p->at(ijet);
      jet_parTlast_probtaum3h1p_b = jet_parTlast_probtaum3h1p->at(ijet);
      jet_parTlast_ptcorr_b = jet_parTlast_ptcorr->at(ijet);
      jet_parTlast_ptnu_b = jet_parTlast_ptnu->at(ijet);
      jet_parTlast_ptreshigh_b = jet_parTlast_ptreshigh->at(ijet);
      jet_parTlast_ptreslow_b = jet_parTlast_ptreslow->at(ijet);
      
      if(pos_matched_genmu >= 0){
	jet_muflav_b  = 1;
	jet_mucharge_b = genMuonsFromResonanceCharge.at(pos_matched_genmu);
      }
      else{
	jet_muflav_b  = 0;
	jet_mucharge_b  = 0;
      }

      if(pos_matched_genele >= 0){
	jet_elflav_b  = 1;
	jet_elcharge_b = genElectronsFromResonanceCharge.at(pos_matched_genele);
      }
      else{
	jet_elflav_b  = 0;
	jet_elcharge_b = 0;
      }

      if(pos_matched_tauh >= 0){
	jet_tauflav_b = 1;
	jet_taudecaymode_b = gentau_decaymode;
	jet_taucharge_b = tau_gen_charge.at(pos_matched_tauh);
      }
      else{
	jet_tauflav_b = 0;
	jet_taudecaymode_b = -1;
	jet_taucharge_b = 0;
      }

      if(pos_matched_ditauh >= 0){
	jet_ditauflav_b = 1;
      }
      else{
	jet_ditauflav_b = 0;
      }
      
      jet_lepflav_b = nlep_in_cone;
	  
      // Gen jet info
      TLorentzVector genJet4V; 
      TLorentzVector genJetWithNu4V;
      if(not (sample == sample_type::data)){
	genJet4V.SetPtEtaPhiM((*jet_genmatch_pt)->at(ijet),(*jet_genmatch_eta)->at(ijet),(*jet_genmatch_phi)->at(ijet),(*jet_genmatch_mass)->at(ijet));
	genJetWithNu4V.SetPtEtaPhiM((*jet_genmatch_wnu_pt)->at(ijet),(*jet_genmatch_wnu_eta)->at(ijet),(*jet_genmatch_wnu_phi)->at(ijet),(*jet_genmatch_wnu_mass)->at(ijet));

	jet_genmatch_pt_b = (*jet_genmatch_pt)->at(ijet);
	jet_genmatch_eta_b = (*jet_genmatch_eta)->at(ijet);
	jet_genmatch_phi_b = (*jet_genmatch_phi)->at(ijet);
	jet_genmatch_mass_b = (*jet_genmatch_mass)->at(ijet);
	jet_genmatch_energy_b = genJet4V.E();

	jet_genmatch_wnu_pt_b = (*jet_genmatch_wnu_pt)->at(ijet);
	jet_genmatch_wnu_eta_b = (*jet_genmatch_wnu_eta)->at(ijet);
	jet_genmatch_wnu_phi_b = (*jet_genmatch_wnu_phi)->at(ijet);
	jet_genmatch_wnu_mass_b = (*jet_genmatch_wnu_mass)->at(ijet);
	jet_genmatch_wnu_energy_b = genJetWithNu4V.E();

	jet_genmatch_nu_pt_b = (genJetWithNu4V-genJet4V).Pt();
	jet_genmatch_nu_eta_b = (genJetWithNu4V-genJet4V).Eta();
	jet_genmatch_nu_phi_b = (genJetWithNu4V-genJet4V).Phi();
	jet_genmatch_nu_mass_b = (genJetWithNu4V-genJet4V).M();
	jet_genmatch_nu_energy_b = (genJetWithNu4V-genJet4V).E();
	
	jet_genmatch_lep_pt_b = genLepton4V.Pt();
	jet_genmatch_lep_eta_b = genLepton4V.Eta();
	jet_genmatch_lep_phi_b = genLepton4V.Phi();
	jet_genmatch_lep_mass_b = genLepton4V.M();
	jet_genmatch_lep_energy_b = genLepton4V.E();
	
	jet_genmatch_lep_vis_pt_b = genLeptonVis4V.Pt();
	jet_genmatch_lep_vis_eta_b = genLeptonVis4V.Eta();
	jet_genmatch_lep_vis_phi_b = genLeptonVis4V.Phi();
	jet_genmatch_lep_vis_mass_b = genLeptonVis4V.M();
	jet_genmatch_lep_vis_energy_b = genLeptonVis4V.E();
      }
      else{
	jet_genmatch_pt_b = -1;
	jet_genmatch_eta_b = -1;
	jet_genmatch_phi_b = -1;
	jet_genmatch_mass_b = -1;
	jet_genmatch_energy_b = -1;
	jet_genmatch_wnu_pt_b = -1;
	jet_genmatch_wnu_eta_b = -1;
	jet_genmatch_wnu_phi_b = -1;
	jet_genmatch_wnu_mass_b = -1;
	jet_genmatch_wnu_energy_b = -1;
	jet_genmatch_lep_pt_b = -1;
	jet_genmatch_lep_eta_b = -1;
	jet_genmatch_lep_phi_b = -1;
	jet_genmatch_lep_mass_b = -1;
	jet_genmatch_lep_energy_b = -1;
	jet_genmatch_lep_vis_pt_b = -1;
	jet_genmatch_lep_vis_eta_b = -1;
	jet_genmatch_lep_vis_phi_b = -1;
	jet_genmatch_lep_vis_mass_b = -1;
	jet_genmatch_lep_vis_energy_b = -1;
      }

      // SV	  
      jet_sv_pt_b.clear(); jet_sv_eta_b.clear(); jet_sv_phi_b.clear(); jet_sv_mass_b.clear(); jet_sv_energy_b.clear();
      jet_sv_deta_b.clear(); jet_sv_dphi_b.clear();
      jet_sv_chi2_b.clear(); jet_sv_dxy_b.clear(); jet_sv_dxysig_b.clear();
      jet_sv_d3d_b.clear(); jet_sv_d3dsig_b.clear(); jet_sv_ntrack_b.clear();
      jet_sv_pt_log_b.clear(); jet_sv_energy_log_b.clear();

      for(size_t isv = 0; isv < jet_sv_pt->size(); isv++){	
	if(ijet != jet_sv_ijet->at(isv)) continue;	    
	jet_sv_pt_b.push_back(jet_sv_pt->at(isv));
	jet_sv_pt_log_b.push_back(std::isnan(std::log(jet_sv_pt->at(isv))) ? 0 : std::log(jet_sv_pt->at(isv)));
	jet_sv_eta_b.push_back(jet_sv_eta->at(isv));	  
	jet_sv_phi_b.push_back(jet_sv_phi->at(isv));	  
	jet_sv_energy_b.push_back(jet_sv_energy->at(isv));
	jet_sv_energy_log_b.push_back(std::isnan(std::log(jet_sv_energy->at(isv))) ? 0 : std::log(jet_sv_energy->at(isv)));
	jet_sv_mass_b.push_back(jet_sv_mass->at(isv));
	jet_sv_deta_b.push_back(jet_sv_eta->at(isv)-jet_eta->at(ijet));
	jet_sv_dphi_b.push_back(jet_sv_phi->at(isv)-jet_phi->at(ijet));
	jet_sv_chi2_b.push_back(jet_sv_chi2->at(isv));
	jet_sv_dxy_b.push_back(std::isnan(jet_sv_dxy->at(isv)) ? 0 : jet_sv_dxy->at(isv));
	jet_sv_dxysig_b.push_back(std::isnan(jet_sv_dxysig->at(isv)) ? 0 : jet_sv_dxysig->at(isv));
	jet_sv_d3d_b.push_back(std::isnan(jet_sv_d3d->at(isv)) ? 0 : jet_sv_d3d->at(isv));
	jet_sv_d3dsig_b.push_back(std::isnan(jet_sv_d3dsig->at(isv)) ? 0 : jet_sv_d3dsig->at(isv));
	jet_sv_ntrack_b.push_back(jet_sv_ntrack->at(isv));
      }

      // Lost tracks
      jet_losttrack_pt_b.clear(); jet_losttrack_eta_b.clear(); jet_losttrack_phi_b.clear(); jet_losttrack_mass_b.clear(); jet_losttrack_energy_b.clear(); 
      jet_losttrack_dxy_b.clear(); jet_losttrack_dz_b.clear(); jet_losttrack_dxysig_b.clear(); jet_losttrack_dzsig_b.clear(); 
      jet_losttrack_deta_b.clear(); jet_losttrack_dphi_b.clear(); 
      jet_losttrack_pt_log_b.clear(); jet_losttrack_energy_log_b.clear(); jet_losttrack_etarel_b.clear(); 
      jet_losttrack_charge_b.clear(); jet_losttrack_frompv_b.clear(); jet_losttrack_id_b.clear(); jet_losttrack_track_qual_b.clear(); jet_losttrack_track_chi2_b.clear();
      jet_losttrack_trackjet_d3d_b.clear(); jet_losttrack_trackjet_d3dsig_b.clear();      
      jet_losttrack_trackjet_dist_b.clear(); jet_losttrack_trackjet_decayL_b.clear();
      jet_losttrack_track_pterr_b.clear(); jet_losttrack_track_etaerr_b.clear(); jet_losttrack_track_phierr_b.clear();
      jet_losttrack_nhits_b.clear(); jet_losttrack_npixhits_b.clear(); jet_losttrack_nstriphits_b.clear(); jet_losttrack_nlosthits_b.clear();
      jet_losttrack_npixlayers_b.clear(); jet_losttrack_nstriplayers_b.clear(), jet_losttrack_track_algo_b.clear();
      
      for(size_t icand = 0; icand < jet_losttrack_pt->size(); icand++){	
	if(ijet != jet_losttrack_ijet->at(icand)) continue;
	if(jet_losttrack_pt->at(icand) < losttrackPtMin) continue;
	jet_losttrack_pt_b.push_back(jet_losttrack_pt->at(icand));
	jet_losttrack_pt_log_b.push_back(std::isnan(std::log(jet_losttrack_pt->at(icand))) ? 0 : std::log(jet_losttrack_pt->at(icand)));	  
	jet_losttrack_eta_b.push_back(jet_losttrack_eta->at(icand));
	jet_losttrack_phi_b.push_back(jet_losttrack_phi->at(icand));
	jet_losttrack_mass_b.push_back(jet_losttrack_mass->at(icand));
	jet_losttrack_energy_b.push_back(jet_losttrack_energy->at(icand));
	jet_losttrack_energy_log_b.push_back(std::isnan(std::log(jet_losttrack_energy->at(icand))) ? 0 : std::log(jet_losttrack_energy->at(icand)));	    
	jet_losttrack_dxy_b.push_back(std::isnan(jet_losttrack_dxy->at(icand)) ? 0 : jet_losttrack_dxy->at(icand));
	jet_losttrack_dz_b.push_back(std::isnan(jet_losttrack_dz->at(icand)) ? 0 : jet_losttrack_dz->at(icand));
	jet_losttrack_dzsig_b.push_back(std::isnan(jet_losttrack_dzsig->at(icand)) ? 0 : jet_losttrack_dzsig->at(icand));
	jet_losttrack_dxysig_b.push_back(std::isnan(jet_losttrack_dxysig->at(icand)) ? 0 : jet_losttrack_dxysig->at(icand));
	jet_losttrack_deta_b.push_back(jet_losttrack_deta->at(icand));
	jet_losttrack_dphi_b.push_back(jet_losttrack_dphi->at(icand));
	jet_losttrack_etarel_b.push_back(std::isnan(jet_losttrack_etarel->at(icand)) ? 0 : jet_losttrack_etarel->at(icand));
	jet_losttrack_frompv_b.push_back(jet_losttrack_frompv->at(icand));
	jet_losttrack_charge_b.push_back(jet_losttrack_charge->at(icand));
	jet_losttrack_track_qual_b.push_back(jet_losttrack_track_qual->at(icand));
	jet_losttrack_track_chi2_b.push_back(jet_losttrack_track_chi2->at(icand));
	jet_losttrack_track_pterr_b.push_back((std::isnan(jet_losttrack_track_pterr->at(icand)) ? 0 : jet_losttrack_track_pterr->at(icand)));
	jet_losttrack_track_etaerr_b.push_back((std::isnan(jet_losttrack_track_etaerr->at(icand)) ? 0 : jet_losttrack_track_etaerr->at(icand)));
	jet_losttrack_track_phierr_b.push_back((std::isnan(jet_losttrack_track_phierr->at(icand)) ? 0 : jet_losttrack_track_phierr->at(icand)));
	jet_losttrack_track_algo_b.push_back(jet_losttrack_track_algo->at(icand));
	jet_losttrack_nhits_b.push_back(jet_losttrack_nhits->at(icand));
	jet_losttrack_npixhits_b.push_back(jet_losttrack_npixhits->at(icand));
	jet_losttrack_nstriphits_b.push_back(jet_losttrack_nstriphits->at(icand));
	jet_losttrack_nlosthits_b.push_back(jet_losttrack_nlosthits->at(icand));
	jet_losttrack_npixlayers_b.push_back(jet_losttrack_npixlayers->at(icand));
	jet_losttrack_nstriplayers_b.push_back(jet_losttrack_nstriplayers->at(icand));	
	jet_losttrack_trackjet_d3d_b.push_back(std::isnan(jet_losttrack_trackjet_d3d->at(icand)) ? 0 : jet_losttrack_trackjet_d3d->at(icand));
	jet_losttrack_trackjet_d3dsig_b.push_back(std::isnan(jet_losttrack_trackjet_d3dsig->at(icand)) ? 0 : jet_losttrack_trackjet_d3dsig->at(icand));
	jet_losttrack_trackjet_dist_b.push_back(std::isnan(jet_losttrack_trackjet_dist->at(icand)) ? 0 : jet_losttrack_trackjet_dist->at(icand));
	jet_losttrack_trackjet_decayL_b.push_back(std::isnan(jet_losttrack_trackjet_decayL->at(icand)) ? 0 : jet_losttrack_trackjet_decayL->at(icand));
	
      }

      // PF candidates	
      jet_pfcand_pt_b.clear(); jet_pfcand_eta_b.clear(); jet_pfcand_phi_b.clear(); jet_pfcand_mass_b.clear(); jet_pfcand_energy_b.clear(); jet_pfcand_calofraction_b.clear();
      jet_pfcand_hcalfraction_b.clear(); jet_pfcand_dxy_b.clear(); jet_pfcand_dz_b.clear(); jet_pfcand_dxysig_b.clear(); jet_pfcand_dzsig_b.clear(); 
      jet_pfcand_pperp_ratio_b.clear(); jet_pfcand_ppara_ratio_b.clear(); jet_pfcand_deta_b.clear(); jet_pfcand_dphi_b.clear(); 
      jet_pfcand_pt_log_b.clear(); jet_pfcand_energy_log_b.clear(); jet_pfcand_etarel_b.clear(); jet_pfcand_puppiw_b.clear(); 
      jet_pfcand_charge_b.clear(); jet_pfcand_frompv_b.clear(); jet_pfcand_id_b.clear(); jet_pfcand_track_qual_b.clear(); jet_pfcand_track_chi2_b.clear();	  
      jet_pfcand_trackjet_d3d_b.clear(); jet_pfcand_trackjet_d3dsig_b.clear(); jet_pfcand_trackjet_dist_b.clear(); jet_pfcand_trackjet_decayL_b.clear();	  
      jet_pfcand_track_pterr_b.clear(); jet_pfcand_track_etaerr_b.clear(); jet_pfcand_track_phierr_b.clear();
      jet_pfcand_nhits_b.clear(); jet_pfcand_npixhits_b.clear(); jet_pfcand_nstriphits_b.clear(); jet_pfcand_nlosthits_b.clear();
      jet_pfcand_npixlayers_b.clear(); jet_pfcand_nstriplayers_b.clear(), jet_pfcand_track_algo_b.clear();
      jet_pfcand_tau_signal_b.clear();
      jet_pfcand_muon_id_b.clear(); jet_pfcand_muon_chi2_b.clear(); jet_pfcand_muon_segcomp_b.clear(); jet_pfcand_muon_isglobal_b.clear(); 
      jet_pfcand_muon_nvalidhit_b.clear(); jet_pfcand_muon_nstation_b.clear();
      jet_pfcand_electron_eOverP_b.clear(); jet_pfcand_electron_detaIn_b.clear(); jet_pfcand_electron_dphiIn_b.clear(); jet_pfcand_electron_r9_b.clear();
      jet_pfcand_electron_convProb_b.clear(); jet_pfcand_electron_sigIetaIeta_b.clear(); jet_pfcand_electron_sigIphiIphi_b.clear();
      jet_pfcand_photon_sigIetaIeta_b.clear(); jet_pfcand_photon_r9_b.clear(); jet_pfcand_photon_eVeto_b.clear();

      jet_pfcand_neu_pt_b.clear(); jet_pfcand_neu_eta_b.clear(); jet_pfcand_neu_phi_b.clear(); jet_pfcand_neu_mass_b.clear(); jet_pfcand_neu_energy_b.clear(); jet_pfcand_neu_calofraction_b.clear();
      jet_pfcand_neu_hcalfraction_b.clear(); jet_pfcand_neu_dxy_b.clear(); jet_pfcand_neu_dz_b.clear();
      jet_pfcand_neu_pperp_ratio_b.clear(); jet_pfcand_neu_ppara_ratio_b.clear(); jet_pfcand_neu_deta_b.clear(); jet_pfcand_neu_dphi_b.clear(); 
      jet_pfcand_neu_pt_log_b.clear(); jet_pfcand_neu_energy_log_b.clear(); jet_pfcand_neu_etarel_b.clear(); jet_pfcand_neu_puppiw_b.clear(); 
      jet_pfcand_neu_charge_b.clear(); jet_pfcand_neu_frompv_b.clear(); jet_pfcand_neu_id_b.clear();
      jet_pfcand_neu_tau_signal_b.clear();
      jet_pfcand_neu_photon_sigIetaIeta_b.clear(); jet_pfcand_neu_photon_r9_b.clear(); jet_pfcand_neu_photon_eVeto_b.clear();

      TLorentzVector tau4V_fromPF;
      std::vector<TLorentzVector> muon4V_fromPF;
      std::vector<TLorentzVector> electron4V_fromPF;
      std::vector<TLorentzVector> photon4V_fromPF;

      jet_ncand_b = 0; jet_nch_b = 0; jet_nneu_b = 0;
      
      for(size_t icand = 0; icand < jet_pfcand_pt->size(); icand++){
	
	if(ijet != jet_pfcand_ijet->at(icand)) continue;
	if(jet_pfcand_pt->at(icand) < pfCandPtMin) continue;
	if(jet_pfcand_puppiw->at(icand) < pfCandPuppiWeightMin) continue;

	// counters
	jet_ncand_b++;
	if(jet_pfcand_charge->at(icand) !=0) jet_nch_b++;
	else jet_nneu_b++;
	
	if(not splitChargedPF){
	  jet_pfcand_pt_b.push_back(jet_pfcand_pt->at(icand));
	  jet_pfcand_pt_log_b.push_back(std::isnan(std::log(jet_pfcand_pt->at(icand))) ? 0 : std::log(jet_pfcand_pt->at(icand)));	  
	  jet_pfcand_eta_b.push_back(jet_pfcand_eta->at(icand));
	  jet_pfcand_phi_b.push_back(jet_pfcand_phi->at(icand));
	  jet_pfcand_mass_b.push_back(jet_pfcand_mass->at(icand));
	  jet_pfcand_energy_b.push_back(jet_pfcand_energy->at(icand));
	  jet_pfcand_energy_log_b.push_back(std::isnan(std::log(jet_pfcand_energy->at(icand))) ? 0 : std::log(jet_pfcand_energy->at(icand)));	    
	  jet_pfcand_calofraction_b.push_back(std::isnan(jet_pfcand_calofraction->at(icand)) ? 0 : jet_pfcand_calofraction->at(icand));	    
	  jet_pfcand_hcalfraction_b.push_back(std::isnan(jet_pfcand_hcalfraction->at(icand)) ? 0 : jet_pfcand_hcalfraction->at(icand));	    	
	  jet_pfcand_dxy_b.push_back(std::isnan(jet_pfcand_dxy->at(icand)) ? 0 : jet_pfcand_dxy->at(icand));
	  jet_pfcand_dz_b.push_back(std::isnan(jet_pfcand_dz->at(icand)) ? 0 : jet_pfcand_dz->at(icand));
	  jet_pfcand_dzsig_b.push_back(std::isnan(jet_pfcand_dzsig->at(icand)) ? 0 : jet_pfcand_dzsig->at(icand));
	  jet_pfcand_dxysig_b.push_back(std::isnan(jet_pfcand_dxysig->at(icand)) ? 0 : jet_pfcand_dxysig->at(icand));
	  jet_pfcand_pperp_ratio_b.push_back(std::isnan(jet_pfcand_pperp_ratio->at(icand)) ? 0 : jet_pfcand_pperp_ratio->at(icand));
	  jet_pfcand_ppara_ratio_b.push_back(std::isnan(jet_pfcand_ppara_ratio->at(icand)) ? 0 : jet_pfcand_ppara_ratio->at(icand));
	  jet_pfcand_deta_b.push_back(jet_pfcand_deta->at(icand));
	  jet_pfcand_dphi_b.push_back(jet_pfcand_dphi->at(icand));
	  jet_pfcand_etarel_b.push_back(std::isnan(jet_pfcand_etarel->at(icand)) ? 0 : jet_pfcand_etarel->at(icand));
	  jet_pfcand_frompv_b.push_back(jet_pfcand_frompv->at(icand));
	  jet_pfcand_charge_b.push_back(jet_pfcand_charge->at(icand));
	  jet_pfcand_puppiw_b.push_back(jet_pfcand_puppiw->at(icand));	  
	  jet_pfcand_track_qual_b.push_back(jet_pfcand_track_qual->at(icand));
	  jet_pfcand_track_chi2_b.push_back(jet_pfcand_track_chi2->at(icand));       
	  jet_pfcand_track_algo_b.push_back(jet_pfcand_track_algo->at(icand));
	  jet_pfcand_track_pterr_b.push_back((std::isnan(jet_pfcand_track_pterr->at(icand)) ? 0 : jet_pfcand_track_pterr->at(icand)));
	  jet_pfcand_track_etaerr_b.push_back((std::isnan(jet_pfcand_track_etaerr->at(icand)) ? 0 : jet_pfcand_track_etaerr->at(icand)));
	  jet_pfcand_track_phierr_b.push_back((std::isnan(jet_pfcand_track_phierr->at(icand)) ? 0 : jet_pfcand_track_phierr->at(icand)));
	  jet_pfcand_nhits_b.push_back(jet_pfcand_nhits->at(icand));
	  jet_pfcand_npixhits_b.push_back(jet_pfcand_npixhits->at(icand));
	  jet_pfcand_nstriphits_b.push_back(jet_pfcand_nstriphits->at(icand));
	  jet_pfcand_nlosthits_b.push_back(jet_pfcand_nlosthits->at(icand));
	  jet_pfcand_npixlayers_b.push_back(jet_pfcand_npixlayers->at(icand));
	  jet_pfcand_nstriplayers_b.push_back(jet_pfcand_nstriplayers->at(icand));	
	  
	  if(jet_pfcand_id->at(icand) == 11 and jet_pfcand_charge->at(icand) != 0) 
	    jet_pfcand_id_b.push_back(0);
	  else if(jet_pfcand_id->at(icand) == 13 and jet_pfcand_charge->at(icand) != 0)
	    jet_pfcand_id_b.push_back(1);
	  else if(jet_pfcand_id->at(icand) == 22 and jet_pfcand_charge->at(icand) == 0)
	    jet_pfcand_id_b.push_back(2);
	  else if(jet_pfcand_id->at(icand) != 22 and jet_pfcand_charge->at(icand) == 0 and jet_pfcand_id->at(icand) != 1 and jet_pfcand_id->at(icand) != 2)
	    jet_pfcand_id_b.push_back(3);
	  else if(jet_pfcand_id->at(icand) != 11 and jet_pfcand_id->at(icand) != 13 and jet_pfcand_charge->at(icand) != 0)
	    jet_pfcand_id_b.push_back(4);
	  else if(jet_pfcand_charge->at(icand) == 0  and jet_pfcand_id->at(icand) == 1)
	    jet_pfcand_id_b.push_back(5);
	  else if(jet_pfcand_charge->at(icand) == 0  and jet_pfcand_id->at(icand) == 2)
	    jet_pfcand_id_b.push_back(6);
	  else
	    jet_pfcand_id_b.push_back(-1);	    
	  
	  jet_pfcand_trackjet_d3d_b.push_back(std::isnan(jet_pfcand_trackjet_d3d->at(icand)) ? 0 : jet_pfcand_trackjet_d3d->at(icand));
	  jet_pfcand_trackjet_d3dsig_b.push_back(std::isnan(jet_pfcand_trackjet_d3dsig->at(icand)) ? 0 : jet_pfcand_trackjet_d3dsig->at(icand));
	  jet_pfcand_trackjet_dist_b.push_back(std::isnan(jet_pfcand_trackjet_dist->at(icand)) ? 0 : jet_pfcand_trackjet_dist->at(icand));
	  jet_pfcand_trackjet_decayL_b.push_back(std::isnan(jet_pfcand_trackjet_decayL->at(icand)) ? 0 : jet_pfcand_trackjet_decayL->at(icand));
	  
	  jet_pfcand_tau_signal_b.push_back(jet_pfcandidate_tau_signal->at(icand));
	  
	  jet_pfcand_muon_id_b.push_back(jet_pfcandidate_muon_id->at(icand));
	  jet_pfcand_muon_chi2_b.push_back(jet_pfcandidate_muon_chi2->at(icand));
	  jet_pfcand_muon_segcomp_b.push_back(jet_pfcandidate_muon_segcomp->at(icand));
	  jet_pfcand_muon_isglobal_b.push_back(jet_pfcandidate_muon_isglobal->at(icand));
	  jet_pfcand_muon_nvalidhit_b.push_back(jet_pfcandidate_muon_nvalidhit->at(icand));
	  jet_pfcand_muon_nstation_b.push_back(jet_pfcandidate_muon_nstation->at(icand));
	  
	  jet_pfcand_electron_eOverP_b.push_back(std::isnan(jet_pfcandidate_electron_eOverP->at(icand)) ? 0 : jet_pfcandidate_electron_eOverP->at(icand));
	  jet_pfcand_electron_detaIn_b.push_back(jet_pfcandidate_electron_detaIn->at(icand));
	  jet_pfcand_electron_dphiIn_b.push_back(jet_pfcandidate_electron_dphiIn->at(icand));
	  jet_pfcand_electron_r9_b.push_back(std::isnan(jet_pfcandidate_electron_r9->at(icand)) ? 0 : jet_pfcandidate_electron_r9->at(icand));
	  jet_pfcand_electron_sigIetaIeta_b.push_back(jet_pfcandidate_electron_sigIetaIeta->at(icand));
	  jet_pfcand_electron_convProb_b.push_back(std::isnan(jet_pfcandidate_electron_convProb->at(icand)) ? 0 : jet_pfcandidate_electron_convProb->at(icand));
	  jet_pfcand_electron_sigIphiIphi_b.push_back(std::isnan(jet_pfcandidate_electron_sigIphiIphi->at(icand)) ? 0 : jet_pfcandidate_electron_sigIphiIphi->at(icand));
	  
	  jet_pfcand_photon_sigIetaIeta_b.push_back(std::isnan(jet_pfcandidate_photon_sigIetaIeta->at(icand)) ? 0 : jet_pfcandidate_photon_sigIetaIeta->at(icand));
	  jet_pfcand_photon_r9_b.push_back(std::isnan(jet_pfcandidate_photon_r9->at(icand)) ? 0 : jet_pfcandidate_photon_r9->at(icand));
	  jet_pfcand_photon_eVeto_b.push_back(std::isnan(jet_pfcandidate_photon_eVeto->at(icand)) ? 0 : jet_pfcandidate_photon_eVeto->at(icand));
	}
	else{
	  if(jet_pfcand_charge->at(icand) !=0){ //charged cands
	    jet_pfcand_pt_b.push_back(jet_pfcand_pt->at(icand));
	    jet_pfcand_pt_log_b.push_back(std::isnan(std::log(jet_pfcand_pt->at(icand))) ? 0 : std::log(jet_pfcand_pt->at(icand)));	  
	    jet_pfcand_eta_b.push_back(jet_pfcand_eta->at(icand));
	    jet_pfcand_phi_b.push_back(jet_pfcand_phi->at(icand));
	    jet_pfcand_mass_b.push_back(jet_pfcand_mass->at(icand));
	    jet_pfcand_energy_b.push_back(jet_pfcand_energy->at(icand));
	    jet_pfcand_energy_log_b.push_back(std::isnan(std::log(jet_pfcand_energy->at(icand))) ? 0 : std::log(jet_pfcand_energy->at(icand)));	    
	    jet_pfcand_calofraction_b.push_back(std::isnan(jet_pfcand_calofraction->at(icand)) ? 0 : jet_pfcand_calofraction->at(icand));	    
	    jet_pfcand_hcalfraction_b.push_back(std::isnan(jet_pfcand_hcalfraction->at(icand)) ? 0 : jet_pfcand_hcalfraction->at(icand));	    	
	    jet_pfcand_dxy_b.push_back(std::isnan(jet_pfcand_dxy->at(icand)) ? 0 : jet_pfcand_dxy->at(icand));
	    jet_pfcand_dz_b.push_back(std::isnan(jet_pfcand_dz->at(icand)) ? 0 : jet_pfcand_dz->at(icand));
	    jet_pfcand_dzsig_b.push_back(std::isnan(jet_pfcand_dzsig->at(icand)) ? 0 : jet_pfcand_dzsig->at(icand));
	    jet_pfcand_dxysig_b.push_back(std::isnan(jet_pfcand_dxysig->at(icand)) ? 0 : jet_pfcand_dxysig->at(icand));
	    jet_pfcand_pperp_ratio_b.push_back(std::isnan(jet_pfcand_pperp_ratio->at(icand)) ? 0 : jet_pfcand_pperp_ratio->at(icand));
	    jet_pfcand_ppara_ratio_b.push_back(std::isnan(jet_pfcand_ppara_ratio->at(icand)) ? 0 : jet_pfcand_ppara_ratio->at(icand));
	    jet_pfcand_deta_b.push_back(jet_pfcand_deta->at(icand));
	    jet_pfcand_dphi_b.push_back(jet_pfcand_dphi->at(icand));
	    jet_pfcand_etarel_b.push_back(std::isnan(jet_pfcand_etarel->at(icand)) ? 0 : jet_pfcand_etarel->at(icand));
	    jet_pfcand_frompv_b.push_back(jet_pfcand_frompv->at(icand));
	    jet_pfcand_charge_b.push_back(jet_pfcand_charge->at(icand));
	    jet_pfcand_puppiw_b.push_back(jet_pfcand_puppiw->at(icand));
	  
	    jet_pfcand_track_qual_b.push_back(jet_pfcand_track_qual->at(icand));
	    jet_pfcand_track_chi2_b.push_back(jet_pfcand_track_chi2->at(icand));       
	    jet_pfcand_track_algo_b.push_back(jet_pfcand_track_algo->at(icand));
	    jet_pfcand_track_pterr_b.push_back((std::isnan(jet_pfcand_track_pterr->at(icand)) ? 0 : jet_pfcand_track_pterr->at(icand)));
	    jet_pfcand_track_etaerr_b.push_back((std::isnan(jet_pfcand_track_etaerr->at(icand)) ? 0 : jet_pfcand_track_etaerr->at(icand)));
	    jet_pfcand_track_phierr_b.push_back((std::isnan(jet_pfcand_track_phierr->at(icand)) ? 0 : jet_pfcand_track_phierr->at(icand)));
	    jet_pfcand_nhits_b.push_back(jet_pfcand_nhits->at(icand));
	    jet_pfcand_npixhits_b.push_back(jet_pfcand_npixhits->at(icand));
	    jet_pfcand_nstriphits_b.push_back(jet_pfcand_nstriphits->at(icand));
	    jet_pfcand_nlosthits_b.push_back(jet_pfcand_nlosthits->at(icand));
	    jet_pfcand_npixlayers_b.push_back(jet_pfcand_npixlayers->at(icand));
	    jet_pfcand_nstriplayers_b.push_back(jet_pfcand_nstriplayers->at(icand));	
	    
	    if(jet_pfcand_id->at(icand) == 11) 
	      jet_pfcand_id_b.push_back(0);
	    else if(jet_pfcand_id->at(icand) == 13)
	      jet_pfcand_id_b.push_back(1);
	    else if(jet_pfcand_id->at(icand) != 11 and jet_pfcand_id->at(icand) != 13 and jet_pfcand_charge->at(icand) != 0)
	      jet_pfcand_id_b.push_back(4);
	    else
	      jet_pfcand_id_b.push_back(-1);	    
	    
	    jet_pfcand_trackjet_d3d_b.push_back(std::isnan(jet_pfcand_trackjet_d3d->at(icand)) ? 0 : jet_pfcand_trackjet_d3d->at(icand));
	    jet_pfcand_trackjet_d3dsig_b.push_back(std::isnan(jet_pfcand_trackjet_d3dsig->at(icand)) ? 0 : jet_pfcand_trackjet_d3dsig->at(icand));
	    jet_pfcand_trackjet_dist_b.push_back(std::isnan(jet_pfcand_trackjet_dist->at(icand)) ? 0 : jet_pfcand_trackjet_dist->at(icand));
	    jet_pfcand_trackjet_decayL_b.push_back(std::isnan(jet_pfcand_trackjet_decayL->at(icand)) ? 0 : jet_pfcand_trackjet_decayL->at(icand));
	    
	    jet_pfcand_tau_signal_b.push_back(jet_pfcandidate_tau_signal->at(icand));
	  
	    jet_pfcand_muon_id_b.push_back(jet_pfcandidate_muon_id->at(icand));
	    jet_pfcand_muon_chi2_b.push_back(jet_pfcandidate_muon_chi2->at(icand));
	    jet_pfcand_muon_segcomp_b.push_back(jet_pfcandidate_muon_segcomp->at(icand));
	    jet_pfcand_muon_isglobal_b.push_back(jet_pfcandidate_muon_isglobal->at(icand));
	    jet_pfcand_muon_nvalidhit_b.push_back(jet_pfcandidate_muon_nvalidhit->at(icand));
	    jet_pfcand_muon_nstation_b.push_back(jet_pfcandidate_muon_nstation->at(icand));
	    
	    jet_pfcand_electron_eOverP_b.push_back(std::isnan(jet_pfcandidate_electron_eOverP->at(icand)) ? 0 : jet_pfcandidate_electron_eOverP->at(icand));
	    jet_pfcand_electron_detaIn_b.push_back(jet_pfcandidate_electron_detaIn->at(icand));
	    jet_pfcand_electron_dphiIn_b.push_back(jet_pfcandidate_electron_dphiIn->at(icand));
	    jet_pfcand_electron_r9_b.push_back(std::isnan(jet_pfcandidate_electron_r9->at(icand)) ? 0 : jet_pfcandidate_electron_r9->at(icand));
	    jet_pfcand_electron_sigIetaIeta_b.push_back(jet_pfcandidate_electron_sigIetaIeta->at(icand));
	    jet_pfcand_electron_convProb_b.push_back(std::isnan(jet_pfcandidate_electron_convProb->at(icand)) ? 0 : jet_pfcandidate_electron_convProb->at(icand));
	    jet_pfcand_electron_sigIphiIphi_b.push_back(std::isnan(jet_pfcandidate_electron_sigIphiIphi->at(icand)) ? 0 : jet_pfcandidate_electron_sigIphiIphi->at(icand));	    
	  }
	  else{
	    
	    jet_pfcand_neu_pt_b.push_back(jet_pfcand_pt->at(icand));
	    jet_pfcand_neu_pt_log_b.push_back(std::isnan(std::log(jet_pfcand_pt->at(icand))) ? 0 : std::log(jet_pfcand_pt->at(icand)));	  
	    jet_pfcand_neu_eta_b.push_back(jet_pfcand_eta->at(icand));
	    jet_pfcand_neu_phi_b.push_back(jet_pfcand_phi->at(icand));
	    jet_pfcand_neu_mass_b.push_back(jet_pfcand_mass->at(icand));
	    jet_pfcand_neu_energy_b.push_back(jet_pfcand_energy->at(icand));
	    jet_pfcand_neu_energy_log_b.push_back(std::isnan(std::log(jet_pfcand_energy->at(icand))) ? 0 : std::log(jet_pfcand_energy->at(icand)));	    
	    jet_pfcand_neu_calofraction_b.push_back(std::isnan(jet_pfcand_calofraction->at(icand)) ? 0 : jet_pfcand_calofraction->at(icand));	    
	    jet_pfcand_neu_hcalfraction_b.push_back(std::isnan(jet_pfcand_hcalfraction->at(icand)) ? 0 : jet_pfcand_hcalfraction->at(icand));	    	
	    jet_pfcand_neu_dxy_b.push_back(std::isnan(jet_pfcand_dxy->at(icand)) ? 0 : jet_pfcand_dxy->at(icand));
	    jet_pfcand_neu_dz_b.push_back(std::isnan(jet_pfcand_dz->at(icand)) ? 0 : jet_pfcand_dz->at(icand));
	    jet_pfcand_neu_pperp_ratio_b.push_back(std::isnan(jet_pfcand_pperp_ratio->at(icand)) ? 0 : jet_pfcand_pperp_ratio->at(icand));
	    jet_pfcand_neu_ppara_ratio_b.push_back(std::isnan(jet_pfcand_ppara_ratio->at(icand)) ? 0 : jet_pfcand_ppara_ratio->at(icand));
	    jet_pfcand_neu_deta_b.push_back(jet_pfcand_deta->at(icand));
	    jet_pfcand_neu_dphi_b.push_back(jet_pfcand_dphi->at(icand));
	    jet_pfcand_neu_etarel_b.push_back(std::isnan(jet_pfcand_etarel->at(icand)) ? 0 : jet_pfcand_etarel->at(icand));
	    jet_pfcand_neu_frompv_b.push_back(jet_pfcand_frompv->at(icand));
	    jet_pfcand_neu_charge_b.push_back(jet_pfcand_charge->at(icand));
	    jet_pfcand_neu_puppiw_b.push_back(jet_pfcand_puppiw->at(icand));
	  
	    if(jet_pfcand_id->at(icand) == 22 and jet_pfcand_charge->at(icand) == 0)
	      jet_pfcand_neu_id_b.push_back(2);
	    else if(jet_pfcand_id->at(icand) != 22 and jet_pfcand_charge->at(icand) == 0 and jet_pfcand_id->at(icand) != 1 and jet_pfcand_id->at(icand) != 2)
	    jet_pfcand_neu_id_b.push_back(3);
	    else if(jet_pfcand_charge->at(icand) == 0  and jet_pfcand_id->at(icand) == 1)
	      jet_pfcand_neu_id_b.push_back(5);
	    else if(jet_pfcand_charge->at(icand) == 0  and jet_pfcand_id->at(icand) == 2)
	      jet_pfcand_neu_id_b.push_back(6);
	    else
	      jet_pfcand_neu_id_b.push_back(-1);	    

	    jet_pfcand_neu_tau_signal_b.push_back(jet_pfcandidate_tau_signal->at(icand));
	    
	    jet_pfcand_neu_photon_sigIetaIeta_b.push_back(std::isnan(jet_pfcandidate_photon_sigIetaIeta->at(icand)) ? 0 : jet_pfcandidate_photon_sigIetaIeta->at(icand));
	    jet_pfcand_neu_photon_r9_b.push_back(std::isnan(jet_pfcandidate_photon_r9->at(icand)) ? 0 : jet_pfcandidate_photon_r9->at(icand));
	    jet_pfcand_neu_photon_eVeto_b.push_back(std::isnan(jet_pfcandidate_photon_eVeto->at(icand)) ? 0 : jet_pfcandidate_photon_eVeto->at(icand));
	  }
	}

	// take muons
	if(jet_pfcand_id->at(icand) == 11){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  electron4V_fromPF.push_back(tmp4V);
	}
	// take electrons
	if(jet_pfcand_id->at(icand) == 13){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  muon4V_fromPF.push_back(tmp4V);
	}
	// take photons
	if(jet_pfcand_id->at(icand) == 22){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  photon4V_fromPF.push_back(tmp4V);
	}
	// take every tau candidate
	if(jet_pfcandidate_tau_signal->at(icand)){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(jet_pfcand_pt->at(icand),jet_pfcand_eta->at(icand),jet_pfcand_phi->at(icand),jet_pfcand_mass->at(icand));
	  tau4V_fromPF += tmp4V;
	}	
      }

      // Matching with reco tau
      minDR = 1000;
      int pos_matched_reco_tauh = -1;
      for(size_t itau = 0; itau < tau_pt->size(); itau++){
	TLorentzVector tmp4V;
	tmp4V.SetPtEtaPhiM(tau_pt->at(itau),tau_eta->at(itau),tau_phi->at(itau),tau_mass->at(itau));
	float dR = tmp4V.DeltaR(jet4V);
	float dRt = tmp4V.DeltaR(tau4V_fromPF);	      
	if (dR < dRCone and dRt < dRMatchingPF and dRt < minDR){ // small numerical roundoff due to float to double approximation
	  pos_matched_reco_tauh = itau;
	  minDR = dR;
	}
      }
	    
      if(pos_matched_reco_tauh >= 0){
	jet_taumatch_pt_b = tau_pt->at(pos_matched_reco_tauh);
	jet_taumatch_eta_b = tau_eta->at(pos_matched_reco_tauh);
	jet_taumatch_phi_b = tau_phi->at(pos_matched_reco_tauh);
	jet_taumatch_mass_b = tau_mass->at(pos_matched_reco_tauh);
	jet_taumatch_charge_b = tau_charge->at(pos_matched_reco_tauh);
	jet_taumatch_decaymode_b = tau_decaymode->at(pos_matched_reco_tauh);
	jet_taumatch_idjet_b = tau_idjet->at(pos_matched_reco_tauh);
	jet_taumatch_idele_b = tau_idele->at(pos_matched_reco_tauh);
	jet_taumatch_idmu_b  = tau_idmu->at(pos_matched_reco_tauh);
	jet_taumatch_dz_b = tau_dz->at(pos_matched_reco_tauh);
	jet_taumatch_dxy_b = tau_dxy->at(pos_matched_reco_tauh);
	jet_taumatch_idjet_wp_b = tau_idjet_wp->at(pos_matched_reco_tauh);
	jet_taumatch_idele_wp_b = tau_idele_wp->at(pos_matched_reco_tauh);
	jet_taumatch_idmu_wp_b = tau_idmu_wp->at(pos_matched_reco_tauh);
      }
      else{
	jet_taumatch_pt_b  = -1;
	jet_taumatch_eta_b = -1;
	jet_taumatch_phi_b = -1;
	jet_taumatch_mass_b = -1;
	jet_taumatch_idjet_b = -1;
	jet_taumatch_idele_b = -1;
	jet_taumatch_idjet_b = -1;
	jet_taumatch_decaymode_b = -1;
	jet_taumatch_charge_b = -1;
	jet_taumatch_dz_b = -1;
	jet_taumatch_dxy_b = -1;
	jet_taumatch_idjet_wp_b = -1;
	jet_taumatch_idele_wp_b = -1;
	jet_taumatch_idmu_wp_b = -1;
      }

      // Matching with muon by considering only the highest pt one
      std::sort(muon4V_fromPF.begin(),muon4V_fromPF.end(),
		[&](const TLorentzVector & a, const TLorentzVector & b){
		  return a.Pt() > b.Pt();
		});

      minDR = 1000;
      int pos_matched_reco_mu = -1;
      if(not muon4V_fromPF.empty()){
	for(size_t imu = 0; imu < muon_pt->size(); imu++){
	  TLorentzVector tmp4V;
	  tmp4V.SetPtEtaPhiM(muon_pt->at(imu),muon_eta->at(imu),muon_phi->at(imu),muon_mass->at(imu));
	  float dR = tmp4V.DeltaR(jet4V);
	  float dRm = muon4V_fromPF.front().DeltaR(tmp4V);
	  if(dR < dRCone and dRm < dRMatchingPF and dRm < minDR){
	    pos_matched_reco_mu = imu;
	    minDR = dRm;	      
	  }
	}
      }
      if(pos_matched_reco_mu >= 0){
	jet_mumatch_pt_b = muon_pt->at(pos_matched_reco_mu);
	jet_mumatch_eta_b = muon_eta->at(pos_matched_reco_mu);
	jet_mumatch_phi_b = muon_phi->at(pos_matched_reco_mu);
	jet_mumatch_mass_b = muon_mass->at(pos_matched_reco_mu);
	jet_mumatch_energy_b = muon_energy->at(pos_matched_reco_mu);
	jet_mumatch_dxy_b = muon_d0->at(pos_matched_reco_mu);
	jet_mumatch_dz_b = muon_dz->at(pos_matched_reco_mu);
	jet_mumatch_id_b = muon_id->at(pos_matched_reco_mu);
	jet_mumatch_iso_b = muon_iso->at(pos_matched_reco_mu);
      }
      else if(not muon4V_fromPF.empty()){
	jet_mumatch_pt_b = muon4V_fromPF.front().Pt();
	jet_mumatch_eta_b = muon4V_fromPF.front().Eta();
	jet_mumatch_phi_b = muon4V_fromPF.front().Phi();
	jet_mumatch_mass_b = muon4V_fromPF.front().M();		
	jet_mumatch_energy_b = muon4V_fromPF.front().E();		
	jet_mumatch_dxy_b = -1;
	jet_mumatch_dz_b = -1;
	jet_mumatch_id_b = -1;
	jet_mumatch_iso_b = -1;
      }
      else{
	jet_mumatch_pt_b = -1;
	jet_mumatch_eta_b = -1;
	jet_mumatch_phi_b = -1;
	jet_mumatch_mass_b = -1;
	jet_mumatch_energy_b = -1;
	jet_mumatch_dxy_b = -1;
	jet_mumatch_dz_b = -1;
	jet_mumatch_id_b = -1;
	jet_mumatch_iso_b = -1;
      }

      // Matching with electron by considering only the highest pt one
      std::sort(electron4V_fromPF.begin(),electron4V_fromPF.end(),
		[&](const TLorentzVector & a, const TLorentzVector & b){
		  return a.Pt() > b.Pt();
		});

      minDR = 1000;
      int pos_matched_reco_ele = -1;
      if(not electron4V_fromPF.empty()){
	for(size_t iele = 0; iele < electron_pt->size(); iele++){
	  TLorentzVector tmp4V;	
	  tmp4V.SetPtEtaPhiM(electron_pt->at(iele),electron_eta->at(iele),electron_phi->at(iele),electron_mass->at(iele));
	  float dR = tmp4V.DeltaR(jet4V);
	  float dRe = tmp4V.DeltaR(electron4V_fromPF.front());
	  if (dR < dRCone and dRe < minDR and dRe < dRMatchingPF){ // small numerical roundoff due to float to double approximation
	    pos_matched_reco_ele = iele;
	    minDR = dR;
	  }
	}
      }
      
      if(pos_matched_reco_ele >= 0){
	jet_elematch_pt_b = electron_pt->at(pos_matched_reco_ele);
	jet_elematch_eta_b = electron_eta->at(pos_matched_reco_ele);
	jet_elematch_phi_b = electron_phi->at(pos_matched_reco_ele);
	jet_elematch_mass_b = electron_mass->at(pos_matched_reco_ele);
	jet_elematch_energy_b = electron_energy->at(pos_matched_reco_ele);
	jet_elematch_dxy_b = electron_d0->at(pos_matched_reco_ele);
	jet_elematch_dz_b = electron_dz->at(pos_matched_reco_ele);
	jet_elematch_id_b = electron_id->at(pos_matched_reco_ele);
	jet_elematch_idscore_b = electron_idscore->at(pos_matched_reco_ele);
      }
      else if(not electron4V_fromPF.empty()){
	jet_elematch_pt_b = electron4V_fromPF.front().Pt();
	jet_elematch_eta_b = electron4V_fromPF.front().Eta();
	jet_elematch_phi_b = electron4V_fromPF.front().Phi();
	jet_elematch_mass_b = electron4V_fromPF.front().M();		
	jet_elematch_energy_b = electron4V_fromPF.front().E();		
	jet_elematch_dxy_b = -1;
	jet_elematch_dz_b = -1;
	jet_elematch_id_b = -1;
	jet_elematch_idscore_b = -1;
      }
      else{
	jet_elematch_pt_b = -1;
	jet_elematch_eta_b = -1;
	jet_elematch_phi_b = -1;
	jet_elematch_mass_b = -1;
	jet_elematch_energy_b = -1;
	jet_elematch_dxy_b = -1;
	jet_elematch_dz_b = -1;
	jet_elematch_id_b = -1;
	jet_elematch_idscore_b = -1;
      }

      // Matching with photon by considering only the highest pt one
      std::sort(photon4V_fromPF.begin(),photon4V_fromPF.end(),
		[&](const TLorentzVector & a, const TLorentzVector & b){
		  return a.Pt() > b.Pt();
		});

      minDR = 1000;
      int pos_matched_reco_pho = -1;
      if(not photon4V_fromPF.empty()){
	for(size_t ipho = 0; ipho < photon_pt->size(); ipho++){
	  TLorentzVector tmp4V;	
	  tmp4V.SetPtEtaPhiM(photon_pt->at(ipho),photon_eta->at(ipho),photon_phi->at(ipho),photon_mass->at(ipho));
	  float dR = tmp4V.DeltaR(jet4V);
	  float dRe = tmp4V.DeltaR(photon4V_fromPF.front());
	  if (dR < dRCone and dRe < minDR and dRe < dRMatchingPF){ // small numerical roundoff due to float to double approximation
	    pos_matched_reco_pho = ipho;
	    minDR = dR;
	  }
	}
      }
      
      if(pos_matched_reco_pho >= 0){
	jet_phomatch_pt_b = photon_pt->at(pos_matched_reco_pho);
	jet_phomatch_eta_b = photon_eta->at(pos_matched_reco_pho);
	jet_phomatch_phi_b = photon_phi->at(pos_matched_reco_pho);
	jet_phomatch_mass_b = photon_mass->at(pos_matched_reco_pho);
	jet_phomatch_energy_b = photon_energy->at(pos_matched_reco_pho);
	jet_phomatch_id_b = photon_id->at(pos_matched_reco_pho);
	jet_phomatch_idscore_b = photon_idscore->at(pos_matched_reco_pho);
      }
      else if(not photon4V_fromPF.empty()){
	jet_phomatch_pt_b = photon4V_fromPF.front().Pt();
	jet_phomatch_eta_b = photon4V_fromPF.front().Eta();
	jet_phomatch_phi_b = photon4V_fromPF.front().Phi();
	jet_phomatch_mass_b = photon4V_fromPF.front().M();		
	jet_phomatch_energy_b = photon4V_fromPF.front().E();		
	jet_phomatch_id_b = -1;
	jet_phomatch_idscore_b = -1;
      }
      else{
	jet_phomatch_pt_b = -1;
	jet_phomatch_eta_b = -1;
	jet_phomatch_phi_b = -1;
	jet_phomatch_mass_b = -1;
	jet_phomatch_energy_b = -1;
	jet_phomatch_id_b = -1;
	jet_phomatch_idscore_b = -1;
      }
      tree_out->Fill();
    }
  }
  std::cout<<"ntupleCreation --> thread "<<workerID<<" stopping "<<std::endl;
};


// Main function
int main(int argc, char **argv){

  boost::program_options::options_description desc("Main options");
  desc.add_options()
    ("inputFileList,i", boost::program_options::value<std::string>(&inputFileList)->default_value(""), "File that contains a list of ROOT files to be processed")
    ("inputFileDIR,d", boost::program_options::value<std::string>(&inputFileDIR)->default_value(""), "Directory that contained files to process")
    ("outputDIR,o", boost::program_options::value<std::string>(&outputDIR)->default_value(""), "Output directory where files need to be created")
    ("outputFileName,f", boost::program_options::value<std::string>(&outputFileName)->default_value(""), "Base name for the output file")
    ("nThreads,n", boost::program_options::value<unsigned int>(&nThreads)->default_value(1), "Number of threads to be used")
    ("maxNumberOfFilesToBeProcessed,max", boost::program_options::value<unsigned int>(&maxNumberOfFilesToBeProcessed)->default_value(1), "When running giving an input directory, split execution in chunks of Nfiles")
    ("mergeThreadOutputFiles,merge", boost::program_options::value<bool>(&mergeThreadOutputFiles)->default_value(false), "Merge the output files of the job into a single one")
    ("jetPtMin,jptmin", boost::program_options::value<float>(&jetPtMin)->default_value(30), "Min jet pT to apply")
    ("jetEtaMax,jetamax", boost::program_options::value<float>(&jetEtaMax)->default_value(2.5), "Max jet eta to apply")
    ("jetEtaMin,jetamin", boost::program_options::value<float>(&jetEtaMin)->default_value(0.), "Min jet eta to apply")
    ("pfCandPtMin,pfptmin", boost::program_options::value<float>(&pfCandPtMin)->default_value(0.), "Minimum pf candidate pt")
    ("losttrackPtMin,ltrkptmin", boost::program_options::value<float>(&losttrackPtMin)->default_value(1.), "Minimum lost track pt")
    ("pfCandPuppiWeightMin,puppiwmin", boost::program_options::value<float>(&pfCandPuppiWeightMin)->default_value(0.), "Minimum pf candidate puppi weight")
    ("sample-type,s", boost::program_options::value<std::string>(&sample_name)->default_value(""), "Type of physics sample given as string")
    ("domain-type,d", boost::program_options::value<std::string>(&domain_name)->default_value(""), "Type of domain region")
    ("saveOnlyGenMatchedJets,genonly", boost::program_options::value<bool>(&saveOnlyGenMatchedJets)->default_value(true), "Save only jets matched to GEN")
    ("applyJetID,jetid", boost::program_options::value<bool>(&applyJetID)->default_value(false), "Apply or not the jetid")
    ("selectOnParTScore,part", boost::program_options::value<bool>(&selectOnParTScore)->default_value(false), "Select on parT score instead of PNET")
    ("useXRootD,x", boost::program_options::value<bool>(&useXRootD)->default_value(true), "Read files via xrootd protocol instead of local-mount")
    ("splitChargedPF,x", boost::program_options::value<bool>(&splitChargedPF)->default_value(false), "Split charge and neutrals PFs into two independet branches")
    ("compressOutputFile,c", boost::program_options::value<bool>(&compressOutputFile)->default_value(false), "Compress output file to save space")
    ("help,h", "Produce help message interface");

  boost::program_options::variables_map options;
  
  try{
    boost::program_options::store(boost::program_options::command_line_parser(argc,argv).options(desc).run(),options);
    boost::program_options::notify(options);
  }
  catch(std::exception &ex) {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cerr << "Use makeSkimmedNtuplesForTraining --help to get a list of all the allowed options"  << std::endl;
    return 999;
  } 
  catch(...) {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if(options.count("help")) {
    std::cout << "Usage: makeSkimmedNtuplesForTraining [options]\n";
    std::cout << desc;
    return 0;
  }

  // Conversion from string to sample type
  sample = convertSampleType(sample_name);
  domain = convertDomainType(domain_name);
  
  std::cout<<"makeSkimmedNtuplesForTraining.cpp Parameter Summary"<<std::endl;
  std::cout<<"inputFileList --> "<<inputFileList<<std::endl;
  std::cout<<"inputFileDIR --> "<<inputFileDIR<<std::endl;
  std::cout<<"outputDIR --> "<<outputDIR<<std::endl;
  std::cout<<"outputFileName --> "<<outputFileName<<std::endl;
  std::cout<<"nThreads --> "<<nThreads<<std::endl;
  std::cout<<"maxNumberOfFilesToBeProcessed --> "<<maxNumberOfFilesToBeProcessed<<std::endl;
  std::cout<<"mergeThreadOutputFiles --> "<<mergeThreadOutputFiles<<std::endl;
  std::cout<<"jetPtMin --> "<<jetPtMin<<std::endl;
  std::cout<<"jetEtaMax --> "<<jetEtaMax<<std::endl;
  std::cout<<"jetEtaMin --> "<<jetEtaMin<<std::endl;
  std::cout<<"pfCandPtMin --> "<<pfCandPtMin<<std::endl;
  std::cout<<"losttrackPtMin --> "<<losttrackPtMin<<std::endl;
  std::cout<<"pfCandPuppiWeightMin --> "<<pfCandPuppiWeightMin<<std::endl;
  std::cout<<"sample_type --> "<<static_cast<int>(sample)<<std::endl;
  std::cout<<"domain_type --> "<<static_cast<int>(domain)<<std::endl;
  std::cout<<"isdata --> "<<((sample == sample_type::data) ? 1 : 0)<<std::endl;
  std::cout<<"saveOnlyGenMatchedJets --> "<<saveOnlyGenMatchedJets<<std::endl;
  std::cout<<"applyJetID --> "<<applyJetID<<std::endl;
  std::cout<<"selectOnParTScore --> "<<selectOnParTScore<<std::endl;
  std::cout<<"useXRootD --> "<<useXRootD<<std::endl;
  std::cout<<"splitChargedPF --> "<<splitChargedPF<<std::endl;
  std::cout<<"compressOutputFile --> "<<compressOutputFile<<std::endl;

  if(not inputFileList.empty() and not inputFileDIR.empty()){
    std::cerr<<"You cannot set inputFileList and inputFileDIR to be simultaneously non empty --> please either provide a directory or a file list"<<std::endl;
    return 1001;
  }

  gSystem->Exec(("mkdir -p "+outputDIR).c_str());

  // Prepare for multi-threading
  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();

  std::string xrootd_eos = std::string(getenv("EOS_MGM_URL"))+"//";

  // Prepare the workflow
  if(not inputFileList.empty() and boost::filesystem::is_regular_file(inputFileList)){ // the input string is a file containing the list of ROOT files to process --> for batch mode

    std::cout<<"Run based on inputFileList "<<std::endl;
    
    std::vector<std::string> fileList;
    std::vector<std::shared_ptr<TFile> >  files_out;
    std::vector<std::shared_ptr<TTree> >  trees_out;      

    std::ifstream inputFile (inputFileList);
    std::cout<<"input file list "<<inputFileList<<std::endl;
    if(inputFile.is_open()){
      std::string line;
      while (getline(inputFile,line)) {
	if(useXRootD)
	  fileList.push_back(xrootd_eos+line);
	else
	  fileList.push_back(line);
      }
      inputFile.close();
    }

    
    for(size_t ithread = 0; ithread < nThreads; ithread++){
      files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_thread%zu.root",ithread)).Data()),"RECREATE"));
      trees_out.emplace_back(new TTree("tree","tree"));
      if(not compressOutputFile){
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	files_out.back()->SetCompressionLevel(4);
      }
      else{
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	files_out.back()->SetCompressionLevel(6);	
      }
    }      
    
    // Count number of events to split in threads
    std::cout<<"Count total number of events to process .."<<std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::unique_ptr<TChain> tree_in (new TChain("dnntree/tree","dnntree/tree"));
    std::cout<<"file list size "<<fileList.size()<<std::endl;
    for(size_t ifile = 0; ifile < fileList.size(); ifile++){
      std::cout<<"file list "<<fileList.at(ifile)<<std::endl;
      tree_in->Add(fileList.at(ifile).c_str());}
    long int nevents = tree_in->GetEntries();
    auto stop  = std::chrono::high_resolution_clock::now(); 
    std::chrono::duration<float> duration = stop-start;
    std::cout<<"Duration of counting loop is "<<duration.count()<<std::endl;

    // operation/function that will be executed in parallel  
    std::cout<<"Event loop to build output tree .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::vector<std::thread> threads;
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(ntupleCreation, workerID, fileList, trees_out, nevents, nThreads);
    for (auto && worker : threads) worker.join();
    stop  = std::chrono::high_resolution_clock::now(); 
    duration = stop-start;
    std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
    if(domain == domain_type::none){
      std::cout<<"Jets rejected by base cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by GEN pt requirement "<<nJetsRejectedLowGenLepton<<" fraction "<<float(nJetsRejectedLowGenLepton)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
    }
    else{
      std::cout<<"Events rejected by domain cuts "<<nEventsRejectedDomainKinematics<<" fraction "<<float(nEventsRejectedDomainKinematics)/float(nevents)<<std::endl;
      std::cout<<"Jets rejected by domain cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
      if(domain == domain_type::emu)
	std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedEMu<<" fraction "<<float(nJetsRejectedEMu)/float(nJetsTotal)<<std::endl;
      else if(domain == domain_type::mutau or domain == domain_type::etau)
	std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedMuTau<<" fraction "<<float(nJetsRejectedMuTau)/float(nJetsTotal)<<std::endl;
      else if(domain == domain_type::ttcharm)
	std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedTTcharm<<" fraction "<<float(nJetsRejectedTTcharm)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
    }
    threads.clear();
    
    std::cout<<"Writing the outout files .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::string name_list = "";
    for(size_t ifile = 0; ifile < files_out.size(); ifile++){
      files_out.at(ifile)->cd();
      name_list += files_out.at(ifile)->GetName();
      name_list += " ";
      trees_out.at(ifile)->Write("",TObject::kOverwrite);
    }
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;

    if(mergeThreadOutputFiles and files_out.size() > 1){
      std::cout<<"Merging outout files .."<<std::endl;
      std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
      gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
      std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
      std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
      unsigned int total_entries = tree->GetEntries();
      gSystem->Exec(("rm "+name_list).c_str());
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
    }   
  }
  else if(not inputFileDIR.empty() and boost::filesystem::is_directory(inputFileDIR)){ // pathInput is a real path to a directory

    std::cout<<"Build list of files in "<<inputFileDIR<<std::endl;

    std::vector<std::string> fileList;

    for(auto const & entry :  boost::filesystem::recursive_directory_iterator(inputFileDIR)){
      
      TString filePath (entry.path().string());
      if(filePath.Contains(".root")){
	if(useXRootD)
	  fileList.push_back(xrootd_eos+entry.path().string());
	else
	  fileList.push_back(entry.path().string());
      }
    }
    
    // split the job into n-steps (blocks) each containing n-files 
    unsigned int numberOfFiles = fileList.size();
    unsigned int numberOfBlocks = 1;  
    if(numberOfFiles > maxNumberOfFilesToBeProcessed){
      if(numberOfFiles%maxNumberOfFilesToBeProcessed == 0)
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed;
      else
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed+1;
    }

    std::cout<<"In iterative mode --> Number of files "<<numberOfFiles<<" numberOfBlocks "<<numberOfBlocks<<std::endl;

    // Loop on the blocks
    for(unsigned int iblock = 0; iblock < numberOfBlocks; iblock++){          
      std::vector<std::string> fileList_block;
      if(maxNumberOfFilesToBeProcessed == 1){
	for(size_t ifile = 0; ifile < fileList.size(); ifile++)
	  fileList_block.push_back(fileList.at(ifile));      
      }
      else{
	for(size_t ifile = iblock*maxNumberOfFilesToBeProcessed; ifile < std::min(fileList.size(),size_t((iblock+1)*maxNumberOfFilesToBeProcessed)); ifile++)
	  fileList_block.push_back(fileList.at(ifile));
      }    
      std::cout<<"Block number "<<iblock<<" number of files "<<fileList_block.size()<<std::endl;
      
      // Build job list and output files --> minimum compression to speed-up the training hjob
      std::vector<std::shared_ptr<TFile> >  files_out;
      std::vector<std::shared_ptr<TTree> >  trees_out;      
      for(unsigned int ithread = 0; ithread < nThreads; ithread++){
	files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_block%d_thread%d.root",iblock,ithread)).Data()),"RECREATE"));
	trees_out.emplace_back(new TTree("tree","tree"));
	if(not compressOutputFile){
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	  files_out.back()->SetCompressionLevel(4);
	}
	else{
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	  files_out.back()->SetCompressionLevel(6);	  
	}
      }      

      // Count number of events to split in threads
      std::cout<<"Count total number of events to process .."<<std::endl;
      auto start = std::chrono::high_resolution_clock::now();
      std::unique_ptr<TChain> tree_in (new TChain("dnntree/tree","dnntree/tree"));
      for(size_t ifile = 0; ifile < fileList_block.size(); ifile++)
	tree_in->Add(fileList_block.at(ifile).c_str());
      long int nevents = tree_in->GetEntries();
      auto stop  = std::chrono::high_resolution_clock::now(); 
      std::chrono::duration<float> duration = stop-start;
      std::cout<<"Duration of counting loop is "<<duration.count()<<std::endl;
      
      nJetsTotal = 0;
      nJetsTraining = 0;
      nJetsRejectedLowGenLepton = 0;
      nEventsRejectedDomainKinematics = 0;
      nJetsRejectedBaseCuts = 0;
      nJetsRejectedEMu = 0;
      nJetsRejectedMuTau = 0;
      nJetsRejectedTTcharm = 0;
      
      // operation/function that will be executed in parallel  
      start = std::chrono::high_resolution_clock::now();
      std::vector<std::thread> threads;
      for (auto workerID : ROOT::TSeqI(nThreads))
	threads.emplace_back(ntupleCreation, workerID, fileList_block, trees_out, nevents, nThreads);
      for (auto && worker : threads) 
	worker.join();
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
      if(domain == domain_type::none){
	std::cout<<"Jets rejected by base cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
	std::cout<<"Jets rejected by GEN pt requirement "<<nJetsRejectedLowGenLepton<<" fraction "<<float(nJetsRejectedLowGenLepton)/float(nJetsTotal)<<std::endl;
	std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
      }
      else{
	std::cout<<"Events rejected by domain cuts "<<nEventsRejectedDomainKinematics<<" fraction "<<float(nEventsRejectedDomainKinematics)/float(nevents)<<std::endl;
	std::cout<<"Jets rejected by domain cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
	if(domain == domain_type::emu)
	  std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedEMu<<" fraction "<<float(nJetsRejectedEMu)/float(nJetsTotal)<<std::endl;
	else if(domain == domain_type::mutau or domain == domain_type::etau)
	  std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedMuTau<<" fraction "<<float(nJetsRejectedMuTau)/float(nJetsTotal)<<std::endl;
	else if(domain == domain_type::ttcharm)
	  std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedTTcharm<<" fraction "<<float(nJetsRejectedTTcharm)/float(nJetsTotal)<<std::endl;
	std::cout<<"Jets accepted for the training "<<nJetsTraining<<" fraction "<<float(nJetsTraining)/float(nJetsTotal)<<std::endl;
      }
      threads.clear();      

      std::cout<<"Writing the outout files .."<<std::endl;
      start = std::chrono::high_resolution_clock::now();
      std::string name_list = "";
      for(size_t ifile = 0; ifile < files_out.size(); ifile++){
	files_out.at(ifile)->cd();
	name_list += files_out.at(ifile)->GetName();
	name_list += " ";
	trees_out.at(ifile)->Write("",TObject::kOverwrite);
      }
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;
      
      if(mergeThreadOutputFiles and files_out.size() > 1){
	std::cout<<"Merging outout files .."<<std::endl;
	std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
	gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
	std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
	std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
	unsigned int total_entries = tree->GetEntries();
	gSystem->Exec(("rm "+name_list).c_str());
	stop  = std::chrono::high_resolution_clock::now(); 
	duration = stop-start;	
	std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
      } 
    }
  }
  std::cout<<"Exiting from the code"<<std::endl;
}
