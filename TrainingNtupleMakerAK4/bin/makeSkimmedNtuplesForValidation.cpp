#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <thread>
#include <atomic>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#include "TFile.h"
#include "TChain.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TString.h"
#include "TTreeReader.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "ROOT/TSeq.hxx"
#include "Compression.h"

/// thresholds for matching                                                                                                                                                           
static float dRCone        = 0.2;
static float ptGenLeptonMin = 8;
static float ptGenTauVisibleMin = 15;
// Dilepton domain
static float minLep1PtDilepton = 28;
static float minLep2PtDilepton = 20;
static unsigned int maxNjetDilepton = 2;
static unsigned int minNjetDilepton = 1;
static float maxMET = 80;
// emu domain
static float minLep1PtEMu = 28;
static float minLep2PtEMu = 20;
static unsigned int maxNjetEMu = 99;
static unsigned int minNjetEMu = 2;
static float minPBVsJet = 0.10;
// Dijet domain
static float minLep1PtDijet = 45;
static float minLep2PtDijet = 35;
static unsigned int maxNjetDijet = 99;
static unsigned int minNjetDijet = 2;
// ttcharm
static float minLep1PtTTcharm = 28;
static float minLep2PtTTcharm = 30;
static unsigned int maxNjetTTcharm = 99;
static unsigned int minNjetTTcharm = 4;
static float minPCVsJet = 0.50;
// mutau
static float minLep1PtMuTau = 28;
static float minLep2PtMuTau = 20;
static float minTauPt = 20;
static float maxTauEta = 2.5;
static float minMassVisible = 55;
static float maxMassVisible = 85;
static float minTransverseMass = 50;
static unsigned int maxNjetMuTau = 2;
static unsigned int minNjetMuTau = 1;
static bool  matchHPSTauCandidate = false;
static float minPTauVsJet = 0.90;
static float minPTauVsMu  = 0.98;
static float minPTauVsEle = 0.80;
// jet to be saved for domain
static unsigned int maxNjetDileptonToStore = 2;
static unsigned int maxNjetEMuToStore = 2;
static unsigned int maxNjetMuTauToStore = 1;
static unsigned int maxNjetDijetToStore = 2;
static unsigned int maxNjetTTcharmToStore = 1;
/// atomic counters
std::atomic_uint nJetsTotal = 0;
std::atomic_uint nJetsRejectedBaseCuts = 0;
std::atomic_uint nJetsRejectedMuTau = 0;
std::atomic_uint nJetsRejectedEMu = 0;
std::atomic_uint nJetsRejectedTTcharm = 0;
std::atomic_uint nJetsRejectedLowGenLepton = 0;
std::atomic_uint nEventsRejectedDomainKinematics = 0;
std::atomic_uint nJetsValidation = 0;

/// sample type definition
enum class sample_type {none=-1,qcd=0,dy=1,wjet=2,ttbar=3,ggH=4,vbfH=5,VH=6,ggHH=7,bulkG=8,data=9};

sample_type convertSampleType (const std::string & sample_name){
  if(sample_name == "sample_type::none") return sample_type::none;
  else if(sample_name == "sample_type::qcd") return sample_type::qcd;
  else if(sample_name == "sample_type::dy") return sample_type::dy;
  else if(sample_name == "sample_type::wjet") return sample_type::wjet;
  else if(sample_name == "sample_type::ttbar") return sample_type::ttbar;
  else if(sample_name == "sample_type::ggH") return sample_type::ggH;
  else if(sample_name == "sample_type::vbfH") return sample_type::vbfH;
  else if(sample_name == "sample_type::VH") return sample_type::VH;
  else if(sample_name == "sample_type::ggHH") return sample_type::ggHH;
  else if(sample_name == "sample_type::bulkG") return sample_type::bulkG;
  else if(sample_name == "sample_type::data") return sample_type::data;
  else return sample_type::none;
}

// Domain definition                                                                                                                                                                
enum class domain_type {none=-1,dimuon=0,dielectron=1,emu=2,mutau=3,etau=4,dijet=5,ttcharm=6};

domain_type convertDomainType (const std::string & domain_name){
  if(domain_name == "domain_type::none") return domain_type::none;
  else if(domain_name == "domain_type::dimuon") return domain_type::dimuon;
  else if(domain_name == "domain_type::dielectron") return domain_type::dielectron;
  else if(domain_name == "domain_type::emu") return domain_type::emu;
  else if(domain_name == "domain_type::mutau") return domain_type::mutau;
  else if(domain_name == "domain_type::etau") return domain_type::etau;
  else if(domain_name == "domain_type::dijet") return domain_type::dijet;
  else if(domain_name == "domain_type::ttcharm") return domain_type::ttcharm;
  else return domain_type::none;
}

// Delta phi between objects
float deltaPhi (float phi1, float phi2){
  if(fabs(phi1-phi2) < TMath::Pi())
    return fabs(phi1-phi2);
  else
    return 2*TMath::Pi()-fabs(phi1-phi2);
}

/// Global parameters that are parsed from command line
std::string inputFileList;
std::string inputFileDIR;
std::string outputDIR;
std::string outputFileName;
unsigned int nThreads;
unsigned int maxNumberOfFilesToBeProcessed;
std::vector<std::string> pnetLabels;
std::vector<std::string> parTLabels;
std::string pnetLabelString;
std::string parTLabelString;
bool mergeThreadOutputFiles;
float jetPtMin;
float jetEtaMax;
float jetEtaMin;
sample_type sample;
std::string sample_name ;
domain_type domain;
std::string domain_name ;
bool saveOnlyGenMatchedJets;
bool applyJetID;
bool selectOnParTScore;
bool useXRootD;
bool compressOutputFile;

/// creation of output tree
void ntupleCreation (const int & workerID, 
		     const std::vector<std::string> & fileList, 
		     const std::vector<std::shared_ptr<TTree> > & trees_out, 
		     const int & nevents, 
		     const std::atomic_int & nthreads
		     ){
  
  // edit the input labels
  std::vector<std::string> suffix_pnet;
  for (auto const & ipnet : pnetLabels){
    suffix_pnet.push_back("_"+ipnet);
  }
  
  // edit the input labels
  std::vector<std::string> suffix_parT;
  for (auto const & ipnet : parTLabels){
    suffix_parT.push_back("_"+ipnet);
  }

  
  std::unique_ptr<TChain> tree_in(new TChain("dnntree/tree","dnntree/tree"));
  for(size_t ifile = 0; ifile < fileList.size(); ifile++)
    tree_in->Add(fileList.at(ifile).c_str());
  auto tree_out = trees_out.at(workerID);

  TTreeReader reader(tree_in.get());

  TTreeReaderValue<unsigned int> run   (reader,"run");
  TTreeReaderValue<unsigned int> lumi  (reader,"lumi");
  TTreeReaderValue<unsigned int> event (reader,"event");

  TTreeReaderValue<unsigned int>* putrue = NULL;
  TTreeReaderValue<float>*        wgt = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_pt = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_eta = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_phi = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_mass = NULL;
  TTreeReaderValue<std::vector<int> >*    gen_particle_id = NULL;
  TTreeReaderValue<std::vector<unsigned int> >* gen_particle_daughters_igen = NULL;
  TTreeReaderValue<std::vector<unsigned int> >* gen_particle_daughters_status = NULL;
  TTreeReaderValue<std::vector<int> >*    gen_particle_daughters_charge = NULL;
  TTreeReaderValue<std::vector<int> >*    gen_particle_daughters_id = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_pt = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_eta = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_phi = NULL;
  TTreeReaderValue<std::vector<float> >*  gen_particle_daughters_mass = NULL;
  
  if(not (sample == sample_type::data)){
    putrue = new TTreeReaderValue<unsigned int>(reader,"putrue");    
    wgt = new TTreeReaderValue<float>(reader,"wgt");
    gen_particle_pt  = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_pt");
    gen_particle_eta = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_eta");
    gen_particle_phi = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_phi");
    gen_particle_mass = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_mass");
    gen_particle_id  = new TTreeReaderValue<std::vector<int> > (reader,"gen_particle_id");
    gen_particle_daughters_igen = new TTreeReaderValue<std::vector<unsigned int> > (reader,"gen_particle_daughters_igen");
    gen_particle_daughters_status = new TTreeReaderValue<std::vector<unsigned int> > (reader,"gen_particle_daughters_status");
    gen_particle_daughters_charge = new TTreeReaderValue<std::vector<int> > (reader,"gen_particle_daughters_charge");
    gen_particle_daughters_id = new TTreeReaderValue<std::vector<int> > (reader,"gen_particle_daughters_id");
    gen_particle_daughters_pt = new TTreeReaderValue<std::vector<float> >  (reader,"gen_particle_daughters_pt");
    gen_particle_daughters_eta = new TTreeReaderValue<std::vector<float> >  (reader,"gen_particle_daughters_eta");
    gen_particle_daughters_phi = new TTreeReaderValue<std::vector<float> > (reader,"gen_particle_daughters_phi");
    gen_particle_daughters_mass = new TTreeReaderValue<std::vector<float> >  (reader,"gen_particle_daughters_mass");
  }
  
  TTreeReaderValue<float> rho (reader,"rho");
  TTreeReaderValue<unsigned int> npv (reader,"npv");
  TTreeReaderValue<unsigned int> nsv (reader,"nsv");
  TTreeReaderValue<float> met (reader,"met");
  TTreeReaderValue<float> met_phi (reader,"met_phi");

  TTreeReaderValue<std::vector<float> > *leppair_pt1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_eta1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_phi1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_mass1 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_pt2 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_eta2 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_phi2 = NULL;
  TTreeReaderValue<std::vector<float> > *leppair_mass2 = NULL;
  TTreeReaderValue<unsigned int > *flags = NULL;

  if(domain != domain_type::none){
    flags = new TTreeReaderValue<unsigned int > (reader,"flags");
    leppair_pt1 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_pt1");
    leppair_eta1 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_eta1");
    leppair_phi1 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_phi1");
    leppair_mass1 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_mass1");
    leppair_pt2 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_pt2");
    leppair_eta2 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_eta2");
    leppair_phi2 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_phi2");
    leppair_mass2 = new TTreeReaderValue<std::vector<float> >(reader,"leppair_mass2");
  }

  TTreeReaderValue<std::vector<float> > jet_pt (reader,"jet_pt");
  TTreeReaderValue<std::vector<float> > jet_pt_raw (reader,"jet_pt_raw");
  TTreeReaderValue<std::vector<float> > jet_eta (reader,"jet_eta");
  TTreeReaderValue<std::vector<float> > jet_phi (reader,"jet_phi");
  TTreeReaderValue<std::vector<float> > jet_mass (reader,"jet_mass");
  TTreeReaderValue<std::vector<float> > jet_mass_raw (reader,"jet_mass_raw");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probb (reader,"jet_deepjet_probb");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probbb (reader,"jet_deepjet_probbb");
  TTreeReaderValue<std::vector<float> > jet_deepjet_problepb (reader,"jet_deepjet_problepb");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probc (reader,"jet_deepjet_probc");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probuds (reader,"jet_deepjet_probuds");
  TTreeReaderValue<std::vector<float> > jet_deepjet_probg (reader,"jet_deepjet_probg");
  TTreeReaderValue<std::vector<float> > jet_pnet_probb (reader,"jet_pnet_probb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probbb (reader,"jet_pnet_probbb");
  TTreeReaderValue<std::vector<float> > jet_pnet_probc (reader,"jet_pnet_probc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probcc (reader,"jet_pnet_probcc");
  TTreeReaderValue<std::vector<float> > jet_pnet_probuds (reader,"jet_pnet_probuds");
  TTreeReaderValue<std::vector<float> > jet_pnet_probg (reader,"jet_pnet_probg");

  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probb;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probc;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probuds;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probg;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probmu;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probele;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaup1h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaup1h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaup1h2p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaup3h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaup3h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaum1h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaum1h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaum1h2p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaum3h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_probtaum3h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_ptcorr;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_ptreshigh;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_pnetlast_ptreslow;

  for (size_t ipnet = 0; ipnet < suffix_pnet.size(); ipnet++){
    jet_pnetlast_probb.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probb").c_str()));
    jet_pnetlast_probc.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probc").c_str()));
    jet_pnetlast_probuds.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probuds").c_str()));
    jet_pnetlast_probg.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probg").c_str()));
    jet_pnetlast_probmu.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probmu").c_str()));
    jet_pnetlast_probele.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probele").c_str()));
    jet_pnetlast_probtaup1h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h0p").c_str()));
    jet_pnetlast_probtaup1h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h1p").c_str()));
    jet_pnetlast_probtaup1h2p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h2p").c_str()));
    jet_pnetlast_probtaup3h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup3h0p").c_str()));
    jet_pnetlast_probtaup3h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup3h1p").c_str()));
    jet_pnetlast_probtaum1h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h0p").c_str()));
    jet_pnetlast_probtaum1h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h1p").c_str()));
    jet_pnetlast_probtaum1h2p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h2p").c_str()));
    jet_pnetlast_probtaum3h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum3h0p").c_str()));   
    jet_pnetlast_probtaum3h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum3h1p").c_str()));
    jet_pnetlast_ptcorr.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptcorr").c_str()));
    jet_pnetlast_ptreshigh.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptreshigh").c_str()));
    jet_pnetlast_ptreslow.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptreslow").c_str()));
  }

  
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probb;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probc;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probuds;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probg;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probmu;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probele;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaup1h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaup1h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaup1h2p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaup3h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaup3h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaum1h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaum1h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaum1h2p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaum3h0p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_probtaum3h1p;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_ptcorr;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_ptreshigh;
  std::vector<TTreeReaderValue<std::vector<float> > > jet_parTlast_ptreslow;

  for (size_t iparT = 0; iparT < suffix_parT.size(); iparT++){
    jet_parTlast_probb.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probb").c_str()));
    jet_parTlast_probc.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probc").c_str()));
    jet_parTlast_probuds.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probuds").c_str()));
    jet_parTlast_probg.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probg").c_str()));
    jet_parTlast_probmu.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probmu").c_str()));
    jet_parTlast_probele.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probele").c_str()));
    jet_parTlast_probtaup1h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h0p").c_str()));
    jet_parTlast_probtaup1h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h1p").c_str()));
    jet_parTlast_probtaup1h2p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h2p").c_str()));
    jet_parTlast_probtaup3h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup3h0p").c_str()));
    jet_parTlast_probtaup3h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup3h1p").c_str()));
    jet_parTlast_probtaum1h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h0p").c_str()));
    jet_parTlast_probtaum1h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h1p").c_str()));
    jet_parTlast_probtaum1h2p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h2p").c_str()));
    jet_parTlast_probtaum3h0p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum3h0p").c_str()));   
    jet_parTlast_probtaum3h1p.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum3h1p").c_str()));
    jet_parTlast_ptcorr.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_ptcorr").c_str()));
    jet_parTlast_ptreshigh.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_ptreshigh").c_str()));
    jet_parTlast_ptreslow.push_back(TTreeReaderValue<std::vector<float> > (reader,("jet_parTlast"+suffix_parT.at(iparT)+"_ptreslow").c_str()));
  }
  				 
  TTreeReaderValue<std::vector<unsigned int> > jet_id (reader,"jet_id");
  TTreeReaderValue<std::vector<unsigned int> > jet_puid (reader,"jet_puid");
  TTreeReaderValue<std::vector<unsigned int> > jet_hflav (reader,"jet_hflav");
  TTreeReaderValue<std::vector<int> > jet_pflav (reader,"jet_pflav");
  TTreeReaderValue<std::vector<unsigned int> > jet_nbhad (reader,"jet_nbhad");
  TTreeReaderValue<std::vector<unsigned int> > jet_nchad (reader,"jet_nchad");    
  TTreeReaderValue<std::vector<unsigned int> > jet_ncand (reader,"jet_ncand");
  TTreeReaderValue<std::vector<float> > jet_chf (reader,"jet_chf");
  TTreeReaderValue<std::vector<float> > jet_nhf (reader,"jet_nhf");
  TTreeReaderValue<std::vector<float> > jet_elf (reader,"jet_elf");
  TTreeReaderValue<std::vector<float> > jet_phf (reader,"jet_phf");
  TTreeReaderValue<std::vector<float> > jet_muf (reader,"jet_muf");

  TTreeReaderValue<std::vector<float> >* jet_genmatch_pt = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_eta = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_phi = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_mass = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_pt = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_eta = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_phi = NULL;
  TTreeReaderValue<std::vector<float> >* jet_genmatch_wnu_mass = NULL;

  if(not (sample == sample_type::data)){
    jet_genmatch_pt = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_pt");
    jet_genmatch_eta = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_eta");
    jet_genmatch_phi = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_phi");
    jet_genmatch_mass = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_mass");
    jet_genmatch_wnu_pt = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_pt");
    jet_genmatch_wnu_eta = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_eta");
    jet_genmatch_wnu_phi = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_phi");
    jet_genmatch_wnu_mass = new TTreeReaderValue<std::vector<float> > (reader,"jet_genmatch_wnu_mass");
  }

  TTreeReaderValue<std::vector<float> > tau_pt (reader,"tau_pt");
  TTreeReaderValue<std::vector<float> > tau_eta (reader,"tau_eta");
  TTreeReaderValue<std::vector<float> > tau_phi (reader,"tau_phi");
  TTreeReaderValue<std::vector<float> > tau_mass (reader,"tau_mass");
  TTreeReaderValue<std::vector<float> > tau_dz (reader,"tau_dz");
  TTreeReaderValue<std::vector<float> > tau_dxy (reader,"tau_dxy");
  TTreeReaderValue<std::vector<unsigned int> > tau_decaymode (reader,"tau_decaymode");
  TTreeReaderValue<std::vector<int> > tau_charge (reader,"tau_charge");
  TTreeReaderValue<std::vector<float> > tau_idjet (reader,"tau_idjet");
  TTreeReaderValue<std::vector<float> > tau_idele (reader,"tau_idele");
  TTreeReaderValue<std::vector<float> > tau_idmu (reader,"tau_idmu");
  TTreeReaderValue<std::vector<unsigned int> > tau_idjet_wp (reader,"tau_idjet_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_idmu_wp (reader,"tau_idmu_wp");
  TTreeReaderValue<std::vector<unsigned int> > tau_idele_wp (reader,"tau_idele_wp");

  TTreeReaderValue<std::vector<float> > muon_pt (reader,"muon_pt");
  TTreeReaderValue<std::vector<float> > muon_eta (reader,"muon_eta");
  TTreeReaderValue<std::vector<float> > muon_phi (reader,"muon_phi");
  TTreeReaderValue<std::vector<float> > muon_mass (reader,"muon_mass");
  TTreeReaderValue<std::vector<int> > muon_charge (reader,"muon_charge");
  TTreeReaderValue<std::vector<unsigned int> > muon_id (reader,"muon_id");
  TTreeReaderValue<std::vector<unsigned int> > muon_iso (reader,"muon_iso");
  TTreeReaderValue<std::vector<float> > muon_d0 (reader,"muon_d0");
  TTreeReaderValue<std::vector<float> > muon_dz (reader,"muon_dz");

  TTreeReaderValue<std::vector<float> > electron_pt (reader,"electron_pt");
  TTreeReaderValue<std::vector<float> > electron_pt_corr (reader,"electron_pt_corr");
  TTreeReaderValue<std::vector<float> > electron_eta (reader,"electron_eta");
  TTreeReaderValue<std::vector<float> > electron_phi (reader,"electron_phi");
  TTreeReaderValue<std::vector<float> > electron_mass (reader,"electron_mass");
  TTreeReaderValue<std::vector<int> > electron_charge (reader,"electron_charge");
  TTreeReaderValue<std::vector<unsigned int> > electron_id (reader,"electron_id");
  TTreeReaderValue<std::vector<float> > electron_idscore (reader,"electron_idscore");
  TTreeReaderValue<std::vector<float> > electron_d0 (reader,"electron_d0");
  TTreeReaderValue<std::vector<float> > electron_dz (reader,"electron_dz");

  TTreeReaderValue<std::vector<float> > photon_pt (reader,"photon_pt");
  TTreeReaderValue<std::vector<float> > photon_eta (reader,"photon_eta");
  TTreeReaderValue<std::vector<float> > photon_phi (reader,"photon_phi");
  TTreeReaderValue<std::vector<float> > photon_mass (reader,"photon_mass");
  TTreeReaderValue<std::vector<unsigned int> > photon_id (reader,"photon_id");
  TTreeReaderValue<std::vector<float> > photon_idscore (reader,"photon_idscore");

  reader.SetTree(tree_in.get());
  auto beginEntry = (Long64_t) nevents*workerID/nthreads;
  auto endEntry   = (Long64_t) nevents*std::min(nthreads.load(std::memory_order_relaxed),workerID+1)/nthreads-1;
  reader.SetEntriesRange(beginEntry,endEntry);
  std::cout<<"ntupleCreation --> thread "<<workerID<<" starting from entry "<<beginEntry<<" to entry "<<endEntry<<std::endl;

  // output ntupla
  unsigned int run_b, lumi_b, event_b, npv_b, nsv_b, npu_b;
  float wgt_b,rho_b,met_b;
  int sample_b, domain_b, isdata_b;
  unsigned int ijet_b;

  tree_out->Branch("run",&run_b,"run/i"); 
  tree_out->Branch("lumi",&lumi_b,"lumi/i"); 
  tree_out->Branch("event",&event_b,"event/i"); 
  tree_out->Branch("npv",&npv_b,"npv/i"); 
  tree_out->Branch("nsv",&nsv_b,"nsv/i"); 
  tree_out->Branch("npu",&npu_b,"npu/i"); 
  tree_out->Branch("wgt",&wgt_b,"wgt/F"); 
  tree_out->Branch("rho",&rho_b,"rho/F"); 
  tree_out->Branch("met",&met_b,"met/F"); 
  tree_out->Branch("sample",&sample_b,"sample/I");
  tree_out->Branch("domain",&domain_b,"domain/I");
  tree_out->Branch("isdata",&isdata_b,"isdata/I");
  tree_out->Branch("ijet",&ijet_b,"ijet/i");

  float jet_pt_b, jet_pt_raw_b, jet_eta_b,jet_phi_b,jet_mass_b, jet_mass_raw_b, jet_energy_b;
  float jet_deepjet_probb_b,jet_deepjet_probc_b,jet_deepjet_probuds_b,jet_deepjet_probg_b;
  float jet_pnet_probb_b,jet_pnet_probc_b,jet_pnet_probuds_b,jet_pnet_probg_b;
  std::vector<float> jet_pnetlast_probb_b,jet_pnetlast_probc_b,jet_pnetlast_probuds_b,jet_pnetlast_probg_b;
  std::vector<float> jet_pnetlast_probtaup1h0p_b, jet_pnetlast_probtaup1h1p_b, jet_pnetlast_probtaup1h2p_b, jet_pnetlast_probtaup3h0p_b, jet_pnetlast_probtaup3h1p_b;
  std::vector<float> jet_pnetlast_probtaum1h0p_b, jet_pnetlast_probtaum1h1p_b, jet_pnetlast_probtaum1h2p_b, jet_pnetlast_probtaum3h0p_b, jet_pnetlast_probtaum3h1p_b;
  std::vector<float> jet_pnetlast_probele_b, jet_pnetlast_probmu_b, jet_pnetlast_ptcorr_b, jet_pnetlast_ptreshigh_b, jet_pnetlast_ptreslow_b;
  float jet_genmatch_pt_b,jet_genmatch_eta_b,jet_genmatch_phi_b,jet_genmatch_mass_b, jet_genmatch_energy_b;
  float jet_genmatch_wnu_pt_b, jet_genmatch_wnu_eta_b, jet_genmatch_wnu_phi_b, jet_genmatch_wnu_mass_b, jet_genmatch_wnu_energy_b;
  float jet_genmatch_lep_pt_b, jet_genmatch_lep_eta_b, jet_genmatch_lep_phi_b, jet_genmatch_lep_mass_b, jet_genmatch_lep_energy_b;
  float jet_genmatch_lep_vis_pt_b, jet_genmatch_lep_vis_eta_b, jet_genmatch_lep_vis_phi_b, jet_genmatch_lep_vis_mass_b, jet_genmatch_lep_vis_energy_b;
  float jet_genmatch_nu_pt_b, jet_genmatch_nu_eta_b, jet_genmatch_nu_phi_b, jet_genmatch_nu_mass_b, jet_genmatch_nu_energy_b;
  unsigned int jet_ncand_b, jet_nbhad_b, jet_nchad_b, jet_hflav_b;
  float jet_chf_b, jet_nhf_b, jet_phf_b, jet_elf_b, jet_muf_b;
  int jet_pflav_b, jet_tauflav_b, jet_muflav_b, jet_elflav_b, jet_taudecaymode_b, jet_lepflav_b;
  int jet_taucharge_b, jet_mucharge_b, jet_elcharge_b;

  tree_out->Branch("jet_pt",&jet_pt_b,"jet_pt/F");
  tree_out->Branch("jet_pt_raw",&jet_pt_raw_b,"jet_pt_raw/F");
  tree_out->Branch("jet_eta",&jet_eta_b,"jet_eta/F");
  tree_out->Branch("jet_phi",&jet_phi_b,"jet_phi/F");
  tree_out->Branch("jet_mass",&jet_mass_b,"jet_mass/F");
  tree_out->Branch("jet_mass_raw",&jet_mass_raw_b,"jet_mass_raw/F");
  tree_out->Branch("jet_energy",&jet_energy_b,"jet_energy/F");
  tree_out->Branch("jet_deepjet_probb",&jet_deepjet_probb_b,"jet_deepjet_probb/F");
  tree_out->Branch("jet_deepjet_probc",&jet_deepjet_probc_b,"jet_deepjet_probc/F");
  tree_out->Branch("jet_deepjet_probuds",&jet_deepjet_probuds_b,"jet_deepjet_probuds/F");
  tree_out->Branch("jet_deepjet_probg",&jet_deepjet_probg_b,"jet_deepjet_probg/F");
  tree_out->Branch("jet_pnet_probb",&jet_pnet_probb_b,"jet_pnet_probb/F");
  tree_out->Branch("jet_pnet_probc",&jet_pnet_probc_b,"jet_pnet_probc/F");
  tree_out->Branch("jet_pnet_probuds",&jet_pnet_probuds_b,"jet_pnet_probuds/F");
  tree_out->Branch("jet_pnet_probg",&jet_pnet_probg_b,"jet_pnet_probg/F");

  jet_pnetlast_probb_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probc_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probuds_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probg_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probmu_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probele_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaup1h0p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaup1h1p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaup1h2p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaup3h0p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaup3h1p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaum1h0p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaum1h1p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaum1h2p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaum3h0p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_probtaum3h1p_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_ptcorr_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_ptreshigh_b.resize(suffix_pnet.size(),0.);
  jet_pnetlast_ptreslow_b.resize(suffix_pnet.size(),0.);
  
  for (size_t ipnet = 0; ipnet < suffix_pnet.size(); ipnet++){
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probb").c_str(),&jet_pnetlast_probb_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probb/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probc").c_str(),&jet_pnetlast_probc_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probc/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probuds").c_str(),&jet_pnetlast_probuds_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probuds/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probg").c_str(),&jet_pnetlast_probg_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probg/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probmu").c_str(),&jet_pnetlast_probmu_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probmu/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probele").c_str(),&jet_pnetlast_probele_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probele/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h0p").c_str(),&jet_pnetlast_probtaup1h0p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h0p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h1p").c_str(),&jet_pnetlast_probtaup1h1p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h1p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h2p").c_str(),&jet_pnetlast_probtaup1h2p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup1h2p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup3h0p").c_str(),&jet_pnetlast_probtaup3h0p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup3h0p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup3h1p").c_str(),&jet_pnetlast_probtaup3h1p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaup3h1p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h0p").c_str(),&jet_pnetlast_probtaum1h0p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h0p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h1p").c_str(),&jet_pnetlast_probtaum1h1p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h1p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h2p").c_str(),&jet_pnetlast_probtaum1h2p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum1h2p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum3h0p").c_str(),&jet_pnetlast_probtaum3h0p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum3h0p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum3h1p").c_str(),&jet_pnetlast_probtaum3h1p_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_probtaum3h1p/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptcorr").c_str(),&jet_pnetlast_ptcorr_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptcorr/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptreshigh").c_str(),&jet_pnetlast_ptreshigh_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptreshigh/F").c_str());
    tree_out->Branch(("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptreslow").c_str(),&jet_pnetlast_ptreslow_b.at(ipnet),("jet_pnetlast"+suffix_pnet.at(ipnet)+"_ptreslow/F").c_str());
  }

  std::vector<float> jet_parTlast_probb_b,jet_parTlast_probc_b,jet_parTlast_probuds_b,jet_parTlast_probg_b;
  std::vector<float> jet_parTlast_probtaup1h0p_b, jet_parTlast_probtaup1h1p_b, jet_parTlast_probtaup1h2p_b, jet_parTlast_probtaup3h0p_b, jet_parTlast_probtaup3h1p_b;
  std::vector<float> jet_parTlast_probtaum1h0p_b, jet_parTlast_probtaum1h1p_b, jet_parTlast_probtaum1h2p_b, jet_parTlast_probtaum3h0p_b, jet_parTlast_probtaum3h1p_b;
  std::vector<float> jet_parTlast_probele_b, jet_parTlast_probmu_b, jet_parTlast_ptcorr_b, jet_parTlast_ptreshigh_b, jet_parTlast_ptreslow_b;

  jet_parTlast_probb_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probc_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probuds_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probg_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probmu_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probele_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaup1h0p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaup1h1p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaup1h2p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaup3h0p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaup3h1p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaum1h0p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaum1h1p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaum1h2p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaum3h0p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_probtaum3h1p_b.resize(suffix_parT.size(),0.);
  jet_parTlast_ptcorr_b.resize(suffix_parT.size(),0.);
  jet_parTlast_ptreshigh_b.resize(suffix_parT.size(),0.);
  jet_parTlast_ptreslow_b.resize(suffix_parT.size(),0.);

  for (size_t iparT = 0; iparT < suffix_parT.size(); iparT++){
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probb").c_str(),&jet_parTlast_probb_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probb/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probc").c_str(),&jet_parTlast_probc_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probc/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probuds").c_str(),&jet_parTlast_probuds_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probuds/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probg").c_str(),&jet_parTlast_probg_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probg/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probmu").c_str(),&jet_parTlast_probmu_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probmu/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probele").c_str(),&jet_parTlast_probele_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probele/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h0p").c_str(),&jet_parTlast_probtaup1h0p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h0p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h1p").c_str(),&jet_parTlast_probtaup1h1p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h1p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h2p").c_str(),&jet_parTlast_probtaup1h2p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup1h2p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup3h0p").c_str(),&jet_parTlast_probtaup3h0p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup3h0p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup3h1p").c_str(),&jet_parTlast_probtaup3h1p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaup3h1p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h0p").c_str(),&jet_parTlast_probtaum1h0p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h0p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h1p").c_str(),&jet_parTlast_probtaum1h1p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h1p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h2p").c_str(),&jet_parTlast_probtaum1h2p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum1h2p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum3h0p").c_str(),&jet_parTlast_probtaum3h0p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum3h0p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum3h1p").c_str(),&jet_parTlast_probtaum3h1p_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_probtaum3h1p/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_ptcorr").c_str(),&jet_parTlast_ptcorr_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_ptcorr/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_ptreshigh").c_str(),&jet_parTlast_ptreshigh_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_ptreshigh/F").c_str());
    tree_out->Branch(("jet_parTlast"+suffix_parT.at(iparT)+"_ptreslow").c_str(),&jet_parTlast_ptreslow_b.at(iparT),("jet_parTlast"+suffix_parT.at(iparT)+"_ptreslow/F").c_str());
  }
  
  tree_out->Branch("jet_chf",&jet_chf_b,"jet_chf/F");
  tree_out->Branch("jet_nhf",&jet_nhf_b,"jet_nhf/F");
  tree_out->Branch("jet_phf",&jet_phf_b,"jet_phf/F");
  tree_out->Branch("jet_elf",&jet_elf_b,"jet_elf/F");
  tree_out->Branch("jet_muf",&jet_muf_b,"jet_muf/F"); 
  tree_out->Branch("jet_ncand",&jet_ncand_b,"jet_ncand/i");
  tree_out->Branch("jet_nbhad",&jet_nbhad_b,"jet_nbhad/i");
  tree_out->Branch("jet_nchad",&jet_nchad_b,"jet_nchad/i");
  tree_out->Branch("jet_hflav",&jet_hflav_b,"jet_hflav/i");
  tree_out->Branch("jet_pflav",&jet_pflav_b,"jet_pflav/I");
  tree_out->Branch("jet_tauflav",&jet_tauflav_b,"jet_tauflav/I");
  tree_out->Branch("jet_taudecaymode",&jet_taudecaymode_b,"jet_taudecaymode/I");
  tree_out->Branch("jet_muflav",&jet_muflav_b,"jet_muflav/I");
  tree_out->Branch("jet_elflav",&jet_elflav_b,"jet_elflav/I");
  tree_out->Branch("jet_lepflav",&jet_lepflav_b,"jet_lepflav/I");
  tree_out->Branch("jet_mucharge",&jet_mucharge_b,"jet_mucharge/I");
  tree_out->Branch("jet_elcharge",&jet_elcharge_b,"jet_elcharge/I");
  tree_out->Branch("jet_taucharge",&jet_taucharge_b,"jet_taucharge/I");

  tree_out->Branch("jet_genmatch_pt",&jet_genmatch_pt_b,"jet_genmatch_pt/F");
  tree_out->Branch("jet_genmatch_eta",&jet_genmatch_eta_b,"jet_genmatch_eta/F");
  tree_out->Branch("jet_genmatch_phi",&jet_genmatch_phi_b,"jet_genmatch_phi/F");
  tree_out->Branch("jet_genmatch_mass",&jet_genmatch_mass_b,"jet_genmatch_mass/F");
  tree_out->Branch("jet_genmatch_energy",&jet_genmatch_energy_b,"jet_genmatch_energy/F");
  tree_out->Branch("jet_genmatch_wnu_pt",&jet_genmatch_wnu_pt_b,"jet_genmatch_wnu_pt/F");
  tree_out->Branch("jet_genmatch_wnu_eta",&jet_genmatch_wnu_eta_b,"jet_genmatch_wnu_eta/F");
  tree_out->Branch("jet_genmatch_wnu_phi",&jet_genmatch_wnu_phi_b,"jet_genmatch_wnu_phi/F");
  tree_out->Branch("jet_genmatch_wnu_mass",&jet_genmatch_wnu_mass_b,"jet_genmatch_wnu_mass/F");
  tree_out->Branch("jet_genmatch_wnu_energy",&jet_genmatch_wnu_energy_b,"jet_genmatch_wnu_energy/F");
  tree_out->Branch("jet_genmatch_nu_pt",&jet_genmatch_nu_pt_b,"jet_genmatch_nu_pt/F");
  tree_out->Branch("jet_genmatch_nu_eta",&jet_genmatch_nu_eta_b,"jet_genmatch_nu_eta/F");
  tree_out->Branch("jet_genmatch_nu_phi",&jet_genmatch_nu_phi_b,"jet_genmatch_nu_phi/F");
  tree_out->Branch("jet_genmatch_nu_mass",&jet_genmatch_nu_mass_b,"jet_genmatch_nu_mass/F");
  tree_out->Branch("jet_genmatch_nu_energy",&jet_genmatch_nu_energy_b,"jet_genmatch_nu_energy/F");
  tree_out->Branch("jet_genmatch_lep_pt",&jet_genmatch_lep_pt_b,"jet_genmatch_lep_pt/F");
  tree_out->Branch("jet_genmatch_lep_eta",&jet_genmatch_lep_eta_b,"jet_genmatch_lep_eta/F");
  tree_out->Branch("jet_genmatch_lep_phi",&jet_genmatch_lep_phi_b,"jet_genmatch_lep_phi/F");
  tree_out->Branch("jet_genmatch_lep_mass",&jet_genmatch_lep_mass_b,"jet_genmatch_lep_mass/F");
  tree_out->Branch("jet_genmatch_lep_energy",&jet_genmatch_lep_energy_b,"jet_genmatch_lep_energy/F");
  tree_out->Branch("jet_genmatch_lep_vis_pt",&jet_genmatch_lep_vis_pt_b,"jet_genmatch_lep_vis_pt/F");
  tree_out->Branch("jet_genmatch_lep_vis_eta",&jet_genmatch_lep_vis_eta_b,"jet_genmatch_lep_vis_eta/F");
  tree_out->Branch("jet_genmatch_lep_vis_phi",&jet_genmatch_lep_vis_phi_b,"jet_genmatch_lep_vis_phi/F");
  tree_out->Branch("jet_genmatch_lep_vis_mass",&jet_genmatch_lep_vis_mass_b,"jet_genmatch_lep_vis_mass/F");
  tree_out->Branch("jet_genmatch_lep_vis_energy",&jet_genmatch_lep_vis_energy_b,"jet_genmatch_lep_vis_energy/F");

  float jet_taumatch_pt_b, jet_taumatch_eta_b, jet_taumatch_phi_b, jet_taumatch_mass_b;
  float jet_taumatch_idjet_b, jet_taumatch_idele_b, jet_taumatch_idmu_b;
  int jet_taumatch_decaymode_b, jet_taumatch_charge_b;
  float jet_taumatch_dxy_b, jet_taumatch_dz_b;
  int jet_taumatch_idjet_wp_b, jet_taumatch_idmu_wp_b, jet_taumatch_idele_wp_b;

  tree_out->Branch("jet_taumatch_pt",&jet_taumatch_pt_b,"jet_taumatch_pt/F");
  tree_out->Branch("jet_taumatch_eta",&jet_taumatch_eta_b,"jet_taumatch_eta/F");
  tree_out->Branch("jet_taumatch_phi",&jet_taumatch_phi_b,"jet_taumatch_phi/F");
  tree_out->Branch("jet_taumatch_mass",&jet_taumatch_mass_b,"jet_taumatch_mass/F");
  tree_out->Branch("jet_taumatch_decaymode",&jet_taumatch_decaymode_b,"jet_taumatch_decaymode/I");
  tree_out->Branch("jet_taumatch_charge",&jet_taumatch_charge_b,"jet_taumatch_charge/I");
  tree_out->Branch("jet_taumatch_idjet",&jet_taumatch_idjet_b,"jet_taumatch_idjet/F");
  tree_out->Branch("jet_taumatch_idmu",&jet_taumatch_idmu_b,"jet_taumatch_idmu/F");
  tree_out->Branch("jet_taumatch_idele",&jet_taumatch_idele_b,"jet_taumatch_idele/F");
  tree_out->Branch("jet_taumatch_dxy",&jet_taumatch_dxy_b,"jet_taumatch_dxy/F");
  tree_out->Branch("jet_taumatch_dz",&jet_taumatch_dz_b,"jet_taumatch_dz/F");
  tree_out->Branch("jet_taumatch_idjet_wp",&jet_taumatch_idjet_wp_b,"jet_taumatch_idjet_wp/I");
  tree_out->Branch("jet_taumatch_idmu_wp",&jet_taumatch_idmu_wp_b,"jet_taumatch_idmu_wp/I");
  tree_out->Branch("jet_taumatch_idele_wp",&jet_taumatch_idele_wp_b,"jet_taumatch_idele_wp/I");

  float jet_mumatch_pt_b, jet_mumatch_eta_b, jet_mumatch_phi_b, jet_mumatch_mass_b, jet_mumatch_dxy_b, jet_mumatch_dz_b;
  int jet_mumatch_id_b, jet_mumatch_iso_b;
      
  tree_out->Branch("jet_mumatch_pt",&jet_mumatch_pt_b,"jet_mumatch_pt/F");
  tree_out->Branch("jet_mumatch_eta",&jet_mumatch_eta_b,"jet_mumatch_eta/F");
  tree_out->Branch("jet_mumatch_phi",&jet_mumatch_phi_b,"jet_mumatch_phi/F");
  tree_out->Branch("jet_mumatch_mass",&jet_mumatch_mass_b,"jet_mumatch_mass/F");
  tree_out->Branch("jet_mumatch_dxy",&jet_mumatch_dxy_b,"jet_mumatch_dxy/F");
  tree_out->Branch("jet_mumatch_dz",&jet_mumatch_dz_b,"jet_mumatch_dz/F");
  tree_out->Branch("jet_mumatch_id",&jet_mumatch_id_b,"jet_mumatch_id/I");
  tree_out->Branch("jet_mumatch_iso",&jet_mumatch_iso_b,"jet_mumatch_iso/I");

  float jet_elematch_pt_b, jet_elematch_eta_b, jet_elematch_phi_b, jet_elematch_mass_b, jet_elematch_dxy_b, jet_elematch_dz_b;
  int jet_elematch_id_b;
  float jet_elematch_idscore_b;

  tree_out->Branch("jet_elematch_pt",&jet_elematch_pt_b,"jet_elematch_pt/F");
  tree_out->Branch("jet_elematch_eta",&jet_elematch_eta_b,"jet_elematch_eta/F");
  tree_out->Branch("jet_elematch_phi",&jet_elematch_phi_b,"jet_elematch_phi/F");
  tree_out->Branch("jet_elematch_mass",&jet_elematch_mass_b,"jet_elematch_mass/F");
  tree_out->Branch("jet_elematch_dxy",&jet_elematch_dxy_b,"jet_elematch_dxy/F");
  tree_out->Branch("jet_elematch_dz",&jet_elematch_dz_b,"jet_elematch_dz/F");
  tree_out->Branch("jet_elematch_id",&jet_elematch_id_b,"jet_elematch_id/I");
  tree_out->Branch("jet_elematch_idscore",&jet_elematch_idscore_b,"jet_elematch_idscore/F");

  float jet_phomatch_pt_b, jet_phomatch_eta_b, jet_phomatch_phi_b, jet_phomatch_mass_b;
  int jet_phomatch_id_b;
  float jet_phomatch_idscore_b;

  tree_out->Branch("jet_phomatch_pt",&jet_phomatch_pt_b,"jet_phomatch_pt/F");
  tree_out->Branch("jet_phomatch_eta",&jet_phomatch_eta_b,"jet_phomatch_eta/F");
  tree_out->Branch("jet_phomatch_phi",&jet_phomatch_phi_b,"jet_phomatch_phi/F");
  tree_out->Branch("jet_phomatch_mass",&jet_phomatch_mass_b,"jet_phomatch_mass/F");
  tree_out->Branch("jet_phomatch_id",&jet_phomatch_id_b,"jet_phomatch_id/I");
  tree_out->Branch("jet_phomatch_idscore",&jet_phomatch_idscore_b,"jet_phomatch_idscore/F");

  while(reader.Next()){
    
    // Gen leptons from resonance decay 
    std::vector<TLorentzVector> genLepFromResonance4V;
    std::vector<TLorentzVector> genMuonsFromResonance4V;
    std::vector<TLorentzVector> genElectronsFromResonance4V;
    std::vector<int> genMuonsFromResonanceCharge;
    std::vector<int> genElectronsFromResonanceCharge;
    std::vector<TLorentzVector> tau_gen_visible;
    std::vector<TLorentzVector> tau_gen;
    std::vector<int> tau_gen_charge;
    std::vector<unsigned int> tau_gen_nch;
    std::vector<unsigned int> tau_gen_np0;
    std::vector<unsigned int> tau_gen_nnh;

    if(not (sample == sample_type::data)){
      for(size_t igen = 0; igen < (*gen_particle_pt)->size(); igen++){
	// select resonances like Higgs, W, Z, taus
	if(abs((*gen_particle_id)->at(igen)) == 25 or
	   abs((*gen_particle_id)->at(igen)) == 23 or
	   abs((*gen_particle_id)->at(igen)) == 24 or
	   abs((*gen_particle_id)->at(igen)) == 15){	
	  for(size_t idau = 0; idau < (*gen_particle_daughters_id)->size(); idau++){
	    // select electrons or muons from the resonance / tau decay
	    if((*gen_particle_daughters_igen)->at(idau) == igen and
	       (abs((*gen_particle_daughters_id)->at(idau)) == 11 or
		abs((*gen_particle_daughters_id)->at(idau)) == 13)){
	      TLorentzVector gen4V;
	      gen4V.SetPtEtaPhiM((*gen_particle_daughters_pt)->at(idau),(*gen_particle_daughters_eta)->at(idau),(*gen_particle_daughters_phi)->at(idau),(*gen_particle_daughters_mass)->at(idau));
	      if(std::find(genLepFromResonance4V.begin(),genLepFromResonance4V.end(),gen4V) == genLepFromResonance4V.end())
		genLepFromResonance4V.push_back(gen4V);
	      if(abs((*gen_particle_daughters_id)->at(idau)) == 13 and 
		 std::find(genMuonsFromResonance4V.begin(),genMuonsFromResonance4V.end(),gen4V) == genMuonsFromResonance4V.end()){
		genMuonsFromResonance4V.push_back(gen4V);
		genMuonsFromResonanceCharge.push_back((*gen_particle_daughters_charge)->at(idau));
	      }
	      if(abs((*gen_particle_daughters_id)->at(idau)) == 11 and 
		 std::find(genElectronsFromResonance4V.begin(),genElectronsFromResonance4V.end(),gen4V) == genElectronsFromResonance4V.end()){
		genElectronsFromResonance4V.push_back(gen4V);		
		genElectronsFromResonanceCharge.push_back((*gen_particle_daughters_charge)->at(idau));
	      }
	    }
	  }
	}
      }   
      // Gen hadronic taus	
      for(size_t igen = 0; igen < (*gen_particle_pt)->size(); igen++){
	if(abs((*gen_particle_id)->at(igen)) == 15){ // hadronic or leptonic tau
	  TLorentzVector tau_gen_tmp;
	  unsigned int tau_gen_nch_tmp = 0;
	  unsigned int tau_gen_np0_tmp = 0;
	  unsigned int tau_gen_nnh_tmp = 0;
	  for(size_t idau = 0; idau < (*gen_particle_daughters_pt)->size(); idau++){
	    if((*gen_particle_daughters_igen)->at(idau) == igen and
	       abs((*gen_particle_daughters_id)->at(idau)) != 11 and // no mu
	       abs((*gen_particle_daughters_id)->at(idau)) != 13 and // no el
	       abs((*gen_particle_daughters_id)->at(idau)) != 12 and // no neutrinos
	       abs((*gen_particle_daughters_id)->at(idau)) != 14 and
	       abs((*gen_particle_daughters_id)->at(idau)) != 16){
	      TLorentzVector tmp4V; 
	      tmp4V.SetPtEtaPhiM((*gen_particle_daughters_pt)->at(idau),(*gen_particle_daughters_eta)->at(idau),(*gen_particle_daughters_phi)->at(idau),(*gen_particle_daughters_mass)->at(idau));
	      tau_gen_tmp += tmp4V;
	      if ((*gen_particle_daughters_charge)->at(idau) != 0 and (*gen_particle_daughters_status)->at(idau) == 1) tau_gen_nch_tmp ++; // charged particles
	      else if((*gen_particle_daughters_charge)->at(idau) == 0 and (*gen_particle_daughters_id)->at(idau) == 111) tau_gen_np0_tmp++;
	      else if((*gen_particle_daughters_charge)->at(idau) == 0 and (*gen_particle_daughters_id)->at(idau) != 111) tau_gen_nnh_tmp++;
	    }
	  }	
	  if(tau_gen_tmp.Pt() > 0){ // good hadronic tau
	    tau_gen_visible.push_back(tau_gen_tmp);
	    tau_gen_tmp.SetPtEtaPhiM((*gen_particle_pt)->at(igen),(*gen_particle_eta)->at(igen),(*gen_particle_phi)->at(igen),(*gen_particle_mass)->at(igen));
	    tau_gen_charge.push_back(((*gen_particle_id)->at(igen) > 0) ? -1 : 1);
	    tau_gen.push_back(tau_gen_tmp);
	    tau_gen_nch.push_back(tau_gen_nch_tmp);
	    tau_gen_np0.push_back(tau_gen_np0_tmp);
	    tau_gen_nnh.push_back(tau_gen_nnh_tmp);
	  }
	}
      } 
    }

    // apply per-event selection expressed by the domain type
    std::vector<unsigned int> jets_no_overlap_leptons_index;

    // Domain specific event selections
    if(domain != domain_type::none){

      TLorentzVector lep1_4v,lep2_4v;
      lep1_4v.SetPtEtaPhiM((*leppair_pt1)->front(),(*leppair_eta1)->front(),(*leppair_phi1)->front(),(*leppair_mass1)->front());
      lep2_4v.SetPtEtaPhiM((*leppair_pt2)->front(),(*leppair_eta2)->front(),(*leppair_phi2)->front(),(*leppair_mass2)->front());      
      if(lep1_4v.Pt() <= 0){nEventsRejectedDomainKinematics++; continue;}
      if(lep2_4v.Pt() <= 0){nEventsRejectedDomainKinematics++; continue;}
      
      // matching leptons jets                                                                                                                                                                  
      int lep1_index = -1;
      int lep2_index = -1;
      float minDR1 = 1000;
      float minDR2 = 1000;
      for(size_t ijet = 0; ijet < jet_pt->size(); ijet++){
        TLorentzVector jet_4v;
        jet_4v.SetPtEtaPhiM(jet_pt->at(ijet),jet_eta->at(ijet),jet_phi->at(ijet),jet_mass->at(ijet));
        float dR1 = jet_4v.DeltaR(lep1_4v);
        float dR2 = jet_4v.DeltaR(lep2_4v);
        if(dR1 <= dR2 and dR1 <= minDR1 and dR1 <= dRCone){
          minDR1 = dR1;
          lep1_index = ijet;
        }
        else if(dR2 < dR1 and dR2 <= minDR2 and dR2 <= dRCone){
          minDR2 = dR2;
          lep2_index = ijet;
        }
      }

      // first selections in the various regions
      if(domain == domain_type::dimuon or domain == domain_type::dielectron){ // dilepton
        if(**flags != 0){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(*met > maxMET) {
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep1_4v.Pt() < minLep1PtDilepton){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep2_4v.Pt() < minLep2PtDilepton){
          nEventsRejectedDomainKinematics++;
          continue;
        }
      }
      else if(domain == domain_type::emu){ // emu
        if(**flags != 0){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep1_4v.Pt() < minLep1PtEMu){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep2_4v.Pt() < minLep2PtEMu){
          nEventsRejectedDomainKinematics++;
          continue;
        }
      }
      if(domain == domain_type::dijet){ // dijet
        if(**flags != 0){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep1_4v.Pt() < minLep1PtDijet){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep2_4v.Pt() < minLep2PtDijet){
          nEventsRejectedDomainKinematics++;
          continue;
        }
      }
      if(domain == domain_type::ttcharm){ // ttcharm
        if(**flags != 0){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep1_4v.Pt() < minLep1PtTTcharm){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep2_4v.Pt() < minLep2PtTTcharm){
          nEventsRejectedDomainKinematics++;
          continue;
        }
      }
      else if(domain == domain_type::mutau or domain == domain_type::etau){ // mutau                                                                                                                
        if(**flags != 0){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep1_4v.Pt() < minLep1PtMuTau){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(lep2_4v.Pt() < minLep2PtMuTau){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(sqrt(2*lep1_4v.Pt()*(*met)*(1-cos(deltaPhi(lep1_4v.Phi(),*met_phi)))) > minTransverseMass) {
          nEventsRejectedDomainKinematics++;
          continue;
        }
	
        int tau_index = -1;
        float minDR = 1000;
        for(size_t itau = 0; itau < tau_pt->size(); itau++){
          TLorentzVector tau_4v; tau_4v.SetPtEtaPhiM(tau_pt->at(itau),tau_eta->at(itau),tau_phi->at(itau),tau_mass->at(itau));
          float dR = lep2_4v.DeltaR(tau_4v);
          if(dR <= minDR and dR <= dRCone){
            minDR = dR;
            tau_index = itau;
          }
        }

        if(matchHPSTauCandidate and tau_index == -1){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if(tau_index != -1)
          lep2_4v.SetPtEtaPhiM(tau_pt->at(tau_index),tau_eta->at(tau_index),tau_phi->at(tau_index),tau_mass->at(tau_index));

        if(lep2_4v.Pt() < minTauPt or fabs(lep2_4v.Eta()) > maxTauEta){
          nEventsRejectedDomainKinematics++;
          continue;
        }
        if((lep1_4v+lep2_4v).M() < minMassVisible or (lep1_4v+lep2_4v).M() > maxMassVisible){
          nEventsRejectedDomainKinematics++;
          continue;
        }

	// compute tau-charge via parT or PNET
        float tau_charge_score = 0.;
        if(selectOnParTScore)
          tau_charge_score = (lep2_index==-1) ? 0 : (jet_parTlast_probtaup1h0p.front()->at(lep2_index)+jet_parTlast_probtaup1h1p.front()->at(lep2_index)+jet_parTlast_probtaup1h2p.front()->at(lep2_index)+jet_parTlast_probtaup3h0p.front()->at(lep2_index)+jet_parTlast_probtaup3h1p.front()->at(lep2_index))/(jet_parTlast_probtaup1h0p.front()->at(lep2_index)+jet_parTlast_probtaup1h1p.front()->at(lep2_index)+jet_parTlast_probtaup1h2p.front()->at(lep2_index)+jet_parTlast_probtaup3h0p.front()->at(lep2_index)+jet_parTlast_probtaup3h1p.front()->at(lep2_index)+jet_parTlast_probtaum1h0p.front()->at(lep2_index)+jet_parTlast_probtaum1h1p.front()->at(lep2_index)+jet_parTlast_probtaum1h2p.front()->at(lep2_index)+jet_parTlast_probtaum3h0p.front()->at(lep2_index)+jet_parTlast_probtaum3h1p.front()->at(lep2_index));
        else
          tau_charge_score = (lep2_index==-1) ? 0 : (jet_pnetlast_probtaup1h0p.front()->at(lep2_index)+jet_pnetlast_probtaup1h1p.front()->at(lep2_index)+jet_pnetlast_probtaup1h2p.front()->at(lep2_index)+jet_pnetlast_probtaup3h0p.front()->at(lep2_index)+jet_pnetlast_probtaup3h1p.front()->at(lep2_index))/(jet_pnetlast_probtaup1h0p.front()->at(lep2_index)+jet_pnetlast_probtaup1h1p.front()->at(lep2_index)+jet_pnetlast_probtaup1h2p.front()->at(lep2_index)+jet_pnetlast_probtaup3h0p.front()->at(lep2_index)+jet_pnetlast_probtaup3h1p.front()->at(lep2_index)+jet_pnetlast_probtaum1h0p.front()->at(lep2_index)+jet_pnetlast_probtaum1h1p.front()->at(lep2_index)+jet_pnetlast_probtaum1h2p.front()->at(lep2_index)+jet_pnetlast_probtaum3h0p.front()->at(lep2_index)+jet_pnetlast_probtaum3h1p.front()->at(lep2_index));
        float tau_charge = (tau_charge_score-0.5)/fabs(tau_charge_score-0.5);
	
        if(domain == domain_type::mutau){
          int mu_index = -1;
          float minDR = 1000;
          for(size_t imu = 0; imu < muon_pt->size(); imu++){
            TLorentzVector muon_4v; muon_4v.SetPtEtaPhiM(muon_pt->at(imu),muon_eta->at(imu),muon_phi->at(imu),muon_mass->at(imu));
            float dR = lep1_4v.DeltaR(muon_4v);
            if(dR <= minDR and dR <= dRCone){
              minDR = dR;
              mu_index = imu;
            }
          }
         if(mu_index != -1 and muon_charge->at(mu_index)*tau_charge > 0) {
            nEventsRejectedDomainKinematics++;
            continue;
          }
          if(mu_index == -1){
            nEventsRejectedDomainKinematics++;
            continue;
          }
        }
        else if(domain == domain_type::etau){
          int ele_index = -1;
          float minDR = 1000;
          for(size_t iele = 0; iele < electron_pt_corr->size(); iele++){
            TLorentzVector electron_4v; electron_4v.SetPtEtaPhiM(electron_pt->at(iele),electron_eta->at(iele),electron_phi->at(iele),electron_mass->at(iele));
            float dR = lep1_4v.DeltaR(electron_4v);
            if(dR <= minDR and dR <= dRCone){
              minDR = dR;
              ele_index = iele;
            }
          }
          if(ele_index != -1 and electron_charge->at(ele_index)*tau_charge > 0) {
            nEventsRejectedDomainKinematics++;
            continue;
          }

         if(ele_index != -1 and electron_charge->at(ele_index)*tau_charge > 0) {
            nEventsRejectedDomainKinematics++;
            continue;
          }
          if(ele_index == -1){
            nEventsRejectedDomainKinematics++;
            continue;
          }
        }
      }

      // save jets not in overlap with leptons
      for(size_t ijet = 0; ijet < jet_pt_raw->size(); ijet++){
        // basic kinematic selections
        if(jet_pt->at(ijet) < jetPtMin) continue;
        if(fabs(jet_eta->at(ijet)) > jetEtaMax) continue;
        if(fabs(jet_eta->at(ijet)) < jetEtaMin) continue;
	// based on index                                                                                                                                                                     
        if(int(ijet) == lep1_index){
          if(domain != domain_type::dijet) continue;
          if(jet_id->at(ijet) == 0) continue;
          if(jet_puid->at(ijet) == 0) continue;
          jets_no_overlap_leptons_index.push_back(ijet);
        }
        else if(int(ijet) == lep2_index){
          if(domain == domain_type::dimuon or domain == domain_type::dielectron or domain == domain_type::emu) continue; // not a jet
          else if(domain == domain_type::dijet or domain == domain_type::ttcharm){ // regular jet
            if(jet_id->at(ijet) == 0) continue;
            if(jet_puid->at(ijet) == 0) continue;
            jets_no_overlap_leptons_index.push_back(ijet);
          }
          else if(domain == domain_type::mutau or domain == domain_type::etau){ // tauh candidate
            jets_no_overlap_leptons_index.push_back(ijet);
          }
        }
        else{
          if(jet_id->at(ijet) == 0) continue;
          if(jet_puid->at(ijet) == 0) continue;
          jets_no_overlap_leptons_index.push_back(ijet);
        }
      }

      // impose requirements on the number of jets that needs to be considered in the analysis
      if(jets_no_overlap_leptons_index.empty()){
        nEventsRejectedDomainKinematics++;
        continue;
      }

      // jet occupancy selections
      if((domain == domain_type::dimuon or domain == domain_type::dielectron) and (jets_no_overlap_leptons_index.size() > maxNjetDilepton or jets_no_overlap_leptons_index.size() < minNjetDilepton)){
        nEventsRejectedDomainKinematics++;
        continue;
      }
      else if(domain == domain_type::emu and (jets_no_overlap_leptons_index.size() > maxNjetEMu or jets_no_overlap_leptons_index.size() < minNjetEMu)){
        nEventsRejectedDomainKinematics++;
        continue;
      }
      else if((domain == domain_type::mutau or domain == domain_type::etau) and (jets_no_overlap_leptons_index.size() > maxNjetMuTau or jets_no_overlap_leptons_index.size() < minNjetMuTau)){
        nEventsRejectedDomainKinematics++;
        continue;
      }
      else if(domain == domain_type::dijet and (jets_no_overlap_leptons_index.size() > maxNjetDijet or jets_no_overlap_leptons_index.size() < minNjetDijet)){
        nEventsRejectedDomainKinematics++;
        continue;
      }
      else if(domain == domain_type::ttcharm and (jets_no_overlap_leptons_index.size() > maxNjetTTcharm or jets_no_overlap_leptons_index.size() < minNjetTTcharm)){
        nEventsRejectedDomainKinematics++;
        continue;
      }

      // Sorting of jets
      if(domain == domain_type::emu){ // sort based on b vs jet
        std::vector<std::pair<int,float> > jet_b_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jet_b_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet)))));
        std::sort(jet_b_score.begin(),
                  jet_b_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_b_score.at(ijet).first;
      }
      else if(domain == domain_type::ttcharm){ // sort based on c vs jet
        std::vector<std::pair<int,float> > jet_c_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jet_c_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet)))));
        std::sort(jet_c_score.begin(),
                  jet_c_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_c_score.at(ijet).first;
      }
      else if(domain == domain_type::dimuon or domain == domain_type::dielectron){
        std::vector<std::pair<int,float> > jet_uds_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jet_uds_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet)))));
        std::sort(jet_uds_score.begin(),
                  jet_uds_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_uds_score.at(ijet).first;
      }
      else if(domain == domain_type::mutau or domain == domain_type::etau){
        std::vector<std::pair<int,float> > jet_tau_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jet_tau_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? (jet_parTlast_probtaup1h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup1h1p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup1h2p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup3h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaup3h1p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum1h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum1h1p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum1h2p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum3h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probtaum3h1p.front()->at(jets_no_overlap_leptons_index.at(ijet)))/(1-jet_parTlast_probmu.front()->at(jets_no_overlap_leptons_index.at(ijet))-jet_parTlast_probele.front()->at(jets_no_overlap_leptons_index.at(ijet))) : (jet_pnetlast_probtaup1h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup1h1p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup1h2p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup3h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaup3h1p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum1h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum1h1p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum1h2p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum3h0p.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probtaum3h1p.front()->at(jets_no_overlap_leptons_index.at(ijet)))/(1-jet_pnetlast_probmu.front()->at(jets_no_overlap_leptons_index.at(ijet))-jet_pnetlast_probele.front()->at(jets_no_overlap_leptons_index.at(ijet)))));
        std::sort(jet_tau_score.begin(),
                  jet_tau_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_tau_score.at(ijet).first;
      }
      else if(domain == domain_type::dijet){
	std::vector<std::pair<int,float> > jet_g_score;
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jet_g_score.push_back(std::pair<int,float>(jets_no_overlap_leptons_index.at(ijet),(selectOnParTScore) ? jet_parTlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_parTlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_parTlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet))) : jet_pnetlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet))/(jet_pnetlast_probb.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probc.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probuds.front()->at(jets_no_overlap_leptons_index.at(ijet))+jet_pnetlast_probg.front()->at(jets_no_overlap_leptons_index.at(ijet)))));
        std::sort(jet_g_score.begin(),
                  jet_g_score.end(),
                  [](const std::pair<int, float> & i1, const std::pair<int, float> & i2) { return i1.second > i2.second; });
        for(size_t ijet = 0; ijet < jets_no_overlap_leptons_index.size(); ijet++)
          jets_no_overlap_leptons_index.at(ijet) = jet_g_score.at(ijet).first;
      }
      
      // dimuon --> take maxNjetDileptonToStore leading UDS jets
      if((domain == domain_type::dimuon or domain == domain_type::dielectron) and jets_no_overlap_leptons_index.size() > maxNjetDileptonToStore)
        jets_no_overlap_leptons_index.resize(maxNjetDileptonToStore);
      // emu --> take maxNjetEMuToStore leading B jets
      else if(domain == domain_type::emu and jets_no_overlap_leptons_index.size() > maxNjetEMuToStore)
        jets_no_overlap_leptons_index.resize(maxNjetEMuToStore);
      // mutau --> take maxNjetMuTauToStore leading tau(h) jets
      else if((domain == domain_type::mutau or domain == domain_type::etau) and jets_no_overlap_leptons_index.size() > maxNjetMuTauToStore)
        jets_no_overlap_leptons_index.resize(maxNjetMuTauToStore);
      // dijet --> take maxNjetDijetToStore leading G jets
      else if(domain == domain_type::dijet and jets_no_overlap_leptons_index.size() > maxNjetDijetToStore)
        jets_no_overlap_leptons_index.resize(maxNjetDijetToStore);
      // ttcharm --> take maxNjetTTcharmToStore leading c jets
      else if(domain == domain_type::ttcharm and jets_no_overlap_leptons_index.size() > maxNjetTTcharmToStore)
        jets_no_overlap_leptons_index.resize(maxNjetTTcharmToStore);
    }
    
    // loop on jets
    for(size_t ijet = 0; ijet < jet_pt->size(); ijet++){      

      // skip jet in overlap with leptons only for domain
      nJetsTotal++;            
      // Jet 4V
      TLorentzVector jet4V;
      jet4V.SetPtEtaPhiM(jet_pt->at(ijet),jet_eta->at(ijet),jet_phi->at(ijet),jet_mass->at(ijet));

      // Add the selection and limit in number of jets in case of domain
      if(domain == domain_type::none){ 
	if(jet_pt_raw->at(ijet) < jetPtMin or
	   fabs(jet_eta->at(ijet)) > jetEtaMax or 
	   fabs(jet_eta->at(ijet)) < jetEtaMin or
	   (not (sample == sample_type::data) and saveOnlyGenMatchedJets and (*jet_genmatch_pt)->at(ijet) <= 0)
	   ){
	  nJetsRejectedBaseCuts++; 
	  continue;
	}
      }
      else{
        if(std::find(jets_no_overlap_leptons_index.begin(),jets_no_overlap_leptons_index.end(),ijet) == jets_no_overlap_leptons_index.end()){
          nJetsRejectedBaseCuts++;
          continue;
        }

	// selection for mutau
        if(domain == domain_type::mutau or domain == domain_type::etau){
          if((selectOnParTScore) ?
             ((jet_parTlast_probtaup1h0p.front()->at(ijet)+jet_parTlast_probtaup1h1p.front()->at(ijet)+jet_parTlast_probtaup1h2p.front()->at(ijet)+jet_parTlast_probtaum1h0p.front()->at(ijet)+jet_parTlast_probtaum1h1p.front()->at(ijet)+jet_parTlast_probtaum1h2p.front()->at(ijet)+jet_parTlast_probtaup3h0p.front()->at(ijet)+jet_parTlast_probtaup3h1p.front()->at(ijet)+jet_parTlast_probtaum3h0p.front()->at(ijet)+jet_parTlast_probtaum3h1p.front()->at(ijet))/(1-jet_parTlast_probmu.front()->at(ijet)-jet_parTlast_probele.front()->at(ijet)) <  minPTauVsJet) :
             ((jet_pnetlast_probtaup1h0p.front()->at(ijet)+jet_pnetlast_probtaup1h1p.front()->at(ijet)+jet_pnetlast_probtaup1h2p.front()->at(ijet)+jet_pnetlast_probtaum1h0p.front()->at(ijet)+jet_pnetlast_probtaum1h1p.front()->at(ijet)+jet_pnetlast_probtaum1h2p.front()->at(ijet)+jet_pnetlast_probtaup3h0p.front()->at(ijet)+jet_pnetlast_probtaup3h1p.front()->at(ijet)+jet_pnetlast_probtaum3h0p.front()->at(ijet)+jet_pnetlast_probtaum3h1p.front()->at(ijet))/(1-jet_pnetlast_probmu.front()->at(ijet)-jet_pnetlast_probele.front()->at(ijet)) <  minPTauVsJet)){
            nJetsRejectedMuTau++;
            continue;
          }
          if((selectOnParTScore) ?
             ((jet_parTlast_probtaup1h0p.front()->at(ijet)+jet_parTlast_probtaup1h1p.front()->at(ijet)+jet_parTlast_probtaup1h2p.front()->at(ijet)+jet_parTlast_probtaum1h0p.front()->at(ijet)+jet_parTlast_probtaum1h1p.front()->at(ijet)+jet_parTlast_probtaum1h2p.front()->at(ijet)+jet_parTlast_probtaup3h0p.front()->at(ijet)+jet_parTlast_probtaup3h1p.front()->at(ijet)+jet_parTlast_probtaum3h0p.front()->at(ijet)+jet_parTlast_probtaum3h1p.front()->at(ijet))/(1-jet_parTlast_probb.front()->at(ijet)-jet_parTlast_probc.front()->at(ijet)-jet_parTlast_probuds.front()->at(ijet)-jet_parTlast_probg.front()->at(ijet)-jet_parTlast_probele.front()->at(ijet)) <  minPTauVsMu) :
             ((jet_pnetlast_probtaup1h0p.front()->at(ijet)+jet_pnetlast_probtaup1h1p.front()->at(ijet)+jet_pnetlast_probtaup1h2p.front()->at(ijet)+jet_pnetlast_probtaum1h0p.front()->at(ijet)+jet_pnetlast_probtaum1h1p.front()->at(ijet)+jet_pnetlast_probtaum1h2p.front()->at(ijet)+jet_pnetlast_probtaup3h0p.front()->at(ijet)+jet_pnetlast_probtaup3h1p.front()->at(ijet)+jet_pnetlast_probtaum3h0p.front()->at(ijet)+jet_pnetlast_probtaum3h1p.front()->at(ijet))/(1-jet_pnetlast_probb.front()->at(ijet)-jet_pnetlast_probc.front()->at(ijet)-jet_pnetlast_probuds.front()->at(ijet)-jet_pnetlast_probg.front()->at(ijet)-jet_pnetlast_probele.front()->at(ijet)) <  minPTauVsMu)){
            nJetsRejectedMuTau++;
            continue;
          }
	  if((selectOnParTScore) ?
             ((jet_parTlast_probtaup1h0p.front()->at(ijet)+jet_parTlast_probtaup1h1p.front()->at(ijet)+jet_parTlast_probtaup1h2p.front()->at(ijet)+jet_parTlast_probtaum1h0p.front()->at(ijet)+jet_parTlast_probtaum1h1p.front()->at(ijet)+jet_parTlast_probtaum1h2p.front()->at(ijet)+jet_parTlast_probtaup3h0p.front()->at(ijet)+jet_parTlast_probtaup3h1p.front()->at(ijet)+jet_parTlast_probtaum3h0p.front()->at(ijet)+jet_parTlast_probtaum3h1p.front()->at(ijet))/(1-jet_parTlast_probb.front()->at(ijet)-jet_parTlast_probc.front()->at(ijet)-jet_parTlast_probuds.front()->at(ijet)-jet_parTlast_probg.front()->at(ijet)-jet_parTlast_probmu.front()->at(ijet)) <  minPTauVsEle) :
             ((jet_pnetlast_probtaup1h0p.front()->at(ijet)+jet_pnetlast_probtaup1h1p.front()->at(ijet)+jet_pnetlast_probtaup1h2p.front()->at(ijet)+jet_pnetlast_probtaum1h0p.front()->at(ijet)+jet_pnetlast_probtaum1h1p.front()->at(ijet)+jet_pnetlast_probtaum1h2p.front()->at(ijet)+jet_pnetlast_probtaup3h0p.front()->at(ijet)+jet_pnetlast_probtaup3h1p.front()->at(ijet)+jet_pnetlast_probtaum3h0p.front()->at(ijet)+jet_pnetlast_probtaum3h1p.front()->at(ijet))/(1-jet_pnetlast_probb.front()->at(ijet)-jet_pnetlast_probc.front()->at(ijet)-jet_pnetlast_probuds.front()->at(ijet)-jet_pnetlast_probg.front()->at(ijet)-jet_pnetlast_probmu.front()->at(ijet)) <  minPTauVsEle)){
            nJetsRejectedMuTau++;
            continue;
          }
        }
        // selection for emu
        else if(domain == domain_type::emu){
          if((selectOnParTScore) ? (jet_parTlast_probb.front()->at(ijet)/(jet_parTlast_probb.front()->at(ijet)+jet_parTlast_probc.front()->at(ijet)+jet_parTlast_probuds.front()->at(ijet)+jet_parTlast_probg.front()->at(ijet)) < minPBVsJet) : (jet_pnetlast_probb.front()->at(ijet)/(jet_pnetlast_probb.front()->at(ijet)+jet_pnetlast_probc.front()->at(ijet)+jet_pnetlast_probuds.front()->at(ijet)+jet_pnetlast_probg.front()->at(ijet)) < minPBVsJet)){
            nJetsRejectedEMu++;
            continue;
          }
        }
	// selection for ttcharm
        else if(domain == domain_type::ttcharm){
          if((selectOnParTScore) ? (jet_parTlast_probc.front()->at(ijet)/(jet_parTlast_probb.front()->at(ijet)+jet_parTlast_probc.front()->at(ijet)+jet_parTlast_probuds.front()->at(ijet)+jet_parTlast_probg.front()->at(ijet)) < minPCVsJet ) : (jet_pnetlast_probc.front()->at(ijet)/(jet_pnetlast_probb.front()->at(ijet)+jet_pnetlast_probc.front()->at(ijet)+jet_pnetlast_probuds.front()->at(ijet)+jet_pnetlast_probg.front()->at(ijet)) < minPCVsJet)){
            nJetsRejectedTTcharm++;
            continue;
          }
        }
      }
      
      // matching with gen-leptons (muons/electrons/hadronic taus)
      float minDR = 1000;
      int nlep_in_cone  = 0;
      int pos_matched_genmu = -1;
      int pos_matched_genele = -1;
      int pos_matched_tauh = -1;
      int gentau_decaymode = -1; 	  
      TLorentzVector genLepton4V;
      TLorentzVector genLeptonVis4V;
      if(not (sample == sample_type::data)){
	for(size_t igen = 0; igen < genMuonsFromResonance4V.size(); igen++){
	  float dR = jet4V.DeltaR(genMuonsFromResonance4V.at(igen));	      
	  if(dR < dRCone) nlep_in_cone++;
	  if(dR < dRCone and dR < minDR){
	    pos_matched_genmu = igen;
	    minDR = dR;
	    genLepton4V = genMuonsFromResonance4V.at(igen);
	    genLeptonVis4V = genMuonsFromResonance4V.at(igen);
	  }
	}
	
	for(size_t igen = 0; igen < genElectronsFromResonance4V.size(); igen++){
	  float dR = jet4V.DeltaR(genElectronsFromResonance4V.at(igen));	      
	  if(dR < dRCone) nlep_in_cone++;
	  if(dR < dRCone and dR < minDR){
	    pos_matched_genmu  = -1;
	    pos_matched_genele = igen;
	    minDR = dR;
	    genLepton4V = genElectronsFromResonance4V.at(igen);
	    genLeptonVis4V = genElectronsFromResonance4V.at(igen);
	  }
	}
	
	for(size_t itau = 0; itau < tau_gen_visible.size(); itau++){
	  float dR = tau_gen_visible.at(itau).DeltaR(jet4V); 
	  if(dR < dRCone) nlep_in_cone++;
	  if(dR < dRCone and dR < minDR){
	    pos_matched_genmu  = -1;
	    pos_matched_genele = -1;
	    pos_matched_tauh = itau;
	    minDR = dR;
	    gentau_decaymode = 5*(tau_gen_nch.at(itau)-1)+tau_gen_np0.at(itau);
	    genLepton4V = tau_gen.at(itau);
	    genLeptonVis4V = tau_gen_visible.at(itau);
	  }
	}

	// exclude, when a jet is matched with a lepton, those for which the matched lepton is below the chosen pt threshold
	if(domain == domain_type::none){
	  // Jet id applied only to jets not overlapping with gen-leptons
	  if(applyJetID and pos_matched_genmu  == -1 and pos_matched_genele == -1 and pos_matched_tauh == -1 and jet_id->at(ijet) == 0){
	    nJetsRejectedBaseCuts++;
	    continue;
	  }
	  if(pos_matched_genmu != -1 and genLeptonVis4V.Pt() < ptGenLeptonMin){
	    nJetsRejectedLowGenLepton++;
	    continue;
	  }
	  if(pos_matched_genele != -1 and genLeptonVis4V.Pt() < ptGenLeptonMin){
	    nJetsRejectedLowGenLepton++;
	    continue;
	  }
	  if(pos_matched_tauh != -1 and genLeptonVis4V.Pt() < ptGenTauVisibleMin){
	    nJetsRejectedLowGenLepton++;
	    continue;
	  }     
	}
      }

      // good jets for the analysis	  
      nJetsValidation++;	  
      
      /// Fill branches
      run_b = *run;
      lumi_b = *lumi;
      event_b = *event;
      npv_b = *npv;
      nsv_b = *nsv;
      if(not (sample == sample_type::data)){
	npu_b = **putrue;
	wgt_b = **wgt;
      }
      rho_b = *rho;
      met_b = *met;	
      sample_b = static_cast<int>(sample);
      domain_b = static_cast<int>(domain);
      isdata_b = (sample == sample_type::data) ? 1 : 0;
      ijet_b = ijet;

      // Basic jet quantities
      jet_pt_b   = jet_pt->at(ijet);
      jet_pt_raw_b = jet_pt_raw->at(ijet);
      jet_eta_b  = jet_eta->at(ijet);
      jet_phi_b  = jet_phi->at(ijet);
      jet_mass_b = jet_mass->at(ijet);
      jet_mass_raw_b = jet_mass_raw->at(ijet);
      jet_energy_b = jet4V.E();

      jet_ncand_b = jet_ncand->at(ijet);

      jet_chf_b = jet_chf->at(ijet);
      jet_nhf_b = jet_nhf->at(ijet);
      jet_phf_b = jet_phf->at(ijet);
      jet_elf_b = jet_elf->at(ijet);
      jet_muf_b = jet_muf->at(ijet);

      if(not (sample == sample_type::data)){
	jet_nbhad_b = jet_nbhad->at(ijet);
	jet_nchad_b = jet_nchad->at(ijet);	
	jet_hflav_b = jet_hflav->at(ijet);
	jet_pflav_b = jet_pflav->at(ijet);
      }
      else{
	jet_nbhad_b = 0;
	jet_nchad_b = 0;
	jet_hflav_b = 0;
	jet_pflav_b = 0;
      }
      
      jet_deepjet_probb_b   = jet_deepjet_probb->at(ijet)+jet_deepjet_probbb->at(ijet)+jet_deepjet_problepb->at(ijet);
      jet_deepjet_probc_b   = jet_deepjet_probc->at(ijet);
      jet_deepjet_probuds_b = jet_deepjet_probuds->at(ijet);
      jet_deepjet_probg_b   = jet_deepjet_probg->at(ijet);

      jet_pnet_probb_b = (jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet))/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      jet_pnet_probc_b = (jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet))/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      jet_pnet_probuds_b = jet_pnet_probuds->at(ijet)/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      jet_pnet_probg_b = jet_pnet_probg->at(ijet)/(jet_pnet_probb->at(ijet)+jet_pnet_probbb->at(ijet)+jet_pnet_probc->at(ijet)+jet_pnet_probcc->at(ijet)+jet_pnet_probuds->at(ijet)+jet_pnet_probg->at(ijet));
      
      for(size_t ipnet = 0; ipnet < suffix_pnet.size(); ipnet++){
	jet_pnetlast_probb_b.at(ipnet) = jet_pnetlast_probb.at(ipnet)->at(ijet);
	jet_pnetlast_probc_b.at(ipnet) = jet_pnetlast_probc.at(ipnet)->at(ijet);
	jet_pnetlast_probuds_b.at(ipnet) = jet_pnetlast_probuds.at(ipnet)->at(ijet);
	jet_pnetlast_probg_b.at(ipnet) = jet_pnetlast_probg.at(ipnet)->at(ijet);
	jet_pnetlast_probmu_b.at(ipnet) = jet_pnetlast_probmu.at(ipnet)->at(ijet);
	jet_pnetlast_probele_b.at(ipnet) = jet_pnetlast_probele.at(ipnet)->at(ijet);
	jet_pnetlast_probtaup1h0p_b.at(ipnet) = jet_pnetlast_probtaup1h0p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaup1h1p_b.at(ipnet) = jet_pnetlast_probtaup1h1p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaup1h2p_b.at(ipnet) = jet_pnetlast_probtaup1h2p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaup3h0p_b.at(ipnet) = jet_pnetlast_probtaup3h0p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaup3h1p_b.at(ipnet) = jet_pnetlast_probtaup3h1p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaum1h0p_b.at(ipnet) = jet_pnetlast_probtaum1h0p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaum1h1p_b.at(ipnet) = jet_pnetlast_probtaum1h1p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaum1h2p_b.at(ipnet) = jet_pnetlast_probtaum1h2p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaum3h0p_b.at(ipnet) = jet_pnetlast_probtaum3h0p.at(ipnet)->at(ijet);
	jet_pnetlast_probtaum3h1p_b.at(ipnet) = jet_pnetlast_probtaum3h1p.at(ipnet)->at(ijet);
	jet_pnetlast_ptcorr_b.at(ipnet) = jet_pnetlast_ptcorr.at(ipnet)->at(ijet);
	jet_pnetlast_ptreshigh_b.at(ipnet) = jet_pnetlast_ptreshigh.at(ipnet)->at(ijet);
	jet_pnetlast_ptreslow_b.at(ipnet) = jet_pnetlast_ptreslow.at(ipnet)->at(ijet);
      }

      for(size_t iparT = 0; iparT < suffix_parT.size(); iparT++){
	jet_parTlast_probb_b.at(iparT) = jet_parTlast_probb.at(iparT)->at(ijet);
	jet_parTlast_probc_b.at(iparT) = jet_parTlast_probc.at(iparT)->at(ijet);
	jet_parTlast_probuds_b.at(iparT) = jet_parTlast_probuds.at(iparT)->at(ijet);
	jet_parTlast_probg_b.at(iparT) = jet_parTlast_probg.at(iparT)->at(ijet);
	jet_parTlast_probmu_b.at(iparT) = jet_parTlast_probmu.at(iparT)->at(ijet);
	jet_parTlast_probele_b.at(iparT) = jet_parTlast_probele.at(iparT)->at(ijet);
	jet_parTlast_probtaup1h0p_b.at(iparT) = jet_parTlast_probtaup1h0p.at(iparT)->at(ijet);
	jet_parTlast_probtaup1h1p_b.at(iparT) = jet_parTlast_probtaup1h1p.at(iparT)->at(ijet);
	jet_parTlast_probtaup1h2p_b.at(iparT) = jet_parTlast_probtaup1h2p.at(iparT)->at(ijet);
	jet_parTlast_probtaup3h0p_b.at(iparT) = jet_parTlast_probtaup3h0p.at(iparT)->at(ijet);
	jet_parTlast_probtaup3h1p_b.at(iparT) = jet_parTlast_probtaup3h1p.at(iparT)->at(ijet);
	jet_parTlast_probtaum1h0p_b.at(iparT) = jet_parTlast_probtaum1h0p.at(iparT)->at(ijet);
	jet_parTlast_probtaum1h1p_b.at(iparT) = jet_parTlast_probtaum1h1p.at(iparT)->at(ijet);
	jet_parTlast_probtaum1h2p_b.at(iparT) = jet_parTlast_probtaum1h2p.at(iparT)->at(ijet);
	jet_parTlast_probtaum3h0p_b.at(iparT) = jet_parTlast_probtaum3h0p.at(iparT)->at(ijet);
	jet_parTlast_probtaum3h1p_b.at(iparT) = jet_parTlast_probtaum3h1p.at(iparT)->at(ijet);
	jet_parTlast_ptcorr_b.at(iparT) = jet_parTlast_ptcorr.at(iparT)->at(ijet);
	jet_parTlast_ptreshigh_b.at(iparT) = jet_parTlast_ptreshigh.at(iparT)->at(ijet);
	jet_parTlast_ptreslow_b.at(iparT) = jet_parTlast_ptreslow.at(iparT)->at(ijet);
      }
      
      if(pos_matched_genmu >= 0){
	jet_muflav_b  = 1;
	jet_mucharge_b = genMuonsFromResonanceCharge.at(pos_matched_genmu);
      }
      else{
	jet_muflav_b  = 0;
	jet_mucharge_b  = 0;
      }

      if(pos_matched_genele >= 0){
	jet_elflav_b  = 1;
	jet_elcharge_b = genElectronsFromResonanceCharge.at(pos_matched_genele);
      }
      else{
	jet_elflav_b  = 0;
	jet_elcharge_b = 0;
      }

      if(pos_matched_tauh >= 0){
	jet_tauflav_b = 1;
	jet_taudecaymode_b = gentau_decaymode;
	jet_taucharge_b = tau_gen_charge.at(pos_matched_tauh);
      }
      else{
	jet_tauflav_b = 0;
	jet_taudecaymode_b = -1;
	jet_taucharge_b = 0;
      }
      
      jet_lepflav_b = nlep_in_cone;
	  
      // Gen jet info
      TLorentzVector genJet4V; 
      TLorentzVector genJetWithNu4V;
      if(not (sample == sample_type::data)){
	genJet4V.SetPtEtaPhiM((*jet_genmatch_pt)->at(ijet),(*jet_genmatch_eta)->at(ijet),(*jet_genmatch_phi)->at(ijet),(*jet_genmatch_mass)->at(ijet));
	genJetWithNu4V.SetPtEtaPhiM((*jet_genmatch_wnu_pt)->at(ijet),(*jet_genmatch_wnu_eta)->at(ijet),(*jet_genmatch_wnu_phi)->at(ijet),(*jet_genmatch_wnu_mass)->at(ijet));

	jet_genmatch_pt_b = (*jet_genmatch_pt)->at(ijet);
	jet_genmatch_eta_b = (*jet_genmatch_eta)->at(ijet);
	jet_genmatch_phi_b = (*jet_genmatch_phi)->at(ijet);
	jet_genmatch_mass_b = (*jet_genmatch_mass)->at(ijet);
	jet_genmatch_energy_b = genJet4V.E();

	jet_genmatch_wnu_pt_b = (*jet_genmatch_wnu_pt)->at(ijet);
	jet_genmatch_wnu_eta_b = (*jet_genmatch_wnu_eta)->at(ijet);
	jet_genmatch_wnu_phi_b = (*jet_genmatch_wnu_phi)->at(ijet);
	jet_genmatch_wnu_mass_b = (*jet_genmatch_wnu_mass)->at(ijet);
	jet_genmatch_wnu_energy_b = genJetWithNu4V.E();

	jet_genmatch_nu_pt_b = (genJetWithNu4V-genJet4V).Pt();
	jet_genmatch_nu_eta_b = (genJetWithNu4V-genJet4V).Eta();
	jet_genmatch_nu_phi_b = (genJetWithNu4V-genJet4V).Phi();
	jet_genmatch_nu_mass_b = (genJetWithNu4V-genJet4V).M();
	jet_genmatch_nu_energy_b = (genJetWithNu4V-genJet4V).E();
	
	jet_genmatch_lep_pt_b = genLepton4V.Pt();
	jet_genmatch_lep_eta_b = genLepton4V.Eta();
	jet_genmatch_lep_phi_b = genLepton4V.Phi();
	jet_genmatch_lep_mass_b = genLepton4V.M();
	jet_genmatch_lep_energy_b = genLepton4V.E();
	
	jet_genmatch_lep_vis_pt_b = genLeptonVis4V.Pt();
	jet_genmatch_lep_vis_eta_b = genLeptonVis4V.Eta();
	jet_genmatch_lep_vis_phi_b = genLeptonVis4V.Phi();
	jet_genmatch_lep_vis_mass_b = genLeptonVis4V.M();
	jet_genmatch_lep_vis_energy_b = genLeptonVis4V.E();
      }
      else{
	jet_genmatch_pt_b = -1;
	jet_genmatch_eta_b = -1;
	jet_genmatch_phi_b = -1;
	jet_genmatch_mass_b = -1;
	jet_genmatch_energy_b = -1;
	jet_genmatch_wnu_pt_b = -1;
	jet_genmatch_wnu_eta_b = -1;
	jet_genmatch_wnu_phi_b = -1;
	jet_genmatch_wnu_mass_b = -1;
	jet_genmatch_wnu_energy_b = -1;
	jet_genmatch_lep_pt_b = -1;
	jet_genmatch_lep_eta_b = -1;
	jet_genmatch_lep_phi_b = -1;
	jet_genmatch_lep_mass_b = -1;
	jet_genmatch_lep_energy_b = -1;
	jet_genmatch_lep_vis_pt_b = -1;
	jet_genmatch_lep_vis_eta_b = -1;
	jet_genmatch_lep_vis_phi_b = -1;
	jet_genmatch_lep_vis_mass_b = -1;
	jet_genmatch_lep_vis_energy_b = -1;
      }

      // Matching with reco tau
      minDR = 1000;
      int pos_matched_reco_tauh = -1;
      for(size_t itau = 0; itau < tau_pt->size(); itau++){
	TLorentzVector tmp4V;
	tmp4V.SetPtEtaPhiM(tau_pt->at(itau),tau_eta->at(itau),tau_phi->at(itau),tau_mass->at(itau));
	float dR = tmp4V.DeltaR(jet4V);
	if (dR < dRCone and dR < minDR){ // small numerical roundoff due to float to double approximation
	  pos_matched_reco_tauh = itau;
	  minDR = dR;
	}
      }
	    
      if(pos_matched_reco_tauh >= 0){
	jet_taumatch_pt_b = tau_pt->at(pos_matched_reco_tauh);
	jet_taumatch_eta_b = tau_eta->at(pos_matched_reco_tauh);
	jet_taumatch_phi_b = tau_phi->at(pos_matched_reco_tauh);
	jet_taumatch_mass_b = tau_mass->at(pos_matched_reco_tauh);
	jet_taumatch_charge_b = tau_charge->at(pos_matched_reco_tauh);
	jet_taumatch_decaymode_b = tau_decaymode->at(pos_matched_reco_tauh);
	jet_taumatch_idjet_b = tau_idjet->at(pos_matched_reco_tauh);
	jet_taumatch_idele_b = tau_idele->at(pos_matched_reco_tauh);
	jet_taumatch_idmu_b  = tau_idmu->at(pos_matched_reco_tauh);
	jet_taumatch_dz_b = tau_dz->at(pos_matched_reco_tauh);
	jet_taumatch_dxy_b = tau_dxy->at(pos_matched_reco_tauh);
	jet_taumatch_idjet_wp_b = tau_idjet_wp->at(pos_matched_reco_tauh);
	jet_taumatch_idele_wp_b = tau_idele_wp->at(pos_matched_reco_tauh);
	jet_taumatch_idmu_wp_b = tau_idmu_wp->at(pos_matched_reco_tauh);
      }
      else{
	jet_taumatch_pt_b  = -1;
	jet_taumatch_eta_b = -1;
	jet_taumatch_phi_b = -1;
	jet_taumatch_mass_b = -1;
	jet_taumatch_idjet_b = -1;
	jet_taumatch_idele_b = -1;
	jet_taumatch_idjet_b = -1;
	jet_taumatch_decaymode_b = -1;
	jet_taumatch_charge_b = -1;
	jet_taumatch_dz_b = -1;
	jet_taumatch_dxy_b = -1;
	jet_taumatch_idjet_wp_b = -1;
	jet_taumatch_idele_wp_b = -1;
	jet_taumatch_idmu_wp_b = -1;
      }

      // Matching with muon by considering only the highest pt one
      minDR = 1000;
      int pos_matched_reco_mu = -1;
      for(size_t imu = 0; imu < muon_pt->size(); imu++){
	TLorentzVector tmp4V;
	tmp4V.SetPtEtaPhiM(muon_pt->at(imu),muon_eta->at(imu),muon_phi->at(imu),muon_mass->at(imu));
	float dR = tmp4V.DeltaR(jet4V);
	if(dR < dRCone and dR < minDR){
	  pos_matched_reco_mu = imu;
	  minDR = dR;	      
	}
      }
      if(pos_matched_reco_mu >= 0){
	jet_mumatch_pt_b = muon_pt->at(pos_matched_reco_mu);
	jet_mumatch_eta_b = muon_eta->at(pos_matched_reco_mu);
	jet_mumatch_phi_b = muon_phi->at(pos_matched_reco_mu);
	jet_mumatch_mass_b = muon_mass->at(pos_matched_reco_mu);
	jet_mumatch_dxy_b = muon_d0->at(pos_matched_reco_mu);
	jet_mumatch_dz_b = muon_dz->at(pos_matched_reco_mu);
	jet_mumatch_id_b = muon_id->at(pos_matched_reco_mu);
	jet_mumatch_iso_b = muon_iso->at(pos_matched_reco_mu);
      }
      else{
	jet_mumatch_pt_b = -1;
	jet_mumatch_eta_b = -1;
	jet_mumatch_phi_b = -1;
	jet_mumatch_mass_b = -1;
	jet_mumatch_dxy_b = -1;
	jet_mumatch_dz_b = -1;
	jet_mumatch_id_b = -1;
	jet_mumatch_iso_b = -1;
      }

      // Matching with electron by considering only the highest pt one
      minDR = 1000;
      int pos_matched_reco_ele = -1;
      for(size_t iele = 0; iele < electron_pt->size(); iele++){
	TLorentzVector tmp4V;	
	tmp4V.SetPtEtaPhiM(electron_pt->at(iele),electron_eta->at(iele),electron_phi->at(iele),electron_mass->at(iele));
	float dR = tmp4V.DeltaR(jet4V);
	if (dR < dRCone and dR < minDR){ // small numerical roundoff due to float to double approximation
	  pos_matched_reco_ele = iele;
	  minDR = dR;
	}
      }
      
      if(pos_matched_reco_ele >= 0){
	jet_elematch_pt_b = electron_pt->at(pos_matched_reco_ele);
	jet_elematch_eta_b = electron_eta->at(pos_matched_reco_ele);
	jet_elematch_phi_b = electron_phi->at(pos_matched_reco_ele);
	jet_elematch_mass_b = electron_mass->at(pos_matched_reco_ele);
	jet_elematch_dxy_b = electron_d0->at(pos_matched_reco_ele);
	jet_elematch_dz_b = electron_dz->at(pos_matched_reco_ele);
	jet_elematch_id_b = electron_id->at(pos_matched_reco_ele);
	jet_elematch_idscore_b = electron_idscore->at(pos_matched_reco_ele);
      }
      else{
	jet_elematch_pt_b = -1;
	jet_elematch_eta_b = -1;
	jet_elematch_phi_b = -1;
	jet_elematch_mass_b = -1;
	jet_elematch_dxy_b = -1;
	jet_elematch_dz_b = -1;
	jet_elematch_id_b = -1;
	jet_elematch_idscore_b = -1;
      }


      // Matching with photon by considering only the highest pt one
      minDR = 1000;
      int pos_matched_reco_pho = -1;
      for(size_t ipho = 0; ipho < photon_pt->size(); ipho++){
	TLorentzVector tmp4V;	
	tmp4V.SetPtEtaPhiM(photon_pt->at(ipho),photon_eta->at(ipho),photon_phi->at(ipho),photon_mass->at(ipho));
	float dR = tmp4V.DeltaR(jet4V);
	if (dR < dRCone and dR < minDR){ // small numerical roundoff due to float to double approximation
	  pos_matched_reco_pho = ipho;
	  minDR = dR;
	}
      }
      
      if(pos_matched_reco_pho >= 0){
	jet_phomatch_pt_b = photon_pt->at(pos_matched_reco_pho);
	jet_phomatch_eta_b = photon_eta->at(pos_matched_reco_pho);
	jet_phomatch_phi_b = photon_phi->at(pos_matched_reco_pho);
	jet_phomatch_mass_b = photon_mass->at(pos_matched_reco_pho);
	jet_phomatch_id_b = photon_id->at(pos_matched_reco_pho);
	jet_phomatch_idscore_b = photon_idscore->at(pos_matched_reco_pho);
      }
      else{
	jet_phomatch_pt_b = -1;
	jet_phomatch_eta_b = -1;
	jet_phomatch_phi_b = -1;
	jet_phomatch_mass_b = -1;
	jet_phomatch_id_b = -1;
	jet_phomatch_idscore_b = -1;
      }
      tree_out->Fill();
    }
  }
  std::cout<<"ntupleCreation --> thread "<<workerID<<" stopping "<<std::endl;
};


// Main function
int main(int argc, char **argv){

  boost::program_options::options_description desc("Main options");
  desc.add_options()
    ("inputFileList,i", boost::program_options::value<std::string>(&inputFileList)->default_value(""), "File that contains a list of ROOT files to be processed")
    ("inputFileDIR,d", boost::program_options::value<std::string>(&inputFileDIR)->default_value(""), "Directory that contained files to process")
    ("outputDIR,o", boost::program_options::value<std::string>(&outputDIR)->default_value(""), "Output directory where files need to be created")
    ("outputFileName,f", boost::program_options::value<std::string>(&outputFileName)->default_value(""), "Base name for the output file")
    ("nThreads,n", boost::program_options::value<unsigned int>(&nThreads)->default_value(1), "Number of threads to be used")
    ("maxNumberOfFilesToBeProcessed,max", boost::program_options::value<unsigned int>(&maxNumberOfFilesToBeProcessed)->default_value(1), "When running giving an input directory, split execution in chunks of Nfiles")    
    ("mergeThreadOutputFiles,merge", boost::program_options::value<bool>(&mergeThreadOutputFiles)->default_value(false), "Merge the output files of the job into a single one")
    ("jetPtMin,jptmin", boost::program_options::value<float>(&jetPtMin)->default_value(30), "Min jet pT to apply")
    ("jetEtaMax,jetamax", boost::program_options::value<float>(&jetEtaMax)->default_value(2.5), "Max jet eta to apply")
    ("jetEtaMin,jetamin", boost::program_options::value<float>(&jetEtaMin)->default_value(0.), "Min jet eta to apply")
    ("pnetLabels", boost::program_options::value<std::string>(&pnetLabelString)->default_value("base"),"PNET labels for various branches")
    ("parTLabels", boost::program_options::value<std::string>(&parTLabelString)->default_value("base"),"ParT labels for various branches")
    ("sample-type,s", boost::program_options::value<std::string>(&sample_name)->default_value(""), "Type of physics sample given as string")
    ("domain-type,d", boost::program_options::value<std::string>(&domain_name)->default_value(""), "Type of domain region")
    ("saveOnlyGenMatchedJets,genonly", boost::program_options::value<bool>(&saveOnlyGenMatchedJets)->default_value(true), "Save only jets matched to GEN")
    ("applyJetID,jetid", boost::program_options::value<bool>(&applyJetID)->default_value(true), "Apply the jetid requirement")
    ("selectOnParTScore,part", boost::program_options::value<bool>(&selectOnParTScore)->default_value(false), "Select on parT score instead of PNET")
    ("useXRootD,x", boost::program_options::value<bool>(&useXRootD)->default_value(true), "Read files via xrootd protocol instead of local-mount")
    ("compressOutputFile,c", boost::program_options::value<bool>(&compressOutputFile)->default_value(false), "Compress output file to save space")
    ("help,h", "Produce help message interface");

  boost::program_options::variables_map options;
  
  try{
    boost::program_options::store(boost::program_options::command_line_parser(argc,argv).options(desc).run(),options);
    boost::program_options::notify(options);
  }
  catch(std::exception &ex) {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cerr << "Use makeSkimmedNtuplesForValidation --help to get a list of all the allowed options"  << std::endl;
    return 999;
  } 
  catch(...) {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if(options.count("help")) {
    std::cout << "Usage: makeSkimmedNtuplesForValidation [options]\n";
    std::cout << desc;
    return 0;
  }

  // Conversion from string to sample type
  sample = convertSampleType(sample_name);
  domain = convertDomainType(domain_name);

  // replace all "default in pnet labels
  boost::split(pnetLabels,pnetLabelString,boost::is_any_of(","));
  boost::split(parTLabels,parTLabelString,boost::is_any_of(","));

  // cout
  std::cout<<"makeSkimmedNtuplesForValidation.cpp Parameter Summary"<<std::endl;
  std::cout<<"inputFileList --> "<<inputFileList<<std::endl;
  std::cout<<"inputFileDIR --> "<<inputFileDIR<<std::endl;
  std::cout<<"outputDIR --> "<<outputDIR<<std::endl;
  std::cout<<"outputFileName --> "<<outputFileName<<std::endl;
  std::cout<<"nThreads --> "<<nThreads<<std::endl;
  std::cout<<"maxNumberOfFilesToBeProcessed --> "<<maxNumberOfFilesToBeProcessed<<std::endl;
  std::cout<<"mergeThreadOutputFiles --> "<<mergeThreadOutputFiles<<std::endl;
  std::cout<<"jetPtMin --> "<<jetPtMin<<std::endl;
  std::cout<<"jetEtaMax --> "<<jetEtaMax<<std::endl;
  std::cout<<"jetEtaMin --> "<<jetEtaMin<<std::endl;
  std::cout<<"pnetLabels --> ";
  std::copy(pnetLabels.begin(),pnetLabels.end(),std::ostream_iterator<std::string>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"parTLabels --> ";
  std::copy(parTLabels.begin(),parTLabels.end(),std::ostream_iterator<std::string>(std::cout," "));
  std::cout<<std::endl;
  std::cout<<"sample_type --> "<<static_cast<int>(sample)<<std::endl;
  std::cout<<"domain_type --> "<<static_cast<int>(domain)<<std::endl;
  std::cout<<"isdata --> "<<((sample == sample_type::data) ? 1 : 0)<<std::endl;
  std::cout<<"saveOnlyGenMatchedJets --> "<<saveOnlyGenMatchedJets<<std::endl;
  std::cout<<"applyJetID --> "<<applyJetID<<std::endl;
  std::cout<<"selectOnParTScore --> "<<selectOnParTScore<<std::endl;
  std::cout<<"useXRootD --> "<<useXRootD<<std::endl;
  std::cout<<"compressOutputFile --> "<<compressOutputFile<<std::endl;

  if(not inputFileList.empty() and not inputFileDIR.empty()){
    std::cerr<<"You cannot set inputFileList and inputFileDIR to be simultaneously non empty --> please either provide a directory or a file list"<<std::endl;
    return 1001;
  }

  gSystem->Exec(("mkdir -p "+outputDIR).c_str());

  // Prepare for multi-threading
  ROOT::EnableImplicitMT(nThreads);
  ROOT::EnableThreadSafety();

  std::string xrootd_eos = std::string(getenv("EOS_MGM_URL"))+"//";

  // Prepare the workflow
  if(not inputFileList.empty() and boost::filesystem::is_regular_file(inputFileList)){ // the input string is a file containing the list of ROOT files to process --> for batch mode
    
    std::vector<std::string> fileList;
    std::vector<std::shared_ptr<TFile> >  files_out;
    std::vector<std::shared_ptr<TTree> >  trees_out;      

    std::ifstream inputFile (inputFileList);
    if(inputFile.is_open()){
      std::string line;
      while (getline(inputFile,line)) {
       if(useXRootD)
	 fileList.push_back(xrootd_eos+line);
       else
	 fileList.push_back(line);
      }
    }
    inputFile.close();

    for(size_t ithread = 0; ithread < nThreads; ithread++){
      files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_thread%zu.root",ithread)).Data()),"RECREATE"));
      trees_out.emplace_back(new TTree("tree","tree"));
      if(not compressOutputFile){
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	files_out.back()->SetCompressionLevel(4);
      }
      else{
	files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	files_out.back()->SetCompressionLevel(6);	
      }
    }      
    
    // Count number of events to split in threads
    std::cout<<"Count total number of events to process .."<<std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    std::unique_ptr<TChain> tree_in (new TChain("dnntree/tree","dnntree/tree"));
    for(size_t ifile = 0; ifile < fileList.size(); ifile++)
      tree_in->Add(fileList.at(ifile).c_str());
    long int nevents = tree_in->GetEntries();
    auto stop  = std::chrono::high_resolution_clock::now(); 
    std::chrono::duration<float> duration = stop-start;
    std::cout<<"Duration of counting loop is "<<duration.count()<<std::endl;
    
    // operation/function that will be executed in parallel  
    std::cout<<"Event loop to build output tree .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::vector<std::thread> threads;
    for (auto workerID : ROOT::TSeqI(nThreads))
      threads.emplace_back(ntupleCreation, workerID, fileList, trees_out, nevents, nThreads);
    for (auto && worker : threads) worker.join();
    stop  = std::chrono::high_resolution_clock::now(); 
    duration = stop-start;
    std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
    if(domain == domain_type::none){
      std::cout<<"Jets rejected by base cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets rejected by GEN pt requirement "<<nJetsRejectedLowGenLepton<<" fraction "<<float(nJetsRejectedLowGenLepton)/float(nJetsTotal)<<std::endl;
      std::cout<<"Jets accepted for the training "<<nJetsValidation<<" fraction "<<float(nJetsValidation)/float(nJetsTotal)<<std::endl;
    }
    else{
      std::cout<<"Events rejected by domain cuts "<<nEventsRejectedDomainKinematics<<" fraction "<<float(nEventsRejectedDomainKinematics)/float(nevents)<<std::endl;
      std::cout<<"Jets rejected by domain cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
      if(domain == domain_type::emu)
        std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedEMu<<" fraction "<<float(nJetsRejectedEMu)/float(nJetsTotal)<<std::endl;
      else if(domain == domain_type::mutau or domain == domain_type::etau)
        std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedMuTau<<" fraction "<<float(nJetsRejectedMuTau)/float(nJetsTotal)<<std::endl;
      else if(domain == domain_type::ttcharm)
        std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedTTcharm<<" fraction "<<float(nJetsRejectedTTcharm)/float(nJetsTotal)<<std::endl;

      std::cout<<"Jets accepted for the training "<<nJetsValidation<<" fraction "<<float(nJetsValidation)/float(nJetsTotal)<<std::endl;
    }
    threads.clear();
    
    std::cout<<"Writing the outout files .."<<std::endl;
    start = std::chrono::high_resolution_clock::now();
    std::string name_list = "";
    for(size_t ifile = 0; ifile < files_out.size(); ifile++){
      files_out.at(ifile)->cd();
      name_list += files_out.at(ifile)->GetName();
      name_list += " ";
      trees_out.at(ifile)->Write("",TObject::kOverwrite);
    }
    stop  = std::chrono::high_resolution_clock::now();
    duration = stop-start;
    std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;

    if(mergeThreadOutputFiles and files_out.size() > 1){
      std::cout<<"Merging outout files .."<<std::endl;
      std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
      gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
      std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
      std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
      unsigned int total_entries = tree->GetEntries();
      gSystem->Exec(("rm "+name_list).c_str());
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
    }   
  }
  else if(not inputFileDIR.empty() and boost::filesystem::is_directory(inputFileDIR)){ // pathInput is a real path to a directory

    std::cout<<"Build list of files in "<<inputFileDIR<<std::endl;

    std::vector<std::string> fileList;

    for(auto const & entry :  boost::filesystem::recursive_directory_iterator(inputFileDIR)){
      TString filePath (entry.path().string());
      if(filePath.Contains(".root")){
	if(useXRootD)
          fileList.push_back(xrootd_eos+entry.path().string());
        else
          fileList.push_back(entry.path().string());
      }
    }

    // split the job into n-steps (blocks) each containing n-files 
    unsigned int numberOfFiles = fileList.size();
    unsigned int numberOfBlocks = 1;  
    if(numberOfFiles > maxNumberOfFilesToBeProcessed){
      if(numberOfFiles%maxNumberOfFilesToBeProcessed == 0)
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed;
      else
	numberOfBlocks = numberOfFiles/maxNumberOfFilesToBeProcessed+1;
    }

    std::cout<<"In iterative mode --> Number of files "<<numberOfFiles<<" numberOfBlocks "<<numberOfBlocks<<std::endl;

    // Loop on the blocks
    for(unsigned int iblock = 0; iblock < numberOfBlocks; iblock++){          
      std::vector<std::string> fileList_block;
      if(maxNumberOfFilesToBeProcessed == 1){
	for(size_t ifile = 0; ifile < fileList.size(); ifile++)
	  fileList_block.push_back(fileList.at(ifile));      
      }
      else{
	for(size_t ifile = iblock*maxNumberOfFilesToBeProcessed; ifile < std::min(fileList.size(),size_t((iblock+1)*maxNumberOfFilesToBeProcessed)); ifile++)
	  fileList_block.push_back(fileList.at(ifile));
      }    
      std::cout<<"Block number "<<iblock<<" number of files "<<fileList_block.size()<<std::endl;
      
      // Build job list and output files --> minimum compression to speed-up the training hjob
      std::vector<std::shared_ptr<TFile> >  files_out;
      std::vector<std::shared_ptr<TTree> >  trees_out;      
      for(unsigned int ithread = 0; ithread < nThreads; ithread++){
	files_out.emplace_back(new TFile(Form("%s/%s",outputDIR.c_str(),TString(outputFileName).ReplaceAll(".root",Form("_block%d_thread%d.root",iblock,ithread)).Data()),"RECREATE"));
	trees_out.emplace_back(new TTree("tree","tree"));
	if(not compressOutputFile){
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZ4);
	  files_out.back()->SetCompressionLevel(4);
	}
	else{
	  files_out.back()->SetCompressionAlgorithm(ROOT::kLZMA);
	  files_out.back()->SetCompressionLevel(6);	  
	}
      }      

      // Count number of events to split in threads
      std::cout<<"Count total number of events to process .."<<std::endl;
      auto start = std::chrono::high_resolution_clock::now();
      std::unique_ptr<TChain> tree_in (new TChain("dnntree/tree","dnntree/tree"));
      for(size_t ifile = 0; ifile < fileList_block.size(); ifile++)
	tree_in->Add(fileList_block.at(ifile).c_str());
      long int nevents = tree_in->GetEntries();
      auto stop  = std::chrono::high_resolution_clock::now(); 
      std::chrono::duration<float> duration = stop-start;
      std::cout<<"Duration of counting loop is "<<duration.count()<<std::endl;
      
      nJetsTotal = 0;
      nJetsValidation = 0;
      nJetsRejectedLowGenLepton = 0;
      nEventsRejectedDomainKinematics = 0;
      nJetsRejectedBaseCuts = 0;

      // operation/function that will be executed in parallel  
      start = std::chrono::high_resolution_clock::now();
      std::vector<std::thread> threads;
      for (auto workerID : ROOT::TSeqI(nThreads))
	threads.emplace_back(ntupleCreation, workerID, fileList_block, trees_out, nevents, nThreads);
      for (auto && worker : threads) 
	worker.join();
      stop  = std::chrono::high_resolution_clock::now(); 
      duration = stop-start;
      std::cout<<"Duration event loop is "<<duration.count()<<std::endl;
      if(domain == domain_type::none){
	std::cout<<"Jets rejected by base cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
	std::cout<<"Jets rejected by GEN pt requirement "<<nJetsRejectedLowGenLepton<<" fraction "<<float(nJetsRejectedLowGenLepton)/float(nJetsTotal)<<std::endl;
	std::cout<<"Jets accepted for the training "<<nJetsValidation<<" fraction "<<float(nJetsValidation)/float(nJetsTotal)<<std::endl;
      }
      else{
	std::cout<<"Events rejected by domain cuts "<<nEventsRejectedDomainKinematics<<" fraction "<<float(nEventsRejectedDomainKinematics)/float(nevents)<<std::endl;
	std::cout<<"Jets rejected by domain cuts "<<nJetsRejectedBaseCuts<<" fraction "<<float(nJetsRejectedBaseCuts)/float(nJetsTotal)<<std::endl;
	if(domain == domain_type::emu)
	  std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedEMu<<" fraction "<<float(nJetsRejectedEMu)/float(nJetsTotal)<<std::endl;
	else if(domain == domain_type::mutau or domain == domain_type::etau)
	  std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedMuTau<<" fraction "<<float(nJetsRejectedMuTau)/float(nJetsTotal)<<std::endl;
	else if(domain == domain_type::ttcharm)
	  std::cout<<"Jets rejected by domain score cuts "<<nJetsRejectedTTcharm<<" fraction "<<float(nJetsRejectedTTcharm)/float(nJetsTotal)<<std::endl;
 	std::cout<<"Jets accepted for the training "<<nJetsValidation<<" fraction "<<float(nJetsValidation)/float(nJetsTotal)<<std::endl;
      }
      threads.clear();      

      std::cout<<"Writing the outout files .."<<std::endl;
      start = std::chrono::high_resolution_clock::now();
      std::string name_list = "";
      for(size_t ifile = 0; ifile < files_out.size(); ifile++){
	files_out.at(ifile)->cd();
	name_list += files_out.at(ifile)->GetName();
	name_list += " ";
	trees_out.at(ifile)->Write("",TObject::kOverwrite);
      }
      stop  = std::chrono::high_resolution_clock::now();
      duration = stop-start;
      std::cout<<"Duration in writing output files is "<<duration.count()<<std::endl;
      
      if(mergeThreadOutputFiles and files_out.size() > 1){
	std::cout<<"Merging outout files .."<<std::endl;
	std::cout<<"hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list<<std::endl;
	gSystem->Exec(("hadd -ff -f "+outputDIR+"/"+outputFileName+" "+name_list).c_str());      
	std::unique_ptr<TFile> outputFile (TFile::Open((outputDIR+"/"+outputFileName).c_str()));
	std::unique_ptr<TTree> tree ((TTree*) outputFile->Get("tree"));
	unsigned int total_entries = tree->GetEntries();
	gSystem->Exec(("rm "+name_list).c_str());
	stop  = std::chrono::high_resolution_clock::now(); 
	duration = stop-start;
	std::cout<<"Duration merging files is "<<duration.count()<<" entries from trees "<<total_entries<<std::endl;    
      } 
    }
  }
  std::cout<<"Exiting from the code"<<std::endl;
}
