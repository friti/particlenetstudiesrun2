#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"

#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Candidate/interface/Candidate.h"
#include "CommonTools/Utils/interface/StringCutObjectSelector.h"

#include <memory>

class CandidateMatchedViewProducer : public edm::stream::EDProducer<> {
public:
  explicit CandidateMatchedViewProducer(const edm::ParameterSet &);
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  
private:

  virtual void beginJob();
  virtual void produce(edm::Event&, const edm::EventSetup&) override;
  virtual void endJob();
  
  edm::EDGetTokenT<edm::View<reco::Candidate> > srcToken_;
  edm::EDGetTokenT<edm::View<reco::Candidate> > matchedToken_;
  double deltaR_;
};

CandidateMatchedViewProducer::CandidateMatchedViewProducer(const edm::ParameterSet &params):
  srcToken_(consumes<edm::View<reco::Candidate>>(params.getParameter<edm::InputTag>("src"))),
  matchedToken_(consumes<edm::View<reco::Candidate>>(params.getParameter<edm::InputTag>("matched"))),
  deltaR_(params.existsAs<double>("deltaR") ? params.getParameter<double>("deltaR") : 0.4){
  produces<reco::CandidateCollection>();
}

// ------------ method called to produce the data  ------------
void CandidateMatchedViewProducer::produce(edm::Event & event, const edm::EventSetup &) {

  edm::Handle<edm::View<reco::Candidate>> candsSrc;
  event.getByToken(srcToken_, candsSrc);
  edm::Handle<edm::View<reco::Candidate>> matchedSrc;
  event.getByToken(matchedToken_, matchedSrc);

  // Create the output collection
  auto outColl = std::make_unique<reco::CandidateCollection>();

  // Loop over candidates
  unsigned int counter = 0;
  for (edm::View<reco::Candidate>::const_iterator cit = candsSrc->begin(); cit != candsSrc->end(); ++cit, ++counter) {
    double minDR = 9999;
    int ipos  = -1;
    for (edm::View<reco::Candidate>::const_iterator mit = matchedSrc->begin(); mit != matchedSrc->end(); ++mit) {      
      double dRval = reco::deltaR(cit->eta(),cit->phi(),mit->eta(),mit->phi());
      if (dRval < deltaR_ and dRval < minDR) {
	minDR = dRval;
	ipos  = counter;
      }
    }
    if(ipos >= 0)
      outColl->push_back(candsSrc->at(ipos));
  }  
  event.put(std::move(outColl));
}

void CandidateMatchedViewProducer::beginJob() {
}

void CandidateMatchedViewProducer::endJob() {
}

void CandidateMatchedViewProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
    edm::ParameterSetDescription desc;
    desc.setUnknown();
    descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(CandidateMatchedViewProducer);
