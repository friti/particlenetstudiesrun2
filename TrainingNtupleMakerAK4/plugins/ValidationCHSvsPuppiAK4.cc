// Standard C++ includes
#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>

// ROOT includes
#include <TTree.h>
#include <TH1F.h>
#include <TGraphAsymmErrors.h>
#include <TTree.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>

// CMSSW framework includes
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/ESGetToken.h"

// CMSSW data formats
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include <DataFormats/Candidate/interface/ShallowCloneCandidate.h>
#include <DataFormats/PatCandidates/interface/CompositeCandidate.h>

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"

// Other relevant CMSSW includes
#include "CommonTools/UtilAlgos/interface/TFileService.h" 
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"

class ValidationCHSvsPuppiAK4 : public edm::one::EDAnalyzer<edm::one::SharedResources, edm::one::WatchRuns> {

public:
  explicit ValidationCHSvsPuppiAK4(const edm::ParameterSet&);
  ~ValidationCHSvsPuppiAK4();
  
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
    
private:

  bool applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi);
  bool applyPileupJetID(const pat::Jet & jet, const std::string & level);

  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;
  
  virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
  virtual void endRun(edm::Run const&, edm::EventSetup const&) override;

  void initializeBranches();

  // MC information
  const edm::EDGetTokenT<std::vector<PileupSummaryInfo> > pileupInfoToken;
  const edm::EDGetTokenT<reco::GenParticleCollection > gensToken;
  const edm::EDGetTokenT<GenEventInfoProduct> genEvtInfoToken;

  // Filter result
  const edm::InputTag filterResultsTag;  
  const edm::EDGetTokenT<edm::TriggerResults> filterResultsToken;

  // Vertices from miniAOD
  const edm::EDGetTokenT<reco::VertexCollection > primaryVerticesToken;
  const edm::EDGetTokenT<reco::VertexCompositePtrCandidateCollection> secondaryVerticesToken;
  edm::EDGetTokenT< double > rhoToken;

  // Pat objects from miniAOD
  const edm::EDGetTokenT<pat::MuonCollection> muonsToken;
  const edm::EDGetTokenT<pat::ElectronCollection> electronsToken;
  const edm::EDGetTokenT<pat::TauCollection> tausToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsToken;
  const edm::EDGetTokenT<pat::JetCollection> jetsPuppiToken;
  const edm::EDGetTokenT<edm::ValueMap<float> > puppiWeightsToken;
  const edm::EDGetTokenT<pat::METCollection> metToken;
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsToken;
  const edm::EDGetTokenT<reco::GenJetCollection > genJetsWithNuToken;

  // MET filters
  std::vector<std::string>   filterPathsVector;
  std::map<std::string, int> filterPathsMap;

  // Flag for the analyzer
  float muonPtMin, muonEtaMax;
  float electronPtMin, electronEtaMax;
  float tauPtMin, tauEtaMax;
  float jetPtMin;
  float jetEtaMax;
  float dRJetGenMatch;
  bool  dumpOnlyJetMatchedToGen;
  bool  isMC;
  std::vector<std::string> pnetDiscriminatorLabels;
  std::vector<std::string> pnetPuppiDiscriminatorLabels;

  // Event coordinates
  unsigned event, run, lumi;
  // Pileup information
  unsigned int putrue;
  // Flags for various event filters
  unsigned int flags;
  // Cross-section and event weight information for MC events
  float xsec, wgt;
  // Rho
  float rho;
  // Generator-level information (Gen and LHE particles)
  std::vector<float>  gen_particle_pt;
  std::vector<float>  gen_particle_eta;
  std::vector<float>  gen_particle_phi;
  std::vector<float>  gen_particle_mass;
  std::vector<int>    gen_particle_id;
  std::vector<unsigned int>  gen_particle_status;
  std::vector<int>    gen_particle_daughters_id;
  std::vector<unsigned int> gen_particle_daughters_igen;
  std::vector<unsigned int> gen_particle_daughters_status;
  std::vector<float>  gen_particle_daughters_pt;
  std::vector<float>  gen_particle_daughters_eta;
  std::vector<float>  gen_particle_daughters_phi;
  std::vector<float>  gen_particle_daughters_mass;
  std::vector<int>    gen_particle_daughters_charge;
  // Vertex RECO
  unsigned int npv;
  unsigned int nsv;
  // Collection of muon 4-vectors, muon ID bytes, muon isolation values
  std::vector<float>  muon_pt;
  std::vector<float>  muon_eta;
  std::vector<float>  muon_phi;
  std::vector<float>  muon_mass;
  std::vector<unsigned int> muon_id;
  std::vector<int>    muon_charge;
  std::vector<unsigned int> muon_iso;
  std::vector<float>  muon_d0;
  std::vector<float>  muon_dz;
  // Collection of electron 4-vectors, electron ID bytes, electron isolation values
  std::vector<float>  electron_pt;
  std::vector<float>  electron_eta;
  std::vector<float>  electron_phi;
  std::vector<float>  electron_mass;
  std::vector<unsigned int> electron_id;
  std::vector<float>  electron_idscore;
  std::vector<int>    electron_charge;
  std::vector<float>  electron_d0;
  std::vector<float>  electron_dz;  
  // Collection of taus 4-vectors, tau ID bytes, tau isolation values
  std::vector<float>  tau_pt;
  std::vector<float>  tau_eta;
  std::vector<float>  tau_phi;
  std::vector<float>  tau_mass;
  std::vector<float>  tau_dxy;
  std::vector<float>  tau_dz;
  std::vector<unsigned int>  tau_decaymode;
  std::vector<unsigned int> tau_idjet_wp;
  std::vector<unsigned int> tau_idmu_wp;
  std::vector<unsigned int> tau_idele_wp;
  std::vector<float>  tau_idjet;
  std::vector<float>  tau_idele;
  std::vector<float>  tau_idmu;
  std::vector<int>    tau_charge;
  std::vector<float>  tau_genmatch_pt;
  std::vector<float>  tau_genmatch_eta;
  std::vector<float>  tau_genmatch_phi;
  std::vector<float>  tau_genmatch_mass;
  std::vector<int> tau_genmatch_decaymode;
  // MET
  float met;
  float met_phi;
  float met_signif;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_pt;
  std::vector<float> jet_eta;
  std::vector<float> jet_phi;
  std::vector<float> jet_mass;
  std::vector<float> jet_energy;
  std::vector<float> jet_pt_raw;
  std::vector<float> jet_mass_raw;
  std::vector<float> jet_energy_raw;
  std::vector<float> jet_chf;
  std::vector<float> jet_nhf;
  std::vector<float> jet_elf;
  std::vector<float> jet_phf;
  std::vector<float> jet_muf;
  std::vector<float> jet_deepjet_probb;
  std::vector<float> jet_deepjet_probbb;
  std::vector<float> jet_deepjet_problepb;
  std::vector<float> jet_deepjet_probc;
  std::vector<float> jet_deepjet_probg;
  std::vector<float> jet_deepjet_probuds;
  std::map<std::string,std::vector<float> > jet_pnetlast_score;
  std::vector<unsigned int> jet_id;
  std::vector<unsigned int> jet_puid;
  std::vector<unsigned int> jet_ncand;
  std::vector<unsigned int> jet_nch;
  std::vector<unsigned int> jet_nnh;
  std::vector<unsigned int> jet_nel;
  std::vector<unsigned int> jet_nph;
  std::vector<unsigned int> jet_nmu;
  std::vector<unsigned int> jet_hflav;
  std::vector<int> jet_pflav;
  std::vector<float> jet_pfcandidate_pt;
  std::vector<float> jet_pfcandidate_eta;
  std::vector<float> jet_pfcandidate_puppiw;
  std::vector<float> jet_pfcandidate_dxy;
  std::vector<float> jet_pfcandidate_dz;  
  std::vector<float> jet_pfcandidate_dxysig;
  std::vector<float> jet_pfcandidate_dzsig;  
  std::vector<int> jet_pfcandidate_charge;
  std::vector<unsigned int> jet_pfcandidate_id;
  std::vector<unsigned int> jet_pfcandidate_frompv;
  std::vector<unsigned int> jet_pfcandidate_ijet;
  std::vector<unsigned int> jet_nbhad;
  std::vector<unsigned int> jet_nchad;
  std::vector<float> jet_genmatch_pt;
  std::vector<float> jet_genmatch_eta;
  std::vector<float> jet_genmatch_phi;
  std::vector<float> jet_genmatch_mass;
  std::vector<float> jet_genmatch_energy;
  std::vector<float> jet_genmatch_wnu_pt;
  std::vector<float> jet_genmatch_wnu_eta;
  std::vector<float> jet_genmatch_wnu_phi;
  std::vector<float> jet_genmatch_wnu_mass;
  std::vector<float> jet_genmatch_wnu_energy;
  // Collection of jet 4-vectors, jet ID bytes and b-tag discriminant values
  std::vector<float> jet_puppi_pt;
  std::vector<float> jet_puppi_eta;
  std::vector<float> jet_puppi_phi;
  std::vector<float> jet_puppi_mass;
  std::vector<float> jet_puppi_energy;
  std::vector<float> jet_puppi_pt_raw;
  std::vector<float> jet_puppi_mass_raw;
  std::vector<float> jet_puppi_energy_raw;
  std::vector<float> jet_puppi_chf;
  std::vector<float> jet_puppi_nhf;
  std::vector<float> jet_puppi_elf;
  std::vector<float> jet_puppi_phf;
  std::vector<float> jet_puppi_muf;
  std::vector<float> jet_puppi_deepjet_probb;
  std::vector<float> jet_puppi_deepjet_probbb;
  std::vector<float> jet_puppi_deepjet_problepb;
  std::vector<float> jet_puppi_deepjet_probc;
  std::vector<float> jet_puppi_deepjet_probg;
  std::vector<float> jet_puppi_deepjet_probuds;
  std::map<std::string,std::vector<float> > jet_puppi_pnetlast_score;
  std::vector<unsigned int> jet_puppi_id;
  std::vector<unsigned int> jet_puppi_puid;
  std::vector<unsigned int> jet_puppi_ncand;
  std::vector<unsigned int> jet_puppi_nch;
  std::vector<unsigned int> jet_puppi_nnh;
  std::vector<unsigned int> jet_puppi_nel;
  std::vector<unsigned int> jet_puppi_nph;
  std::vector<unsigned int> jet_puppi_nmu;
  std::vector<unsigned int> jet_puppi_hflav;
  std::vector<int> jet_puppi_pflav;
  std::vector<unsigned int> jet_puppi_nbhad;
  std::vector<unsigned int> jet_puppi_nchad;
  std::vector<float> jet_puppi_genmatch_pt;
  std::vector<float> jet_puppi_genmatch_eta;
  std::vector<float> jet_puppi_genmatch_phi;
  std::vector<float> jet_puppi_genmatch_mass;
  std::vector<float> jet_puppi_genmatch_energy;
  std::vector<float> jet_puppi_genmatch_wnu_pt;
  std::vector<float> jet_puppi_genmatch_wnu_eta;
  std::vector<float> jet_puppi_genmatch_wnu_phi;
  std::vector<float> jet_puppi_genmatch_wnu_mass;
  std::vector<float> jet_puppi_genmatch_wnu_energy;
  
  // TTree carrying the event weight information
  TTree* tree;

  // Sorters to order object collections in decreasing order of pT
  template<typename T> 
  class PatPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i.pt() > j.pt());
    }
  };
  PatPtSorter<pat::Muon>     muonSorter;
  PatPtSorter<pat::Electron> electronSorter;
  PatPtSorter<pat::Tau>      tauSorter;
  PatPtSorter<pat::PackedCandidate>  packedPFCandidateSorter;

  template<typename T> 
  class CandPtrPtSorter {
  public:
    bool operator()(const T & i, const T & j) const {
      return (i->pt() > j->pt());
    }
  };
  CandPtrPtSorter<reco::CandidatePtr>  candidatePtrSorter;

  template<typename T> 
  class PatRefPtSorter {
  public:
    bool operator()(const T& i, const T& j) const {
      return (i->pt() > j->pt());
    }
  };
  PatRefPtSorter<pat::JetRef>      jetRefSorter;
  PatRefPtSorter<reco::GenJetRef>  genJetRefSorter;
  PatRefPtSorter<edm::Ptr<pat::Jet> > jetPtrSorter;

};


ValidationCHSvsPuppiAK4::ValidationCHSvsPuppiAK4(const edm::ParameterSet& iConfig): 
  pileupInfoToken          (mayConsume<std::vector<PileupSummaryInfo> >  (iConfig.getParameter<edm::InputTag>("pileUpInfo"))),
  gensToken                (mayConsume<reco::GenParticleCollection>   (iConfig.getParameter<edm::InputTag>("genParticles"))),
  genEvtInfoToken          (mayConsume<GenEventInfoProduct>  (iConfig.getParameter<edm::InputTag>("genEventInfo"))),
  filterResultsTag         (iConfig.getParameter<edm::InputTag>("filterResults")),
  filterResultsToken       (consumes<edm::TriggerResults>  (filterResultsTag)),
  primaryVerticesToken     (consumes<reco::VertexCollection>  (iConfig.getParameter<edm::InputTag>("pVertices"))),
  secondaryVerticesToken   (consumes<reco::VertexCompositePtrCandidateCollection>  (iConfig.getParameter<edm::InputTag>("sVertices"))),
  rhoToken                 (consumes<double>(iConfig.getParameter<edm::InputTag>("rho"))),
  muonsToken               (consumes<pat::MuonCollection>  (iConfig.getParameter<edm::InputTag>("muons"))), 
  electronsToken           (consumes<pat::ElectronCollection>  (iConfig.getParameter<edm::InputTag>("electrons"))), 
  tausToken                (consumes<pat::TauCollection>  (iConfig.getParameter<edm::InputTag>("taus"))), 
  jetsToken                (consumes<pat::JetCollection>  (iConfig.getParameter<edm::InputTag>("jets"))),
  jetsPuppiToken           (consumes<pat::JetCollection>  (iConfig.getParameter<edm::InputTag>("jetsPuppi"))),
  puppiWeightsToken        (mayConsume<edm::ValueMap<float> > (iConfig.getParameter<edm::InputTag>("puppiWeights"))),
  metToken                 (consumes<pat::METCollection>  (iConfig.getParameter<edm::InputTag>("met"))),
  genJetsToken             (consumes<reco::GenJetCollection > (iConfig.getParameter<edm::InputTag>("genJets"))),
  genJetsWithNuToken       (consumes<reco::GenJetCollection > (iConfig.getParameter<edm::InputTag>("genJetsWithNu"))),
  muonPtMin                (iConfig.existsAs<double>("muonPtMin")    ? iConfig.getParameter<double>("muonPtMin") : 15.),
  muonEtaMax               (iConfig.existsAs<double>("muonEtaMax")   ? iConfig.getParameter<double>("muonEtaMax") : 2.4),
  electronPtMin            (iConfig.existsAs<double>("electronPtMin")   ? iConfig.getParameter<double>("electronPtMin") : 15.),
  electronEtaMax           (iConfig.existsAs<double>("electronEtaMax")  ? iConfig.getParameter<double>("electronEtaMax") : 2.5),
  tauPtMin                 (iConfig.existsAs<double>("tauPtMin")   ? iConfig.getParameter<double>("tauPtMin") : 20.),
  tauEtaMax                (iConfig.existsAs<double>("tauEtaMax")  ? iConfig.getParameter<double>("tauEtaMax") : 2.5),
  jetPtMin                 (iConfig.existsAs<double>("jetPtMin")       ? iConfig.getParameter<double>("jetPtMin") : 25.),
  jetEtaMax                (iConfig.existsAs<double>("jetEtaMax")      ? iConfig.getParameter<double>("jetEtaMax") : 2.5),
  dRJetGenMatch            (iConfig.existsAs<double>("dRJetGenMatch")        ? iConfig.getParameter<double>("dRJetGenMatch") : 0.4),
  dumpOnlyJetMatchedToGen  (iConfig.existsAs<bool>("dumpOnlyJetMatchedToGen")  ? iConfig.getParameter<bool>("dumpOnlyJetMatchedToGen") : false),
  isMC                     (iConfig.existsAs<bool>("isMC")   ? iConfig.getParameter<bool>  ("isMC") : true),
  pnetDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("pnetDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("pnetDiscriminatorLabels") : std::vector<std::string> ()),
  pnetPuppiDiscriminatorLabels  (iConfig.existsAs<std::vector<std::string> > ("pnetPuppiDiscriminatorLabels") ? iConfig.getParameter<std::vector<std::string>>("pnetPuppiDiscriminatorLabels") : std::vector<std::string> ()),
  xsec                     (iConfig.existsAs<double>("xsec") ? iConfig.getParameter<double>("xsec") * 1000.0 : 1.){

  usesResource("TFileService");
}

ValidationCHSvsPuppiAK4::~ValidationCHSvsPuppiAK4() {}

void ValidationCHSvsPuppiAK4::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  edm::Handle<edm::TriggerResults>  filterResultsH;
  iEvent.getByToken(filterResultsToken, filterResultsH); 

  edm::Handle<reco::VertexCollection > primaryVerticesH;
  iEvent.getByToken(primaryVerticesToken, primaryVerticesH);

  edm::Handle<reco::VertexCompositePtrCandidateCollection> secondaryVerticesH;
  iEvent.getByToken(secondaryVerticesToken, secondaryVerticesH);
  reco::VertexCompositePtrCandidateCollection svColl = *secondaryVerticesH;

  edm::Handle<double> rho_val;
  iEvent.getByToken(rhoToken,rho_val);  

  edm::Handle<pat::MuonCollection> muonsH;
  iEvent.getByToken(muonsToken,muonsH);
  pat::MuonCollection muonsColl = *muonsH;

  edm::Handle<pat::ElectronCollection> electronsH;
  iEvent.getByToken(electronsToken,electronsH);
  pat::ElectronCollection electronsColl = *electronsH;

  edm::Handle<pat::TauCollection> tausH;
  iEvent.getByToken(tausToken,tausH);
  pat::TauCollection tausColl = *tausH;
    
  edm::Handle<pat::JetCollection> jetsH;
  iEvent.getByToken(jetsToken, jetsH);

  edm::Handle<pat::JetCollection> jetsPuppiH;
  iEvent.getByToken(jetsPuppiToken, jetsPuppiH);

  edm::Handle<edm::ValueMap<float> > puppiWeightsH;
  iEvent.getByToken(puppiWeightsToken,puppiWeightsH);

  edm::Handle<pat::METCollection> metH;
  iEvent.getByToken(metToken, metH);

  edm::Handle<std::vector<PileupSummaryInfo> > pileupInfoH;
  edm::Handle<GenEventInfoProduct> genEvtInfoH;
  edm::Handle<reco::GenParticleCollection> gensH;
  edm::Handle<reco::GenJetCollection> genJetsH;
  edm::Handle<reco::GenJetCollection> genJetsWithNuH;
  if(isMC){    
    iEvent.getByToken(pileupInfoToken, pileupInfoH);  
    iEvent.getByToken(genEvtInfoToken, genEvtInfoH);    
    iEvent.getByToken(gensToken, gensH);
    iEvent.getByToken(genJetsToken, genJetsH);    
    iEvent.getByToken(genJetsWithNuToken, genJetsWithNuH);
  }

  initializeBranches();
     
  event = iEvent.id().event();
  run   = iEvent.id().run();
  lumi  = iEvent.luminosityBlock();

  wgt = 1.0;
  if(genEvtInfoH.isValid())
    wgt = genEvtInfoH->weight(); 

  // Pileup information
  putrue = 0;
  if(pileupInfoH.isValid()){
    for (auto pileupInfo_iter = pileupInfoH->begin(); pileupInfo_iter != pileupInfoH->end(); ++pileupInfo_iter) {
      if (pileupInfo_iter->getBunchCrossing() == 0) 
	putrue = (unsigned int) pileupInfo_iter->getTrueNumInteractions();
    }
  }

  // Vertices RECO
  npv  = (unsigned int) primaryVerticesH->size();
  nsv  = (unsigned int) secondaryVerticesH->size();

  // MET filter info
  unsigned int flagvtx       = 1 * 1;
  unsigned int flaghalo      = 1 * 2;
  unsigned int flaghbhe      = 1 * 4;
  unsigned int flaghbheiso   = 1 * 8;
  unsigned int flagecaltp    = 1 * 16;
  unsigned int flagbadmuon   = 1 * 32; 
  unsigned int flagbadhad    = 1 * 64;
 
  // Which MET filters passed
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    if (filterPathsMap[filterPathsVector[i]] == -1) continue;
    if (i == 0  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagvtx       = 0; // goodVertices
    if (i == 1  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghalo      = 0; // CSCTightHaloFilter
    if (i == 2  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbhe      = 0; // HBHENoiseFilter
    if (i == 3  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flaghbheiso   = 0; // HBHENoiseIsoFilter
    if (i == 4  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagecaltp    = 0; // EcalDeadCellTriggerPrimitiveFilter
    if (i == 5  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadmuon   = 0; // badmuon
    if (i == 6  and filterResultsH->accept(filterPathsMap[filterPathsVector[i]])) flagbadhad    = 0; // badhadrons
  }

  flags = flagvtx + flaghalo + flaghbhe + flaghbheiso + flagecaltp + flagbadmuon + flagbadhad;

  // GEN particle info                                                                                                                                                                           
  if(gensH.isValid()){
    unsigned int igen = 0;
    for (auto gens_iter = gensH->begin(); gens_iter != gensH->end(); ++gens_iter) {
      // Higgs, Z and W beforethe decay and after the shower effects                                                                                                                            
      if((abs(gens_iter->pdgId()) == 25 or abs(gens_iter->pdgId()) == 24 or abs(gens_iter->pdgId()) == 23) and
         gens_iter->isLastCopy() and
         gens_iter->statusFlags().fromHardProcess()){
	
        gen_particle_pt.push_back(gens_iter->pt());
        gen_particle_eta.push_back(gens_iter->eta());
        gen_particle_phi.push_back(gens_iter->phi());
        gen_particle_mass.push_back(gens_iter->mass());
        gen_particle_id.push_back(gens_iter->pdgId());
        gen_particle_status.push_back(gens_iter->status());

        for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
          gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
          gen_particle_daughters_igen.push_back(igen);
          gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
          gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
          gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
          gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());
          gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
          gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
        }
        igen++;
      }
      // Final states Leptons (e,mu) and Neutrinos --> exclude taus. They need to be prompt or from Tau decay                                                                                        
      if (abs(gens_iter->pdgId()) > 10 and abs(gens_iter->pdgId()) < 17 and abs(gens_iter->pdgId()) != 15  and
          (gens_iter->isPromptFinalState() or
           gens_iter->isDirectPromptTauDecayProductFinalState())) {

        gen_particle_pt.push_back(gens_iter->pt());
        gen_particle_eta.push_back(gens_iter->eta());
        gen_particle_phi.push_back(gens_iter->phi());
        gen_particle_mass.push_back(gens_iter->mass());
        gen_particle_id.push_back(gens_iter->pdgId());
        gen_particle_status.push_back(gens_iter->status());
        igen++;
      }

      // Final state quarks or gluons from the hard process before the shower --> partons in which H/Z/W/top decay into                                                                              
      if (((abs(gens_iter->pdgId()) >= 1 and abs(gens_iter->pdgId()) <= 5) or abs(gens_iter->pdgId()) == 21) and
          gens_iter->statusFlags().fromHardProcess() and
          gens_iter->statusFlags().isFirstCopy()){
        gen_particle_pt.push_back(gens_iter->pt());
        gen_particle_eta.push_back(gens_iter->eta());
        gen_particle_phi.push_back(gens_iter->phi());
        gen_particle_mass.push_back(gens_iter->mass());
        gen_particle_id.push_back(gens_iter->pdgId());
        gen_particle_status.push_back(gens_iter->status());
        igen++;
      }

      // Special case of taus: last-copy, from hard process and, prompt and decayed                                                                                                             
      if(abs(gens_iter->pdgId()) == 15 and
         gens_iter->isLastCopy() and
         gens_iter->statusFlags().fromHardProcess() and
         gens_iter->isPromptDecayed()){ // hadronic taus                                                                                                                                              

        gen_particle_pt.push_back(gens_iter->pt());
        gen_particle_eta.push_back(gens_iter->eta());
        gen_particle_phi.push_back(gens_iter->phi());
        gen_particle_mass.push_back(gens_iter->mass());
        gen_particle_id.push_back(gens_iter->pdgId());
        gen_particle_status.push_back(gens_iter->status());

        // only store the final decay particles                                                                                                                                                     
        for(size_t idau = 0; idau < gens_iter->numberOfDaughters(); idau++){
          if(not dynamic_cast<const reco::GenParticle*>(gens_iter->daughter(idau))->statusFlags().isPromptTauDecayProduct()) continue;
          gen_particle_daughters_id.push_back(gens_iter->daughter(idau)->pdgId());
          gen_particle_daughters_igen.push_back(igen);
          gen_particle_daughters_pt.push_back(gens_iter->daughter(idau)->pt());
          gen_particle_daughters_eta.push_back(gens_iter->daughter(idau)->eta());
          gen_particle_daughters_phi.push_back(gens_iter->daughter(idau)->phi());
          gen_particle_daughters_mass.push_back(gens_iter->daughter(idau)->mass());
          gen_particle_daughters_status.push_back(gens_iter->daughter(idau)->status());
          gen_particle_daughters_charge.push_back(gens_iter->daughter(idau)->charge());
        }
        igen++;
      }
    }
  }

  /// MET
  met      = metH->front().corPt();
  met_phi  = metH->front().corPhi();
  met_signif = metH->front().significance();
    
  // Sorting muons based on pT
  sort(muonsColl.begin(),muonsColl.end(),muonSorter);

  for (size_t i = 0; i < muonsColl.size(); i++) {

    if(muonsColl[i].pt() < muonPtMin) continue;
    if(fabs(muonsColl[i].eta()) > muonEtaMax) continue;

    muon_pt.push_back(muonsColl[i].pt());
    muon_eta.push_back(muonsColl[i].eta());
    muon_phi.push_back(muonsColl[i].phi());
    muon_mass.push_back(muonsColl[i].mass());

    // Muon isolation
    int isoval = 0;
    if(muonsColl[i].passed(reco::Muon::PFIsoLoose))
      isoval += 1;
    if(muonsColl[i].passed(reco::Muon::PFIsoMedium))
      isoval += 2;
    if(muonsColl[i].passed(reco::Muon::PFIsoTight))
      isoval += 4;
    if(muonsColl[i].passed(reco::Muon::PFIsoVeryTight))
      isoval += 8;
    if(muonsColl[i].passed(reco::Muon::MiniIsoLoose))
      isoval += 16;
    if(muonsColl[i].passed(reco::Muon::MiniIsoMedium))
      isoval += 32;
    if(muonsColl[i].passed(reco::Muon::MiniIsoTight))
      isoval += 64;
    
    muon_iso.push_back(isoval);
    
    // Muon id
    int midval = 0;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdLoose))
       midval += 1;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdMedium))
       midval += 2;
    if(muonsColl[i].passed(reco::Muon::CutBasedIdTight))
       midval += 4;
    if(muonsColl[i].passed(reco::Muon::MvaLoose))
       midval += 8;
    if(muonsColl[i].passed(reco::Muon::MvaMedium))
       midval += 16;
    if(muonsColl[i].passed(reco::Muon::MvaTight))
       midval += 32;
    
    muon_id.push_back(midval);
    muon_charge.push_back(muonsColl[i].charge());
    muon_d0.push_back(muonsColl[i].muonBestTrack()->dxy(primaryVerticesH->at(0).position()));
    muon_dz.push_back(muonsColl[i].muonBestTrack()->dz(primaryVerticesH->at(0).position()));
  }
  
  // Sorting electrons based on pT
  sort(electronsColl.begin(),electronsColl.end(),electronSorter);

  for (size_t i = 0; i < electronsColl.size(); i++) {

    if(electronsColl[i].pt() < electronPtMin) continue;
    if(fabs(electronsColl[i].eta()) > electronEtaMax) continue;

    electron_pt.push_back(electronsColl[i].pt());
    electron_eta.push_back(electronsColl[i].eta());
    electron_phi.push_back(electronsColl[i].phi());
    electron_mass.push_back(electronsColl[i].mass());

    int eidval = 0;   
    if(electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-loose"))
      eidval += 1;
    if(electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-medium"))
      eidval += 2;
    if(electronsColl[i].electronID("cutBasedElectronID-Fall17-94X-V2-tight"))
      eidval += 4;
    if(electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wpLoose"))
      eidval += 8;
    if(electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp90"))
      eidval += 16;
    if(electronsColl[i].electronID("mvaEleID-Fall17-iso-V2-wp80"))
      eidval += 32;

    electron_id.push_back(eidval);
    electron_idscore.push_back(electronsColl[i].userFloat("ElectronMVAEstimatorRun2Fall17IsoV2Values"));
    electron_charge.push_back(electronsColl[i].charge());
    electron_d0.push_back(electronsColl[i].gsfTrack()->dxy(primaryVerticesH->at(0).position()));
    electron_dz.push_back(electronsColl[i].gsfTrack()->dz(primaryVerticesH->at(0).position()));
  }

  // Sorting taus based on pT
  sort(tausColl.begin(),tausColl.end(),tauSorter);
  
  for (size_t i = 0; i < tausColl.size(); i++) {
    
    if(tausColl[i].pt() < tauPtMin) continue;
    if(fabs(tausColl[i].eta()) > tauEtaMax) continue;

    tau_pt.push_back(tausColl[i].pt());
    tau_eta.push_back(tausColl[i].eta());
    tau_phi.push_back(tausColl[i].phi());
    tau_mass.push_back(tausColl[i].mass());
    tau_dxy.push_back(tausColl[i].dxy());
    tau_dz.push_back((tausColl[i].leadChargedHadrCand().get() ? dynamic_cast<const pat::PackedCandidate*>(tausColl[i].leadChargedHadrCand().get())->dz() : 0.));
    tau_decaymode.push_back(tausColl[i].decayMode());
    tau_idjet.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSjetraw"));
    tau_idele.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSeraw"));
    tau_idmu.push_back(tausColl[i].tauID("byDeepTau2017v2p1VSmuraw"));
    tau_charge.push_back(tausColl[i].charge());    

    int tauvsjetid = 0;
    if(tausColl[i].tauID("byVVVLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 1;
    if(tausColl[i].tauID("byVVLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 2;
    if(tausColl[i].tauID("byVLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 4;
    if(tausColl[i].tauID("byLooseDeepTau2017v2p1VSjet"))
      tauvsjetid += 8;
    if(tausColl[i].tauID("byMediumDeepTau2017v2p1VSjet"))
      tauvsjetid += 16;
    if(tausColl[i].tauID("byTightDeepTau2017v2p1VSjet"))
      tauvsjetid += 32;

    tau_idjet_wp.push_back(tauvsjetid);

    int tauvsmuid = 0;
    if(tausColl[i].tauID("byVLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 1;
    if(tausColl[i].tauID("byLooseDeepTau2017v2p1VSmu"))
      tauvsmuid += 2;
    if(tausColl[i].tauID("byMediumDeepTau2017v2p1VSmu"))
      tauvsmuid += 4;
    if(tausColl[i].tauID("byTightDeepTau2017v2p1VSmu"))
      tauvsmuid += 8;

    tau_idmu_wp.push_back(tauvsmuid);

    int tauvselid = 0;
    if(tausColl[i].tauID("byVVVLooseDeepTau2017v2p1VSe"))
      tauvselid += 1;
    if(tausColl[i].tauID("byVVLooseDeepTau2017v2p1VSe"))
      tauvselid += 2;
    if(tausColl[i].tauID("byVLooseDeepTau2017v2p1VSe"))
      tauvselid += 4;
    if(tausColl[i].tauID("byLooseDeepTau2017v2p1VSe"))
      tauvselid += 8;
    if(tausColl[i].tauID("byMediumDeepTau2017v2p1VSe"))
      tauvselid += 16;
    if(tausColl[i].tauID("byTightDeepTau2017v2p1VSe"))
      tauvselid += 32;

    tau_idele_wp.push_back(tauvselid);

    if(tausColl[i].genJet()){
      tau_genmatch_pt.push_back(tausColl[i].genJet()->pt());
      tau_genmatch_eta.push_back(tausColl[i].genJet()->eta());
      tau_genmatch_phi.push_back(tausColl[i].genJet()->phi());
      tau_genmatch_mass.push_back(tausColl[i].genJet()->mass());
      // reconstruct the decay mode of the gen-jet                                                                                                                                                
      unsigned int tau_ch = 0;
      unsigned int tau_ph = 0;
      unsigned int tau_nh = 0;
      auto gen_constituents = tausColl[i].genJet()->getGenConstituents();
      for(size_t iconst = 0; iconst < gen_constituents.size(); iconst++){
        auto part = gen_constituents[iconst];
        if(part->status() != 1) continue;
        if(part->charge() == 0 and abs(part->pdgId()) == 22) tau_ph++;
        if(part->charge() == 0 and abs(part->pdgId()) != 22) tau_nh++;
        if(part->charge() != 0 and abs(part->pdgId()) != 11 and abs(part->pdgId()) != 13) tau_ch++;
      }
      tau_genmatch_decaymode.push_back(5*(tau_ch-1)+tau_ph/2+tau_nh);
    }
    else{
      tau_genmatch_pt.push_back(-1);
      tau_genmatch_eta.push_back(-1);
      tau_genmatch_phi.push_back(-1);
      tau_genmatch_mass.push_back(-1);
      tau_genmatch_decaymode.push_back(-1);
    }
  }
  
  // Standard gen-jets excluding the neutrinos
  std::vector<reco::GenJetRef> jetv_gen;   
  if(genJetsH.isValid()){
    for (auto jets_iter = genJetsH->begin(); jets_iter != genJetsH->end(); ++jets_iter) {                                                                                                   
      reco::GenJetRef jref (genJetsH, jets_iter - genJetsH->begin());                                                                                                                      
      jetv_gen.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen.begin(), jetv_gen.end(), genJetRefSorter);
  }

  std::vector<reco::GenJetRef> jetv_gen_wnu;   
  if(genJetsWithNuH.isValid()){
    for (auto jets_iter = genJetsWithNuH->begin(); jets_iter != genJetsWithNuH->end(); ++jets_iter) {                                                                                                
      reco::GenJetRef jref (genJetsWithNuH, jets_iter - genJetsWithNuH->begin());                                                                                                                   
      jetv_gen_wnu.push_back(jref);                                                                                                                                                              
    }
    sort(jetv_gen_wnu.begin(), jetv_gen_wnu.end(), genJetRefSorter);
  }

  std::vector<pat::JetRef> jetv;   
  for (auto jets_iter = jetsH->begin(); jets_iter != jetsH->end(); ++jets_iter) {                                                                                                                     
    pat::JetRef jref(jetsH, jets_iter - jetsH->begin());                                                                                                                                            
    if (jref->pt() < jetPtMin and jref->correctedJet("Uncorrected").pt() < jetPtMin) continue;
    if (fabs(jref->eta()) > jetEtaMax) continue;                 
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
    jetv.push_back(jref);                                                                                                                                                                           
  }    
  sort(jetv.begin(), jetv.end(), jetRefSorter);

  std::vector<pat::JetRef> jetv_puppi;   
  for (auto jets_iter = jetsPuppiH->begin(); jets_iter != jetsPuppiH->end(); ++jets_iter) {                                                                                                      
    pat::JetRef jref(jetsPuppiH, jets_iter - jetsPuppiH->begin());                                                                                                                                   
    if (jref->pt() < jetPtMin and jref->correctedJet("Uncorrected").pt() < jetPtMin) continue;
    if (fabs(jref->eta()) > jetEtaMax) continue;                 
    if (isMC and dumpOnlyJetMatchedToGen and not jref->genJet()) continue;
    jetv_puppi.push_back(jref);                                                                                                                                                         
  }    
  sort(jetv_puppi.begin(), jetv_puppi.end(), jetRefSorter);

  ///
  for (size_t i = 0; i < jetv.size(); i++) {

    jet_pt.push_back(jetv[i]->pt());
    jet_eta.push_back(jetv[i]->eta());
    jet_phi.push_back(jetv[i]->phi());
    jet_mass.push_back(jetv[i]->mass());
    jet_energy.push_back(jetv[i]->energy());
    jet_pt_raw.push_back(jetv[i]->correctedJet("Uncorrected").pt());
    jet_mass_raw.push_back(jetv[i]->correctedJet("Uncorrected").mass());
    jet_energy_raw.push_back(jetv[i]->correctedJet("Uncorrected").energy());

    jet_deepjet_probb.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probb"));
    jet_deepjet_probbb.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probbb"));
    jet_deepjet_problepb.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:problepb"));
    jet_deepjet_probc.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probc"));
    jet_deepjet_probg.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probg"));
    jet_deepjet_probuds.push_back(jetv[i]->bDiscriminator("pfDeepFlavourJetTags:probuds"));

    for(auto & ilabel : pnetDiscriminatorLabels)
      jet_pnetlast_score[ilabel].push_back(jetv[i]->bDiscriminator(("pfParticleNetAK4LastJetTags:"+ilabel).c_str()));

    int jetid = 0;
    if(applyJetID(*jetv[i],"tight",false)) jetid += 1;
    jet_id.push_back(jetid);

    int jetpuid = 0;    
    if(applyPileupJetID(*jetv[i],"loose"))  jetpuid += 1;
    if(applyPileupJetID(*jetv[i],"medium")) jetpuid += 2;
    if(applyPileupJetID(*jetv[i],"tight"))  jetpuid += 4;
    jet_puid.push_back(jetpuid);

    // Energy fractions
    jet_chf.push_back(jetv[i]->chargedHadronEnergyFraction());
    jet_nhf.push_back(jetv[i]->neutralHadronEnergyFraction());
    jet_elf.push_back(jetv[i]->electronEnergyFraction());
    jet_phf.push_back(jetv[i]->photonEnergyFraction());
    jet_muf.push_back(jetv[i]->muonEnergyFraction());

    // PF components
    jet_ncand.push_back(jetv[i]->chargedMultiplicity()+jetv[i]->neutralMultiplicity());
    jet_nch.push_back(jetv[i]->chargedHadronMultiplicity());
    jet_nnh.push_back(jetv[i]->neutralHadronMultiplicity());
    jet_nel.push_back(jetv[i]->electronMultiplicity());
    jet_nph.push_back(jetv[i]->photonMultiplicity());
    jet_nmu.push_back(jetv[i]->muonMultiplicity());
    jet_hflav.push_back(jetv[i]->hadronFlavour());
    jet_pflav.push_back(jetv[i]->partonFlavour());
    jet_nbhad.push_back(jetv[i]->jetFlavourInfo().getbHadrons().size());
    jet_nchad.push_back(jetv[i]->jetFlavourInfo().getcHadrons().size());
    
    // Matching with gen-jets
    int pos_matched = -1;
    float minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4()) < minDR){
        pos_matched = igen;
        minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv[i]->p4());
      }
    }

    if(pos_matched >= 0){
      jet_genmatch_pt.push_back(jetv_gen[pos_matched]->pt());
      jet_genmatch_eta.push_back(jetv_gen[pos_matched]->eta());
      jet_genmatch_phi.push_back(jetv_gen[pos_matched]->phi());
      jet_genmatch_mass.push_back(jetv_gen[pos_matched]->mass());
      jet_genmatch_energy.push_back(jetv_gen[pos_matched]->energy());
    }
    else{
      jet_genmatch_pt.push_back(0);
      jet_genmatch_eta.push_back(0);
      jet_genmatch_phi.push_back(0);
      jet_genmatch_mass.push_back(0);
      jet_genmatch_energy.push_back(0);
    }    
    
    //////////                                                                                                                                                                                       
    pos_matched = -1;
    minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen_wnu.size(); igen++){
      if(reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[i]->p4()) < minDR){
        pos_matched = igen;
        minDR = reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv[i]->p4());
      }
    }

    if(pos_matched >= 0){
      jet_genmatch_wnu_pt.push_back(jetv_gen_wnu[pos_matched]->pt());
      jet_genmatch_wnu_eta.push_back(jetv_gen_wnu[pos_matched]->eta());
      jet_genmatch_wnu_phi.push_back(jetv_gen_wnu[pos_matched]->phi());
      jet_genmatch_wnu_mass.push_back(jetv_gen_wnu[pos_matched]->mass());
      jet_genmatch_wnu_energy.push_back(jetv_gen_wnu[pos_matched]->energy());
    }
    else{
      jet_genmatch_wnu_pt.push_back(0);
      jet_genmatch_wnu_eta.push_back(0);
      jet_genmatch_wnu_phi.push_back(0);
      jet_genmatch_wnu_mass.push_back(0);
      jet_genmatch_wnu_energy.push_back(0);
    }

    // pf candidate
    std::vector<pat::PackedCandidate> vectorOfConstituents;
    std::vector<reco::CandidatePtr> vectorOfConstituentsPtr;
    for(unsigned ipart = 0; ipart < jetv[i]->numberOfDaughters(); ipart++){
      const pat::PackedCandidate* pfPart = dynamic_cast<const pat::PackedCandidate*> (jetv[i]->daughter(ipart));
      vectorOfConstituents.push_back(*pfPart);
      const reco::CandidatePtr & pfPartPtr = jetv[i]->daughterPtr(ipart);
      vectorOfConstituentsPtr.push_back(pfPartPtr);
    }

    std::sort(vectorOfConstituents.begin(),vectorOfConstituents.end(),packedPFCandidateSorter);
    std::sort(vectorOfConstituentsPtr.begin(),vectorOfConstituentsPtr.end(),candidatePtrSorter);
    
    size_t ipart = 0;
    for(auto const & pfcand : vectorOfConstituents){
      jet_pfcandidate_pt.push_back(pfcand.pt());
      jet_pfcandidate_eta.push_back(pfcand.eta());
      jet_pfcandidate_charge.push_back(pfcand.charge());
      jet_pfcandidate_id.push_back(abs(pfcand.pdgId()));
      if(puppiWeightsH.isValid())
	jet_pfcandidate_puppiw.push_back((*puppiWeightsH)[vectorOfConstituentsPtr.at(ipart)]);
      else
	jet_pfcandidate_puppiw.push_back(pfcand.puppiWeight());
      jet_pfcandidate_ijet.push_back(i);
      jet_pfcandidate_frompv.push_back(pfcand.fromPV());
      jet_pfcandidate_dz.push_back(pfcand.dz(primaryVerticesH->front().position()));
      jet_pfcandidate_dxy.push_back(pfcand.dxy(primaryVerticesH->front().position()));
      const reco::Track* track = pfcand.bestTrack();
      if(track){
	jet_pfcandidate_dzsig.push_back(fabs(pfcand.dz(primaryVerticesH->front().position())/pfcand.dzError()));
	jet_pfcandidate_dxysig.push_back(fabs(pfcand.dxy(primaryVerticesH->front().position())/pfcand.dxyError()));
      }
      else{
	jet_pfcandidate_dzsig.push_back(0);
	jet_pfcandidate_dxysig.push_back(0);
      }
      
      ipart++;
    }
  }

  ///
  for (size_t i = 0; i < jetv_puppi.size(); i++) {

    jet_puppi_pt.push_back(jetv_puppi[i]->pt());
    jet_puppi_eta.push_back(jetv_puppi[i]->eta());
    jet_puppi_phi.push_back(jetv_puppi[i]->phi());
    jet_puppi_mass.push_back(jetv_puppi[i]->mass());
    jet_puppi_energy.push_back(jetv_puppi[i]->energy());
    jet_puppi_pt_raw.push_back(jetv_puppi[i]->correctedJet("Uncorrected").pt());
    jet_puppi_mass_raw.push_back(jetv_puppi[i]->correctedJet("Uncorrected").mass());
    jet_puppi_energy_raw.push_back(jetv_puppi[i]->correctedJet("Uncorrected").energy());

    jet_puppi_deepjet_probb.push_back(jetv_puppi[i]->bDiscriminator("pfDeepFlavourJetTags:probb"));
    jet_puppi_deepjet_probbb.push_back(jetv_puppi[i]->bDiscriminator("pfDeepFlavourJetTags:probbb"));
    jet_puppi_deepjet_problepb.push_back(jetv_puppi[i]->bDiscriminator("pfDeepFlavourJetTags:problepb"));
    jet_puppi_deepjet_probc.push_back(jetv_puppi[i]->bDiscriminator("pfDeepFlavourJetTags:probc"));
    jet_puppi_deepjet_probg.push_back(jetv_puppi[i]->bDiscriminator("pfDeepFlavourJetTags:probg"));
    jet_puppi_deepjet_probuds.push_back(jetv_puppi[i]->bDiscriminator("pfDeepFlavourJetTags:probuds"));

    for(auto & ilabel : pnetPuppiDiscriminatorLabels)
      jet_puppi_pnetlast_score[ilabel].push_back(jetv_puppi[i]->bDiscriminator(("pfParticleNetPuppiAK4LastJetTags:"+ilabel).c_str()));

    int jetid = 0;
    if(applyJetID(*jetv_puppi[i],"tight",true)) jetid += 1;
    jet_puppi_id.push_back(jetid);

    int jetpuid = 0;
    if(applyPileupJetID(*jetv_puppi[i],"loose"))  jetid += 1;
    if(applyPileupJetID(*jetv_puppi[i],"medium")) jetid += 2;
    if(applyPileupJetID(*jetv_puppi[i],"tight"))  jetid += 4;
    jet_puppi_puid.push_back(jetpuid);

    // Energy fractions
    jet_puppi_chf.push_back(jetv_puppi[i]->chargedHadronEnergyFraction());
    jet_puppi_nhf.push_back(jetv_puppi[i]->neutralHadronEnergyFraction());
    jet_puppi_elf.push_back(jetv_puppi[i]->electronEnergyFraction());
    jet_puppi_phf.push_back(jetv_puppi[i]->photonEnergyFraction());
    jet_puppi_muf.push_back(jetv_puppi[i]->muonEnergyFraction());

    // PF components
    jet_puppi_ncand.push_back(jetv_puppi[i]->chargedMultiplicity()+jetv_puppi[i]->neutralMultiplicity());
    jet_puppi_nch.push_back(jetv_puppi[i]->chargedHadronMultiplicity());
    jet_puppi_nnh.push_back(jetv_puppi[i]->neutralHadronMultiplicity());
    jet_puppi_nel.push_back(jetv_puppi[i]->electronMultiplicity());
    jet_puppi_nph.push_back(jetv_puppi[i]->photonMultiplicity());
    jet_puppi_nmu.push_back(jetv_puppi[i]->muonMultiplicity());
    jet_puppi_hflav.push_back(jetv_puppi[i]->hadronFlavour());
    jet_puppi_pflav.push_back(jetv_puppi[i]->partonFlavour());
    jet_puppi_nbhad.push_back(jetv_puppi[i]->jetFlavourInfo().getbHadrons().size());
    jet_puppi_nchad.push_back(jetv_puppi[i]->jetFlavourInfo().getcHadrons().size());
    
    // Matching with gen-jets
    int pos_matched = -1;
    float minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen.size(); igen++){
      if(reco::deltaR(jetv_gen[igen]->p4(),jetv_puppi[i]->p4()) < minDR){
        pos_matched = igen;
        minDR = reco::deltaR(jetv_gen[igen]->p4(),jetv_puppi[i]->p4());
      }
    }

    if(pos_matched >= 0){
      jet_puppi_genmatch_pt.push_back(jetv_gen[pos_matched]->pt());
      jet_puppi_genmatch_eta.push_back(jetv_gen[pos_matched]->eta());
      jet_puppi_genmatch_phi.push_back(jetv_gen[pos_matched]->phi());
      jet_puppi_genmatch_mass.push_back(jetv_gen[pos_matched]->mass());
      jet_puppi_genmatch_energy.push_back(jetv_gen[pos_matched]->energy());
    }
    else{
      jet_puppi_genmatch_pt.push_back(0);
      jet_puppi_genmatch_eta.push_back(0);
      jet_puppi_genmatch_phi.push_back(0);
      jet_puppi_genmatch_mass.push_back(0);
      jet_puppi_genmatch_energy.push_back(0);
    }    
    
    //////////                                                                                                                                                                                       
    pos_matched = -1;
    minDR = dRJetGenMatch;
    for(size_t igen = 0; igen < jetv_gen_wnu.size(); igen++){
      if(reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv_puppi[i]->p4()) < minDR){
        pos_matched = igen;
        minDR = reco::deltaR(jetv_gen_wnu[igen]->p4(),jetv_puppi[i]->p4());
      }
    }

    if(pos_matched >= 0){
      jet_puppi_genmatch_wnu_pt.push_back(jetv_gen_wnu[pos_matched]->pt());
      jet_puppi_genmatch_wnu_eta.push_back(jetv_gen_wnu[pos_matched]->eta());
      jet_puppi_genmatch_wnu_phi.push_back(jetv_gen_wnu[pos_matched]->phi());
      jet_puppi_genmatch_wnu_mass.push_back(jetv_gen_wnu[pos_matched]->mass());
      jet_puppi_genmatch_wnu_energy.push_back(jetv_gen_wnu[pos_matched]->energy());
    }
    else{
      jet_puppi_genmatch_wnu_pt.push_back(0);
      jet_puppi_genmatch_wnu_eta.push_back(0);
      jet_puppi_genmatch_wnu_phi.push_back(0);
      jet_puppi_genmatch_wnu_mass.push_back(0);
      jet_puppi_genmatch_wnu_energy.push_back(0);
    }
  }
  
  tree->Fill();
}


void ValidationCHSvsPuppiAK4::beginJob() {

  // Access the TFileService
  edm::Service<TFileService> fs;  
  tree = fs->make<TTree>("tree","tree");

  tree->Branch("event", &event, "event/i");
  tree->Branch("run", &run, "run/i");
  tree->Branch("lumi", &lumi, "lumi/i");
  tree->Branch("xsec", &xsec, "xsec/F");
  tree->Branch("wgt" , &wgt , "wgt/F");
  tree->Branch("flags", &flags, "flags/i");
  tree->Branch("putrue", &putrue, "putrue/i");
  tree->Branch("rho", &rho, "rho/F");
  tree->Branch("npv", &npv, "npv/i");
  tree->Branch("nsv", &nsv, "nsv/i");

  // GEN particles
  if(isMC){
    tree->Branch("gen_particle_pt","std::vector<float>", &gen_particle_pt);
    tree->Branch("gen_particle_eta","std::vector<float>", &gen_particle_eta);
    tree->Branch("gen_particle_phi","std::vector<float>", &gen_particle_phi);
    tree->Branch("gen_particle_mass","std::vector<float>", &gen_particle_mass);
    tree->Branch("gen_particle_id","std::vector<int>", &gen_particle_id);
    tree->Branch("gen_particle_status","std::vector<unsigned int>", &gen_particle_status);
    tree->Branch("gen_particle_daughters_id","std::vector<int>", &gen_particle_daughters_id);
    tree->Branch("gen_particle_daughters_igen","std::vector<unsigned int>", &gen_particle_daughters_igen);
    tree->Branch("gen_particle_daughters_pt","std::vector<float>", &gen_particle_daughters_pt);
    tree->Branch("gen_particle_daughters_eta","std::vector<float>", &gen_particle_daughters_eta);
    tree->Branch("gen_particle_daughters_phi","std::vector<float>", &gen_particle_daughters_phi);
    tree->Branch("gen_particle_daughters_mass","std::vector<float>", &gen_particle_daughters_mass);
    tree->Branch("gen_particle_daughters_status","std::vector<unsigned int>", &gen_particle_daughters_status);
    tree->Branch("gen_particle_daughters_charge","std::vector<int>", &gen_particle_daughters_charge);
  }

  tree->Branch("muon_pt", "std::vector<float>", &muon_pt);
  tree->Branch("muon_eta", "std::vector<float>", &muon_eta);
  tree->Branch("muon_phi", "std::vector<float>", &muon_phi);
  tree->Branch("muon_mass", "std::vector<float>", &muon_mass);
  tree->Branch("muon_id", "std::vector<unsigned int>" , &muon_id);
  tree->Branch("muon_iso", "std::vector<unsigned int>" , &muon_iso);
  tree->Branch("muon_charge", "std::vector<int>" , &muon_charge);
  tree->Branch("muon_d0", "std::vector<float>" , &muon_d0);
  tree->Branch("muon_dz", "std::vector<float>" , &muon_dz);

  tree->Branch("electron_pt", "std::vector<float>", &electron_pt);
  tree->Branch("electron_eta", "std::vector<float>", &electron_eta);
  tree->Branch("electron_phi", "std::vector<float>", &electron_phi);
  tree->Branch("electron_mass", "std::vector<float>", &electron_mass);
  tree->Branch("electron_id", "std::vector<unsigned int>" , &electron_id);
  tree->Branch("electron_idscore", "std::vector<float>" , &electron_idscore);
  tree->Branch("electron_charge", "std::vector<int>" , &electron_charge);
  tree->Branch("electron_d0", "std::vector<float>" , &electron_d0);
  tree->Branch("electron_dz", "std::vector<float>" , &electron_dz);

  tree->Branch("tau_pt", "std::vector<float>" , &tau_pt);
  tree->Branch("tau_eta", "std::vector<float>" , &tau_eta);
  tree->Branch("tau_phi", "std::vector<float>" , &tau_phi);
  tree->Branch("tau_mass", "std::vector<float>" , &tau_mass);
  tree->Branch("tau_dxy", "std::vector<float>" , &tau_dxy);
  tree->Branch("tau_dz", "std::vector<float>" , &tau_dz);
  tree->Branch("tau_charge", "std::vector<int>" , &tau_charge);
  tree->Branch("tau_decaymode", "std::vector<unsigned int>" , &tau_decaymode);
  tree->Branch("tau_idjet_wp", "std::vector<unsigned int>" , &tau_idjet_wp);
  tree->Branch("tau_idmu_wp", "std::vector<unsigned int>" , &tau_idmu_wp);
  tree->Branch("tau_idele_wp", "std::vector<unsigned int>" , &tau_idele_wp);
  tree->Branch("tau_idjet", "std::vector<float>" , &tau_idjet);
  tree->Branch("tau_idele", "std::vector<float>" , &tau_idele);
  tree->Branch("tau_idmu", "std::vector<float>" , &tau_idmu);
  if(isMC){
    tree->Branch("tau_genmatch_pt", "std::vector<float>" , &tau_genmatch_pt);
    tree->Branch("tau_genmatch_eta", "std::vector<float>" , &tau_genmatch_eta);
    tree->Branch("tau_genmatch_phi", "std::vector<float>" , &tau_genmatch_phi);
    tree->Branch("tau_genmatch_mass", "std::vector<float>" , &tau_genmatch_mass);
  }

  tree->Branch("met",&met,"met/F");
  tree->Branch("met_phi", &met_phi, "met_phi/F");
  tree->Branch("met_signif", &met_signif, "met_signif/F");

  tree->Branch("jet_pt", "std::vector<float>" , &jet_pt);
  tree->Branch("jet_eta", "std::vector<float>" , &jet_eta);
  tree->Branch("jet_phi", "std::vector<float>" , &jet_phi);
  tree->Branch("jet_mass", "std::vector<float>" , &jet_mass);
  tree->Branch("jet_energy", "std::vector<float>" , &jet_energy);
  tree->Branch("jet_pt_raw", "std::vector<float>" , &jet_pt_raw);
  tree->Branch("jet_mass_raw", "std::vector<float>" , &jet_mass_raw);
  tree->Branch("jet_energy_raw", "std::vector<float>" , &jet_energy_raw);
  tree->Branch("jet_chf", "std::vector<float>" , &jet_chf);
  tree->Branch("jet_nhf", "std::vector<float>" , &jet_nhf);
  tree->Branch("jet_elf", "std::vector<float>" , &jet_elf);
  tree->Branch("jet_phf", "std::vector<float>" , &jet_phf);
  tree->Branch("jet_muf", "std::vector<float>" , &jet_muf);
  tree->Branch("jet_deepjet_probb", "std::vector<float>" , &jet_deepjet_probb);
  tree->Branch("jet_deepjet_probbb", "std::vector<float>" , &jet_deepjet_probbb);
  tree->Branch("jet_deepjet_probc", "std::vector<float>" , &jet_deepjet_probc);
  tree->Branch("jet_deepjet_problepb", "std::vector<float>" , &jet_deepjet_problepb);
  tree->Branch("jet_deepjet_probg", "std::vector<float>" , &jet_deepjet_probg);
  tree->Branch("jet_deepjet_probuds", "std::vector<float>" , &jet_deepjet_probuds);

  for(const auto & label : pnetDiscriminatorLabels){
    jet_pnetlast_score[label] = std::vector<float>();
    tree->Branch(("jet_pnetlast_"+label).c_str(),"std::vector<float>", &jet_pnetlast_score[label]);
  }

  tree->Branch("jet_id", "std::vector<unsigned int>" , &jet_id);
  tree->Branch("jet_puid", "std::vector<unsigned int>" , &jet_puid);
  tree->Branch("jet_ncand", "std::vector<unsigned int>" , &jet_ncand);
  tree->Branch("jet_nch", "std::vector<unsigned int>" , &jet_nch);
  tree->Branch("jet_nnh", "std::vector<unsigned int>" , &jet_nnh);
  tree->Branch("jet_nel", "std::vector<unsigned int>" , &jet_nel);
  tree->Branch("jet_nph", "std::vector<unsigned int>" , &jet_nph);
  tree->Branch("jet_nmu", "std::vector<unsigned int>" , &jet_nmu);
  tree->Branch("jet_hflav", "std::vector<unsigned int>" , &jet_hflav);
  tree->Branch("jet_pflav", "std::vector<int>" , &jet_pflav);
  tree->Branch("jet_nbhad", "std::vector<unsigned int>" , &jet_nbhad);
  tree->Branch("jet_nchad", "std::vector<unsigned int>" , &jet_nchad);
  if(isMC){
    tree->Branch("jet_genmatch_pt","std::vector<float>" , &jet_genmatch_pt);
    tree->Branch("jet_genmatch_eta","std::vector<float>" , &jet_genmatch_eta);
    tree->Branch("jet_genmatch_phi","std::vector<float>" , &jet_genmatch_phi);
    tree->Branch("jet_genmatch_mass","std::vector<float>" , &jet_genmatch_mass);
    tree->Branch("jet_genmatch_energy","std::vector<float>" , &jet_genmatch_energy);
    tree->Branch("jet_genmatch_wnu_pt","std::vector<float>" , &jet_genmatch_wnu_pt);
    tree->Branch("jet_genmatch_wnu_eta","std::vector<float>" , &jet_genmatch_wnu_eta);
    tree->Branch("jet_genmatch_wnu_phi","std::vector<float>" , &jet_genmatch_wnu_phi);
    tree->Branch("jet_genmatch_wnu_mass","std::vector<float>" , &jet_genmatch_wnu_mass);
    tree->Branch("jet_genmatch_wnu_energy","std::vector<float>" , &jet_genmatch_wnu_energy);
  }
  tree->Branch("jet_pfcandidate_pt","std::vector<float>" , &jet_pfcandidate_pt);
  tree->Branch("jet_pfcandidate_eta","std::vector<float>" , &jet_pfcandidate_eta);
  tree->Branch("jet_pfcandidate_dxy","std::vector<float>" , &jet_pfcandidate_dxy);
  tree->Branch("jet_pfcandidate_dz","std::vector<float>" , &jet_pfcandidate_dz);
  tree->Branch("jet_pfcandidate_dxysig","std::vector<float>" , &jet_pfcandidate_dxysig);
  tree->Branch("jet_pfcandidate_dzsig","std::vector<float>" , &jet_pfcandidate_dzsig);
  tree->Branch("jet_pfcandidate_charge","std::vector<int>" , &jet_pfcandidate_charge);
  tree->Branch("jet_pfcandidate_id","std::vector<unsigned int>" , &jet_pfcandidate_id);
  tree->Branch("jet_pfcandidate_frompv","std::vector<unsigned int>" , &jet_pfcandidate_frompv);
  tree->Branch("jet_pfcandidate_puppiw","std::vector<float>" , &jet_pfcandidate_puppiw);
  tree->Branch("jet_pfcandidate_ijet","std::vector<unsigned int>" , &jet_pfcandidate_ijet);

  tree->Branch("jet_puppi_pt", "std::vector<float>" , &jet_puppi_pt);
  tree->Branch("jet_puppi_eta", "std::vector<float>" , &jet_puppi_eta);
  tree->Branch("jet_puppi_phi", "std::vector<float>" , &jet_puppi_phi);
  tree->Branch("jet_puppi_mass", "std::vector<float>" , &jet_puppi_mass);
  tree->Branch("jet_puppi_energy", "std::vector<float>" , &jet_puppi_energy);
  tree->Branch("jet_puppi_pt_raw", "std::vector<float>" , &jet_puppi_pt_raw);
  tree->Branch("jet_puppi_mass_raw", "std::vector<float>" , &jet_puppi_mass_raw);
  tree->Branch("jet_puppi_energy_raw", "std::vector<float>" , &jet_puppi_energy_raw);
  tree->Branch("jet_puppi_chf", "std::vector<float>" , &jet_puppi_chf);
  tree->Branch("jet_puppi_nhf", "std::vector<float>" , &jet_puppi_nhf);
  tree->Branch("jet_puppi_elf", "std::vector<float>" , &jet_puppi_elf);
  tree->Branch("jet_puppi_phf", "std::vector<float>" , &jet_puppi_phf);
  tree->Branch("jet_puppi_muf", "std::vector<float>" , &jet_puppi_muf);
  tree->Branch("jet_puppi_deepjet_probb", "std::vector<float>" , &jet_puppi_deepjet_probb);
  tree->Branch("jet_puppi_deepjet_probbb", "std::vector<float>" , &jet_puppi_deepjet_probbb);
  tree->Branch("jet_puppi_deepjet_probc", "std::vector<float>" , &jet_puppi_deepjet_probc);
  tree->Branch("jet_puppi_deepjet_problepb", "std::vector<float>" , &jet_puppi_deepjet_problepb);
  tree->Branch("jet_puppi_deepjet_probg", "std::vector<float>" , &jet_puppi_deepjet_probg);
  tree->Branch("jet_puppi_deepjet_probuds", "std::vector<float>" , &jet_puppi_deepjet_probuds);

  for(const auto & label : pnetPuppiDiscriminatorLabels){
    jet_puppi_pnetlast_score[label] = std::vector<float>();
    tree->Branch(("jet_puppi_pnetlast_"+label).c_str(),"std::vector<float>", &jet_puppi_pnetlast_score[label]);
  }

  tree->Branch("jet_puppi_id", "std::vector<unsigned int>" , &jet_puppi_id);
  tree->Branch("jet_puppi_puid", "std::vector<unsigned int>" , &jet_puppi_puid);
  tree->Branch("jet_puppi_ncand", "std::vector<unsigned int>" , &jet_puppi_ncand);
  tree->Branch("jet_puppi_nch", "std::vector<unsigned int>" , &jet_puppi_nch);
  tree->Branch("jet_puppi_nnh", "std::vector<unsigned int>" , &jet_puppi_nnh);
  tree->Branch("jet_puppi_nel", "std::vector<unsigned int>" , &jet_puppi_nel);
  tree->Branch("jet_puppi_nph", "std::vector<unsigned int>" , &jet_puppi_nph);
  tree->Branch("jet_puppi_nmu", "std::vector<unsigned int>" , &jet_puppi_nmu);
  tree->Branch("jet_puppi_hflav", "std::vector<unsigned int>" , &jet_puppi_hflav);
  tree->Branch("jet_puppi_pflav", "std::vector<int>" , &jet_puppi_pflav);
  tree->Branch("jet_puppi_nbhad", "std::vector<unsigned int>" , &jet_puppi_nbhad);
  tree->Branch("jet_puppi_nchad", "std::vector<unsigned int>" , &jet_puppi_nchad);
  if(isMC){
    tree->Branch("jet_puppi_genmatch_pt","std::vector<float>" , &jet_puppi_genmatch_pt);
    tree->Branch("jet_puppi_genmatch_eta","std::vector<float>" , &jet_puppi_genmatch_eta);
    tree->Branch("jet_puppi_genmatch_phi","std::vector<float>" , &jet_puppi_genmatch_phi);
    tree->Branch("jet_puppi_genmatch_mass","std::vector<float>" , &jet_puppi_genmatch_mass);
    tree->Branch("jet_puppi_genmatch_energy","std::vector<float>" , &jet_puppi_genmatch_energy);
    tree->Branch("jet_puppi_genmatch_wnu_pt","std::vector<float>" , &jet_puppi_genmatch_wnu_pt);
    tree->Branch("jet_puppi_genmatch_wnu_eta","std::vector<float>" , &jet_puppi_genmatch_wnu_eta);
    tree->Branch("jet_puppi_genmatch_wnu_phi","std::vector<float>" , &jet_puppi_genmatch_wnu_phi);
    tree->Branch("jet_puppi_genmatch_wnu_mass","std::vector<float>" , &jet_puppi_genmatch_wnu_mass);
    tree->Branch("jet_puppi_genmatch_wnu_energy","std::vector<float>" , &jet_puppi_genmatch_wnu_energy);
  }

  
}  

void ValidationCHSvsPuppiAK4::endJob() {
}

void ValidationCHSvsPuppiAK4::beginRun(edm::Run const& iRun, edm::EventSetup const& iSetup) {


  HLTConfigProvider fltrConfig;
  bool flag = false;
  fltrConfig.init(iRun, iSetup, filterResultsTag.process(), flag);
  
  // MET filter Paths
  filterPathsMap.clear();
  filterPathsVector.clear();
  filterPathsVector.push_back("Flag_goodVertices");
  filterPathsVector.push_back("Flag_globalSuperTightHalo2016Filter");
  filterPathsVector.push_back("Flag_HBHENoiseFilter");
  filterPathsVector.push_back("Flag_HBHENoiseIsoFilter");
  filterPathsVector.push_back("Flag_EcalDeadCellTriggerPrimitiveFilter");
  filterPathsVector.push_back("Flag_BadPFMuonFilter");
  filterPathsVector.push_back("Flag_BadChargedCandidateFilter");
  
  for (size_t i = 0; i < filterPathsVector.size(); i++) {
    filterPathsMap[filterPathsVector[i]] = -1;
  }
  
  for(size_t i = 0; i < filterPathsVector.size(); i++){
    TPRegexp pattern(filterPathsVector[i]);
    for(size_t j = 0; j < fltrConfig.triggerNames().size(); j++){
      std::string pathName = fltrConfig.triggerNames()[j];
      if(TString(pathName).Contains(pattern)){
	filterPathsMap[filterPathsVector[i]] = j;
      }
    }
  }
}

void ValidationCHSvsPuppiAK4::endRun(edm::Run const&, edm::EventSetup const&) {
}

void ValidationCHSvsPuppiAK4::initializeBranches(){

  event  = 0;
  run    = 0;
  lumi   = 0;
  putrue = 0;
  flags  = 0;
  wgt    = 0.;
  rho    = 0.;
  
  gen_particle_pt.clear();
  gen_particle_eta.clear();
  gen_particle_phi.clear();
  gen_particle_mass.clear();
  gen_particle_id.clear();
  gen_particle_status.clear();
  gen_particle_daughters_id.clear();
  gen_particle_daughters_igen.clear();
  gen_particle_daughters_pt.clear();
  gen_particle_daughters_eta.clear();
  gen_particle_daughters_phi.clear();
  gen_particle_daughters_mass.clear();
  gen_particle_daughters_status.clear();
  gen_particle_daughters_charge.clear();

  muon_pt.clear();
  muon_eta.clear();
  muon_phi.clear();
  muon_mass.clear();
  muon_id.clear();
  muon_charge.clear();
  muon_iso.clear();
  muon_d0.clear();
  muon_dz.clear();

  electron_pt.clear();
  electron_eta.clear();
  electron_phi.clear();
  electron_mass.clear();
  electron_id.clear();
  electron_idscore.clear();
  electron_charge.clear();
  electron_d0.clear();
  electron_dz.clear();

  tau_pt.clear();
  tau_eta.clear();
  tau_phi.clear();
  tau_mass.clear();
  tau_dxy.clear();
  tau_dz.clear();
  tau_decaymode.clear();
  tau_idjet.clear();
  tau_idele.clear();
  tau_idmu.clear();
  tau_idjet_wp.clear();
  tau_idmu_wp.clear();
  tau_idele_wp.clear();
  tau_charge.clear();
  tau_genmatch_pt.clear();
  tau_genmatch_eta.clear();
  tau_genmatch_phi.clear();
  tau_genmatch_mass.clear();
  tau_genmatch_decaymode.clear();

  met = 0.;
  met_phi = 0;
  met_signif = 0;

  npv = 0;
  nsv = 0;

  jet_pt.clear();
  jet_eta.clear();
  jet_phi.clear();
  jet_mass.clear();
  jet_energy.clear();
  jet_pt_raw.clear();
  jet_mass_raw.clear();
  jet_energy_raw.clear();
  jet_chf.clear();
  jet_nhf.clear();
  jet_elf.clear();
  jet_phf.clear();
  jet_muf.clear();
  jet_deepjet_probb.clear();
  jet_deepjet_probbb.clear();
  jet_deepjet_problepb.clear();
  jet_deepjet_probc.clear();
  jet_deepjet_probg.clear();
  jet_deepjet_probuds.clear();
  for(auto & imap : jet_pnetlast_score)
    imap.second.clear();
  jet_id.clear();
  jet_puid.clear();
  jet_ncand.clear();
  jet_nch.clear();
  jet_nnh.clear();
  jet_nel.clear();
  jet_nph.clear();
  jet_nmu.clear();
  jet_hflav.clear();
  jet_pflav.clear();
  jet_nbhad.clear();
  jet_nchad.clear();
  jet_genmatch_pt.clear();
  jet_genmatch_eta.clear();
  jet_genmatch_phi.clear();
  jet_genmatch_mass.clear();
  jet_genmatch_energy.clear();
  jet_genmatch_wnu_pt.clear();
  jet_genmatch_wnu_eta.clear();
  jet_genmatch_wnu_phi.clear();
  jet_genmatch_wnu_mass.clear();
  jet_genmatch_wnu_energy.clear();
  jet_pfcandidate_pt.clear();
  jet_pfcandidate_eta.clear();
  jet_pfcandidate_dxy.clear();
  jet_pfcandidate_dz.clear();
  jet_pfcandidate_dxysig.clear();
  jet_pfcandidate_dzsig.clear();
  jet_pfcandidate_charge.clear();
  jet_pfcandidate_id.clear();
  jet_pfcandidate_frompv.clear();
  jet_pfcandidate_puppiw.clear();
  jet_pfcandidate_ijet.clear();

  jet_puppi_pt.clear();
  jet_puppi_eta.clear();
  jet_puppi_phi.clear();
  jet_puppi_mass.clear();
  jet_puppi_energy.clear();
  jet_puppi_pt_raw.clear();
  jet_puppi_mass_raw.clear();
  jet_puppi_energy_raw.clear();
  jet_puppi_chf.clear();
  jet_puppi_nhf.clear();
  jet_puppi_elf.clear();
  jet_puppi_phf.clear();
  jet_puppi_muf.clear();
  jet_puppi_deepjet_probb.clear();
  jet_puppi_deepjet_probbb.clear();
  jet_puppi_deepjet_problepb.clear();
  jet_puppi_deepjet_probc.clear();
  jet_puppi_deepjet_probg.clear();
  jet_puppi_deepjet_probuds.clear();
  for(auto & imap : jet_puppi_pnetlast_score)
    imap.second.clear();
  jet_puppi_id.clear();
  jet_puppi_puid.clear();
  jet_puppi_ncand.clear();
  jet_puppi_nch.clear();
  jet_puppi_nnh.clear();
  jet_puppi_nel.clear();
  jet_puppi_nph.clear();
  jet_puppi_nmu.clear();
  jet_puppi_hflav.clear();
  jet_puppi_pflav.clear();
  jet_puppi_nbhad.clear();
  jet_puppi_nchad.clear();
  jet_puppi_genmatch_pt.clear();
  jet_puppi_genmatch_eta.clear();
  jet_puppi_genmatch_phi.clear();
  jet_puppi_genmatch_mass.clear();
  jet_puppi_genmatch_energy.clear();
  jet_puppi_genmatch_wnu_pt.clear();
  jet_puppi_genmatch_wnu_eta.clear();
  jet_puppi_genmatch_wnu_phi.clear();
  jet_puppi_genmatch_wnu_mass.clear();
  jet_puppi_genmatch_wnu_energy.clear();
}

void ValidationCHSvsPuppiAK4::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}


// to apply jet ID: https://twiki.cern.ch/twiki/bin/view/CMS/JetID13TeVUL                                                                                                                             
bool ValidationCHSvsPuppiAK4::applyJetID(const pat::Jet & jet, const std::string & level, const bool & isPuppi){

  if(level != "tight" and level != "tightLepVeto")
    return true;

  double eta  = jet.eta();
  double nhf  = jet.neutralHadronEnergyFraction();
  double nemf = jet.neutralEmEnergyFraction();
  double chf  = jet.chargedHadronEnergyFraction();
  double muf  = jet.muonEnergyFraction();
  double cemf = jet.chargedEmEnergyFraction();

  int jetid  = 0;
  if(isPuppi){
    float np   = jet.userFloat("patPuppiJetSpecificProducer:puppiMultiplicity");
    float nnp  = jet.userFloat("patPuppiJetSpecificProducer:neutralPuppiMultiplicity");
    float nch  = np-nnp;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 1;               
    if (fabs(eta) > 2.7 and fabs(eta) <= 3.0 and nhf < 0.999) jetid += 2;
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 1;
    if (fabs(eta) > 3.0 and nemf < 0.90 and nnp > 2) jetid += 2;
  }
  else{
    int np   = jet.chargedMultiplicity()+jet.neutralMultiplicity();
    int nnp  = jet.neutralMultiplicity();
    int nch  = jet.chargedMultiplicity();
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0) jetid += 1;
    if (fabs(eta) <= 2.6 and nhf < 0.90 and nemf < 0.90 and np > 1 and chf > 0 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0) jetid += 1;
    if (fabs(eta) > 2.6 and fabs(eta) <= 2.7 and nhf < 0.90 and nemf < 0.99 and nch > 0 and muf < 0.8 and cemf < 0.8) jetid += 2;
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 1;
    if (nemf > 0.01 and nemf < 0.99 and nnp > 1 and fabs(eta) > 2.7 and fabs(eta) <= 3.0) jetid += 2;
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 1;
    if (nemf < 0.90 and nnp > 10 and nhf > 0.2 and fabs(eta) > 3.0) jetid += 2;
  }

  if(level == "tight" and jetid > 1) return true;
  else if(level == "tightLepVeto" and jetid > 2) return true;
  else return false;

}

bool ValidationCHSvsPuppiAK4::applyPileupJetID(const pat::Jet & jet, const std::string & level){
  bool passpuid = false;
  if(jet.hasUserInt("pileupJetIdUpdated:fullId")){
    if(level == "loose"  and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 0)) or jet.pt() > 50)) passpuid = true;
    if(level == "medium" and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 1)) or jet.pt() > 50)) passpuid = true;
    if(level == "tight"  and (bool(jet.userInt("pileupJetIdUpdated:fullId") & (1 << 2)) or jet.pt() > 50)) passpuid = true;
  }
  else if (jet.hasUserInt("pileupJetId:fullId")){
    if(level == "loose"  and (bool(jet.userInt("pileupJetId:fullId") & (1 << 0)) or jet.pt() > 50)) passpuid = true;
    if(level == "medium" and (bool(jet.userInt("pileupJetId:fullId") & (1 << 1)) or jet.pt() > 50)) passpuid = true;
    if(level == "tight"  and (bool(jet.userInt("pileupJetId:fullId") & (1 << 2)) or jet.pt() > 50)) passpuid = true;
  }
  return passpuid;
}


DEFINE_FWK_MODULE(ValidationCHSvsPuppiAK4);

