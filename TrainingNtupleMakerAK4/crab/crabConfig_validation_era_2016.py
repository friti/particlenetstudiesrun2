import os
from CRABClient.UserUtilities import config

config = config()
config.General.transferOutputs = True
config.General.transferLogs = False

config.JobType.pluginName  = 'Analysis'
config.JobType.psetName    = os.getenv('CMSSW_BASE')+'//src/ParticleNetStudiesRun2/TrainingNtupleMakerAK4/test/makeValidationNtuple_cfg.py'
config.JobType.allowUndistributedCMSSW = True
config.JobType.maxMemoryMB = 2500
config.JobType.numCores    = 2

config.JobType.sendPythonFolder = True

config.Data.inputDBS      = 'global'
config.Data.outLFNDirBase = '/store/group/phys_higgs/HiggsExo/rgerosa/ParticleNetUL/NtupleValidationAK4VsEra/2016'
config.Site.storageSite   = 'T2_CH_CERN'

## make list of all samples
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.samples_validation_era_2016 import AddAllSamples
samples = {};
AddAllSamples(samples);

## consider only the same that matches the name of the current directory created by the main script submitting this workflow
dset = os.getcwd().replace(os.path.dirname(os.getcwd())+'/','')
config.Data.inputDataset   = samples[dset][0]
config.Data.splitting      = samples[dset][2]
config.Data.unitsPerJob    = samples[dset][3]
if samples[dset][4]:
    config.Data.lumiMask   = samples[dset][4]
if samples[dset][5] > 0:
    config.Data.totalUnits = samples[dset][5]

if samples[dset][4] and samples[dset][5] > 0:
    print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' lumiMask: '+config.Data.lumiMask+' totalUnits: '+str(config.Data.totalUnits))
elif samples[dset][4] and samples[dset][5] <= 0:
    print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' lumiMask: '+config.Data.lumiMask)
elif not samples[dset][4] and samples[dset][5] > 0:
    print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' totalUnits: '+str(config.Data.totalUnits))
else:
    print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob));

params = samples[dset][1]
config.JobType.pyCfgParams = params
print ("Submitting jobs with pyCfg parameters: "+" ".join(params));
