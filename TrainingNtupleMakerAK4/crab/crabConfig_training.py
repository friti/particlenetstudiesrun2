import os
from CRABClient.UserUtilities import config
import datetime

production_tag = datetime.date.today().strftime('%Y%b%d')

### General options
config = config()
config.General.transferOutputs = True

config.Data.publication = False
config.General.transferLogs = False

config.JobType.pluginName  = 'Analysis'
config.JobType.psetName    = os.getenv('CMSSW_BASE')+'/src/ParticleNetStudiesRun2/TrainingNtupleMakerAK4/test/makeTrainingNtuple_cfg.py'
config.JobType.allowUndistributedCMSSW = True
config.JobType.maxMemoryMB = 2500
#config.JobType.numCores    = 2
config.JobType.numCores    = 1

config.Data.inputDBS      = 'global'
config.Data.outLFNDirBase = '/store/user/friti/%s' % ('training_ntuples_parTv1_' + production_tag)
config.Data.partialDataset = True
config.Site.storageSite   = 'T2_CH_CSCS'
config.General.workArea = 'jobs_%s' % production_tag

## make list of all samples
from ParticleNetStudiesRun2.TrainingNtupleMakerAK4.samples_training import AddAllSamples
samples = {};
AddAllSamples(samples);

## consider only the same that matches the name of the current directory created by the main script submitting this workflow
dset = os.getcwd().replace(os.path.dirname(os.getcwd())+'/','')
config.Data.inputDataset   = samples[dset][0]
config.Data.splitting      = samples[dset][2]
config.Data.unitsPerJob    = samples[dset][3]
if samples[dset][4]:
    config.Data.lumiMask   = samples[dset][4]
if samples[dset][5] > 0:
    config.Data.totalUnits = samples[dset][5]

### python parameters
params = samples[dset][1]
params.append('nThreads='+str(config.JobType.numCores)) ## this must match number of numCores option
config.JobType.pyCfgParams = params
print ('Submitting jobs for dataset: '+config.Data.inputDataset+' splitting mode: '+config.Data.splitting+' unitsPerJob: '+str(config.Data.unitsPerJob)+' totalUnits: '+str(config.Data.totalUnits))
print ("Submitting jobs with pyCfg parameters: "+" ".join(params));
