#ifndef ParticleNetStudiesRun2_TrainingNtupleMakerAK4_TreeFillerUtils_h
#define ParticleNetStudiesRun2_TrainingNtupleMakerAK4_TreeFillerUtils_h

// basic C++ headers
#include <memory>
#include <vector>
#include <map>
#include <string>
#include <cmath>
#include <algorithm>

//boost lybraries                                                                                                                                                                                    
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

#include "TLorentzVector.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/ESHandle.h"

#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"

#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/GenLumiInfoHeader.h"

#include "CondTools/BTau/interface/BTagCalibrationReader.h"

class PdfVariation {
 public:
  PdfVariation(){
    lhaid = -1;
  };
  inline bool operator==(const PdfVariation & b){
    return lhaid == b.lhaid;
  }  
  int lhaid;
  std::vector<int>   pdfvar;
  std::vector<float> weight;
};


class QCDScaleVariation {
 public:
  QCDScaleVariation(){};
  inline bool operator==(const QCDScaleVariation & b){
    return (muR == b.muR and muF == b.muF);
  }  
  float muR;
  float muF;
  int scalevar;
};

class PSVariation {
 public:
  PSVariation(){
    pos = 0;
  };
  inline bool operator==(const PSVariation & b){
    return (variation == b.variation);
  }  
  std::string variation;
  int pos;
};


// to clean jets with leptons
bool isCleanWithMuons(const pat::JetRef & jet, const std::vector<pat::MuonRef> & muons, const float & dR);
bool isCleanWithElectrons(const pat::JetRef & jet, const std::vector<pat::ElectronRef> & electrons, const float & dR);
bool isCleanWithTaus(const pat::JetRef & jet, const std::vector<pat::TauRef> & taus, const float & dR);
// PDF helpers
int getLHAID(const std::string & pdfSetName);
int getLHAID(const int & lhaid);
int getPDFOrder(const std::string & pdfSetName);
int getPDFOrder(const int & lhaid);
// to save generator weights
void fillPDFVariations(const LHERunInfoProduct & myLHERunInfoProduct, std::vector<PdfVariation> & pdfvariation);
void fillQCDScaleVariations(const LHERunInfoProduct & myLHERunInfoProduct, std::vector<QCDScaleVariation> & qcdscale);
void fillPSVariations(const GenLumiInfoHeader & myGenLumiInfoHeader, std::vector<PSVariation> & psvariation);

#endif
